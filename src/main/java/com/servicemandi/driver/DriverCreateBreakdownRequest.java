package com.servicemandi.driver;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;
import io.appium.java_client.android.AndroidKeyCode;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class DriverCreateBreakdownRequest extends Configuration {

	public DriverCreateBreakdownRequest() {

	}

	public String driverBreakdownRequest(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {
		
		if(isVerified) {
			System.out.println("isVerified in driverBreakdownRequest try"+isVerified);

		try {
			
			System.out.println("Entered DriverBreakdownRequest");
			Utils.sleepTimeMedium();
			Utils.sleepTimeLow();
			String registerVehicleNo = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/form/div/ion-searchbar/div/input";
			driverWait(registerVehicleNo);
			clickDriverElement(Configuration.LocatorType.XPATH, registerVehicleNo);
			Thread.sleep(1000);
			System.out.println("excelInputData.getVehicleNumber()::::::" +excelInputData.getVehicleNumber());
			enterDriverValue(Configuration.LocatorType.XPATH, registerVehicleNo, excelInputData.getVehicleNumber());

			
			/*String registerVehicleNoList = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/form/div/div/ul/li";
			clickDriverElement(Configuration.LocatorType.XPATH, registerVehicleNoList);
			System.out.println("Created New Request Order::: ");*/
			Thread.sleep(1000);
			hideKeyBoard(driveDriver);
			Thread.sleep(1000);
			String breakdownNoBtn = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/div/button[1]";
			driverWait(breakdownNoBtn);
			clickDriverElement(Configuration.LocatorType.XPATH, breakdownNoBtn);

			Utils.sleepTimeLow();
			String ReqNoPath="body > ion-app > ng-component > ion-nav > page-service-request > ion-content > div.scroll-content > div > div > div > div.loaderDv.loaderDvFont";
			String ReqNo=driveDriver.findElement(By.cssSelector(ReqNoPath)).getText();
			System.out.println("ReqNo:::"+ReqNo);
			
			String[] value1 = ReqNo.split("Request ");
			String mRequestOrderId = value1[1].trim();
			System.out.println("ReqNumber ::: " + mRequestOrderId);
						
			
			System.out.println("Driver : New request Created");
			writeReport(LogStatus.PASS, "Driver:New Order created:::Passed"+mRequestOrderId);
			isVerified=true;
			
			return mRequestOrderId;
			
		} catch (Exception e) {
			e.printStackTrace();
			Driver_TakeClick();
			eLogger.log(LogStatus.FAIL,"Driver:New Order creation Failed");
			isVerified=false;
		}}
		else {
		eLogger.log(LogStatus.FAIL, "Driver:New Order creation Failed");
		isVerified=false;
		}
		return "";
	}

	public void fmCreateBDOrder(ExcelInputData excelInputData,String mRequestOrderId,FleetManager fleetManager,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in fmCreateBDOrder try"+isVerified);
			try {
			Utils.sleepTimeLow();
			System.out.println("Inside fmCreateBDOrder");
			//String mRequestOrderId="25143";
			System.out.println("mRequestOrderId:::"+mRequestOrderId);
			
			String fmNewRequest = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-header/div/ion-segment/ion-segment-button[1]/p";
			fleetManagerWait(fmNewRequest);
			clickFMElement(Configuration.LocatorType.XPATH, fmNewRequest);
			Thread.sleep(5000);			
			String totalCountofcards = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/*";
			Thread.sleep(4000);
			List<WebElement> totalCountofcardsList = fleetManagerDriver.findElements(By.xpath(totalCountofcards));
			Thread.sleep(1000);
			System.out.println("totalCountofcardsList Size :" + totalCountofcardsList.size());
			for (int i = 0; i <= totalCountofcardsList.size(); i++) {
				
				String cardDetails = totalCountofcardsList.get(i).getText();
				System.out.println("cardDetails::::"+cardDetails);
				if(cardDetails.contains(mRequestOrderId))
				{
				 Thread.sleep(1000);
				 JavascriptExecutor js = (JavascriptExecutor) fleetManagerDriver;
				 WebElement Element = fleetManagerDriver.findElement(By.xpath("//*[contains(text(), "+mRequestOrderId+")]"));
			     //This will scroll the page till the element is found		
			     js.executeScript("arguments[0].scrollIntoView();", Element);
			     Thread.sleep(1000);
			     System.out.println("I value:::::::::::::"+i);
			     String BDPath="/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card["+ (i + 1) +"]/ion-card-content/div[2]/button[1]";
			     				// /html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card/ion-card-content/div[2]/button[1]/span
			     fleetManagerDriver.findElement(By.xpath(BDPath)).click();

			     Thread.sleep(15000);
			     System.out.println("Request No::"+mRequestOrderId+":::Vehicle Number clicked in FM for Driver-NewOrderRequest");
			     break;
				}
			}
			
			/*String fmBreakDown = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card/ion-card-content/div[2]/button[1]/span";
			fleetManagerWait(fmBreakDown);
			clickFMElement(Configuration.LocatorType.XPATH, fmBreakDown);
			System.out.println(" fmBreakDown");*/
			Thread.sleep(3000);			
			fleetManagerWait(LocatorPath.manageOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
			Thread.sleep(1000);
						
			System.out.println("Clicked ManageOrder");
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM::New Order created:Passed");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			eLogger.log(LogStatus.FAIL, "FM::New Order creation Failed");
			isVerified=false;
		}}else {
			eLogger.log(LogStatus.FAIL, "FM::New Order creation Failed");
			isVerified=false;
			}	

	}
	
	public void fmDeleteBDOrder(FleetManager fleetManager,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in fmDeleteBDOrder try"+isVerified);
		try {
			Utils.sleepTimeLow();
			System.out.println("Inside fmDeleteBDOrder");
			
			String fmDelete = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card/ion-card-content/div[1]/ion-icon";
			fleetManagerWait(fmDelete);
			clickFMElement(Configuration.LocatorType.XPATH, fmDelete);
			
			String fmDeleteYesBtn = "/html/body/ion-app/ion-alert/div/div[3]/button[1]";
			fleetManagerWait(fmDeleteYesBtn);
			clickFMElement(Configuration.LocatorType.XPATH, fmDeleteYesBtn);				
			
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM:: BD deletion Passed");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			eLogger.log(LogStatus.FAIL, "FM:: BD deletion Failed");
			isVerified=false;
		}}else {
			eLogger.log(LogStatus.FAIL, "FM:: BD deletion Failed");
			isVerified=false;
			}	
		
	}}

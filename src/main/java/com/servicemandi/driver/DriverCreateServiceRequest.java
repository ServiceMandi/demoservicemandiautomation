package com.servicemandi.driver;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

public class DriverCreateServiceRequest extends Configuration {

	public DriverCreateServiceRequest() {

	}

	public String driverServiceRepairsRequest(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {

	
			System.out.println("isVerified in driverServiceRepairsRequest try"+isVerified);
			try {
				System.out.println("excelInputData.getVehicleNumber()"+excelInputData.getVehicleNumber());
			Thread.sleep(5000);
			String registerVehicleNo = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/form/div/ion-searchbar/div/input";
			driverWait(registerVehicleNo);
			clickDriverElement(Configuration.LocatorType.XPATH, registerVehicleNo);
			Thread.sleep(2000);
			enterDriverValue(Configuration.LocatorType.XPATH, registerVehicleNo, excelInputData.getVehicleNumber());

			Thread.sleep(3000);
			String registerVehicleNoList = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/form/div/div/ul/li";
			clickDriverElement(Configuration.LocatorType.XPATH, registerVehicleNoList);
			System.out.println("Created New Request Order::: ");

			hideKeyBoard(driveDriver);
			String SRYesBtn = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div[2]/div/button[2]";
			driverWait(SRYesBtn);
			clickDriverElement(Configuration.LocatorType.XPATH, SRYesBtn);

			Thread.sleep(8000);
			String ReqNoPath="body > ion-app > ng-component > ion-nav > page-service-request > ion-content > div.scroll-content > div > div > div > div.loaderDv.loaderDvFont";
			String ReqNo=driveDriver.findElement(By.cssSelector(ReqNoPath)).getText();
			System.out.println("ReqNo:::"+ReqNo);
			
			String[] value1 = ReqNo.split("Request ");
			String ReqNumber = value1[1].trim();
			System.out.println("ReqNumber ::: " + ReqNumber);
			/*String orderId1 = "/html/body/ion-app/ng-component/ion-nav/page-service-request/ion-content/div[2]/div/div/div[3]";
			Thread.sleep(1000);
			driverWait(orderId1);
			String RequestNo = driveDriver.findElement(By.xpath(orderId1)).getText();
			System.out.println("RequestNo::: " + RequestNo);*/

			writeReport(LogStatus.PASS, "Driver:New Order Creation Passed");
			Thread.sleep(5000);
			return ReqNumber;
		} catch (Exception e) {
			e.printStackTrace();
			Driver_TakeClick();
			writeReport(LogStatus.FAIL, "Driver:New Order Creation Failed");
			isVerified=false;
		}
		
		return "";
	}

	public void fmCreateServiceRepairsOrder(FleetManager fleetManager,String mRequestOrderId,boolean isVerified) throws InterruptedException, IOException {
		
		
			System.out.println("isVerified in fmCreateServiceRepairsOrder try"+isVerified);
			try {
				Thread.sleep(5000);		
			System.out.println("Inside fmCreateServiceRepairsOrder");
			System.out.println("mRequestOrderId:::"+mRequestOrderId);
		
			String fmNewRequest = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-header/div/ion-segment/ion-segment-button[1]/p";
			fleetManagerWait(fmNewRequest);
			clickFMElement(Configuration.LocatorType.XPATH, fmNewRequest);
					
			Thread.sleep(7000);			
			String totalCountofcards = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/*";
			
			List<WebElement> totalCountofcardsList = fleetManagerDriver.findElements(By.xpath(totalCountofcards));
			Thread.sleep(1000);
			System.out.println("totalCountofcardsList Size :" + totalCountofcardsList.size());
			for (int i = 0; i <= totalCountofcardsList.size(); i++) {
				Thread.sleep(1000);
				String cardDetails = totalCountofcardsList.get(i).getText();
				System.out.println("cardDetails::::"+cardDetails);
				if(cardDetails.contains(mRequestOrderId))
				{
				 Thread.sleep(1000);
				 JavascriptExecutor js = (JavascriptExecutor) fleetManagerDriver;
				 WebElement Element = fleetManagerDriver.findElement(By.xpath("//*[contains(text(), "+mRequestOrderId+")]"));
			     //This will scroll the page till the element is found		
			     js.executeScript("arguments[0].scrollIntoView();", Element);
			     Thread.sleep(1000);
			     System.out.println("I value:::::::::::::"+i);
			     				
			     String SRPath="/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card["+ (i + 1) +"]/ion-card-content/div[2]/button[2]";
			     fleetManagerDriver.findElement(By.xpath(SRPath)).click();

			     Thread.sleep(10000);
			     System.out.println("Request No::"+mRequestOrderId+":::Vehicle Number clicked in FM for Driver-NewOrderRequest");
			     break;
				}
				}
			
			/*String fmServiceRepairs = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-card/ion-card-content/div[2]/button[2]/span";
			fleetManagerWait(fmServiceRepairs);
			clickFMElement(Configuration.LocatorType.XPATH, fmServiceRepairs);
			System.out.println(" fmServiceRepairs");*/

			/*fleetManagerWait(LocatorPath.manageOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
			Thread.sleep(1000);
			System.out.println("Clicked ManageOrder");*/
			
			
			writeReport(LogStatus.PASS, "FM::New Order Created Passed");
			Thread.sleep(2000);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			Driver_TakeClick();
			writeReport(LogStatus.FAIL, "FM::New Order Creation Failed");
			isVerified=false;
		}}
		
}

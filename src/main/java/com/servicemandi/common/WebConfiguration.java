package com.servicemandi.common;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WebConfiguration{

	private static final String String = null;
	private WebElement element;
	
	protected static WebDriver driver = null;
	protected static WebDriverWait driverWait = null;
	protected static WebDriverWait browserWait = null;
	public static ExtentReports extentReports;
	public static ExtentTest eLogger;

	public enum LocatorType {
		XPATH, ID, CLASS, NAME, CSS
	}
	
	public static void webLaunch() {
		
 		System.out.println("Inside Browserlaunch");
		System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver.exe"); // Old
		driver = new ChromeDriver();

		//driver.get("http://35.189.183.69:8080/web/customer-service/login");
		driver.get("https://luat.servicemandi.com/web/customer-service/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		
	}
	
	
	public static void farmerWebLaunch() {
		
 		System.out.println("Inside farmerWebLaunch");
		System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver.exe"); // Old
		driver = new ChromeDriver();

		driver.get("https://uat.servicemandi.com/farmer/index.html#/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		
	}
	
	
	
	
	
	
	

	public static void moveToNextFlow(String newWorkFlow) throws InterruptedException {
		
	}
	public static String reportConfiguration(String newWorkFlow) throws InterruptedException {

		System.out.println("WorkflowName::::" + newWorkFlow);
		DateFormat df = new SimpleDateFormat("ddMMyyyyhhmmss");
		Thread.sleep(2000);
		System.out.println("DateFormat::::" + df.format(new Date()));
		///C:\\Users\\14402\\git\\demoservicemandiautomation\\SM_Report\\
		extentReports = new ExtentReports("D:\\SM_EXE\\SMReport\\" + newWorkFlow + ".html",true,DisplayOrder.NEWEST_FIRST);
		//extentReports = new ExtentReports("D:\\SM_EXE\\SMReport\\" + newWorkFlow + ".html",true,DisplayOrder.OLDEST_FIRST);
		
		
		
		//extentReports = new ExtentReports("D:\\SMReports\\" + newWorkFlow + ".html",true,DisplayOrder.OLDEST_FIRST);
		return newWorkFlow;
	}

	public static String closeWorkFlowReport() {
		String Status="Pass";
		System.out.println("closeWorkFlowReport");
		extentReports.endTest(eLogger);
		return Status;
	}

	public static String createNewWorkFlowReport(String newWorkFlow) {

		eLogger = extentReports.startTest(newWorkFlow);
		return newWorkFlow;
	}

	public WebElement getWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (driver.findElements(By.xpath(locator)).size() > 0) {
				element = driver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (driver.findElements(By.name(locator)).size() > 0) {
				element = driver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (driver.findElements(By.className(locator)).size() > 0) {
				element = driver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (driver.findElements(By.cssSelector(locator)).size() > 0) {
				element = driver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickElement(LocatorType type, String locator) {

		try {
			WebElement elem = getWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterValue(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}
	
	public void DriverWait(final String locator) throws InterruptedException {
		
		
		try {	
			driverWait = new WebDriverWait(driver,45);
			driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));				
		/*Wait<WebDriver> wait=new FluentWait<WebDriver>(fleetManagerDriver) 
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(3, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement element=wait.until(new Function<WebDriver, WebElement>(){
			public WebElement apply(WebDriver fleetManagerDriver)
			{
				return fleetManagerDriver.findElement(By.xpath(locator));
			}
			
		});
		
		System.out.println("Element is displayed"+element.isDisplayed());*/
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public void browserWait(final String locator) throws InterruptedException {

		Thread.sleep(1000);
		browserWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		
	}

	public static void saveReport() throws InterruptedException {
		String Status="Pass";
	
		System.out.println("saveReport");
		writeReport(LogStatus.PASS, "End of Work Flow");
		extentReports.endTest(eLogger);
		extentReports.flush();
		//extentReports.close();
		
	}

	
	public static String getElementTextValue(WebDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getText();
		} catch (Exception e) {

		}
		return "";
	}

	public static String getElementAttributeValue(WebDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		} catch (Exception e) {

		}
		return "";
	}

	public static void writeReport(LogStatus logStatus, String status) throws InterruptedException {

		if (eLogger == null) {

			reportConfiguration(String );
		}

		status = status + " : " + Utils.timeCalculation();
		switch (logStatus) {

		case PASS:
			eLogger.log(LogStatus.PASS, status);
			break;
		case FAIL:
			eLogger.log(LogStatus.FAIL, status);
			break;
		case INFO:
			eLogger.log(LogStatus.INFO, status);
		}
	}

	public static void hideKeyBoard(AndroidDriver androidDriver) {

		try {
			androidDriver.hideKeyboard();
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public static void TakeSnapshot() throws InterruptedException, IOException {
		System.out.println("Inside TakeSnapshot method");
		
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat("dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "FailSnapshot__"+ df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "ERRDriver_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.FAIL, "SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
				
		}
	public static void TakePositiveSnapshot() throws InterruptedException, IOException {
		System.out.println("Inside TakePositiveSnapshot method");
		
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat("dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "PassSnapshot_"+ df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "ERRDriver_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
				
		}
	
	public static void catchcall() throws InterruptedException, IOException{
		
		Thread.sleep(1000);
		try {
			//Alert alert = driver.switchTo().alert();
			//Thread.sleep(2000);
			//String GetErrorText = alert.getText();
			//System.out.println("System aborted due to::::" + GetErrorText);
			TakeSnapshot();
			//alert.accept();
			Thread.sleep(1000);
			//writeReport(LogStatus.FAIL, "System aborted due to::::" + GetErrorText);
		
		}catch(Exception e) {
			
		} 
		
	}
	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}   
	
	
	
	public static String getCurrentDay (){
        //Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
 
        //Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("Today Int: " + todayInt +"\n");
 
        //Integer to String Conversion
        String todayStr = Integer.toString(todayInt);
        System.out.println("Today Str: " + todayStr + "\n");
 
        return todayStr;
    }
	
}

package com.servicemandi.common;

import com.mongodb.util.Util;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;


public class ProfileCreation extends Configuration {

	public ProfileCreation() {

	}

	public void fleetManageUserCreation(ExcelInputData excelInputData) throws InterruptedException {

		try {
			
			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();
			System.out.println("Entered Fleet Manager Login method ::: ");
			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			

			LinkedHashMap<String, List<String>> newFMUserDetails = ReadingExcelData.readingFleetManagerData("FMUserData");

			String userName = newFMUserDetails.get("Name").get(1);
			String mobileNumber = newFMUserDetails.get("Mobile No").get(1);
			System.out.println("mobileNumber::::::::::"+mobileNumber);
			String ExistingEmail = newFMUserDetails.get("ExistingEmail").get(1);
			String NewEMail= newFMUserDetails.get("NewEMail").get(1);
			String address = newFMUserDetails.get("Address").get(1);
			String state = newFMUserDetails.get("State").get(1);

								
			String mobilePath1 = "//html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input/div";
			fleetManagerWait(LocatorPath.mobilePath);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			Thread.sleep(1000);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, mobileNumber);
			
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			//fleetManagerWait(LocatorPath.userRegisterName);
			Thread.sleep(10000);
			//hideKeyBoard(fleetManagerDriver);
			
			fleetManagerWait(LocatorPath.userRegisterName);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterName));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys(userName);
			fleetManagerWait(LocatorPath.userRegisterEmail);
			WebElement emailElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterEmail));
			emailElement.clear();
			emailElement.clear();
			emailElement.sendKeys(ExistingEmail);
			fleetManagerWait(LocatorPath.userRegisterAddress);
			WebElement addressElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterAddress));
			addressElement.clear();
			addressElement.clear();
			addressElement.sendKeys(address);
			hideKeyBoard(fleetManagerDriver);
			fleetManagerWait(LocatorPath.userRegisterState);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterState);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.stateListPath);
			List<WebElement> stateList = fleetManagerDriver.findElements(By.xpath(LocatorPath.stateListPath));
			System.out.println(" State list size :::" + stateList.size());

			for (int s = 0; s < stateList.size(); s++) {
				String statelist = stateList.get(s).getText();
				if (statelist.equalsIgnoreCase(state)) {
					stateList.get(s).click();
					writeReport(LogStatus.PASS, "FM:::User State Changed");
					Thread.sleep(500);
					break;
				}
			}
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.alertOkPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			Thread.sleep(6000);
			while(fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterTermsPath)).isDisplayed())
			{
				fleetManagerWait(LocatorPath.userRegisterTermsPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterTermsPath);
			}
			
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.userRegisterAccept);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterAccept);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.userRegisterUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterUpPath);
			//Existing Email id Alert:
			Thread.sleep(5000);
			
			if(fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg)).isDisplayed()) {
				WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg));
				String AlertMsgText=AlertMsg1.getText();
				System.out.println("FM::Alert message for existing Email ID::" + AlertMsgText);
				writeReport(LogStatus.FAIL, "FM::Alert message for existing Email ID::-"+AlertMsgText);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);
				
																									
				WebElement emailElement2 = fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-register/ion-content/div[2]/div/form/ion-item[4]/div[1]/div"));
				emailElement2.click();												
				Thread.sleep(1000);
				fleetManagerDriver.findElement(By.xpath("/html/body/ion-app/ng-component/ion-nav/page-register/ion-content/div[2]/div/form/ion-item[4]/div[1]/div/ion-input/input[1]")).clear();
				fleetManagerDriver.findElement(By.xpath("/html/body/ion-app/ng-component/ion-nav/page-register/ion-content/div[2]/div/form/ion-item[4]/div[1]/div/ion-input/input[1]")).sendKeys(NewEMail);
				
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.userRegisterUpPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterUpPath);	
				writeReport(LogStatus.PASS, "FM::NEW EMAIL ID entered::-"+NewEMail);
			}
			else{			
			Thread.sleep(5000);
			writeReport(LogStatus.PASS, "FM::User Created Successfully");}
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "FM::User Creation Failed");
		}
	}

	public void editUserProfile() throws InterruptedException, IOException {

		try {
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println("AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Profile")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM: Clicked MY PROFILE tab:::Passed");
					Thread.sleep(2000);
					break;
				}
			}
			/*//for checking name and email id:
			String listpath="//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/*";
			fleetManagerWait(listpath);
			List<WebElement>fmEditList = fleetManagerDriver.findElements(By.xpath(listpath));
			for (int i = 0; i < fmEditList.size(); i++) {

				WebElement listElement = fmEditList.get(i);
				String listValue = listElement.getText();
				
				System.out.println("AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Profile")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM: Clicked MY PROFILE tab:::Passed");
					Thread.sleep(2000);
					break;
				}
			}*/
			
			
			LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData.readingFleetManagerData("FMUserData");
			//List<String> mobileNumberList = userEditDetails.get("Mobile No");
			List<String> userNameList = userEditDetails.get("Name");
			/*List<String> mobileNumberList = userEditDetails.get("Mobile No");
			List<String> mobileNumberList = userEditDetails.get("Email");
			List<String> mobileNumberList = userEditDetails.get("GSTIN");
			List<String> mobileNumberList = userEditDetails.get("Address");
			List<String> StateList = userEditDetails.get("State");*/
			
			
			//ReadingExcelData.readingFMUserData();
			System.out.println(" Entered the editFMProfile method ::: ");
			System.out.println(" Account list size :::" + myAccountList.size());
			System.out.println("Username Value::::::::::::::"+userEditDetails.get("Name").get(1));
			
			String ProfileName=userEditDetails.get("Name").get(1);
			String ProfileMobileNo = userEditDetails.get("Mobile No").get(1);
			String ExistingEmail =userEditDetails.get("ExistingEmail").get(1);
			String NewEMail=userEditDetails.get("NewEMail").get(1);
			
			String ProfileGSTIN=userEditDetails.get("GSTIN").get(1);
			String ProfileAddress=userEditDetails.get("Address").get(1);
			String ProfileState=userEditDetails.get("State").get(1);
			
			
			/*String ProfileName = excelInputData.getProfileName();
			String ProfileMobileNo = excelInputData.getProfileMobileNo();
			String ProfileEmail =excelInputData.getProfileEmail();
			String ProfileGSTIN = excelInputData.getProfileGSTIN();
			String ProfileAddress = excelInputData.getProfileAddress();
			String ProfileState = excelInputData.getProfileState();*/

			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.editIconPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.editIconPath);
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.editNamePath);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editNamePath));
			//String ExistingName=nameElement.getText();
			/*System.out.println("ExistingName::"+nameElement.getText());
			if(nameElement.getText().equalsIgnoreCase(ProfileName))
			{	nameElement.clear();
				nameElement.clear();
				nameElement.sendKeys(ProfileName);
				writeReport(LogStatus.PASS, "FM:: Existing User Name and Excel Data Username are same");
			}
			else*/
			//{
				WebElement nameElement1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.editNamePath));
			nameElement1.clear();
			nameElement1.clear();
			nameElement1.sendKeys(ProfileName);
			//writeReport(LogStatus.PASS, "FM:: New User Name edited:::"+ProfileName);
			//}
			
			

			/*
			 * fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.
			 * xpath(Constants.editMobilePath))); WebElement mobileElement =
			 * fmDriver.findElement(By.xpath(Constants.editMobilePath));
			 * mobileElement.clear(); mobileElement.clear();
			 * mobileElement.sendKeys(mobileNumber);
			 */

			fleetManagerWait(LocatorPath.editEmailPath);
			WebElement emailElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editEmailPath));
			System.out.println("emailElement::"+emailElement.getText());
			emailElement.clear();
			emailElement.clear();
			emailElement.sendKeys(ExistingEmail);
			//writeReport(LogStatus.PASS, "FM: Existing EMail-ID Entered::" +ExistingEmailID);
			
			
			

			fleetManagerWait(LocatorPath.editGSTNPath);
			WebElement GSTElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editGSTNPath));
			GSTElement.clear();
			GSTElement.clear();
			GSTElement.sendKeys(ProfileGSTIN);

			fleetManagerWait(LocatorPath.editAddressPath);
			WebElement addressElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editAddressPath));
			addressElement.clear();
			addressElement.clear();
		    addressElement.sendKeys(ProfileAddress);
			writeReport(LogStatus.PASS, "FM User Address Edited");

			fleetManagerWait(LocatorPath.editStatePath);
			WebElement stateElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editStatePath));
			stateElement.click();

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.stateListPath);
			List<WebElement> stateList = fleetManagerDriver.findElements(By.xpath(LocatorPath.stateListPath));
			System.out.println("State list size :::" + stateList.size());

			for (int s = 0; s < stateList.size(); s++) {
				String statelist = stateList.get(s).getText();
				if (statelist.equalsIgnoreCase(ProfileState)) {
					stateList.get(s).click();
					writeReport(LogStatus.PASS, "FM:::User State Changed");
					Thread.sleep(1000);
					break;
				}
			}
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.alertOkPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);

			/*
			 * Thread.sleep(1000);
			 * fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.
			 * xpath(Constants.editTogglePath)));
			 * clickElement(LocatorType.XPATH, Constants.editTogglePath);
			 * Thread.sleep(1000);
			 */

			Thread.sleep(1000);
			FM_TakeClick();
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.savePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.savePath);
			
			Thread.sleep(5000);
			//Existing MailID entered:
			//========================
			if(fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg)).isDisplayed()) {
				WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg));
				String AlertMsgText=AlertMsg1.getText();
				Thread.sleep(1000);
				
				System.out.println("FM::Alert message::" + AlertMsgText);
				writeReport(LogStatus.FAIL, "FM::Alert message displays::-"+AlertMsgText);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
			
				fleetManagerWait(LocatorPath.editEmailPath);
				WebElement emailElement1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.editEmailPath));
				//String ExistingMailId=emailElement.getText();
				emailElement1.clear();
				emailElement1.sendKeys(NewEMail);
				//writeReport(LogStatus.PASS, "FM: Existing EMail-ID Entered::"+ExistingMailId);
				writeReport(LogStatus.PASS, "FM: New EMail-ID Entered::"+NewEMail);
				
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				Thread.sleep(3000);
				fleetManagerWait(LocatorPath.savePath);
				clickFMElement(LocatorType.XPATH, LocatorPath.savePath);
				Thread.sleep(5000);
				String OKbtn="/html/body/ion-app/ion-alert/div/div[3]/button/span";
				clickFMElement(LocatorType.XPATH,OKbtn);
				//To verify the edited data:
				List<WebElement> myAccountList1 = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
				for (int i = 0; i < myAccountList1.size(); i++) {

					WebElement listElement = myAccountList1.get(i);
					String listValue = listElement.getText();
					System.out.println("AL inside loop text value ::: " + listValue);
					if (listValue.equalsIgnoreCase("My Profile")) {
						listElement.click();
						Thread.sleep(4000);
						writeReport(LogStatus.PASS, "FM: Clicked MY PROFILE tab for verification");
						FM_TakeClick();
						Thread.sleep(2000);
						break;
					}}
				/*LinkedHashMap<String, List<String>> userEditDetails1 = ReadingExcelData.readingFleetManagerData("FMUserData");
					
					String ExistingEmailID1 =userEditDetails1.get("Email").get(1);
					String NewEmailID1=userEditDetails1.get("ChangeExistingMailID").get(1);
					
				String editedNamepath1="/html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/form/ion-row[1]/ion-col/div/ion-input/button";
				String editedName1=fleetManagerDriver.findElement(By.xpath(editedNamepath)).getText();
				System.out.println("editedName::::"+editedName);
				Thread.sleep(2000);
				
				String editedMailpath="//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/form/ion-row[3]/ion-col/div/ion-input/input[1]";
				String editedMail=fleetManagerDriver.findElement(By.xpath(editedMailpath)).getText();
				System.out.println("editedMail::::"+editedMail);
				Thread.sleep(2000);
				
				if(editedName.equalsIgnoreCase(ProfileName)){
					writeReport(LogStatus.PASS, "FM:: Profile Name edited successfully");
				}
				else
				{writeReport(LogStatus.PASS, "FM:: Profile Name not edited");}	
				
				if(editedMail.equalsIgnoreCase(NewEmailID1)){
					writeReport(LogStatus.PASS, "FM:: Email Id edited successfully");
				}
				else
				{writeReport(LogStatus.PASS, "FM:: Email Id not edited");}
				
				Thread.sleep(2000);		
				writeReport(LogStatus.PASS, "FM:: Edit Profile Status::: Successfully Saved");*/
				
			}
			else {
			System.out.println("FM:: Edit Profile Status::: Successfully Saved.");}
			Thread.sleep(1000);
			
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL, "FM:::Profile edition failed");
		}
	}

	public void addNewVehicle() throws InterruptedException, IOException {

		try {
			Utils.sleepTimeLow();

			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" Inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Vehicles")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM:: My Vehicles selected");
					Thread.sleep(1000);
					break;
				}
			}
			Utils.sleepTimeLow();
			/*fleetManagerWait(LocatorPath.addVehiclePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.addVehiclePath);*/
			createNewVehicle(Utils.excelInputDataList.get(1));

		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.PASS, "FM:: My Vehicles not selected");
		}
	}

	public void createNewVehicle(ExcelInputData excelInputData) throws InterruptedException, IOException {

			System.out.println("Entered the ADD vehicle createNewVehicle method ");
			Thread.sleep(15000);//added due to system slowness
			fleetManagerWait(LocatorPath.addVehiclePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.addVehiclePath);
			Thread.sleep(8000);
			LinkedHashMap<String, List<String>> vehicleDetailsList = ReadingExcelData.readingFleetManagerData("FMUserData");

			String make = vehicleDetailsList.get("Select Make").get(1);
			String category = vehicleDetailsList.get("Category").get(1);
			String loadRange = vehicleDetailsList.get("Load Range").get(1);
			String tyres = vehicleDetailsList.get("Tyres").get(1);
			String modelYear = vehicleDetailsList.get("Model Year").get(1);
			String type = vehicleDetailsList.get("Type").get(1);
			String registerNo = vehicleDetailsList.get("Register No").get(1);
			String KMReading = vehicleDetailsList.get("KMReading").get(1);
			String vehicleInsExpyDate = vehicleDetailsList.get("Vehicle Insurance Expiry Date").get(1);
			String FCExpiryDate = vehicleDetailsList.get("FC Expiry Date").get(1);
			String ChangedRegisterNo = vehicleDetailsList.get("Changed RegisterNo").get(1);
			Thread.sleep(2000);

			for (int c = 1; c < 6; c++) {

				Thread.sleep(4000);
				
				//String categoryPath = "//div[2]/div[1]/ion-list/div[" + c + "]";
				String categoryPath ="//html/body/ion-app/ng-component/ion-nav/page-add-vehicle/ion-content/div[2]/div[1]/ion-list/div[" + c + "]";
				//Add Vehicle:
				System.out.println("Category path :::" + categoryPath);
				fleetManagerWait(categoryPath);
				clickFMElement(LocatorType.XPATH, categoryPath);
				Thread.sleep(2000);

				fleetManagerWait(LocatorPath.vehicleAlertPath);
				List<WebElement> categoryList = fleetManagerDriver.findElements(By.xpath(LocatorPath.vehicleAlertPath));
				System.out.println("Category List  size :::" + categoryList.size());

				String checkingValue = "";
				String reportStatus = "";
				switch (c) {
				case 1:
					checkingValue = make;
					reportStatus = "Make";
					break;
				case 2:
					checkingValue = category;
					reportStatus = "Vehicle category ";
					break;
				case 3:
					checkingValue = loadRange;
					reportStatus = "Vehicle load range ";
					break;
				case 4:
					checkingValue = tyres;
					reportStatus = "Vehicle No of Tyres ";
					break;
				case 5:
					checkingValue = modelYear;
					reportStatus = "Vehicle Model years";
					break;
				}

				for (int i = 0; i < categoryList.size(); i++) {

					WebElement listElement = categoryList.get(i);
					String listValue = listElement.getText();
					System.out.println("loop text value ::: " + listValue);
					if (listValue.equalsIgnoreCase(checkingValue)) {
						Thread.sleep(1000);
						listElement.click();
						writeReport(LogStatus.PASS, "FM:: Selected -> " + reportStatus +"::::"+checkingValue);
						Thread.sleep(1000);
						break;
					}
				}

				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.alertOkPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}

			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.nonElectronicPath);
			//VEHICLE TYPE:
			if (type.equalsIgnoreCase("Non-Electronic"))
				clickFMElement(LocatorType.XPATH, LocatorPath.nonElectronicPath);
			else
				clickFMElement(LocatorType.XPATH, LocatorPath.electronicPath);

			writeReport(LogStatus.PASS, "FM:: Vehicle type selected ");
			
			//REGISTER NO:
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.registerNoPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.registerNoPath);
			enterFMValue(LocatorType.XPATH, LocatorPath.registerNoPath, registerNo);
			writeReport(LogStatus.PASS, "FM:: Register number entered  ");
			
			
			
			//Date Selection:
			//==============
		/*	Thread.sleep(2000);
			String VehInsExpiryDate="//*[@id=\"insurance_date\"]";
			clickFMElement(LocatorType.XPATH, VehInsExpiryDate);
			
			((AppiumDriver<WebElement>)fleetManagerDriver).context("NATIVE_APP");
			
			System.out.println("vehicleInsExpyDate::::"+vehicleInsExpyDate);
			WebElement VehInsExDate=fleetManagerDriver.findElement(By.xpath("//android.view.View[@content-desc='"+vehicleInsExpyDate+"']"));
			VehInsExDate.click();
			
			Thread.sleep(1000);
			fleetManagerDriver.findElement(By.xpath("//android.widget.Button[@text='Set']")).click();
			switchToFMWebView();
			Thread.sleep(2000);
			String FCDate="//input[@class='ng-untouched ng-pristine ng-valid']";
			clickFMElement(LocatorType.XPATH, FCDate);
			
			((AppiumDriver<WebElement>)fleetManagerDriver).context("NATIVE_APP");
			System.out.println("FCExpiryDate::::"+FCExpiryDate);
			WebElement FCExpDate=fleetManagerDriver.findElement(By.xpath("//android.view.View[@content-desc='"+FCExpiryDate+"']"));
			FCExpDate.click();
			
			Thread.sleep(1000);
			fleetManagerDriver.findElement(By.xpath("//android.widget.Button[@text='Set']")).click();
			switchToFMWebView();
			
			hideKeyBoard(fleetManagerDriver);*/
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.vehicleSubmitPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.vehicleSubmitPath);
			//writeReport(LogStatus.PASS, "FM:: Vehicle successfully Added  ");
			Thread.sleep(5000);
			//Registerd No Alert:
			//====================
			try {
			if(fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg)).isDisplayed()) {
				WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg));
				String AlertMsgText=AlertMsg1.getText();
				Thread.sleep(1000);
				
				System.out.println("FM::Alert message::" + AlertMsgText);
				writeReport(LogStatus.FAIL, "FM::Alert message displays::-"+AlertMsgText);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
			
				WebElement emailElement2 = fleetManagerDriver.findElement(By.xpath(LocatorPath.registerNoPath));
				emailElement2.clear();
				//emailElement2.click();	
				Thread.sleep(2000);
				//fleetManagerWait(LocatorPath.registerNoPath);
				emailElement2.sendKeys(ChangedRegisterNo);
			}else{
				System.out.println("In else block");
				}
			
				//KM READING:
				fleetManagerWait(LocatorPath.KMReadingPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.KMReadingPath);
				System.out.println("KMReading:::"+KMReading);
				fleetManagerWait(LocatorPath.KMReadingPath);
				enterFMValue(LocatorType.XPATH, LocatorPath.KMReadingPath, KMReading);
				writeReport(LogStatus.PASS, "FM:: KM Reading entered :::"+KMReading);
				
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.vehicleSubmitPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.vehicleSubmitPath);
				writeReport(LogStatus.PASS, "FM::NEW REGISTER NUMBER entered::::-"+ChangedRegisterNo);
				
				
			
		} catch (Exception e) {
			
			System.out.println("FM::NEW REGISTER NUMBER entered::");	
			
		}
	}
	
	public void createNewDriver(ExcelInputData excelInputData) throws InterruptedException {

		try {
			Thread.sleep(5000);
			System.out.println("Entered the Add vehicle method ::: ");
			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			System.out.println(" AL Account list size :::" + myAccountList.size());

			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Drivers")) {
					listElement.click();
					Thread.sleep(2000);
					break;
				}
			}

			addNewDriver(Utils.excelInputDataList.get(1));
			writeReport(LogStatus.FAIL, "FM:::Clicked My Drivers link");
		} catch (Exception e) {
			
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "FM:::Not Clicked My Drivers link");
		}

	}

	public void addNewDriver(ExcelInputData excelInputData) throws InterruptedException {

		try {
		/*	Thread.sleep(3000);
			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);*/
			
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.myDriverPath);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.myDriverPath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.addDriverPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.addDriverPath);
			Thread.sleep(3000);
			System.out.println("Entered ADD Driver method ");

			LinkedHashMap<String, List<String>> driverList = ReadingExcelData.readingFleetManagerData("FMUserData");

			String driverName = driverList.get("DriverName").get(1);
			String ExistingDriverMobile = driverList.get("ExistingDriverMobile").get(1);
			String NewDriverMobile = driverList.get("NewDriverMobile").get(1);
			System.out.println("driverName"+driverName);
			System.out.println("ExistingDriverMobile"+ExistingDriverMobile);
			System.out.println("NewDriverMobile"+NewDriverMobile);
						
			fleetManagerWait(LocatorPath.driverNamePath);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverNamePath));
			nameElement.sendKeys(driverName);

			fleetManagerWait(LocatorPath.driverMobilePath);
			WebElement mobileElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverMobilePath));
			mobileElement.clear();
			mobileElement.clear();
			mobileElement.sendKeys(ExistingDriverMobile);
			hideKeyBoard(fleetManagerDriver);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.profileAddDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.profileAddDriver);
			Thread.sleep(5000);
			
			//Existing DriverNo Alert:
			//=========================
			
			Thread.sleep(1000);
			if(fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg)).isDisplayed()) {
				WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(LocatorPath.existingEmailAlertMsg));
				String AlertMsgText=AlertMsg1.getText();
				Thread.sleep(1000);
				System.out.println("FM::Alert message::" + AlertMsgText);
				writeReport(LogStatus.FAIL, "FM:: Alert message displays::-"+AlertMsgText);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);
			
				WebElement emailElement2 = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverMobilePath));
				emailElement2.clear();
				Thread.sleep(1000);
				fleetManagerDriver.findElement(By.xpath(LocatorPath.driverMobilePath)).sendKeys(NewDriverMobile);
				
				/*fleetManagerWait(LocatorPath.driverMobilePath);
				WebElement mobileElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverMobilePath));
				mobileElement.clear();
				mobileElement.clear();
				mobileElement.sendKeys(driverMobile);*/
				
				
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				fleetManagerWait(LocatorPath.profileAddDriver);
				clickFMElement(LocatorType.XPATH, LocatorPath.profileAddDriver);
				Thread.sleep(5000);
				fleetManagerWait(LocatorPath.profileDonePath);
				clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.profileDonePath);
				Thread.sleep(1000);
				writeReport(LogStatus.PASS, "FM::Driver Mobile Number changed and entered::-"+NewDriverMobile);
			}
			else{
			System.out.println("FM:: Driver successfully Added"  );
			}
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.profileDonePath);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.profileDonePath);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM::New Driver Created");
			
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "FM:: New Driver not Created");
		}
	}

	public void mechanicUserCreation(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException {

		try {
			mechanicCapabilities();
			switchToMechanicWebView();
			Utils.sleepTimeLow();
			writeReport(LogStatus.PASS, "Technician creation entered ");
			System.out.println("Enter the Technician login method ");
			mechanicWait(LocatorPath.languagePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.languagePath);
			mechanicWait(LocatorPath.signUpPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.signUpPath);
			mechanicWait(LocatorPath.mechanicMobilePath);

			String mechanicMobilePath = "//*[@id=\"nav\"]/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input";
			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			
			LinkedHashMap<String, List<String>> mechanicList = ReadingExcelData.readingFleetManagerData("FMUserData");

			String MechanicName = mechanicList.get("MechanicName").get(1);
			String MechanicMobile = mechanicList.get("MechanicMobile").get(1);
			String Pincode = mechanicList.get("Pincode").get(1);
			
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, MechanicMobile);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);

			Thread.sleep(1000); 
			mechanicWait(LocatorPath.userRegisterName);
			WebElement nameElement = mechanicDriver.findElement(By.xpath(LocatorPath.userRegisterName));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys(MechanicName);
			mechanicWait(LocatorPath.userRegisterEmail);
			writeReport(LogStatus.PASS, "Technician::New number entered:::Passed");

			WebElement pincodeElement = mechanicDriver.findElement(By.xpath(LocatorPath.userRegisterEmail));
			pincodeElement.clear();
			pincodeElement.clear();
			pincodeElement.sendKeys(Pincode);
			hideKeyBoard(mechanicDriver);

			Thread.sleep(500);

			String userRegisterTermsPath = "//page-register/ion-content/div[2]/div/form/div/span";
			mechanicWait(userRegisterTermsPath);
			clickMechanicElement(LocatorType.XPATH, userRegisterTermsPath);
			Thread.sleep(5000);

			String userRegisterAccept = "//page-termsnconditions/ion-content/div[2]/ion-row/button[2]";
			mechanicWait(userRegisterAccept);
			clickMechanicElement(LocatorType.XPATH, userRegisterAccept);
			clickMechanicElement(LocatorType.XPATH, userRegisterAccept);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicCheckBox);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicCheckBox);
			mechanicWait(LocatorPath.mechanicRegister);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRegister);
			Thread.sleep(5000);
			
			String RegisterAlertPath="//div[@class='alert-head']";
			String RegisterAlert=mechanicDriver.findElement(By.xpath(RegisterAlertPath)).getText();
			System.out.println("RegisterAlert::"+RegisterAlert);
			if(RegisterAlert.equalsIgnoreCase("Thank you for your interest.Our Onboarding team will get in touch with you shortly"))
			{
				writeReport(LogStatus.PASS, "Technician:::Profile creation successfully completed:::Passed");
				MECH_TakeClick();
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				isVerified=true;
			}else
			{writeReport(LogStatus.FAIL, "Technician:::Already registered Number alert is not displayed:::Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "Technician::Profile creation::Failed");
		}
	}
}

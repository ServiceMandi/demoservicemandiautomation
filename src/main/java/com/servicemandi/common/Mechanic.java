package com.servicemandi.common;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.concurrent.BackgroundInitializer;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;


public class Mechanic extends Configuration {

	public Mechanic() {
	}

	public void login(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

			mechanicCapabilities();
			switchToMechanicWebView();
			Thread.sleep(2000);
			
			System.out.println("Entered Technician login method ");
			
			String mechanicAppVersion_Path = "//*[@id=\"nav\"]/page-language-sel/ion-content/div[2]/div[1]";
			mechanicWait(mechanicAppVersion_Path);
			WebElement mVersion = mechanicDriver.findElement(By.xpath(mechanicAppVersion_Path));
			String MechanicAppVersion = mVersion.getText();
			System.out.println("Technician::App Version ::" + MechanicAppVersion);

			Thread.sleep(2000);
			mechanicWait(LocatorPath.languagePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.languagePath);
			Thread.sleep(5000);
			
			
			mechanicWait(LocatorPath.signUpPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.signUpPath);
			mechanicWait(LocatorPath.mechanicMobilePath);
			String mechanicMobilePath = "//*[@id=\"nav\"]/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input";
			
			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			System.out.println("excelInputData.getMechanicNumber():::"+excelInputData.getMechanicNumber());
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, excelInputData.getMechanicNumber());
			//mechanicDriver.findElement(By.xpath(mechanicMobilePath)).sendKeys(excelInputData.getMechanicNumber());
			Thread.sleep(1000);
			hideKeyBoard(mechanicDriver);
			
			//mechanicDriver.findElement(By.xpath(mechanicMobilePath)).sendKeys(excelInputData.getMechanicNumber());
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);

			mechanicWait(LocatorPath.mechanicGaragePath);

			if (getElementAttributeValue(mechanicDriver, LocatorPath.mechanicGaragePath)
					.equalsIgnoreCase(excelInputData.getSelectGarage()))
				writeReport(LogStatus.PASS, "Technician:: Workshop name matches");
			else
				writeReport(LogStatus.PASS, "Technician:: Workshop name does not match");

			if (getElementAttributeValue(mechanicDriver, LocatorPath.mechanicNamePath)
					.equalsIgnoreCase(excelInputData.getMechanicName()))
				writeReport(LogStatus.PASS, "Technician Name matches");
			else
				writeReport(LogStatus.PASS, "Technician Name does not match");

			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicLoginPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicLoginPath);
			
			Thread.sleep(30000);//System takes long time here, later to be removed
			mechanicWait(LocatorPath.onlineOffLinePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onlineOffLinePath);
			Thread.sleep(2000);
			System.out.println("Technician Login:: Passed");
			writeReport(LogStatus.PASS, "Technician App Version::::::" + MechanicAppVersion);
			writeReport(LogStatus.PASS, "Technician Login::Passed");
			
	}

	public void Profilelogin(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

		if(isVerified) {
			System.out.println("isVerified in Technician login try"+isVerified);
		try {
			mechanicCapabilities();
			Thread.sleep(3000);
			switchToMechanicWebView();
			Utils.sleepTimeMedium();
			
			System.out.println("Entered Technician login method ");

			Thread.sleep(2000);
			mechanicWait(LocatorPath.languagePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.languagePath);
			Thread.sleep(1000);
			
			
			mechanicWait(LocatorPath.signUpPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.signUpPath);
			mechanicWait(LocatorPath.mechanicMobilePath);
			String mechanicMobilePath = "//*[@id=\"nav\"]/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input";
			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			
			LinkedHashMap<String, List<String>> driverList = ReadingExcelData.readingFleetManagerData("FMUserData");

			String MechanicName = driverList.get("MechanicName").get(1);
			String MechanicMobile = driverList.get("MechanicMobile").get(1);
			
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, MechanicMobile);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);
			Thread.sleep(5000);
						
			String NumberAlreadyExistsAlertPath="//div[@class='alert-head']";
			String NumberAlreadyExistsAlert=mechanicDriver.findElement(By.xpath(NumberAlreadyExistsAlertPath)).getText();
			System.out.println("NumberAlreadyExistsAlert::"+NumberAlreadyExistsAlert);
			if(NumberAlreadyExistsAlert.equalsIgnoreCase("This number is already registered, pending for approval"))
			{
				writeReport(LogStatus.PASS, "Technician:::Already registered Number alert is displayed:::Passed");
				MECH_TakeClick();
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				isVerified=true;
			}else
			{writeReport(LogStatus.FAIL, "Technician:::Already registered Number alert is not displayed:::Failed");
			}
			
		} catch (Exception e) {
			MECH_TakeClick();
			writeReport(LogStatus.FAIL, "Technician Login::: Failed");
			e.printStackTrace();
			Thread.sleep(500);
			isVerified=false;
		}}else
		{writeReport(LogStatus.FAIL, "Technician Login::: Failed");
		isVerified=false;}		
		}
	
	
	
	
	public void mechanicAcceptOrder(String orderId, boolean isVerified) throws InterruptedException, IOException {
									 
			Thread.sleep(8000);
			
			String actualOrder = "";
			mechanicWait(LocatorPath.mechanicNewRequest);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicNewRequest);
			Thread.sleep(5000);
			mechanicWait(LocatorPath.orderListPath);
			List<WebElement> orderTotalList = mechanicDriver.findElements(By.xpath(LocatorPath.orderListPath));
			System.out.println("Total Order list size :::" + orderTotalList.size());
					
			boolean order = false;
			for (int i = 1; i <= orderTotalList.size(); i++) {

				if (!order) {
					String actualOrderId = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[2]/ion-card[" + i + "]/ion-row/div/*";
					List<WebElement> actualorderList = mechanicDriver.findElements(By.xpath(actualOrderId));
					System.out.println("actualorderList ::" + actualorderList.size());
					for (int j = 0; j < actualorderList.size(); j++) {
						String actualMechOrderID = actualorderList.get(j).getText();
						System.out.println("actualOrder ::: " + actualMechOrderID);
						System.out.println("J ::: " + j);
						if (j == 2) {
							String[] value1 = actualMechOrderID.split(":");
							actualOrder = value1[1].trim();
							System.out.println("actualOrder ::: " + actualOrder);
						}
						if (actualOrder.equalsIgnoreCase(orderId)) {
							writeReport(LogStatus.PASS, "Technician:: Order id verified. Id Is -> " + actualMechOrderID);
							String ordersPathList = "//*[@id='nav']/page-home/ion-content/div[2]/div/div/div[2]/ion-card["+ i + "]/*/*/*/*/*/*";
							List<WebElement> orderList = mechanicDriver.findElements(By.xpath(ordersPathList));
							System.out.println("RIGHT::::::Order list size :::" + orderList.size());
							
							for (int g = 1; g <= orderList.size(); g++) {
								System.out.println("G ::: " + g);
								String acceptOrderText = orderList.get(g).getText();
								System.out.println("OrderID ::: " + acceptOrderText);
								if (acceptOrderText.equalsIgnoreCase("ACCEPT")) {
									
									//mechanicDriver.findElement(By.xpath("//*[contains(text(), 'ACCEPT')]"));
									JavascriptExecutor js = (JavascriptExecutor) mechanicDriver;
									 WebElement Element = mechanicDriver.findElement(By.xpath("//*[contains(text(), 'ACCEPT')]"));
								     //This will scroll the page till the element is found		
								     js.executeScript("arguments[0].scrollIntoView();", Element);
								     //Element.click();
								     Thread.sleep(1000);
									 orderList.get(g).click();
									 Thread.sleep(4000);
									 System.out.println("Technician::Accepted the order ::::");
									 writeReport(LogStatus.PASS, "Technician:::Accepted the order::Passed");
									 order = true;
									break;
									
								}
								}
						}
					}
				} else
					break;
			}
		}

	public void mechanicRejectOrder(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException {

		if(isVerified) {
		System.out.println("isVerified in mechanicRejectOrder try"+isVerified);
		try {
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicNewRequest);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicNewRequest);
			Utils.sleepTimeLow();
			
			
			JavascriptExecutor js = (JavascriptExecutor) mechanicDriver;
			 WebElement Element = mechanicDriver.findElement(By.xpath("//*[contains(text(), 'REJECT')]"));
		     //This will scroll the page till the element is found		
		     js.executeScript("arguments[0].scrollIntoView();", Element);
		     Element.click();
			/*mechanicWait(LocatorPath.mechanicRejectOrder);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRejectOrder);*/
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicRejectReasonList);
			List<WebElement> reasonList = mechanicDriver.findElements(By.xpath(LocatorPath.mechanicRejectReasonList));
			for (int i = 0; i < reasonList.size(); i++) {
				
				if (i == excelInputData.getRejectReason()) {
					reasonList.get(i).click();
					Thread.sleep(2000);
					writeReport(LogStatus.PASS, "Reject Reason ::: -> " + reasonList.get(i).getText() + " - Selected");
					break;
				}
			}
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertYesButton);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertYesButton);
			Thread.sleep(2000);
			System.out.println("Technician::Rejected the order ::::");
			writeReport(LogStatus.PASS, "Technician:::Reject order:::Passed");
			isVerified=true;
		}catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "Technician:::Reject order:::Failed");
			isVerified=false;
		}}
		else {
		writeReport(LogStatus.FAIL, "Technician:::Reject order Failed");
		isVerified=false;
		}
	}

	public void onGoingOrderPathClick(String locator) throws InterruptedException {

		try {
			Thread.sleep(4000);
			System.out.println("Inside Mechanic OnGoingOrder tab");
			mechanicWait(LocatorPath.onGoingOrdersTab);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onGoingOrdersTab);
			Thread.sleep(4000);
			System.out.println("Clicked the Mechanic OnGoingOrder tab");
			
			String onGoingVehicleNo = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/ion-searchbar/div/input";
			mechanicWait(onGoingVehicleNo);
			clickMechanicElement(LocatorType.XPATH, onGoingVehicleNo);
			enterValueMechanic(LocatorType.XPATH, onGoingVehicleNo, locator);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			//Thread.sleep(1000);
			//hideKeyBoard(mechanicDriver);
			Thread.sleep(1000);
			mechanicWait("//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card");
			List<WebElement> links1=mechanicDriver.findElements(By.xpath("//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/*/*"));
			System.out.println(links1.size());
			
			for(int i=5;i<=5; i++) {
			String StateText=links1.get(i).getText(); 
			System.out.println("I value"+i+  "Statetext:::"+StateText);
			if((i==5)&&(StateText.isEmpty()))
			{	
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				//System.out.println("onGoingPath :::" + onGoingPath);
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				writeReport(LogStatus.PASS, "Technician::Order State:::BLINK TEXT OFF MODE");
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}			
			if((i==5) && (StateText.equalsIgnoreCase("TRACK LOCATION"))) {
				System.out.println("Technician::StateText"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				break;
			}			
			if ((i==5) && (StateText.equalsIgnoreCase("START JOBS"))) {
				System.out.println("Technician:::::StateText::::"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}
			if ((i==5) && (StateText.equalsIgnoreCase("CONFIRM CASH RECEIPT"))) {
				System.out.println("Technician:StateText"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}
			if ((i==5) && (StateText.equalsIgnoreCase("GIVE ESTIMATE"))) {
				System.out.println("Technician:StateText"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}
			if ((i==5) && (StateText.equalsIgnoreCase("GIVE RATING"))) {
				System.out.println("Technician:StateText"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}
			if ((i==5) && (StateText.equalsIgnoreCase("UPDATE JOB STATUS"))) {
				System.out.println("Technician:StateText"+StateText);
				writeReport(LogStatus.PASS, "Technician::Order State::"+StateText);
				//String onGoingPath = "//*[contains(text(), '" + locator + "')]";
				//System.out.println("onGoingPath"+onGoingPath);
				//Thread.sleep(1000);
				//mechanicWait(onGoingPath);
				String onGoingPath ="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card/ion-card-content/ion-row[1]/ion-col[1]";
				mechanicDriver.findElement(By.xpath(onGoingPath)).click();
				System.out.println("Technician::Clicked ONGOING ORDERS PANEL");
				break;
			}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Technician OnGoingOrder tab click:- Failed");
			writeReport(LogStatus.FAIL, "Technician OnGoingOrder tab click:- Failed::");
			
		}

		
	}

	public void driverConfirmCashReceipt(ExcelInputData excelInputData, Driver driver,boolean isVerified) throws InterruptedException {
		if(isVerified) {
			System.out.println("isVerified in driverConfirmCashReceipt try"+isVerified);
		try {
		Thread.sleep(2000);
		onGoingOrderPathClick(excelInputData.getVehicleNumber());
		mechanicWait(LocatorPath.alertYesButton);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.alertYesButton);
		Thread.sleep(2000);
		System.out.println("Entered Technician Confirm cash receipt");
		writeReport(LogStatus.PASS, "Technician::Clicked Confirm cash receipt");
		isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,"Technician::Confirm cash receipt method Failed::");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL, "Technician:: Confirm cash receipt method:::Failed");
			isVerified=false;
			}
	}

	public void confirmCashReceipt(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {

		
		System.out.println(" in ConfirmCashReceipt");
		
			Thread.sleep(5000);
			onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(4000);
			mechanicWait(LocatorPath.alertYesButton);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertYesButton);
			System.out.println("Entered Technician Confirm cash receipt");
			writeReport(LogStatus.PASS, "Technician::Clicked Confirm cash receipt");
			//isVerified=true;
		
	}

	public void mechanicRating(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		
		System.out.println("in Technician Rating");
		Thread.sleep(3000);
			// mechanicWait(LocatorPath.mechanicThankYouPath);
			// clickMechanicElement(LocatorType.XPATH,
			// LocatorPath.mechanicThankYouPath);
			// Thread.sleep(2000);
			// mechanicWait(LocatorPath.mechanicRatingPath);
			// clickMechanicElement(LocatorType.XPATH,
			// LocatorPath.mechanicRatingPath);

			System.out.println("excelInputData.getRating::::" + excelInputData.getMechRating());
			Thread.sleep(1000);
			String mechanicRatingPath = "//*[@id=\"nav\"]/page-rating/ion-content/div[2]/ion-card[2]/ion-card-content/ion-row/span/starrating/span/ion-col["
					+ excelInputData.getMechRating() + "]";
			mechanicWait(mechanicRatingPath);

			mechanicDriver.findElement(By.xpath(mechanicRatingPath)).click();
			Thread.sleep(4000);

			/*
			 * mechanicWait(LocatorPath.mechanicFeedBackPath);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.mechanicFeedBackPath);
			 */

			System.out.println("excelInputData.getReason()::::" + excelInputData.getMechReason());
			Thread.sleep(4000);
			String mechanicFeedBackPath = "//html/body/ion-app/ion-alert/div/div[3]/div/button["
					+ excelInputData.getMechReason() + "]/span/div[2]";
			mechanicWait(mechanicFeedBackPath);
			mechanicDriver.findElement(By.xpath(mechanicFeedBackPath)).click();
			Thread.sleep(3000);

			mechanicWait(LocatorPath.mechanicRatingSubmit);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRatingSubmit);
			Thread.sleep(4000);
			writeReport(LogStatus.PASS, "Technician Rated the order ");
			System.out.println("Technician rated the order ::");
			isVerified=true;
		
	}

	public void mechanicRateCard() throws InterruptedException, IOException {

		try {
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.myAccountPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicAccountListPath);
			List<WebElement> myAccountList = mechanicDriver.findElements(By.xpath(LocatorPath.mechanicAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println("Inside myAccount loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Rate Card")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM:: My Vehicles selected");
					Thread.sleep(1000);
					break;
				}
			}
			Utils.sleepTimeLow();

			LinkedHashMap<String, List<String>> mechanicList = ReadingExcelData.readingFleetManagerData("FMUserData");
			String MechanicRateCard = mechanicList.get("MechanicRateCard").get(1);
			
			String rateCardEdit = "//ion-content/div[2]/ion-card[1]/ion-card-content/ion-row[2]/input";
			mechanicWait(rateCardEdit);
						
			List<WebElement> rateCardElement = mechanicDriver.findElements(By.xpath(rateCardEdit));
			rateCardElement.get(0).clear();
			rateCardElement.get(0).clear();
			rateCardElement.get(0).sendKeys(MechanicRateCard);
			hideKeyBoard(mechanicDriver);
			Thread.sleep(2000);
			String applyButton = "//page-rate-card/ion-footer/button";
			mechanicWait(applyButton);
			clickMechanicElement(LocatorType.XPATH, applyButton);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
			Thread.sleep(2000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
			writeReport(LogStatus.PASS, "Technician:::Rate Card Changed:::"+MechanicRateCard);
		} catch (Exception e) {
			e.printStackTrace();
			MECH_TakeClick();
			writeReport(LogStatus.FAIL, "Technician:::Rate Card Edit Failed");
		}
	}

	public void mechanicCancelOrder(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException {

		if(isVerified) {
		System.out.println("isVerified in mechanicCancelOrder try"+isVerified);
		try {
			System.out.println("Mechanic:Inside Cancel Method");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.onGoingOrdersTab);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onGoingOrdersTab);
			Utils.sleepTimeLow();
			String mechaniccancel = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card[1]/ion-card-content/ion-row[5]/ion-col[2]";
			mechanicWait(mechaniccancel);
			clickMechanicElement(LocatorType.XPATH, mechaniccancel);
			Utils.sleepTimeLow();
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			mechanicWait(yesBtn);
			clickMechanicElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(1000);
			/*
			 * String Reason =
			 * "//html/body/ion-app/ion-alert/div/div[3]/div/button[3]/span/div[2]";
			 * mechanicWait(Reason); clickMechanicElement(LocatorType.XPATH,
			 * Reason);
			 */
			Thread.sleep(1000);
			String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["
					+ excelInputData.getCancelReason() + "]";
			clickFMElement(LocatorType.XPATH, fmReasonForCancel);
			Thread.sleep(1000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Technician: Job is Cancelled");
			isVerified=true;

		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "Technician: Job is not Cancelled");
			isVerified=false;
		}}
		else {
		writeReport(LogStatus.FAIL, "Technician Login:::Failed");
		isVerified=false;
		}
	}

	public void onGoingOrderCompleteOrder(ExcelInputData excelInputData, String locator,boolean isVerified) throws IOException, InterruptedException {
		
				
			System.out.println("Inside onGoingOrderCompleteOrder method");

			Thread.sleep(7000);
			String CompleteOrder = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			mechanicWait(CompleteOrder);
			clickMechanicElement(LocatorType.XPATH, CompleteOrder);
			Thread.sleep(1000);
			
			//EnterJobCard(Utils.excelInputDataList.get(1));
			Thread.sleep(3000);
			String JobCard = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/div[1]/input";
			mechanicWait(JobCard);
			clickMechanicElement(LocatorType.XPATH, JobCard);
			enterValueMechanic(LocatorType.XPATH, JobCard, excelInputData.getJobCard());
			writeReport(LogStatus.PASS, "Technician: Job card Value is:::" + excelInputData.getJobCard());
			Thread.sleep(1000);
			String BillAmt = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/div[2]/input";
			mechanicWait(BillAmt);
			clickMechanicElement(LocatorType.XPATH, BillAmt);
			enterValueMechanic(LocatorType.XPATH, BillAmt, excelInputData.getBillAmount());
			writeReport(LogStatus.PASS, "Technician: Bill Amount Value is:::" + excelInputData.getBillAmount());
			hideKeyBoard(mechanicDriver);
			Thread.sleep(1000);
			String Okbtn = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/button[1]/span";
			clickMechanicElement(LocatorType.XPATH, Okbtn);
			Thread.sleep(3000);
			
			
			writeReport(LogStatus.PASS,"Technician::OnGoingOrderCompleteOrder:: Passed");
			//Check for duplicate JOB CARD#:
			try {			
			WebElement JobAlreadyExistsAlert = mechanicDriver.findElement(By.xpath("//div[@class='alert-wrapper']"));
			String alreadyexistAlert=JobAlreadyExistsAlert.getText();
			writeReport(LogStatus.FAIL, "Technician::Job Card Already Exists Alert captured::::" + alreadyexistAlert);
			Thread.sleep(2000);
			if (JobAlreadyExistsAlert.isDisplayed()) {
				System.out.println("Technician::Job Card Already Exists Alert captured::::" + alreadyexistAlert);
				mechanicDriver.findElement(By.xpath("//ion-icon[@class='icon_Logo icon icon-md']")).click();//Clicking on Vehicle Image to go back 
				//and Complete the order 
				Thread.sleep(3000);
				String CompleteOrderPath="//*[contains(text(), 'Complete Order')]";
				clickMechanicElement(LocatorType.XPATH, CompleteOrderPath);
				EnterJobCard(Utils.excelInputDataList.get(1));
				
			}
			
			
			// mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			// clickMechanicElement(LocatorType.XPATH,
			// LocatorPath.backImagePath);
			//isVerified=true;
		} catch (Exception e) {
			System.out.println("Technician::CompleteOrder Job doesnt exist previously :: Passed");
			//e.printStackTrace();
			//MECH_TakeClick();
			//writeReport(LogStatus.FAIL,"Technician::OnGoingOrderCompleteOrder:: Failed");
			//isVerified=false;
		}
				
	}
	
	public void EnterJobCard(ExcelInputData excelInputData) throws InterruptedException {
	
		Thread.sleep(3000);
		String JobCard = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/div[1]/input";
		mechanicWait(JobCard);
		clickMechanicElement(LocatorType.XPATH, JobCard);
		enterValueMechanic(LocatorType.XPATH, JobCard, excelInputData.getJobCard());
		writeReport(LogStatus.PASS, "Technician: Job card Value is:::" + excelInputData.getJobCard());
		Thread.sleep(1000);
		String BillAmt = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/div[2]/input";
		mechanicWait(BillAmt);
		clickMechanicElement(LocatorType.XPATH, BillAmt);
		enterValueMechanic(LocatorType.XPATH, BillAmt, excelInputData.getBillAmount());
		writeReport(LogStatus.PASS, "Technician: Bill Amount Value is:::" + excelInputData.getBillAmount());
		hideKeyBoard(mechanicDriver);
		Thread.sleep(1000);
		String Okbtn = "//html/body/ion-app/ion-modal/div/page-complete-order/ion-content/div[2]/div/button[1]/span";
		clickMechanicElement(LocatorType.XPATH, Okbtn);
		Thread.sleep(3000);
		
	
}
	
	

	public void myPreviousOrder(ExcelInputData excelInputData,Mechanic mechanic,String mRequestOrderId1,boolean isVerified) throws InterruptedException {
			System.out.println("mRequestOrderId1::::::::::::"+mRequestOrderId1);
				
 		if(isVerified){
			System.out.println("isVerified in myPreviousOrder try"+isVerified);
  
				try{
				System.out.println("Inside try");
 				Thread.sleep(7000);  
 				String MyAccLink = "//*[contains(text(),'MY')]";
 				//String MyAccLink ="/html/body/ion-app/page-home/ion-header/ion-segment/ion-segment-button[3]";
 				mechanicWait(MyAccLink);
				clickMechanicElement(LocatorType.XPATH,MyAccLink);
				Thread.sleep(2000);
				
				String MyPrevOrdr = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div/ion-list/ion-item[1]/div[1]/div/ion-label/ion-col[2]";
				clickMechanicElement(LocatorType.XPATH, MyPrevOrdr);
				Utils.sleepTimeLow();

				String MyPrevSearch = "//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/ion-searchbar/div/input";

				clickMechanicElement(LocatorType.XPATH, MyPrevSearch);
				enterValueMechanic(LocatorType.XPATH, MyPrevSearch, excelInputData.getVehicleNumber());
				Thread.sleep(1000);
				String InnerStrVal = "//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/div/ion-list/ion-item/div[1]/div/ion-label";
				clickMechanicElement(LocatorType.XPATH, InnerStrVal);
				Thread.sleep(6000);
				
					
				List<WebElement> prevOrderList = mechanicDriver.findElements(By.xpath("//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/ion-list/*"));
				System.out.println("prevOrderList.size" + prevOrderList.size());
				for (int i = 0; i < prevOrderList.size(); i++) {
				String p=prevOrderList.get(i).getText();														
				System.out.println("prevOrder nAME" + p);
				String OrderStatus = "completed";
				
				if(p.contains(mRequestOrderId1)) {
					System.out.println("Technician::Order Id matches" +mRequestOrderId1);
					writeReport(LogStatus.PASS,"Technician::Order ID matches:::::"+mRequestOrderId1);
				}else {writeReport(LogStatus.FAIL,"Technician::Order ID doesnt match:::::"+mRequestOrderId1);}
				 if ((p.contains(OrderStatus))) {
					System.out.println("Technician::Inside first if" + p + "equals:::::" + OrderStatus);
					writeReport(LogStatus.PASS,"Technician::OrderStatus matches:::::"+OrderStatus);
					}else {writeReport(LogStatus.FAIL,"Technician::Order Status doesnt match:::::"+OrderStatus);}
				if((p.contains(excelInputData.JobCard))) {
						
						//System.out.println("Inside Second if ::::" + p + "equals:::::" + excelInputData.JobCard+":::::"+ excelInputData.BillAmount);
						//String [] Str1 = p.split("?");
						//System.out.println("P after split::::"+Str1);
						
						//JOB176?176.00
								
				System.out.println(p.startsWith(excelInputData.JobCard));
				
				if(true) {
					writeReport(LogStatus.PASS,"Technician::Job Card"+ excelInputData.JobCard+ "& Bill Amount"+ excelInputData.BillAmount+" matches:::::");								
				}else
				{ writeReport(LogStatus.FAIL,"Technician::Job Card"+ excelInputData.JobCard+ "& Bill Amount"+ excelInputData.BillAmount+" doesnt match:::::");}
				break;
				}
				}
				
			}catch (Exception e) 
			{e.printStackTrace();}
			}
	}



	public String MechMyPreviousOrder(ExcelInputData excelInputData,String mRequestOrderId,boolean isVerified) throws InterruptedException, IOException {
	
		java.lang.String mechactualvalue="0.0";
		//String finalValidationAmount
		System.out.println("mRequestOrderId1::::::::::::"+mRequestOrderId);
		//System.out.println("finalValidationAmount::::::::::::"+finalValidationAmount);
	
		
	
		System.out.println("in MechMyPreviousOrder ");

		
		Thread.sleep(3000);  
		String MyAccLink = "//*[contains(text(),'MY')]";
		mechanicWait(MyAccLink);
		clickMechanicElement(LocatorType.XPATH, MyAccLink);
		Thread.sleep(4000);
						
		
		String MyPrevOrdr = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/ion-list/ion-item[1]/div[1]/div/ion-label/ion-col[2]";
		mechanicWait(MyPrevOrdr);
		clickMechanicElement(LocatorType.XPATH, MyPrevOrdr);
		//1 Utils.sleepTimeLow();

		String MyPrevSearch = "//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/ion-searchbar/div/input";

		clickMechanicElement(LocatorType.XPATH, MyPrevSearch);
		enterValueMechanic(LocatorType.XPATH, MyPrevSearch, excelInputData.getVehicleNumber());
		Thread.sleep(1000);
		String InnerStrVal = "//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/div/ion-list/ion-item/div[1]/div/ion-label";
		clickMechanicElement(LocatorType.XPATH, InnerStrVal);
		// 2 Thread.sleep(6000);
		Thread.sleep(2000);
		
		//*[@id="nav"]/page-previous-orders/ion-content/div[2]/ion-list
		//*[@id="nav"]/page-previous-orders/ion-content/div[2]/ion-list/ion-card[1]
		List<WebElement> prevOrderList = mechanicDriver.findElements(By.xpath("//*[@id=\"nav\"]/page-previous-orders/ion-content/div[2]/ion-list/*/*/*"));
		System.out.println("prevOrderList.size" + prevOrderList.size());
		Thread.sleep(2000);
		for (int i = 0; i < prevOrderList.size(); i++) {
		String p=prevOrderList.get(i).getText();														
		System.out.println("prevOrder nAME:::" + p +"::::i value:::" +i);
		String OrderStatus = "completed";
		
		LinkedHashMap<String, List<String>> vehicleDetailsList = ReadingExcelData.readingFleetManagerData("FMUserData");
		String Make = vehicleDetailsList.get("Select Make").get(1);
		String Category = vehicleDetailsList.get("Category").get(1);
		
		if((i==0) && (p.contains(mRequestOrderId)))
		{	
			System.out.println("Order Id matches:::" +p);
			writeReport(LogStatus.PASS,"Technician::Order Id matches:::" +p);
		}
		
	    if((i==1) && (p.contains(Make) && (p.contains(Category))))
	    {
	    	System.out.println("Make & Category matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::Make & Category matches:::"+p);
	    }
				
	    if((i==2) && (p.contains(excelInputData.getVehicleNumber())))
	    {
	    	System.out.println("Vehicle Number matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::Vehicle Number matches:::"+p);
	    }
	    
	    if((i==3) && (p.contains(OrderStatus)))
	    {
	    	System.out.println("Order Status matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::Order Status matches:::"+p);
	    }
		
	    if((i==4) && (p.contains(excelInputData.getDriverName())))
	    {
	    	System.out.println("Driver Name matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::Driver Name matches:::"+p);
	    }
	    
	    if((i==5) && (p.contains(excelInputData.getFMName())))
	    {
	    	System.out.println("FM Name matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::FM Name matches:::"+p);
	    }
	    if((i==6) && (p.contains(excelInputData.getKMReading())))
	    {
	    	System.out.println("Technician::KM reading matches:::"+p);
	    	writeReport(LogStatus.PASS,"Technician::KM reading matches:::"+p);
	    }
	    if((i==14))
		{
	    	//System.out.println("Final Bill Amt:::"+Utils.finalBillAmt);
			System.out.println("Technician::ESTIMATE & BILL AMOUNT:::"+p);
			writeReport(LogStatus.PASS,"Technician::ESTIMATE & BILL AMOUNT:::"+p);
			String[] value1 = p.split("₹");
			mechactualvalue = value1[2].trim();
			System.out.println("mechactualvalue:::::"+mechactualvalue);
						
		}
		if(i==15)
		{
			break;
		}	
			
		}	
		return mechactualvalue;
	}
	
	public void MECHLogout(ExcelInputData excelInputData) throws InterruptedException
	{
		Thread.sleep(5000);
	  String BackButtonPath="//*[@id=\"nav\"]/page-previous-orders/ion-header/ion-navbar/button";
	  clickMechanicElement(LocatorType.XPATH, BackButtonPath);
	  
	 Thread.sleep(4000);
	  String LogoutPath="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/ion-list/ion-item[10]/div[1]/div/ion-label/ion-col[2]";
	  clickMechanicElement(LocatorType.XPATH, LogoutPath);
	  String YesBtn="//html/body/ion-app/ion-alert/div/div[3]/button[1]";
	  Thread.sleep(3000);
	  clickMechanicElement(LocatorType.XPATH, YesBtn);
	  Thread.sleep(5000);
	  System.out.println("MECH:: System logged out");
	  writeReport(LogStatus.PASS, "MECH::System logged out:::" );
	  
	}
	public void MECHLogoutwithoutback(ExcelInputData excelInputData) throws InterruptedException
	{
	  Thread.sleep(2000);
	  String LogoutPath="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/ion-list/ion-item[10]/div[1]/div/ion-label/ion-col[2]";
	  clickMechanicElement(LocatorType.XPATH, LogoutPath);
	  String YesBtn="//html/body/ion-app/ion-alert/div/div[3]/button[1]";
	  Thread.sleep(2000);
	  clickMechanicElement(LocatorType.XPATH, YesBtn);
	  Thread.sleep(5000);
	  System.out.println("MECH:: System logged out");
	  writeReport(LogStatus.PASS, "MECH::System logged out:::" );
	  
	}
	
	
	 public void MECHLogoutfrmMyAccLink(ExcelInputData excelInputData) throws InterruptedException
		{
		 
		  Thread.sleep(3000);
			 
			 String MyAccLink = "//*[contains(text(),'MY')]";
			 mechanicWait(MyAccLink);
			 clickMechanicElement(LocatorType.XPATH, MyAccLink);
		  String LogoutPath="//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/ion-list/ion-item[10]/div[1]/div/ion-label/ion-col[2]";
		  clickMechanicElement(LocatorType.XPATH, LogoutPath);
		  String YesBtn="//html/body/ion-app/ion-alert/div/div[3]/button[1]";
		  Thread.sleep(2000);
		  clickMechanicElement(LocatorType.XPATH, YesBtn);
		  Thread.sleep(5000);
		  System.out.println("MECH:: System logged out");
		  writeReport(LogStatus.PASS, "MECH::System logged out:::" );
		  
		}
}
	
	
		
		
		
		
		//else {writeReport(LogStatus.FAIL,"Order ID doesnt match:::::"+mRequestOrderId1);}
		 /*if ((p.contains(OrderStatus))) {
			System.out.println("Inside first if" + p + "equals:::::" + OrderStatus);
			writeReport(LogStatus.PASS,"OrderStatus matches:::::"+OrderStatus);
		 }}*///else {writeReport(LogStatus.FAIL,"Order Status doesnt match:::::"+OrderStatus);}
		/*if((p.contains(excelInputData.JobCard))) {
				
				//System.out.println("Inside Second if ::::" + p + "equals:::::" + excelInputData.JobCard+":::::"+ excelInputData.BillAmount);
				//String [] Str1 = p.split("?");
				//System.out.println("P after split::::"+Str1);
				
				//JOB176?176.00
						
		System.out.println(p.startsWith(excelInputData.JobCard));
		
		if(true) {
			writeReport(LogStatus.PASS,"Job Card"+ excelInputData.JobCard+ "& Bill Amount"+ excelInputData.BillAmount+" matches:::::");								
		}else
		{ writeReport(LogStatus.FAIL,"Job Card"+ excelInputData.JobCard+ "& Bill Amount"+ excelInputData.BillAmount+" doesnt match:::::");}
		break;
		}
		}*/
		
			
					
					
					
					
					
					
			


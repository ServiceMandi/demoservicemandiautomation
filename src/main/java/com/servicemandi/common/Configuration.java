
package com.servicemandi.common;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;


public class Configuration <fleetManagerDriver> {

	private static final String String = null;
	private static DesiredCapabilities desiredCapabilities;
	private WebElement element;
	
	protected static AppiumDriver AppiumDriver=null;
	protected static AndroidDriver fleetManagerDriver = null;
	protected static AndroidDriver mechanicDriver = null;
	protected static AndroidDriver driveDriver = null;
	protected static WebDriverWait fmWait = null;
	protected static WebDriverWait mechWait = null;
	protected static WebDriverWait driverWait = null;
	protected static WebDriverWait browserWait = null;
	
	public static ExtentReports extentReports;
	public static ExtentTest eLogger;
	public static String fmAppPackage = "WEBVIEW_com.servicemandi.fm";
	public static String mechAppPackage = "WEBVIEW_com.servicemandi.mechanic";
	public static String driveAppPackage = "WEBVIEW_com.servicemandi.driver";
	
	
	public enum LocatorType {
		XPATH, ID, CLASS, NAME, CSS
	}

	public static void fleetManagerCapabilities() {

		try {
			
			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G");

			desiredCapabilities.setCapability("appium-version", "1.7.1");
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "4200d9904ed114f3"); // Deepa
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID,"420015a3b2fa1449"); // Priya
			//desiredCapabilities.setCapability(MobileCapabilityType.UDID, "42005e58ec05241b");
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			//desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm_beta"); 
			//desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm_beta.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			//desiredCapabilities.setCapability("automationName", "Appium");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			
			//desiredCapabilities.setCapability("nativeWebScreenshot",true);
			//desiredCapabilities.setCapability("androidScreenshotPath","screenshots");
			

			fleetManagerDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), desiredCapabilities);

			fleetManagerDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
		
						
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error ::" + e.toString());
		}
	}
		
	public static AndroidDriver getFleetManagerInstance() {

		if (fleetManagerDriver != null) {
			return fleetManagerDriver;
		}
		return null;
	}
	
	public static AndroidDriver getDriverManagerInstance() {

		if (driveDriver != null) {
			return driveDriver;
		}
		return null;
	}

	public static AndroidDriver getMechanicDriver() {

		if (mechanicDriver != null) {
			return mechanicDriver;
		}
		return null;
	}
	
	public static void mechanicCapabilities() {

		try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G"); // Mechanic
			desiredCapabilities.setCapability("appium-version", "1.7.1");
			// Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "4200145bf0e11499");
			// Priya Mec
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,"42005e40ec9e2415");
			
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.mechanic_beta");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.mechanic_beta.MainActivity");
			//desiredCapabilities.setCapability("appPackage", "com.servicemandi.mechanic");
			//desiredCapabilities.setCapability("appActivity", "com.servicemandi.mechanic.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			
			mechanicDriver = new AndroidDriver(new URL("http://127.0.0.1:4273/wd/hub"), desiredCapabilities);
			mechanicDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			
			/*
			 * mechWait = new
			 * FluentWait<WebDriver>(mechanicDriver).withTimeout(30,
			 * TimeUnit.SECONDS) .pollingEvery(5,
			 * TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			 */

		} catch (Exception e) {
		}
	}

	public static void driverCapabilities() {

		try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G"); // Driver
			desiredCapabilities.setCapability("appium-version", "1.7.1");
			// Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID,"32011c38412e3479"); 
			// Priya
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,"4200d98a4ef0145d");
			
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			//desiredCapabilities.setCapability("appPackage", "com.servicemandi.driver");
			//desiredCapabilities.setCapability("appActivity", "com.servicemandi.driver.MainActivity");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.driver_beta");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.driver_beta.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			
			driveDriver = new AndroidDriver(new URL("http://127.0.0.1:4274/wd/hub"), desiredCapabilities);
			driveDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			

			/*
			 * mechWait = new
			 * FluentWait<WebDriver>(mechanicDriver).withTimeout(30,
			 * TimeUnit.SECONDS) .pollingEvery(5,
			 * TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
			 */

		} catch (Exception e) {
		}
	}
	public static void moveToNextFlow(String newWorkFlow) throws InterruptedException {
		
	}
	public static String reportConfiguration(String newWorkFlow) throws InterruptedException {

		System.out.println("WorkflowName::::" + newWorkFlow);
		DateFormat df = new SimpleDateFormat("dd MM yyyy hh mm");
		Thread.sleep(2000);
		System.out.println("DateFormat::::" + df.format(new Date()));
		///C:\\Users\\14402\\git\\demoservicemandiautomation\\SM_Report\\
		extentReports = new ExtentReports("D:\\SM_EXE\\SMReport\\" + newWorkFlow + df.format(new Date()) + ".html",true,DisplayOrder.OLDEST_FIRST);
		//extentReports = new ExtentReports("D:\\SMReports\\" + newWorkFlow + ".html",true,DisplayOrder.OLDEST_FIRST);
		return newWorkFlow;
	}

	public static String closeWorkFlowReport() {
		String Status="Pass";
		System.out.println("closeWorkFlowReport");
		extentReports.endTest(eLogger);
		return Status;
	}

	public static String createNewWorkFlowReport(String newWorkFlow) {

		eLogger = extentReports.startTest(newWorkFlow);
		return newWorkFlow;
	}

	public WebElement getFMWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (fleetManagerDriver.findElements(By.xpath(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (fleetManagerDriver.findElements(By.name(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (fleetManagerDriver.findElements(By.className(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (fleetManagerDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickFMElement(LocatorType type, String locator) {

		try {
			WebElement elem = getFMWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterFMValue(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getFMWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * This method switches to web view context.
	 */
	public static void switchToFMWebView() {

		try {
			Set<String> availableContexts = fleetManagerDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(fmAppPackage)) {
					fleetManagerDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	public void fleetManagerWait(final String locator) throws InterruptedException {
		
		
		try {	
			fmWait = new WebDriverWait(fleetManagerDriver,45);
			fmWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));				
		/*Wait<WebDriver> wait=new FluentWait<WebDriver>(fleetManagerDriver) 
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(3, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement element=wait.until(new Function<WebDriver, WebElement>(){
			public WebElement apply(WebDriver fleetManagerDriver)
			{
				return fleetManagerDriver.findElement(By.xpath(locator));
			}
			
		});
		
		System.out.println("Element is displayed"+element.isDisplayed());*/
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public void browserWait(final String locator) throws InterruptedException {

		Thread.sleep(1000);
		browserWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		
	}

	public WebElement getMechanicWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (mechanicDriver.findElements(By.xpath(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (mechanicDriver.findElements(By.name(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (mechanicDriver.findElements(By.className(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (mechanicDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickMechanicElement(LocatorType type, String locator) {

		try {
			WebElement elem = getMechanicWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterValueMechanic(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getMechanicWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}

	public static void switchToMechanicWebView() {

		try {
			Set<String> availableContexts = mechanicDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(mechAppPackage)) {
					mechanicDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void mechanicWait(final String locator) throws InterruptedException {
	
		try{
		
		mechWait = new WebDriverWait(mechanicDriver, 45);	
		mechWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		/*Thread.sleep(1000);
		Wait<AndroidDriver> wait=new FluentWait<AndroidDriver>(mechanicDriver) 
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		WebElement element=wait.until(new Function<AndroidDriver, WebElement>(){
			public WebElement apply(AndroidDriver mechanicDriver)
			{
				
				return mechanicDriver.findElement(By.xpath(locator));
			}
			
		});
		
		System.out.println("Element is displayed"+element.isDisplayed());*/
		}catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public static void mechanicClassPathWait(final String locator) {

		mechWait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));

		/*
		 * mechWait.until(new Function<WebDriver, WebElement>() { public
		 * WebElement apply(WebDriver driver) { return
		 * driver.findElement(By.className(locator)); } });
		 */

	}

	public WebElement getDriverWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (driveDriver.findElements(By.xpath(locator)).size() > 0) {
				element = driveDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (driveDriver.findElements(By.name(locator)).size() > 0) {
				element = driveDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (driveDriver.findElements(By.className(locator)).size() > 0) {
				element = driveDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (driveDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = driveDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickDriverElement(LocatorType type, String locator) {

		try {
			WebElement elem = getDriverWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterDriverValue(LocatorType type, String locator, String data) {

		try {
			System.out.println("***********");
			WebElement elem = getDriverWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/**
	 * This method switches to web view context.
	 */
	public static void switchToDriverWebView() {

		try {
			Set<String> availableContexts = driveDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(driveAppPackage)) {
					driveDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void driverWait(final String locator) throws InterruptedException {

		/*Thread.sleep(1000);
		Wait driverWait = new FluentWait<AndroidDriver<WebElement>>(driveDriver) 			
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
				
		driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));*/
		driverWait = new WebDriverWait(driveDriver, 45);
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		/*
		 * fmWait.until(new Function<WebDriver, WebElement>() { public
		 * WebElement apply(WebDriver driver) {
		 * 
		 * WebElement element = driver.findElement(By.xpath(locator)); String
		 * getTextOnPage = element.getText(); System.out.println(getTextOnPage);
		 * return null; } });
		 */
	}
	
	/**
	 * This method destroys the created android driver instance.
	 */
	public static void tearDownFleetManager() {
		try {
			Thread.sleep(5000);
			fleetManagerDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static void saveReport() throws InterruptedException {
		String Status="Pass";
	
		System.out.println("saveReport");
		writeReport(LogStatus.PASS, "End of Work Flow");
		extentReports.endTest(eLogger);
		extentReports.flush();
		//extentReports.close();
		
	}

	/**
	 * This method destroys the created android driver instance.
	 */
	public static void tearDownMechanic() {
		try {
			Thread.sleep(2000);
			mechanicDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static void tearDownDriver() {
		try {
			Thread.sleep(2000);
			driveDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static String getElementTextValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getText();
		} catch (Exception e) {

		}
		return "";
	}

	public static String getElementAttributeValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		} catch (Exception e) {

		}
		return "";
	}

	public static void writeReport(LogStatus logStatus, String status) throws InterruptedException {

		if (eLogger == null) {

			reportConfiguration(String );
		}

		status = status + " : " + Utils.timeCalculation();
		switch (logStatus) {

		case PASS:
			eLogger.log(LogStatus.PASS, status);
			break;
		case FAIL:
			eLogger.log(LogStatus.FAIL, status);
			break;
		case INFO:
			eLogger.log(LogStatus.INFO, status);
		}
	}

	public static void hideKeyBoard(AndroidDriver androidDriver) {

		try {
			androidDriver.hideKeyboard();
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void catchcall() throws InterruptedException, IOException{
	
		Thread.sleep(1000);
		try {
		String alertmsg = "//div[@class='alert-head']";
		if (fleetManagerDriver.findElement(By.xpath(alertmsg)).isDisplayed()) {
			Thread.sleep(2000);
			String GetErrorText = fleetManagerDriver.findElement(By.xpath(alertmsg)).getText();
			System.out.println("FM::System aborted due to::::" + GetErrorText);
			FM_TakeClick();
			Thread.sleep(1000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.FAIL, "FM::System aborted due to::::" + GetErrorText);
		} else if (mechanicDriver.findElement(By.xpath(alertmsg)).isDisplayed()) {
			Thread.sleep(2000);
			String GetErrorText = mechanicDriver.findElement(By.xpath(alertmsg)).getText();
			System.out.println("TECHNICIAN::System aborted due to::::" + GetErrorText);
			MECH_TakeClick();
			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.FAIL, "TECHNICIAN::System aborted due to::::" + GetErrorText);
				
		}
	}catch(Exception e) {
	if (fleetManagerDriver.equals(fleetManagerDriver)) {
			Thread.sleep(1000);
			ERRFM_TakeClick();
			Thread.sleep(1000);
		} else if (mechanicDriver.equals(mechanicDriver)) {
			Thread.sleep(1000);
			ERRMECH_TakeClick();
			Thread.sleep(1000);
							
		}
		
	}
	
	}
	
	
	public static void FM_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside TakeClick method");
		((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) fleetManagerDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "_FM_"+ df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "FM_"+df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "FM::::SNAPSHOT::::: " + eLogger.addScreenCapture(DestFile));
		
		switchToFMWebView();
		}
		
	public static void MECH_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside TakeClick method");
		((AppiumDriver<WebElement>) mechanicDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) mechanicDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "_TECHINICIAN_" + df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "TECHINICIAN_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "TECHINICIAN:::::SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
		
		switchToMechanicWebView();
		}
	
	public void Driver_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside TakeClick method");
		((AppiumDriver<WebElement>) driveDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) driveDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat("dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "_DRIVER_" + df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "DRIVER_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "DRIVER:::::SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
		
		switchToDriverWebView();
		}
	
	public static void ERRFM_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside ERRFM_TakeClick()");
		((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) fleetManagerDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat("dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\"+"ERRFM_"+ df1.format(new Date()) +".png";
		//String DestFile = System.getProperty("user.dir") + "\\target\\smImages\\"+"ERRFM_"+ df1.format(new Date()) + ".png";
							
		//String DestFile = "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "ERRFM_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		
		//FileUtils.getFile(SrcFile, "\\\\10.1.221.101\\SM_automation\\"+"ERRFM_"+ df1.format(new Date()) +".png");
		Thread.sleep(2000);
		writeReport(LogStatus.FAIL, "FM::::SNAPSHOT::::: " + eLogger.addScreenCapture("\\\\10.1.221.101\\SM_automation\\"+"ERRFM_"+ df1.format(new Date()) +".png"));
		
		switchToFMWebView();
	}
		
	public static void ERRMECH_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside TakeClick method");
		((AppiumDriver<WebElement>) mechanicDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) mechanicDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" +"ERRMECH_"+ df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "ERRMECH_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.FAIL, "TECHINICIAN:::::SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
		
		switchToMechanicWebView();
		}
	
	public void ERRDriver_TakeClick() throws InterruptedException, IOException {
		System.out.println("Inside TakeClick method");
		((AppiumDriver<WebElement>) driveDriver).context("NATIVE_APP");
		Thread.sleep(2000);

		TakesScreenshot scrShot = ((TakesScreenshot) driveDriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		Thread.sleep(2000);

		DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh mm ss");
		String DestFile = "\\\\10.1.221.101\\SM_automation\\" + "ERRDriver_"+ df1.format(new Date()) + ".png";
		//String DestFile =  "D:\\Deepa\\GitFolder\\GitPull\\Screenshots\\"+ "ERRDriver_"+ df1.format(new Date()) + ".png";
		Thread.sleep(2000);
		System.out.println("Date Format::::" + df1.format(new Date()));
		FileUtils.copyFile(SrcFile, new File(DestFile));
		Thread.sleep(2000);
		writeReport(LogStatus.FAIL, "DRIVER:::::SNAPSHOT::: " + eLogger.addScreenCapture(DestFile));
		
		switchToDriverWebView();
		}
	
	
}

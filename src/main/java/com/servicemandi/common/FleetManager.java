
package com.servicemandi.common;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;

 
public class FleetManager extends Configuration {

	public FleetManager() {

	}

	public void login(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

	
			fleetManagerCapabilities();
			System.out.println("Entered FM Login method ::: ");
			
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeMedium();

			//String fleetMAppVerion_Path = "//html/body/ion-app/ng-component/ion-nav/page-language-sel/ion-content/div[2]/div[1]";
			String fleetMAppVerion_Path ="//div[@class='version-class']";
			WebElement fmVersion = fleetManagerDriver.findElement(By.xpath(fleetMAppVerion_Path));
			String FMAppVersion = fmVersion.getText();
			System.out.println("FM App Version ::" + FMAppVersion);
			Thread.sleep(3000);
			
			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM:: AppVersion:::" + FMAppVersion);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);

			Thread.sleep(1000);
			// String mobilePath1 =
			// "/html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input/input[1]";
			String mobilePath1 = "//html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input/div";
			fleetManagerWait(LocatorPath.mobilePath);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			clickFMElement(LocatorType.XPATH, mobilePath1);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getFMNumber());
			Thread.sleep(1000);

			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
			Thread.sleep(25000);
			
			fleetManagerWait(LocatorPath.liveOrderTab);
			//fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-header/div/ion-segment/ion-segment-button[2]/p")).click();
			clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			
			//Thread.sleep(2000);
			
			writeReport(LogStatus.PASS, "FM:: Login Passed");
			isVerified = true;
		
	}

	public void clickLiveOrderTab(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in clickLiveOrderTab try" + isVerified);
			try {
				Thread.sleep(2000);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(500);

				String fmhomePageText = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/ion-list/ion-item/div[1]/div/ion-label/p";
				fleetManagerWait(fmhomePageText);
				String fmhomePageTextMsg = fleetManagerDriver.findElement(By.xpath(fmhomePageText)).getText();
				System.out.println("fmhomePageTextMsg:::" + fmhomePageTextMsg);
				if (fmhomePageTextMsg.startsWith("You have")) {
					System.out.println("fmhomePageTextMsg::is Displayed");
					isVerified = true;
				} else {
					System.out.println("fmhomePageTextMsg::is not Displayed");
					Thread.sleep(1000);
					String fmSearch = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[1]/ion-searchbar/div/input";
					clickFMElement(LocatorType.XPATH, fmSearch);
					enterFMValue(LocatorType.XPATH, fmSearch, excelInputData.getVehicleNumber());
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(2000);
					String delicon = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card/ion-card-content/ion-icon";
					clickFMElement(LocatorType.XPATH, delicon);
					String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
					fleetManagerWait(yesBtn);
					clickFMElement(LocatorType.XPATH, yesBtn);
					Thread.sleep(1000);
					String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
					clickFMElement(LocatorType.XPATH, fmReasonForCancel);
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(2000);
					isVerified = true;
				}
			} catch (Exception e) {
				// e.printStackTrace();
				System.out.println("fmhomePageTextMsg::is not Displayed");
				Thread.sleep(2000);
				String fmSearch = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[1]/ion-searchbar/div/input";
				clickFMElement(LocatorType.XPATH, fmSearch);
				enterFMValue(LocatorType.XPATH, fmSearch, excelInputData.getVehicleNumber());
				Thread.sleep(500);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);
				String delicon = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card/ion-card-content/ion-icon";
				clickFMElement(LocatorType.XPATH, delicon);
				String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
				fleetManagerWait(yesBtn);
				clickFMElement(LocatorType.XPATH, yesBtn);
				Thread.sleep(1000);
				String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["
						+ excelInputData.getCancelReason() + "]";
				clickFMElement(LocatorType.XPATH, fmReasonForCancel);
				Thread.sleep(1000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				isVerified = true;
				

			}
		} else {
			System.out.println("clickLiveOrderTab Failed::::");
			isVerified = false;
		}
	}

	public void selectLiveOrderSearch(String locator, boolean isVerified) throws InterruptedException {
		
			System.out.println("in FM:selectLiveOrderSearch ");
		
				Thread.sleep(3000);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(4000);
				System.out.println("FM::Live Order Tab clicked ::");

				String fmSearch = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[1]/ion-searchbar/div/input";
				clickFMElement(LocatorType.XPATH, fmSearch);
				enterFMValue(LocatorType.XPATH, fmSearch, locator);
				//Thread.sleep(1000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				//hideKeyBoard(fleetManagerDriver);
				//Thread.sleep(2000);
				List<WebElement> links1 = fleetManagerDriver.findElements(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card/*/*"));
				System.out.println(links1.size());

				for (int i = 7; i <= links1.size(); i++) {
					String FmStateText = links1.get(i).getText();
					System.out.println(+i+":::Value::::" +FmStateText);
					
					if ((i == 8) && (FmStateText.equalsIgnoreCase("APPROVE ESTIMATE"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(4000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					if ((i == 8) && (FmStateText.equalsIgnoreCase("APPROVE REVISED ESTIMATE"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					if ((i == 8) && (FmStateText.equalsIgnoreCase("VIEW BILL"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(1000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						Thread.sleep(1000);
						break;
					}
					if ((i == 8)&&(FmStateText.isEmpty())) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(4000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						//clickFMElement(LocatorType.XPATH, liveOrderPath);
						writeReport(LogStatus.PASS, "FM::Order State:::BLINK TEXT OFF MODE" );
						Thread.sleep(1000);
						break;
					}
					if ((i == 8) && (FmStateText.equalsIgnoreCase("GIVE RATING"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					
					if ((i == 8) && (FmStateText.equalsIgnoreCase("FINDING WORKSHOPS"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
											
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					if ((i == 9) && (FmStateText.equalsIgnoreCase("TRACK LOCATION"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					if ((i == 9) && (FmStateText.equalsIgnoreCase("CONFIRM WORKSHOP"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
					if((i==9)&&(FmStateText.isEmpty()))
					{	
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
						fleetManagerWait(liveOrderPath);
						fleetManagerDriver.findElement(By.xpath(liveOrderPath)).click();
						
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						break;
					}
				
				}
			}
	
	
	public void selectLiveOrderSearchCancelOrder(ExcelInputData excelInputData,String locator, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in FM:selectLiveOrderSearchCancelOrder try" + isVerified);
			try {
				Thread.sleep(2000);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(2000);
				System.out.println("FM::Live Order Tab clicked ::");

				String fmSearch = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[1]/ion-searchbar/div/input";
				clickFMElement(LocatorType.XPATH, fmSearch);
				enterFMValue(LocatorType.XPATH, fmSearch, locator);
				Thread.sleep(1000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				
				Thread.sleep(2000);
				fleetManagerWait(LocatorPath.cancelOrderPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
				Utils.sleepTimeLow();
				String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
				fleetManagerWait(yesBtn);
				clickFMElement(LocatorType.XPATH, yesBtn);
				Thread.sleep(1000);
				String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
				clickFMElement(LocatorType.XPATH, fmReasonForCancel);
				Thread.sleep(1000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(3000);
				writeReport(LogStatus.PASS, "FM: SelectLiveOrderSearch::Order is Deleted");

		} catch (Exception e) {
				writeReport(LogStatus.FAIL, "FM: SelectLiveOrderSearch:Delete Failed::");
		}}else{
				writeReport(LogStatus.FAIL, "FM: SelectLiveOrderSearch:Delete Failed::");
				isVerified = false;
				}
		}

	public void driverViewBill(ExcelInputData excelInputData, Driver driver, boolean isVerified)
			throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in DriverViewBill try" + isVerified);
			try {
				Thread.sleep(2000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View Bill method ::");
				Thread.sleep(2000);
				Utils.checkBillWithIGST();
				fleetManagerWait(LocatorPath.payCashButtonPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				Thread.sleep(2000);

				// Utils.checkBillWithOutGST();
				// Utils.checkBillWithIGST();
				// Utils.checkBillWithGST();

				writeReport(LogStatus.PASS, "FM: View bill & Pay cash");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM: DriverViewBill::Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM: DriverViewBill::Failed");
			isVerified = false;
		}
	}

	public void deleteFMOrder(boolean isVerified) throws InterruptedException {

		if (isVerified) {
			System.out.println("isVerified in mechanic try" + isVerified);

			try {
				Thread.sleep(4000);
				System.out.println("Inside fmCreateBDOrder");
				Thread.sleep(4000);
				// Utils.sleepTimeLow();
				String fmNewRequest = "/html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-header/div/ion-segment/ion-segment-button[1]/p";
				fleetManagerWait(fmNewRequest);
				clickFMElement(Configuration.LocatorType.XPATH, fmNewRequest);

				Utils.sleepTimeMedium();
				fleetManagerWait(LocatorPath.deletelOrderPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.deletelOrderPath);

				Utils.sleepTimeLow();
				String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
				fleetManagerWait(yesBtn);
				clickFMElement(LocatorType.XPATH, yesBtn);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				writeReport(LogStatus.PASS, "FM: FM Order deleted");
				isVerified = true;

			} catch (Exception e) {

				writeReport(LogStatus.FAIL, "FM: DeleteFMOrder Failed::::::");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM: DeleteFMOrder Failed::::::");
			isVerified = false;
		}
	}

	public void viewBill(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {
		//double ViewBillReturnValue=0.0;
		 
		System.out.println("in viewBill");
		Thread.sleep(3000);
		selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
		System.out.println("Entered View Bill method ::");
		writeReport(LogStatus.PASS, "FM::Entered View bill method::Passed");
		Thread.sleep(2000);
		 
		try {

		String viewdetBillAlert = fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ion-alert/div/div[3]/button/span")).getText();

		if (viewdetBillAlert.equals("VIEW DETAILED BILL")) {
		 
		fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ion-alert/div/div[3]/button/span")).click();
		 
		} else {
		System.out.println("In else block");

		}
		Utils.checkBillWithGST();
		fleetManagerWait(LocatorPath.payCashButtonPath);
		clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
		writeReport(LogStatus.PASS, "FM::Clicked Pay cash::Passed");
		fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		Utils.checkBillWithGST();
		fleetManagerWait(LocatorPath.payCashButtonPath);
		clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
		writeReport(LogStatus.PASS, "FM::Clicked Pay cash::Passed");
		fleetManagerWait(LocatorPath.liveOrderTab);

		}
	}

	public void viewBillRetailerInvoice(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in viewBill" + isVerified);
			try {
				Thread.sleep(3000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View Bill method ::");
				writeReport(LogStatus.PASS, "FM::Entered View bill method::Passed");
				Thread.sleep(5000);
				
				fleetManagerWait(LocatorPath.Addicon);
				// clickFMElement(LocatorType.XPATH,LocatorPath.Addicon);
				WebElement addbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Addicon));
				addbutton.click();
				Thread.sleep(2000);
				
				fleetManagerWait(LocatorPath.RetailerIcon);
				WebElement Retailerbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.RetailerIcon));
				Retailerbutton.click();
				Thread.sleep(5000);
				((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
				Thread.sleep(2000);

				List<MobileElement> imagesList = fleetManagerDriver.findElements(By.xpath("//android.webkit.WebView[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='1']//android.view.View[@index='0']/*/*"));
				System.out.println("ViewBill::::"+imagesList.size());  
				
				if(imagesList.size()==5)
				{
					System.out.println("FM::"+imagesList.size()+":::Images Uploaded::Passed");
					writeReport(LogStatus.PASS, "FM::"+imagesList.size()+":::Images Uploaded::Passed");
				}else
				{
					System.out.println("FM:::Count of Images mismatches"+imagesList.size()+":::Failed");
					writeReport(LogStatus.FAIL, "FM:::Count of Images mismatches"+imagesList.size()+":::Failed");
				}
				switchToFMWebView();
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(4000);
				fleetManagerWait(LocatorPath.payCashButtonPath);
				Utils.checkBillWithGST();
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				writeReport(LogStatus.PASS, "FM::Clicked Pay cash::Passed");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				FM_TakeClick();
				writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash:::Failed");
			isVerified = false;
		}

	}

	public void viewBillRetailerInvoiceNoImageFound(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in viewBillRetailerInvoiceNoImageFound" + isVerified);
			try {
				Thread.sleep(3000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View Bill method ::");
				writeReport(LogStatus.PASS, "FM::Entered View bill method::Passed");
				Thread.sleep(5000);
				
				fleetManagerWait(LocatorPath.Addicon);
				// clickFMElement(LocatorType.XPATH,LocatorPath.Addicon);
				WebElement addbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Addicon));
				addbutton.click();
				Thread.sleep(1000);
				
				fleetManagerWait(LocatorPath.RetailerIcon);
				WebElement Retailerbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.RetailerIcon));
				Retailerbutton.click();
				
				Thread.sleep(3000);
				String NoAlertText=fleetManagerDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
				
				System.out.println("NoAlertText::::"+NoAlertText);
				writeReport(LogStatus.PASS, "FM::NoAlertText Captured::::"+NoAlertText+"::::Passed");
				
				FM_TakeClick();
				//fleetManagerDriver.findElement(By.xpath("//*[contains(text(),'OK')]")).click();
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.payCashButtonPath);
				Utils.checkBillWithGST();
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				writeReport(LogStatus.PASS, "FM::Clicked View bill -Pay cash::Passed");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::View bill & Pay Cash::::Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::View bill & Pay Cash::::Failed");
			isVerified = false;
		}
	}
	
	
	public void viewBillPayOnline(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in viewBillPayOnline" + isVerified);
			try {
				Thread.sleep(3000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View Bill method ::");
				Thread.sleep(3000);

				// Utils.checkBillWithOutGST();
				// Utils.checkBillWithIGST();
				// Utils.checkBillWithGST();
				fleetManagerWait(LocatorPath.payOnlinePath);
				clickFMElement(LocatorType.XPATH, LocatorPath.payOnlinePath);
				Thread.sleep(8000);
				String NetBanking = "//*[@id=\"OPTNBK\"]/span[2]";
				clickFMElement(LocatorType.XPATH, NetBanking);
				Thread.sleep(1000);
				String SelectBank = "//*[@id=\"netBankingBank\"]";
				clickFMElement(LocatorType.XPATH, SelectBank);
				Thread.sleep(1000);
				// shud include code for Avenues Test:
				String MakePayment = "//*[@id=\"SubmitBillShip\"]";
				clickFMElement(LocatorType.XPATH, MakePayment);

				writeReport(LogStatus.PASS, "FM::View bill & Pay Online Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::ViewBillPayOnline Failed:::");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::ViewBillPayOnline:::Failed");
			isVerified = false;
		}

	}

	@SuppressWarnings("unchecked")
	public void viewBillDownloadPDF(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in viewBillDesktopVersion" + isVerified);
			try {
				Thread.sleep(2000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View Bill method ::");
				Thread.sleep(5000);

				fleetManagerWait(LocatorPath.Addicon);
				// clickFMElement(LocatorType.XPATH,LocatorPath.Addicon);
				WebElement addbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Addicon));
				addbutton.click();

				// ----------------------------------------------------------------------
				// Download Link:

				Utils.checkBillWithIGST();
				fleetManagerWait(LocatorPath.Downloadicon);
				// clickFMElement(LocatorType.XPATH,LocatorPath.Downloadicon);
				WebElement Downloadbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Downloadicon));
				Downloadbutton.click();
				Thread.sleep(2000);
				((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
				Thread.sleep(2000);

				/*
				 * String pdfDetails1=fleetManagerDriver.findElement(By.xpath(
				 * "//android.widget.FrameLayout[@index='0']/android.view.View[@index='0']/android.view.View[@index='0']"
				 * )).getAttribute("contentDescription"); System.out.println("pdf details1:::"
				 * +pdfDetails1);
				 */

				// String fileWithPath="C:\\Users\\htl_mohana\\Downloads\\Snapshots";
				TakesScreenshot scrShot = ((TakesScreenshot) fleetManagerDriver);
				File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
				Thread.sleep(2000);

				// Move image file to new destination
				// File DestFile=new File(fileWithPath);

				// Copy file at destination

				// FileUtils.copyFile(SrcFile, DestFile);
				DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh");
				String DestFile = System.getProperty("user.dir") + "_Invoice PDF_" + df1.format(new Date()) + ".png";
				Thread.sleep(2000);
				System.out.println("Date Format::::" + df1.format(new Date()));
				FileUtils.copyFile(SrcFile, new File(DestFile));
				Thread.sleep(2000);
				fleetManagerDriver.findElement(By.xpath("//android.widget.ImageButton")).click();
				Thread.sleep(1000);
				writeReport(LogStatus.PASS, "FM :Invoice Snapshot::: " + eLogger.addScreenCapture(DestFile));
				switchToFMWebView();
				Thread.sleep(1000);
				/*
				 * String pdfDetails2=fleetManagerDriver.findElement(By.xpath(
				 * "//android.widget.FrameLayout[@index='0']/android.view.View[@index='0']/android.view.View[@index='1']"
				 * )).getAttribute("contentDescription"); System.out.println("pdf details2:::"
				 * +pdfDetails2);
				 * 
				 * String pdfDetails=fleetManagerDriver System.out.println("pdf details:::"
				 * +pdfDetails);
				 * 
				 * 
				 * for(int i=0;i<=pdfDetails.size();i++) { String
				 * pdfinfo=pdfDetails.get(i).getText(); System.out.println(pdfinfo); }
				 */
				writeReport(LogStatus.PASS, "FM::ViewBillDownloadPDF::Passed");
				fleetManagerWait(LocatorPath.payCashButtonPath);
				Utils.checkBillWithGST();
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				writeReport(LogStatus.PASS, "FM::Clicked Pay Cash::Passed");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;

			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::ViewBillDownloadPDF Failed:::");
				writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::ViewBillDownloadPDF:::Failed");
			writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
			isVerified = false;
		}

	}

	public void viewBillSendMail(ExcelInputData excelInputData,String mRequestOrderId1,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in viewBillDesktopVersion"+isVerified);
			try {
				Thread.sleep(2000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
				System.out.println("Entered View Bill method ::");
				Thread.sleep(5000);
				
				
				fleetManagerWait(LocatorPath.Addicon);
				WebElement addbutton=fleetManagerDriver.findElement(By.xpath(LocatorPath.Addicon));
				addbutton.click();
				
				//GMail Link:
				
				Utils.checkBillWithIGST();
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, LocatorPath.Mailicon);
				Thread.sleep(2000);
				
				((AppiumDriver<WebElement>)fleetManagerDriver).context("NATIVE_APP");
				Thread.sleep(3000);
				List<MobileElement> elements =fleetManagerDriver.findElements(By.xpath("//android.widget.TextView[@text='Gmail']"));
				System.out.println(elements.size());
				elements.get(0).click();
				
				Thread.sleep(5000);
				//fleetManagerWait("com.google.android.gm:id/to");
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/to")).click();
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/to")).sendKeys("mohanadeepa.chandran@hindujatech.com");
				Thread.sleep(2000);
				
				fleetManagerDriver.findElement(By.xpath("//android.widget.EditText[@text='Service Mandi Invoice']")).clear();
				Thread.sleep(1000);
				fleetManagerDriver.findElement(By.xpath("//android.widget.EditText[@text='Subject']")).sendKeys("Invoice No ::"+mRequestOrderId1);
				Thread.sleep(1000);
				/*String Invoicetext=fleetManagerDriver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/attachment_tile_title')]")).getText();
				System.out.println("Invoicetext::"+Invoicetext);*/
				
				
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/send")).click();
				Thread.sleep(3000);
				switchToFMWebView();
					writeReport(LogStatus.PASS, "FM::ViewBillDownloadPDF::Passed");
					fleetManagerWait(LocatorPath.payCashButtonPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
					writeReport(LogStatus.PASS, "FM::Clicked Pay Cash::Passed");
					fleetManagerWait(LocatorPath.liveOrderTab);
					
					//Include Desktop Webversion code
					isVerified=true;
				               
				} catch (Exception e) {
					e.printStackTrace();
					writeReport(LogStatus.FAIL, "FM::ViewBillSendMail Failed:::");
					writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
					isVerified=false;
				}}
				else{
					writeReport(LogStatus.FAIL, "FM::ViewBillSendMail:::Failed");
					writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
					isVerified=false;
					}

			}

	public void viewBillForwardMail(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in viewBillForwardMail" + isVerified);
			try {
				Thread.sleep(2000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				Thread.sleep(5000);
				System.out.println("Entered View Bill method ::");
				Thread.sleep(5000);

				fleetManagerWait(LocatorPath.Addicon);
				WebElement addbutton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Addicon));
				addbutton.click();

				// Forward Link:

				// Utils.checkBillWithIGST();
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.Forwardicon);
				WebElement FwdButton = fleetManagerDriver.findElement(By.xpath(LocatorPath.Forwardicon));
				FwdButton.click();
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.EnterName);
				WebElement EnterName = fleetManagerDriver.findElement(By.xpath(LocatorPath.EnterName));
				EnterName.click();
				EnterName.sendKeys("deepa");
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.EnterMobileNo);
				WebElement EnterMobileNo = fleetManagerDriver.findElement(By.xpath(LocatorPath.EnterMobileNo));
				EnterMobileNo.click();
				EnterMobileNo.sendKeys("9955664488");
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.EnterEmailID);
				WebElement EnterEmailID = fleetManagerDriver.findElement(By.xpath(LocatorPath.EnterEmailID));
				EnterEmailID.click();
				EnterEmailID.sendKeys("deepa.rajaraam@gmail.com");
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.OkBtnpath);
				WebElement OkBtn = fleetManagerDriver.findElement(By.xpath(LocatorPath.OkBtnpath));
				OkBtn.click();
				Thread.sleep(3000);
				String PaymtMsg = "//h3[@class='alert-sub-title']";
				fleetManagerWait(PaymtMsg);
				WebElement PaymtMsgAlert = fleetManagerDriver.findElement(By.xpath(PaymtMsg));
				String Message = PaymtMsgAlert.getText();
				System.out.println("Message::::" + Message);
				writeReport(LogStatus.PASS, "Payment Message::" + Message);
				Thread.sleep(500);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				// Gmail Link:

				Thread.sleep(2000);
				((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
				Thread.sleep(3000);
				List<MobileElement> elements = fleetManagerDriver
						.findElements(By.xpath("//android.widget.TextView[@text='Gmail']"));
				System.out.println(elements.size());
				elements.get(0).click();

				Thread.sleep(5000);
				// fleetManagerWait("com.google.android.gm:id/to");
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/to")).click();
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/to"))
						.sendKeys("mohanadeepa.chandran@hindujatech.com");
				Thread.sleep(2000);
				fleetManagerDriver.findElement(By.id("com.google.android.gm:id/send")).click();
				Thread.sleep(3000);
				switchToFMWebView();
				writeReport(LogStatus.PASS, "FM::ViewBillDownloadPDF::Passed");
				fleetManagerWait(LocatorPath.payCashButtonPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				writeReport(LogStatus.PASS, "FM::Clicked Pay Cash::Passed");
				fleetManagerWait(LocatorPath.liveOrderTab);

				isVerified = true;

			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::ViewBillForwardMail Failed:::");
				writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::ViewBillForwardMail:::Failed");
			writeReport(LogStatus.FAIL, "FM: ViewBill & PayCash::: Failed");
			isVerified = false;
		}
	}

	// Pay Online:
	/*
	 * fleetManagerWait(LocatorPath.payOnlinePath);
	 * clickFMElement(LocatorType.XPATH, LocatorPath.payOnlinePath);
	 * Thread.sleep(8000); String NetBanking="//*[@id=\"OPTNBK\"]/span[2]";
	 * clickFMElement(LocatorType.XPATH, NetBanking); Thread.sleep(1000); String
	 * SelectBank="//*[@id=\"netBankingBank\"]"; clickFMElement(LocatorType.XPATH,
	 * SelectBank); Thread.sleep(1000); //shud include code for Avenues Test: String
	 * MakePayment="//*[@id=\"SubmitBillShip\"]"; clickFMElement(LocatorType.XPATH,
	 * MakePayment);
	 */

	/*
	 * writeReport(LogStatus.PASS, "FM::Entered View bill::Passed");
	 * isVerified=true; } catch(Exception e) { e.printStackTrace();
	 * writeReport(LogStatus.FAIL, "FM::ViewBillPayOnline Failed:::");
	 * isVerified=false; }} else { writeReport(LogStatus.FAIL,
	 * "FM::ViewBillPayOnline:::Failed"); isVerified=false; }
	 */
	// }

	public void viewRevisedBill(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("viewRevisedBill try" + isVerified);
			try {
				Thread.sleep(1000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered View bill method ::");
				Thread.sleep(2000);
				
				/*fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ion-alert/div/div[3]/button/span")).click();
				writeReport(LogStatus.PASS, "FM::Clicked View Detailed Bill::Passed");*/
				fleetManagerWait(LocatorPath.payCashButtonPath);
				Utils.checkBillWithGST();
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				writeReport(LogStatus.PASS, "FM:: View bill & Pay cash");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::ViewRevisedBill method Failed:::");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::ViewRevisedBill method failed");
			isVerified = false;
		}
	}

	public void driverViewRevisedBill(ExcelInputData excelInputData, Driver driver, boolean isVerified)
			throws InterruptedException {

		if (isVerified) {
			System.out.println("viewRevisedBill try" + isVerified);
			try {
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered view bill method ::");
				Thread.sleep(2000);
				// try {
				// fleetManagerWait(LocatorPath.detailedBillPath);
				// clickFMElement(LocatorType.XPATH, LocatorPath.detailedBillPath);
				// }catch(Exception e) {
				// }
				// Thread.sleep(1000);
				fleetManagerWait(LocatorPath.payCashButtonPath);
				// Utils.checkBillWithOutGST(); //Utils.checkBillWithIGST();
				// Utils.checkBillWithGST();
				clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
				// driver.pushNotification();
				writeReport(LogStatus.PASS, "FM View bill & Pay cash");
				fleetManagerWait(LocatorPath.liveOrderTab);
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::driverViewRevisedBill method Failed:::");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "FM::driverViewRevisedBill method Failed");
			isVerified = false;
		}
	}

	public void fleetManagerRating(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

		
			System.out.println("in fleetManagerRating");

			Thread.sleep(3000);
			selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				
				System.out.println("Entered FM rating method ::");
				// fleetManagerWait(LocatorPath.fleetManagerRatingPath);
				// clickFMElement(LocatorType.XPATH,
				// LocatorPath.fleetManagerRatingPath);
				Thread.sleep(4000);

				String fleetManagerRatingPath1 = "//html/body/ion-app/ng-component/ion-nav/page-rating/ion-content/div[2]/div/div/div[2]/div/ion-icon"
						+ "[" + excelInputData.getFmRating() + "]";
				System.out.println("excelInputData.getRating::::" + excelInputData.getFmRating());
				Thread.sleep(2000);
				fleetManagerWait(fleetManagerRatingPath1);
				fleetManagerDriver.findElement(By.xpath(fleetManagerRatingPath1)).click();
				Thread.sleep(4000);
				String fleetManagerFeedBackPath1 = "//html/body/ion-app/ion-alert/div/div[3]/div/button["
						+ excelInputData.getFmReason() + "]/span/div[2]";
				fleetManagerWait(fleetManagerFeedBackPath1);
				fleetManagerDriver.findElement(By.xpath(fleetManagerFeedBackPath1)).click();

				// fleetManagerWait(LocatorPath.fleetManagerFeedBackPath);
				// clickFMElement(LocatorType.XPATH,
				// LocatorPath.fleetManagerFeedBackPath);
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.fleetManagerRatingSubmit);
				clickFMElement(LocatorType.XPATH, LocatorPath.fleetManagerRatingSubmit);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
	
	}

	public void fleetManagerConfirmGarage(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("isVerified in fleetManagerConfirmWorkshop");
			 
				Thread.sleep(4000);
				selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
				System.out.println("Entered FM Confirm Workshop method ::");
				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.confirmGarageButtonPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.confirmGarageButtonPath);
				writeReport(LogStatus.PASS, "FM::Confirm Workshop ::Passed");
				// Thread.sleep(2000);
				// fleetManagerWait(LocatorPath.TrackLocationPath);
				// clickFMElement(LocatorType.XPATH, LocatorPath.TrackLocationPath);
				// writeReport(LogStatus.PASS, "FM checked track location
				// ");
				Thread.sleep(4000);
				String trackLocationPage="//html/body/ion-app/ng-component/ion-nav/page-confirm-garage/ion-footer/div/button/span";
				fleetManagerWait(trackLocationPage);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(2000);
			
	}

	public void cancelOrder(ExcelInputData excelInputData, String locator, boolean isVerified) throws InterruptedException, IOException {
	
			System.out.println("in cancelOrder ");
			
				Thread.sleep(3000);
				System.out.println("Inside Cancel Order");
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(3000);
			
				//selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
									
				System.out.println("FM::Live Order Tab clicked ::");

				String fmSearch = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[1]/ion-searchbar/div/input";
				clickFMElement(LocatorType.XPATH, fmSearch);
				enterFMValue(LocatorType.XPATH, fmSearch,excelInputData.getVehicleNumber());
				Thread.sleep(1000);
				//fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				//Thread.sleep(2000);
				hideKeyBoard(fleetManagerDriver);
				Thread.sleep(2000);
				List<WebElement> links1 = fleetManagerDriver.findElements(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card/*/*"));
				System.out.println(links1.size());

				for (int i = 8; i <= links1.size(); i++) {
					String FmStateText = links1.get(i).getText();
					System.out.println(+i+"value" +FmStateText);
					if ((i == 9) && (FmStateText.equalsIgnoreCase("TRACK LOCATION"))) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
											
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						Thread.sleep(1000);
						fleetManagerWait(LocatorPath.cancelOrderPath);
						clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
						Thread.sleep(2000);
						String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
						fleetManagerWait(yesBtn);
						clickFMElement(LocatorType.XPATH, yesBtn);
						Thread.sleep(2000);
						String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
						clickFMElement(LocatorType.XPATH, fmReasonForCancel);
						Thread.sleep(1000);
						fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
						Thread.sleep(3000);
						writeReport(LogStatus.PASS, "FM: Job is Cancelled at TRACK LOCATION");
						break;
					}
					if ((i == 9) && (FmStateText.isEmpty())) {
						String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
						System.out.println("The live Order path :::" + liveOrderPath);
						Thread.sleep(2000);
											
						writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
						Thread.sleep(1000);
						fleetManagerWait(LocatorPath.cancelOrderPath);
						clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
						Thread.sleep(2000);
						String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
						fleetManagerWait(yesBtn);
						clickFMElement(LocatorType.XPATH, yesBtn);
						Thread.sleep(2000);
						String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
						clickFMElement(LocatorType.XPATH, fmReasonForCancel);
						Thread.sleep(1000);
						fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
						Thread.sleep(3000);
						writeReport(LogStatus.PASS, "FM: Job is Cancelled");
						break;
					}
				if ((i == 9) && (FmStateText.equalsIgnoreCase("CONFIRM WORKSHOP"))) {
					String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
					System.out.println("The live Order path :::" + liveOrderPath);
					Thread.sleep(2000);
										
					writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
					Thread.sleep(1000);
					fleetManagerWait(LocatorPath.cancelOrderPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
					Thread.sleep(2000);
					String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
					fleetManagerWait(yesBtn);
					clickFMElement(LocatorType.XPATH, yesBtn);
					Thread.sleep(2000);
					String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
					clickFMElement(LocatorType.XPATH, fmReasonForCancel);
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(3000);
					writeReport(LogStatus.PASS, "FM: Job is Cancelled at CONFIRM WORKSHOP");
					break;
				}
				if ((i == 9) && (FmStateText.equalsIgnoreCase("APPROVE ESTIMATE"))) {
					String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
					System.out.println("The live Order path :::" + liveOrderPath);
					Thread.sleep(2000);
										
					writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
					Thread.sleep(1000);
					fleetManagerWait(LocatorPath.cancelOrderPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
					Thread.sleep(2000);
					String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
					fleetManagerWait(yesBtn);
					clickFMElement(LocatorType.XPATH, yesBtn);
					Thread.sleep(2000);
					String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
					clickFMElement(LocatorType.XPATH, fmReasonForCancel);
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(3000);
					writeReport(LogStatus.PASS, "FM: Job is Cancelled at APPROVE ESTIMATE");
					break;
				}
				if ((i == 8) && (FmStateText.equalsIgnoreCase("FINDING WORKSHOPS"))) {
					String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
					System.out.println("The live Order path :::" + liveOrderPath);
					Thread.sleep(2000);
										
					writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
					Thread.sleep(1000);
					fleetManagerWait(LocatorPath.cancelOrderPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
					Thread.sleep(2000);
					String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
					fleetManagerWait(yesBtn);
					clickFMElement(LocatorType.XPATH, yesBtn);
					Thread.sleep(2000);
					String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
					clickFMElement(LocatorType.XPATH, fmReasonForCancel);
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(3000);
					writeReport(LogStatus.PASS, "FM: Job is Cancelled at FINDING GARAGE");
					break;
				}
				if ((i == 8) &&  (FmStateText.isEmpty())){
					String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
					System.out.println("The live Order path :::" + liveOrderPath);
					Thread.sleep(2000);
										
					writeReport(LogStatus.PASS, "FM::Order State::" + links1.get(i).getText());
					Thread.sleep(1000);
					fleetManagerWait(LocatorPath.cancelOrderPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
					Thread.sleep(2000);
					String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
					fleetManagerWait(yesBtn);
					clickFMElement(LocatorType.XPATH, yesBtn);
					Thread.sleep(2000);
					String fmReasonForCancel = "//html/body/ion-app/ion-alert/div/div[3]/div/button["+ excelInputData.getCancelReason() + "]";
					clickFMElement(LocatorType.XPATH, fmReasonForCancel);
					Thread.sleep(1000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(3000);
					writeReport(LogStatus.PASS, "FM: Job is Cancelled");
					break;
				}
				}		
	}

	public void fmLoginWithIncorrectMobileNo(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException {
		/*if (isVerified) {
			System.out.println("isVerified in fleetManagerConfirmWorkshop" + isVerified);*/
		try {

			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();

			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			fleetManagerWait(LocatorPath.mobilePath);

			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			
			LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData.readingFleetManagerData("NegativeWorkFlow");
			List<String> MobileNumberList = userEditDetails.get("MobileNumber");
			List<String> InvalidMobileNoList = userEditDetails.get("InvalidMobileNo");
			
			String MobileNumber=userEditDetails.get("MobileNumber").get(1);
			String InvalidMobileNo = userEditDetails.get("InvalidMobileNo").get(1);
			
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, MobileNumber);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);

			Thread.sleep(2000);
			String mobileErrMsg = "/html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/p";
			String mobileMsg = getFMWebElement(LocatorType.XPATH, mobileErrMsg).getText();

			try {
				if (mobileMsg.contentEquals("Please enter a valid mobile number")) {
					writeReport(LogStatus.FAIL,
							"FM ::::Invalid Mobile Number entered::: " + InvalidMobileNo);
					Thread.sleep(2000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
					fleetManagerWait(LocatorPath.signUpPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
					fleetManagerWait(LocatorPath.mobilePath);
					clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
					enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, MobileNumber);
					writeReport(LogStatus.PASS,
							"FM ::::Valid Mobile Number entered::: " + MobileNumber);
					
				}
			 }catch (Exception e) {
				e.printStackTrace();
				
			}
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
			Thread.sleep(2000);
			// logger.info("**************FM: Login
			// Passed**********************");
			writeReport(LogStatus.PASS, "FM: Login Passed");
			//isVerified = true;
		} catch (Exception e) {
			e.printStackTrace();
			//isVerified = false;
		}/*}
		else {
			writeReport(LogStatus.FAIL, "FM: Login Failed");
			//isVerified = false;
		}*/
	}

	public void negativefmLogin(ExcelInputData excelInputData, boolean isVerified) {

		try {

			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();

			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			fleetManagerWait(LocatorPath.mobilePath);

			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getMobileNumber());
			writeReport(LogStatus.PASS, "FM ::::Valid Mobile Number entered::: " + excelInputData.getMobileNumber());

			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM: Login Passed");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public String FMcheckMyPreviousOrder(ExcelInputData excelInputData,String mechactualvalue, String mRequestOrderId1, boolean isVerified)
			throws InterruptedException, IOException {
		String fmactualvalue="0.0";
		System.out.println("mRequestOrderId1::::::::::::" + mRequestOrderId1);

		
			System.out.println("in FMcheckMyPreviousOrder");

			
				Thread.sleep(2000);

				String MyAccLink = "//*[contains(text(),'MY')]";
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, MyAccLink);
				Thread.sleep(500);

				String MyPrevOrdr = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/ion-list/ion-item[4]/div[1]/div/ion-label";
				clickFMElement(LocatorType.XPATH, MyPrevOrdr);
				Utils.sleepTimeLow();

				//String MyPrevSearch = "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/div[2]/ion-searchbar/div/input";
				String MyPrevSearch = "//input[@class='searchbar-input']";
				clickFMElement(LocatorType.XPATH, MyPrevSearch);
				fleetManagerDriver.findElement(By.xpath(MyPrevSearch)).sendKeys(excelInputData.getVehicleNumber());
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(5000);
				String InnerStrVal = "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/div[2]/div/ul/li";
				clickFMElement(LocatorType.XPATH, InnerStrVal);
				Thread.sleep(5000);

				List<WebElement> prevOrderList = fleetManagerDriver.findElements(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/ion-list/ion-card[1]/*"));
				// "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/ion-list/ion-card[1]";
				System.out.println("prevOrderList.size" + prevOrderList.size());
				/*for (int i = 0; i < prevOrderList.size(); i++) {
					String p = prevOrderList.get(i).getText();
					System.out.println("prevOrder nAME:::" + p + "::::i value:::" + i);
					String OrderStatus = "completed";

					if (p.contains(mRequestOrderId1)) {
						System.out.println("FM::::Order Id matches" + mRequestOrderId1);
					}
					if ((i == 1) && (p.contains(OrderStatus))) {
						System.out.println("FM::Order Status matches:::::" + p);
						writeReport(LogStatus.PASS, "FM::Order ID matches:::::");
						writeReport(LogStatus.PASS, "FM::Order Status matches:::::");
						Thread.sleep(1000);
						writeReport(LogStatus.PASS, "FM::My Previous Order Details:::" + p);
						System.out.println("FM::My Previous Order Details:::" + p);
						break;
					}
				}*/
				
				for (int i = 0; i < prevOrderList.size(); i++) {
					String p=prevOrderList.get(i).getText();														
					System.out.println("prevOrder nAME:::" + p +"::::i value:::" +i);
					String OrderStatus = "completed";
					System.out.println("Driver Number ::"+excelInputData.getDriverNumber());
					LinkedHashMap<String, List<String>> vehicleDetailsList = ReadingExcelData.readingFleetManagerData("FMUserData");
					String Make = vehicleDetailsList.get("Select Make").get(1);
					String Category = vehicleDetailsList.get("Category").get(1);
					
					if((i==0) && (p.contains(mRequestOrderId1)) && (p.contains(Make)) && (p.contains(Category)) && (p.contains(excelInputData.getVehicleNumber())) && (p.contains(OrderStatus)) && (p.contains(excelInputData.getDriverName())))
					{	
						System.out.println("Order Id matches:::" +p);
						writeReport(LogStatus.PASS,"FM::Order Id, MAKE & CATEGORY,Vehicle Number,Order status and DriverName matches:::" +p);
					}
				
				    
				    if((i==1) && (p.contains(excelInputData.getDriverName())))
				    	{ 
				    	System.out.println("Driver Number ::"+excelInputData.getDriverName());
				    	System.out.println("Driver Number matches:::"+p);
				    	System.out.println("FM::TOTAL BILL AMOUNT:::"+p);
						
						String[] value1 = p.split("₹");
						fmactualvalue = value1[1].trim();
						System.out.println("fmactualvalue:::::"+fmactualvalue);
						System.out.println("mechactualvalue:::::"+mechactualvalue);
						if(fmactualvalue.equals(mechactualvalue))
						{
							System.out.println("TOTAL BILL AMOUNT matches in FM and TECHNICIAN:::"+fmactualvalue);
							writeReport(LogStatus.PASS,"FM::Driver Number matches:::"+p);
							writeReport(LogStatus.PASS,"FM:::TOTAL BILL AMOUNT matches in FM and TECHNICIAN:::"+fmactualvalue);
							
						}
					break;	
					}
				 }
				return fmactualvalue;
			}

	public void FMLogout(ExcelInputData excelInputData) throws InterruptedException
	{
	  Thread.sleep(1000); 		
	  String BackButtonPath="//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-header/ion-navbar/button";
	  clickFMElement(LocatorType.XPATH, BackButtonPath);
	  Thread.sleep(2000);
	  
	  String MyAccLink = "//*[contains(text(),'MY')]";
	  fleetManagerWait(MyAccLink);
	  clickFMElement(LocatorType.XPATH, MyAccLink);
	  Thread.sleep(2000);
	  String LogoutPath="//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/ion-list/ion-item[9]/div[1]";
	  clickFMElement(LocatorType.XPATH, LogoutPath);
	  String YesBtn="//html/body/ion-app/ion-alert/div/div[3]/button[1]";
	  Thread.sleep(3000);
	  clickFMElement(LocatorType.XPATH, YesBtn);
	  Thread.sleep(4000);
	  System.out.println("FM:: System logged out");
	  writeReport(LogStatus.PASS, "FM::System logged out:::" );
	  
	}
	public void FMLogoutfrmMyAccLink(ExcelInputData excelInputData) throws InterruptedException
	{
	  
		Thread.sleep(2000);
		  
		  String MyAccLink = "//*[contains(text(),'MY')]";
		  fleetManagerWait(MyAccLink);
		  clickFMElement(LocatorType.XPATH, MyAccLink);
		  Thread.sleep(2000);
		  String LogoutPath="//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/ion-list/ion-item[8]/div[1]";
		  clickFMElement(LocatorType.XPATH, LogoutPath);
		  String YesBtn="//html/body/ion-app/ion-alert/div/div[3]/button[1]";
		  Thread.sleep(3000);
		  clickFMElement(LocatorType.XPATH, YesBtn);
		  Thread.sleep(4000);
		  System.out.println("FM:: System logged out");
		  writeReport(LogStatus.PASS, "FM::System logged out:::" );
	}
	
	public void FMcheckMyPrevOrderCheckImage(ExcelInputData excelInputData, String mRequestOrderId1, boolean isVerified)
			throws InterruptedException, IOException {
		System.out.println("mRequestOrderId1::::::::::::" + mRequestOrderId1);

		if (isVerified) {
			System.out.println("isVerified in FMcheckMyPreviousOrder try" + isVerified);
			
			
			try {
				Thread.sleep(2000);

				String MyAccLink = "//*[contains(text(),'MY')]";
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, MyAccLink);
				Thread.sleep(500);

				String MyPrevOrdr = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/ion-list/ion-item[4]/div[1]/div/ion-label";
				clickFMElement(LocatorType.XPATH, MyPrevOrdr);
				Utils.sleepTimeLow();

				//String MyPrevSearch = "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/div[2]/ion-searchbar/div/input";
				String MyPrevSearch = "//input[@class='searchbar-input']";
				clickFMElement(LocatorType.XPATH, MyPrevSearch);
				fleetManagerDriver.findElement(By.xpath(MyPrevSearch)).sendKeys(excelInputData.getVehicleNumber());
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);
				String InnerStrVal = "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/div[2]/div/ul/li";
				clickFMElement(LocatorType.XPATH, InnerStrVal);
				Thread.sleep(5000);

				List<WebElement> prevOrderList = fleetManagerDriver.findElements(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/ion-list/ion-card[1]/*"));
				// "//html/body/ion-app/ng-component/ion-nav/page-previous-orders/ion-content/div[2]/ion-list/ion-card[1]";
				System.out.println("prevOrderList.size" + prevOrderList.size());
				for (int i = 0; i < prevOrderList.size(); i++) {
					String p = prevOrderList.get(i).getText();
					System.out.println("prevOrder nAME:::" + p + "::::i value:::" + i);
					String OrderStatus = "Completed";

					if (p.contains(mRequestOrderId1)) {
						System.out.println("FM::::Order Id matches" + mRequestOrderId1);
					}
					if ((i == 1) && (p.contains(OrderStatus))) {
						System.out.println("FM::Order Status matches:::::" + p);
						writeReport(LogStatus.PASS, "FM::Order ID matches:::::");
						writeReport(LogStatus.PASS, "FM::Order Status matches:::::");
						Thread.sleep(1000);
						writeReport(LogStatus.PASS, "FM::My Previous Order Details:::" + p);
						System.out.println("FM::My Previous Order Details:::" + p);
						
						
						fleetManagerDriver.findElement(By.cssSelector("ion-col>ion-label[class*='viewDetail']")).click();
						System.out.println("Clicked FM:: View Bill");
						//Click Add icon:
						Thread.sleep(3000);
						clickFMElement(LocatorType.CSS,"body > ion-app > ng-component > ion-nav > page-bill-page > ion-content > div.fixed-content > ion-fab > button");
						System.out.println("Clicked FM:: Add icon");
						clickFMElement(LocatorType.CSS, LocatorPath.RetailerIcon);
						Thread.sleep(4000);
						
						((AppiumDriver<WebElement>) fleetManagerDriver).context("NATIVE_APP");
						Thread.sleep(2000);
						List<MobileElement> imagesList = fleetManagerDriver.findElements(By.xpath("//android.webkit.WebView[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='0']//android.view.View[@index='1']//android.view.View[@index='0']/*/*"));
						System.out.println("ViewBill::::"+imagesList.size());  
						FM_TakeClick();
						
						if(imagesList.size()==5)
						{
							System.out.println("FM::"+imagesList.size()+":::Image Count matches::Passed");
							writeReport(LogStatus.PASS, "FM::"+imagesList.size()+":::Image Count matches::Passed");
						}else if(imagesList.size()==0)
						{
							System.out.println("FM::::NO IMAGES FOUND::Passed");
							writeReport(LogStatus.PASS, "FM:::NO IMAGES FOUND::Passed");
						}
						
						fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
						Thread.sleep(3000);
						break;
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
				FM_TakeClick();
				writeReport(LogStatus.FAIL, "FM::My Previous Order Details:::Failed");
			}
		}
	}
	
	}




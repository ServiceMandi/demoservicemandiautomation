package com.servicemandi.common;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class Driver extends Configuration {

	private ProfileCreation ProfileCreation;
	
	
	public Driver() {

	}

	public void login(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {

		if(isVerified) {
			System.out.println("isVerified in Driver login"+isVerified);
			try {
     		driverCapabilities();
			switchToDriverWebView();
			Utils.sleepTimeLow();
			Thread.sleep(3000);
			
			System.out.println("Entered the Driver Manager Login method ::: ");
			
			String driverAppVersion_Path="//div[@class='version-class']";
			driverWait(driverAppVersion_Path);
			WebElement dVersion=driveDriver.findElement(By.xpath(driverAppVersion_Path));
			String DriverAppVersion=dVersion.getText();
			System.out.println("Driver App Version ::"+DriverAppVersion);
			
			Thread.sleep(3000);
			driverWait(LocatorPath.languagePath);
			//String driverlanguagePath="/html/body/ion-app/ng-component/ion-nav/page-language-sel/ion-content/div[2]/div[2]/div[2]/div[1]";
			clickDriverElement(LocatorType.XPATH,LocatorPath.languagePath);
			Thread.sleep(2000);
			String mobilePath = "//html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input/div";
			String MB="//html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div[2]/form/ion-item/div[1]/div/ion-input/input[1]";
			driverWait(LocatorPath.mobilePath);
			
			clickDriverElement(LocatorType.XPATH, mobilePath);
			Thread.sleep(2000);
			System.out.println("Excel Mobile number:" + excelInputData.getDriverMobileNo());
			enterDriverValue(LocatorType.XPATH, MB, excelInputData.getDriverMobileNo());
			
			
			//driveDriver.findElement(By.xpath(MB)).sendKeys(excelInputData.getDriverMobileNo());
			hideKeyBoard(driveDriver);
			//enterDriverValue(LocatorType.XPATH, LocatorPath.requestOTP,"9940209485");

			Thread.sleep(1000);

			clickDriverElement(LocatorType.XPATH, LocatorPath.requestOTP);
			Thread.sleep(15000);
			String Register = "/html/body/ion-app/ng-component/ion-nav/page-registration/ion-content/div[2]/div/form/button/span";
			driverWait(Register);
			clickDriverElement(Configuration.LocatorType.XPATH, Register);
			writeReport(LogStatus.PASS, "Driver App Version:::::::"+DriverAppVersion);
			writeReport(LogStatus.PASS, "Driver Login Passed");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			Driver_TakeClick();
			writeReport(LogStatus.FAIL, "Driver Login Failed ::::");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "Driver Login Failed :::");
			isVerified=false;
			}
	}

	public void pushNotification(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in pushNotification try"+isVerified);
		try {
			System.out.println("Inside Push Notification");
			Thread.sleep(4000);
			//driverWait(LocatorPath.AlertBox);
			//for(int i1=0;i1<=4;i1++) {
			//String pushNotificationText = driveDriver.findElement(By.xpath("//*[@id=\"alert-subhdr-"+i1+"\"]")).getText();
			String pushNotificationTextMsg="//h3[@class='alert-sub-title']";
			driverWait(pushNotificationTextMsg);
			String pushNotificationText=driveDriver.findElement(By.xpath(pushNotificationTextMsg)).getText();
			System.out.println("PushNotificationText ::::::" + pushNotificationText);

			// Verification:
			//System.out.println("excelInputData.getDriverName()::::" + excelInputData.getDriverName());
		
				System.out.println("Substring of PushNotificationText (starting from 5th index) has DriverName prefix: "
								+ pushNotificationText.startsWith(excelInputData.getDriverName(), 5));
				Thread.sleep(3000);
				driveDriver.pressKeyCode(AndroidKeyCode.ENTER);
				writeReport(LogStatus.PASS, "Driver App Push Notification::::" +pushNotificationText);
				
				//i1=i1+1;
				isVerified=true;
		}catch (Exception e) {
				e.printStackTrace();
				Driver_TakeClick();
				writeReport(LogStatus.FAIL, "Driver:::PushNotification method failed::::");
				isVerified=false;
			}}else {
				writeReport(LogStatus.FAIL, "Driver:::PushNotification method failed");
				isVerified=false;
				}
			}

	public void selectLiveOrderSearch(String locator) {

		try {
			Thread.sleep(1000);
			driverWait(LocatorPath.liveOrderTab);
			clickDriverElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			Utils.sleepTimeLow();

			// String liveOrderPath =
			// "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card[1]/ion-card-content/ion-label[1]";
			String liveOrderPath = "//*[contains(text(), '" + locator + "')]";
			System.out.println("The live Order path :::" + liveOrderPath);
			driverWait(liveOrderPath);
			WebElement liveOrderElement = getFMWebElement(LocatorType.XPATH, liveOrderPath);
			// liveOrderElement.click();

			if (liveOrderElement.getText().equalsIgnoreCase(locator)) {
				liveOrderElement.click();
				System.out.println("Clicked Driver Live order state ::" + liveOrderElement.getText());
				Thread.sleep(1000);
			}

		} catch (Exception e) {

		}
	}

	public void viewBill(ExcelInputData excelInputData) throws InterruptedException {

		Thread.sleep(2000);
		selectLiveOrderSearch(excelInputData.getVehicleNumber());
		System.out.println("Entered View Bill method ::");
		Thread.sleep(2000);
		driverWait(LocatorPath.payCashButtonPath);
		// Utils.checkBillWithOutGST();
		// Utils.checkBillWithIGST();
		Utils.checkBillWithGST();
		clickDriverElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
		writeReport(LogStatus.PASS, "Driver manger: view bill & Pay cash");
		driverWait(LocatorPath.liveOrderTab);

	}

	public void viewRevisedBill(ExcelInputData excelInputData) throws InterruptedException {

		selectLiveOrderSearch(excelInputData.getVehicleNumber());
		System.out.println("Entered view bill method ::");
		Thread.sleep(2000);
		driverWait(LocatorPath.detailedBillPath);
		clickDriverElement(LocatorType.XPATH, LocatorPath.detailedBillPath);
		Thread.sleep(1000);
		driverWait(LocatorPath.payCashButtonPath);
		// Utils.checkBillWithOutGST(); //Utils.checkBillWithIGST();
		// Utils.checkBillWithGST();
		clickDriverElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
		writeReport(LogStatus.PASS, "Driver manager: View bill & Pay cash");
		driverWait(LocatorPath.liveOrderTab);

	}

	public void driverRating(ExcelInputData excelInputData) throws InterruptedException {

		selectLiveOrderSearch(excelInputData.getVehicleNumber());
		System.out.println("Entered Driver manager: Rating method ::");
		driverWait(LocatorPath.fleetManagerRatingPath);
		clickDriverElement(LocatorType.XPATH, LocatorPath.fleetManagerRatingPath);
		Thread.sleep(2000);
		driverWait(LocatorPath.fleetManagerFeedBackPath);
		clickDriverElement(LocatorType.XPATH, LocatorPath.fleetManagerFeedBackPath);
		Thread.sleep(1000);
		driverWait(LocatorPath.fleetManagerRatingSubmit);
		clickDriverElement(LocatorType.XPATH, LocatorPath.fleetManagerRatingSubmit);
		writeReport(LogStatus.PASS, "Driver manger: Rating the order");
		driverWait(LocatorPath.liveOrderTab);

	}

	public void driverConfirmGarage(ExcelInputData excelInputData) throws InterruptedException {

		try {

			selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Driver manager - Confirm Garage method ::");
			Thread.sleep(2000);
			driverWait(LocatorPath.confirmGarageButtonPath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.confirmGarageButtonPath);
			writeReport(LogStatus.PASS, "Driver:: Confirm the garage  ");
			Thread.sleep(3000);
			driverWait(LocatorPath.TrackLocationPath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.TrackLocationPath);
			writeReport(LogStatus.PASS, "Driver manager checked track location ");
			Thread.sleep(2000);
			driveDriver.pressKeyCode(AndroidKeyCode.BACK);

		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "Driver Manager confirm Failed");
		}
	}

	public void cancelOrder() {

		try {

			Thread.sleep(1000);
			driverWait(LocatorPath.liveOrderTab);
			clickDriverElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			Utils.sleepTimeMedium();
			driverWait(LocatorPath.cancelOrderPath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
			Utils.sleepTimeLow();
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			driverWait(yesBtn);
			clickDriverElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			driveDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.PASS, "Driver Manager: Job is Cancelled");

		} catch (Exception e) {

		}
	}

	public void driverLoginWithIncorrectMobileNo(ExcelInputData excelInputData) {

		try {

			driverCapabilities();
			Utils.sleepTimeLow();
			switchToDriverWebView();
			Utils.sleepTimeLow();

			driverWait(LocatorPath.languagePath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.languagePath);
			driverWait(LocatorPath.signUpPath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.signUpPath);
			driverWait(LocatorPath.mobilePath);

			clickDriverElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			enterDriverValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getInvalidMobileNo());
			driveDriver.pressKeyCode(AndroidKeyCode.BACK);

			Thread.sleep(2000);
			String mobileErrMsg = "/html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/p";
			String mobileMsg = getFMWebElement(LocatorType.XPATH, mobileErrMsg).getText();

			try {
				if (mobileMsg.contentEquals("Please enter a valid mobile number")) {
					// logger.info("**************FM: INVALID MOBILE NUMBER is
					// ENTERED**********************");
					writeReport(LogStatus.FAIL, "DRIVER:: INVALID MOBILE NUMBER " + mobileMsg + "is ENTERED- Verified");
					Thread.sleep(2000);
					driveDriver.pressKeyCode(AndroidKeyCode.BACK);
					driverWait(LocatorPath.signUpPath);
					clickDriverElement(LocatorType.XPATH, LocatorPath.signUpPath);
					driverWait(LocatorPath.mobilePath);
					clickDriverElement(LocatorType.XPATH, LocatorPath.mobilePath);
					enterDriverValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getMobileNumber());

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			clickDriverElement(LocatorType.XPATH, LocatorPath.requestOTP);
			driverWait(LocatorPath.loginPath);
			clickDriverElement(LocatorType.XPATH, LocatorPath.loginPath);
			Thread.sleep(2000);
			// logger.info("**************FM: Login
			// Passed**********************");
			writeReport(LogStatus.PASS, "Driver: Login Passed");
		} catch (Exception e) {

		}
	}

	public void newDriverCreation(ExcelInputData excelInputData) {
		
		try {
		driverCapabilities(); 
		Utils.sleepTimeLow();
		switchToDriverWebView();
		Utils.sleepTimeLow();
		driverWait(LocatorPath.languagePath);
		clickDriverElement(LocatorType.XPATH, LocatorPath.languagePath);
		Thread.sleep(2000);
		String mobileNo = "//input[@placeholder='Enter your mobile number']";
		LinkedHashMap<String, List<String>> driverList = ReadingExcelData.readingFleetManagerData("FMUserData");
		String DriverMobile = driverList.get("ExistingDriverMobile").get(1);
		String DriverName = driverList.get("DriverName").get(1);
		
		System.out.println("DriverMobile::"+DriverMobile);
		driverWait(mobileNo);
		WebElement mobileElement = driveDriver.findElement(By.xpath(mobileNo));
		mobileElement.clear();
		mobileElement.sendKeys(DriverMobile);
		hideKeyBoard(driveDriver);
		Thread.sleep(1000);
		clickDriverElement(LocatorType.XPATH, LocatorPath.requestOTP);
		Thread.sleep(3000);
		String Register = "/html/body/ion-app/ng-component/ion-nav/page-registration/ion-content/div[2]/div/form/button/span";
		driverWait(Register);

		System.out.println("DriverName"+DriverName);
		System.out.println("DriverMobile"+DriverMobile);

		/*String Dname=getElementAttributeValue(driveDriver, LocatorPath.driverNamePath);
		String Dmobile=getElementAttributeValue(driveDriver, LocatorPath.driverNamePath);
		System.out.println(Dname);
		System.out.println(Dmobile);*/

		if (getElementAttributeValue(driveDriver, LocatorPath.driverNamePath).equalsIgnoreCase(DriverName))
			{writeReport(LogStatus.PASS, "DriverApp driver name matches");}
		else
			{writeReport(LogStatus.FAIL, "DriverApp driver does not match");}

		if(getElementAttributeValue(driveDriver, LocatorPath.driverMobilePath).equalsIgnoreCase(DriverMobile))
		{writeReport(LogStatus.PASS, "DriverApp Mobile Number matches");}
		else
		{writeReport(LogStatus.FAIL, "DriverApp Mobile Number does not match");}
		
		Thread.sleep(2000);
		clickDriverElement(Configuration.LocatorType.XPATH, Register);
		Thread.sleep(8000);
		/*fleetManagerWait(LocatorPath.fleetManagerFeedBackPath);
		clickFMElement(LocatorType.XPATH, LocatorPath.fleetManagerFeedBackPath);*/
		Thread.sleep(2000); 
		fleetManagerWait(LocatorPath.myAccountPath);
		clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
		Thread.sleep(1000);
		fleetManagerWait(LocatorPath.myAccountListPath);
		List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
		System.out.println(" AL Account list size :::" + myAccountList.size());

		for (int i = 0; i < myAccountList.size(); i++) {

		WebElement listElement = myAccountList.get(i);
		String listValue = listElement.getText();
		System.out.println(" AL inside loop text value ::: " + listValue);
		if (listValue.equalsIgnoreCase("My Drivers")) {
		listElement.click();
		Thread.sleep(2000);
		break;
		}
		}
		String driverName = driverList.get("DriverName").get(1);
		String searchTextField="//html/body/ion-app/ng-component/ion-nav/page-show-drivers/ion-content/div[2]/div/div[1]/ion-searchbar/div/input";
		fleetManagerWait(searchTextField);
		WebElement nameElement = fleetManagerDriver.findElement(By.xpath(searchTextField));
		nameElement.clear();
		Thread.sleep(1000);
		nameElement.sendKeys(driverName);
		hideKeyBoard(fleetManagerDriver);
		String activePath="//html/body/ion-app/ng-component/ion-nav/page-show-drivers/ion-content/div[2]/div/div[2]/ion-card/ion-card-content/p[1]";
		WebElement active=fleetManagerDriver.findElement(By.xpath(activePath));
		String activeText=active.getText();
		if(activeText.equalsIgnoreCase("Active")){
		writeReport(LogStatus.PASS, "FM:::Status changed from Inactive to Active");
		}else{
		writeReport(LogStatus.FAIL, "FM:::Status not Changed from Inactive to Active");
		}
		} catch (Exception e) {

		}
		}
}

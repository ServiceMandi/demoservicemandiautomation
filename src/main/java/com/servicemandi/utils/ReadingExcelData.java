package com.servicemandi.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class ReadingExcelData {

	private static Workbook workbook = null;

	public static Workbook getWorkBook() {

		try {
			if (workbook == null) {
				String newFilePath = System.getProperty("user.dir") + "\\Service_Mandi_Input_new.xls";
				System.out.println("New file path :::" + newFilePath);
				FileInputStream fs = new FileInputStream(newFilePath);
				workbook = Workbook.getWorkbook(fs);
			}

		} catch (Exception e) {

		}
		return workbook;
	}

	public static void readingWorkFlowData() throws IOException, BiffException {

		try {
			if (Utils.excelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("WorkFlowData");

				List<String> ExpectedColumns = new ArrayList<String>();
				System.out.println("::::::::::::::::::::::::::::");
				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 1);
					String d = celll.getContents();
				//	System.out.println("D values: "+d);
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}

				List<String> FMNumberList = columnDataValues.get("FMNumber");
				List<String> FMNameList=columnDataValues.get("FMName");
				List<String> vehicleNumberList = columnDataValues.get("VehicleNumber");
				List<String> driverNumberList = columnDataValues.get("DriverNumber");
				List<String> locationList = columnDataValues.get("Location");
				List<String> enterParts = columnDataValues.get("Enter Parts");
				List<String> mechanicNameList = columnDataValues.get("MechanicName");
				List<String> mechanicNumberList = columnDataValues.get("MechanicNumber");
				List<String> driverNameList = columnDataValues.get("DriverName");
				List<String> driverMobileNoList = columnDataValues.get("DriverMobileNo");
				List<String> fmSelectJobList = columnDataValues.get("FMselectJob");
				List<String> fmSelectJobList1 = columnDataValues.get("FMselectJob1");
				List<String> selectGarageList = columnDataValues.get("SelectGarage");
				List<String> srLabourAmountList = columnDataValues.get("SRLabourAmount");
				List<String> srPartsAmountList = columnDataValues.get("SRPartsAmount");
				List<String> srRevisedJobList = columnDataValues.get("SRRevisedJob");
				List<String> srselectRetailerList = columnDataValues.get("SRSelectRetailer");
				List<String> bdJobSelectionList = columnDataValues.get("BDJobSelection");
				List<String> bdRevisedJobSelectionList = columnDataValues.get("BDRevisedJob");
				List<String> bdLabourAmountList = columnDataValues.get("BDLabourAmount");
				List<String> bdPartsAmountList = columnDataValues.get("BDPartsAmount");
				List<String> bdselectRetailerList = columnDataValues.get("BDSelectRetailer");
				List<String> multiJobCountList = columnDataValues.get("MultiJobCount");
				List<String> AddImageCountList = columnDataValues.get("AddImageCount");
				
				List<String> revisedBillValue1List = columnDataValues.get("RevisedBillValue1");
				List<String> revisedBillValue2List = columnDataValues.get("RevisedBillValue2");
				List<String> kmReadingList = columnDataValues.get("KMReading");
				List<String> mechRatingsList = columnDataValues.get("MechRatings");
				List<String> mechReasonList = columnDataValues.get("MechReason");
				List<String> fmRatingsList = columnDataValues.get("FmRatings");
				List<String> fmReasonList = columnDataValues.get("FmReason");
				List<String> cancelReasonList = columnDataValues.get("CancelReason");
				List<String> jobCardList = columnDataValues.get("JobCard");
				List<String> billAmountList = columnDataValues.get("BillAmount");
				
				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < FMNumberList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setFMNumber(FMNumberList.get(i));
					userDetailsList.get(i).setFMName(FMNameList.get(i));
					userDetailsList.get(i).setVehicleNumber(vehicleNumberList.get(i));
					userDetailsList.get(i).setDriverNumber(driverNumberList.get(i));
					userDetailsList.get(i).setLocation(locationList.get(i));
					userDetailsList.get(i).setMechanicName(mechanicNameList.get(i));
					userDetailsList.get(i).setMechanicNumber(mechanicNumberList.get(i));
					userDetailsList.get(i).setDriverName(driverNameList.get(i));
					userDetailsList.get(i).setDriverMobileNo(driverMobileNoList.get(i));
					userDetailsList.get(i).setFmselectJob(fmSelectJobList.get(i));
					userDetailsList.get(i).setFmselectJob1(fmSelectJobList1.get(i));
					userDetailsList.get(i).setSelectGarage(selectGarageList.get(i));
					userDetailsList.get(i).setSrLabourAmount(srLabourAmountList.get(i));
					userDetailsList.get(i).setSrPartsAmount(srPartsAmountList.get(i));
					userDetailsList.get(i).setSrRevisedJob(srRevisedJobList.get(i));
					userDetailsList.get(i).setSrSelectRetailer(srselectRetailerList.get(i));
					userDetailsList.get(i).setBdJobSelection(bdJobSelectionList.get(i));
					userDetailsList.get(i).setBdRevisedJobSelection(bdRevisedJobSelectionList.get(i));
					userDetailsList.get(i).setBdLabourAmount(bdLabourAmountList.get(i));
					userDetailsList.get(i).setBdPartsAmount(bdPartsAmountList.get(i));
					userDetailsList.get(i).setBDSelectRetailer(bdselectRetailerList.get(i));
					userDetailsList.get(i).setMultiJobCount(multiJobCountList.get(i));
					userDetailsList.get(i).setAddImageCount(AddImageCountList.get(i));
					
					userDetailsList.get(i).setRevisedBillValue1(revisedBillValue1List.get(i));
					userDetailsList.get(i).setRevisedBillValue2(revisedBillValue2List.get(i));
					userDetailsList.get(i).setKMReading(kmReadingList.get(i));
					userDetailsList.get(i).setMechRating(mechRatingsList.get(i));
					userDetailsList.get(i).setMechReason(mechReasonList.get(i));
					userDetailsList.get(i).setFmReason(fmReasonList.get(i));
					userDetailsList.get(i).setFmRating(fmRatingsList.get(i));
					userDetailsList.get(i).setCancelReason(cancelReasonList.get(i));
					userDetailsList.get(i).setJobCard(jobCardList.get(i));
					userDetailsList.get(i).setBillAmount(billAmountList.get(i));

				}

				Utils.excelInputDataList = userDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static LinkedHashMap<String, List<String>> readingFleetManagerData(String sheetName) {

		try {

			Sheet sheet = getWorkBook().getSheet(sheetName);
			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 1);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();
			/// read values from user edit sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}
			return columnDataValues;
		} catch (Exception e) {

		}
		return null;
	}


	public static void readingNegativeData() throws IOException, BiffException {
		
			if (Utils.excelInputDataList == null) {
			Sheet sheet = getWorkBook().getSheet("NegativeWorkFlow");

			List<String> ExpectedColumns = new ArrayList<String>();
			System.out.println("::::::::::::::::::::::::::::");
			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 1);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();

			/// read values from driver sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}

			List<String> mobileNumberList = columnDataValues.get("MobileNumber");
			List<String> inValidMobileNo = columnDataValues.get("InvalidMobileNo");
			List<String> vehicleNumberList = columnDataValues.get("VehicleNumber");
			List<String> driverNameList = columnDataValues.get("DriverNumber");
			List<String> locationList = columnDataValues.get("Location");
			List<String> mechanicNameList = columnDataValues.get("MechanicName");
			List<String> mechanicNumberList = columnDataValues.get("MechanicNumber");
			List<String> fmselectJobList = columnDataValues.get("FMselectJob");
			List<String> fmselectJob1List = columnDataValues.get("FMselectJob1");
			List<String> selectGarageList = columnDataValues.get("SelectGarage");
			List<String> enterParts = columnDataValues.get("Enter Parts");
			List<String> srlabourAmountList = columnDataValues.get("SRLabourAmount");
			List<String> srpartsAmountList = columnDataValues.get("SRPartsAmount");
			List<String> selectRetailerList = columnDataValues.get("SRSelectRetailer");
			List<String> bdJobSelectionList = columnDataValues.get("BDJobSelection");
			List<String> bdRevisedJobList = columnDataValues.get("BDRevisedJob");
			List<String> bdLabourAmountList = columnDataValues.get("BDLabourAmount");
			List<String> bdPartsAmountList = columnDataValues.get("BDPartsAmount");
			List<String> bdSelectRetailerList = columnDataValues.get("BDSelectRetailer");
			List<String> multiJobCountList = columnDataValues.get("MultiJobCount");
			List<String> revisedBillValue1List = columnDataValues.get("RevisedBillValue1");
			List<String> revisedBillValue2List = columnDataValues.get("RevisedBillValue2");
			
			

			ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

			for (int i = 0; i < mobileNumberList.size(); i++) {
				ExcelInputData appInputDetail = new ExcelInputData();
				userDetailsList.add(appInputDetail);
				userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
				userDetailsList.get(i).setVehicleNumber(vehicleNumberList.get(i));
				userDetailsList.get(i).setDriverName(driverNameList.get(i));
				userDetailsList.get(i).setLocation(locationList.get(i));
				userDetailsList.get(i).setMechanicNumber(mechanicNumberList.get(i));
				userDetailsList.get(i).setMechanicName(mechanicNameList.get(i));
				userDetailsList.get(i).setFmselectJob(fmselectJobList.get(i));
				userDetailsList.get(i).setFmselectJob1(fmselectJob1List.get(i));
				userDetailsList.get(i).setSelectGarage(selectGarageList.get(i));
				userDetailsList.get(i).setEnterParts(enterParts.get(i));
				userDetailsList.get(i).setSelectRetailer(selectRetailerList.get(i));
				userDetailsList.get(i).setSrLabourAmount(srlabourAmountList.get(i));
				userDetailsList.get(i).setSrPartsAmount(srpartsAmountList.get(i));
				userDetailsList.get(i).setBdJobSelection(bdJobSelectionList.get(i));
				userDetailsList.get(i).setRevisedBillValue1(revisedBillValue1List.get(i));
				userDetailsList.get(i).setRevisedBillValue2(revisedBillValue2List.get(i));
				userDetailsList.get(i).setMultiJobCount(multiJobCountList.get(i));
				userDetailsList.get(i).setInvalidMobileNo(inValidMobileNo.get(i));

			}

			Utils.excelInputDataList = userDetailsList;

		} 
	}


public static void readingFMUserData() throws IOException, BiffException, InterruptedException {

	if (Utils.excelInputDataList == null) {
		Sheet sheet = getWorkBook().getSheet("FMUserData");

		List<String> ExpectedColumns = new ArrayList<String>();

		int masterSheetColumnIndex = sheet.getColumns();

		for (int x = 0; x < masterSheetColumnIndex; x++) {
			Cell celll = sheet.getCell(x, 0);
			String d = celll.getContents();
			ExpectedColumns.add(d);
		}

		LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

		List<String> column1 = new ArrayList<String>();

		/// read values from driver sheet for each column
		for (int j = 0; j < masterSheetColumnIndex; j++) {
			column1 = new ArrayList<String>();
			for (int i = 1; i < sheet.getRows(); i++) {
				Cell cell = sheet.getCell(j, i);
				column1.add(cell.getContents());
			}
			columnDataValues.put(ExpectedColumns.get(j), column1);
		}
		

		List<String> ProfileNameList = columnDataValues.get("Name");
		List<String> ProfileMobileNoList = columnDataValues.get("Mobile No");
		List<String> ProfileEmailList = columnDataValues.get("Email");
		List<String> ProfileGSTINList = columnDataValues.get("GSTIN");
		List<String> ProfileAddressList = columnDataValues.get("Address");
		List<String> ProfileStateList = columnDataValues.get("State");
		

		ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();
		
		for (int i = 0; i < columnDataValues.size(); i++) {
			ExcelInputData appInputDetail = new ExcelInputData();
			userDetailsList.add(appInputDetail);
			userDetailsList.get(i).setProfileName(ProfileNameList.get(i));
			userDetailsList.get(i).setProfileMobileNo(ProfileMobileNoList.get(i));
			userDetailsList.get(i).setProfileEmail(ProfileEmailList.get(i));
			userDetailsList.get(i).setProfileGSTIN(ProfileGSTINList.get(i));
			userDetailsList.get(i).setProfileAddress(ProfileAddressList.get(i));
			userDetailsList.get(i).setProfileState(ProfileStateList.get(i));
			Thread.sleep(1000);
			
		}

		Utils.excelInputDataList = userDetailsList;

	}
}
}

package com.servicemandi.utils;


public class ExcelInputData {

	public String FMNumber;
	public String FMName;

	public String vehicleNumber;
	public String driverNumber;
	public String location;
	public String mechanicName;
	public String mechanicNumber;
	public String driverName;
	public String driverMobileNo;
	public String selectGarage;
	public String srLabourAmount;
	public String srPartsAmount;
	public String srRevisedJob;
	

	public String srSelectRetailer;
	public String bdJobSelection;
	public String bdRevisedJobSelection;
	public String BdLabourAmount;
	public String BdPartsAmount;
	public String BDSelectRetailer;
	public String RevisedBillValue1;
	public String RevisedBillValue2;
	public String multiJobCount;
	public String KMReading;
	public String AddImageCount;
	
	public String mechRating;
	public String mechReason;
	public String fmRating;
	public String fmReason;
	public String CancelReason;
	public int RejectReason;
	public String JobCard;
	public String BillAmount;
	
	//Invalid Scenarios:
	public String MobileNumber;
	public String invalidMobileNo;
	public String VehicleNumber;
	public String enterParts;
	public String selectRetailer;
	public String srlabourAmount;
	public String srpartsAmount;
	public String fmselectJob;
	public String fmselectJob1;
	
	//FM USER DATA PROFILE:
	public String ProfileName;
	public String ProfileMobileNo;
	public String ProfileEmail;
	public String ProfileGSTIN;
	public String ProfileAddress;
	public String ProfileState;
	
	//Farmer-BDJobs:
	public String BDJobs;
	
	
	

	//===================================================================
	// Getters and Setters methods:
	public String getFMNumber() {
		return FMNumber;
	}

	public void setFMNumber(String FMNumber) {
		this.FMNumber = FMNumber;
	}

	// ------------------------------------
	public String getFMName() {
		return FMName;
	}

	public void setFMName(String fMName) {
		FMName = fMName;
	}

	// ------------------------------------
	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	// ------------------------------------
	public String getDriverNumber() {
		return driverNumber;
	}

	public void setDriverNumber(String driverNumber) {
		this.driverNumber = driverNumber;
	}

	// ------------------------------------
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	// ------------------------------------
	public String getMechanicName() {
		return mechanicName;
	}

	public void setMechanicName(String mechanicName) {
		this.mechanicName = mechanicName;
	}

	// ------------------------------------
	public String getMechanicNumber() {
		return mechanicNumber;
	}

	public void setMechanicNumber(String mechanicNumber) {
		this.mechanicNumber = mechanicNumber;
	}

	// ------------------------------------
	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	// ------------------------------------
	public String getDriverMobileNo() {
		return driverMobileNo;
	}

	public void setDriverMobileNo(String driverMobileNo) {
		this.driverMobileNo = driverMobileNo;
	}

	// ------------------------------------
	
	public String getSelectGarage() {
		return selectGarage;
	}

	public void setSelectGarage(String selectGarage) {
		this.selectGarage = selectGarage;
	}

	// ------------------------------------
	public String getSrLabourAmount() {
		return srLabourAmount;
	}

	public void setSrLabourAmount(String srLabourAmount) {
		this.srLabourAmount = srLabourAmount;
	}

	// ------------------------------------
	public String getSrPartsAmount() {
		return srPartsAmount;
	}

	public void setSrPartsAmount(String srPartsAmount) {
		this.srPartsAmount = srPartsAmount;
	}
	//------------------------------------
	public String getSrRevisedJob() {
		return srRevisedJob;
	}

	public void setSrRevisedJob(String srRevisedJob) {
		this.srRevisedJob = srRevisedJob;
	}
	// ------------------------------------
	public String getSrSelectRetailer() {
		return srSelectRetailer;
	}

	public void setSrSelectRetailer(String srSelectRetailer) {
		this.srSelectRetailer = srSelectRetailer;
	}

	// ------------------------------------
	public String getBdJobSelection() {
		return bdJobSelection;
	}

	public void setBdJobSelection(String bdJobSelection) {
		this.bdJobSelection = bdJobSelection;
	}
	// ------------------------------------
	
	public String getBdRevisedJobSelection() {
		return bdRevisedJobSelection;
	}

	public void setBdRevisedJobSelection(String bdRevisedJobSelection) {
		this.bdRevisedJobSelection = bdRevisedJobSelection;
	}
	
	// ------------------------------------
	public String getBdLabourAmount() {
		return BdLabourAmount;
	}

	public void setBdLabourAmount(String bdLabourAmount) {
		BdLabourAmount = bdLabourAmount;
	}

	// ------------------------------------
	public String getBdPartsAmount() {
		return BdPartsAmount;
	}

	public void setBdPartsAmount(String bdPartsAmount) {
		BdPartsAmount = bdPartsAmount;
	}

	// ------------------------------------
	public String getBDSelectRetailer() {
		return BDSelectRetailer;
	}

	public void setBDSelectRetailer(String bDSelectRetailer) {
		BDSelectRetailer = bDSelectRetailer;
	}

	// ------------------------------------
	public String getMultiJobCount() {
		return multiJobCount;
	}

	public void setMultiJobCount(String multiJobCount) {
		this.multiJobCount = multiJobCount;
	}

	// ------------------------------------
	public String getRevisedBillValue1() {
		return RevisedBillValue1;
	}

	public void setRevisedBillValue1(String revisedBillValue1) {
		this.RevisedBillValue1 = revisedBillValue1;
	}

	// ------------------------------------
	public String getRevisedBillValue2() {
		return RevisedBillValue2;
	}

	public void setRevisedBillValue2(String revisedBillValue2) {
		this.RevisedBillValue2 = revisedBillValue2;
	}
	// ------------------------------------
	
	public String getKMReading() {
		return KMReading;
	}

	public void setKMReading(String KMReading) {
		this.KMReading = KMReading;
	}
	
	//------------------------------------
	public String getAddImageCount() {
		return AddImageCount;
	}

	public void setAddImageCount(String AddImageCount) {
		this.AddImageCount = AddImageCount;
	}
	// ------------------------------------
	public String getMechRating() {
		return mechRating;
	}

	public void setMechRating(String mechRating) {
		this.mechRating = mechRating;
	}

	// ------------------------------------
	public String getMechReason() {
		return mechReason;
	}

	public void setMechReason(String mechReason) {
		this.mechReason = mechReason;
	}
	//----------------------------------------------
	public String getFmRating() {
		return fmRating;
	}

	public void setFmRating(String fmRating) {
		this.fmRating = fmRating;
	}
	// ------------------------------------
	public String getFmReason() {
		return fmReason;
	}

	public void setFmReason(String fmReason) {
		this.fmReason = fmReason;
	}
	
	// ------------------------------------
	public String getCancelReason() {
		return CancelReason;
	}

	public void setCancelReason(String CancelReason) {
		this.CancelReason = CancelReason;
	}
	// ------------------------------------
	public int getRejectReason() {
			return RejectReason;
	}

	public void setRejectReason(int RejectReason) {
		this.RejectReason = RejectReason;
	}
	// ------------------------------------
	public String getJobCard() {
		return JobCard;
	}

	public void setJobCard(String jobCard) {
		JobCard = jobCard;
	}
	// ------------------------------------
	public String getBillAmount() {
		return BillAmount;
	}

	public void setBillAmount(String billAmount) {
		BillAmount = billAmount;
	}
	// ------------------------------------
	
	
	// =====================================================================
	// Invalid Scenarios:
	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}
	// =================================================
	public String getFmselectJob() {
		return fmselectJob;
	}

	public void setFmselectJob(String fmselectJob) {
		this.fmselectJob = fmselectJob;
	}
	// =================================================
	public String getFmselectJob1() {
		return fmselectJob1;
	}

	public void setFmselectJob1(String fmselectJob1) {
		this.fmselectJob1 = fmselectJob1;
	}
	// =================================================
	public String getEnterParts() {
		return enterParts;
	}

	public void setEnterParts(String enterParts) {
		this.enterParts = enterParts;
	}
	// =================================================
	public String getSelectRetailer() {
		return selectRetailer;
	}

	public void setSelectRetailer(String selectRetailer) {
		this.selectRetailer = selectRetailer;
	}
	// =================================================
	public String getsrLabourAmount() {
		return srlabourAmount;
	}

	public void setsrLabourAmount(String srlabourAmount) {
		this.srlabourAmount = srlabourAmount;
	}
	// =================================================
	public String getsrPartsAmount() {
		return srPartsAmount;
	}

	public void setsrPartsAmount(String srPartsAmount) {
		this.srlabourAmount = srPartsAmount;
	}
	
	// =================================================
	
	public String getInvalidMobileNo() {
		return invalidMobileNo;
	}

	public void setInvalidMobileNo(String invalidMobileNo) {
		this.invalidMobileNo = invalidMobileNo;
	}
//============Profile User Data:=============================

	public String getProfileName() {
		return ProfileName;
	}

	public void setProfileName(String profileName) {
		ProfileName = profileName;
	}
	// =================================================
	public String getProfileMobileNo() {
		return ProfileMobileNo;
	}

	public void setProfileMobileNo(String profileMobileNo) {
		ProfileMobileNo = profileMobileNo;
	}
	// =================================================
	public String getProfileEmail() {
		return ProfileEmail;
	}

	public void setProfileEmail(String profileEmail) {
		ProfileEmail = profileEmail;
	}
	// =================================================
	public String getProfileGSTIN() {
		return ProfileGSTIN;
	}

	public void setProfileGSTIN(String profileGSTIN) {
		ProfileGSTIN = profileGSTIN;
	}
	// =================================================
	public String getProfileAddress() {
		return ProfileAddress;
	}

	public void setProfileAddress(String profileAddress) {
		ProfileAddress = profileAddress;
	}
	//=================================================
	public String getProfileState() {
		return ProfileState;
	}

	public void setProfileState(String profileState) {
		ProfileState = profileState;
	}
	//===================================================
	public String getBDJobs() {
		return BDJobs;
	}

	public void setBDJobs(String bDJobs) {
		BDJobs = bDJobs;
	}
	//===================================================
	
}

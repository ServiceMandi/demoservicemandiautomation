package com.servicemandi.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Utils {

	public static List<ExcelInputData> excelInputDataList;
	private static Date previousTime;
	private static long startTime = 0;
	private static long endTime = 0;
	private static long totalTime = 0;
	
	public static double finalBillAmt=0.0;
	
	public static void sleepTimeLong() throws InterruptedException {

		Thread.sleep(20000);
	}

	public static void sleepTimeMedium() throws InterruptedException {
		Thread.sleep(10000);
	}

	public static void sleepTimeLow() throws InterruptedException {
		Thread.sleep(5000);
	}

	public static String timeCalculation() {

		if (startTime == 0) {
			startTime = System.nanoTime();
			//System.out.println("startTime:::"+TimeUnit.SECONDS.convert(startTime, TimeUnit.NANOSECONDS));
		}
		endTime = System.nanoTime();
		//System.out.println("endTime:::"+TimeUnit.SECONDS.convert(endTime, TimeUnit.NANOSECONDS));
		totalTime = endTime - startTime;
	//	System.out.println("totalTime:::"+TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS));
		startTime = endTime; 
		return "The time duration is " + TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS) + " Seconds";

	}

	public static void checkBillWithOutGST() {

		try {
			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));
			List<WebElement> billJobsList = Configuration.getFleetManagerInstance().findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;

			for (int i = 0; i < billJobsList.size(); i++) {
				String validationString = billJobsList.get(i).getText();
				if (i == 0 || i == 1) {

				} else if (validationString.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")) {
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString);
						}
					}
				}
			}

			double finalAmount = cumulativeAmount - discountAmount;

			if (cumulativeAmount == totalLabourAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Total Labour Amount is Verified");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Total Labour Amount is Verified Failed");

			if (finalAmount == finalBillAmount) {
				Configuration.writeReport(LogStatus.PASS, "View bill Final Amount is Verified & matches");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View bill Final Amount does't match");

		} catch (Exception e) {

		}
	}

	public static void checkBillWithIGST() {

		try {

			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));

			List<WebElement> billJobsList = Configuration.getFleetManagerInstance()
					.findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;
			double IGSTAmount = 0.0;

			for (int i = 0; i < billJobsList.size(); i++) {

				String validationString = billJobsList.get(i).getText();

				System.out.println("ValidationString :::  " + validationString);
				if (i == 0 || i == 1) {

				} else if (validationString.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.contains("IGST")) {
					String value = validationString.split("\\s+")[1];
					IGSTAmount = Double.parseDouble(value);
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")
								&& !billJobsList.get((i - 1)).getText().contains("IGST")) {
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString);
						}
					}
				}
			}

			double IGSTValidationAmount = (totalLabourAmount / 100) * 18;
			double finalAmount = totalLabourAmount + IGSTValidationAmount;
			double finalValidationAmount = finalAmount - discountAmount;

			if (cumulativeAmount == totalLabourAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Total Labour Amount is Verified");
				System.out.println("cumulativeAmount:::"+cumulativeAmount);
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Total Labour Amount Failed");

			if (IGSTValidationAmount == IGSTAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill IGST amount is Verified");
				System.out.println("IGSTValidationAmount:::"+IGSTValidationAmount);
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill IGST amount Failed ");

			if (finalBillAmount == finalValidationAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Final service bill amount is Verified");
				System.out.println("finalBillAmount::"+finalBillAmount);
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Final service bill amount Failed ");

		} catch (Exception e) {

		}
	}

	public static double checkBillWithGST() throws InterruptedException {
		double returnValue=0.0; 
		
			System.out.println("Inside View Bill method:::::::::");
			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));

			List<WebElement> billJobsList = Configuration.getFleetManagerInstance().findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;
			double SGSTAmount = 0.0;
			double CGSTAmount = 0.0;
			double IGSTAmount = 0.0;

			for (int i = 0; i < billJobsList.size()-1; i++) {
				
				String validationString = billJobsList.get(i).getText();
				System.out.println("validationString:::"+validationString);
				System.out.println("billJobsList.size():::"+billJobsList.size());
				

				String validationString1 = billJobsList.get(i).getText();

				System.out.println("View Bill Validation String :::  " + validationString1);
				if (i == 0 || i == 1) {

				} else if (validationString1.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
					Configuration.writeReport(LogStatus.PASS, "TotalLabourAmount:::"+totalLabourAmount+" is Verified::Passed");					
				} else if (validationString1.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
					Configuration.writeReport(LogStatus.PASS, "DiscountAmount:::"+discountAmount+" is Verified::Passed");
				} else if (validationString1.contains("SGST")) {
					String value = validationString1.split("\\s+")[1];
					SGSTAmount = Double.parseDouble(value);
					Configuration.writeReport(LogStatus.PASS, "SGSTAmount:::"+SGSTAmount+" is Verified::Passed");
				} else if (validationString1.contains("CGST")) {
					String value = validationString1.split("\\s+")[1];
					CGSTAmount = Double.parseDouble(value);
					
					Configuration.writeReport(LogStatus.PASS, "CGSTAmount:::"+CGSTAmount+" is Verified::Passed");
				} else if (validationString1.contains("IGST")) {
					String value = validationString1.split("\\s+")[1];
					IGSTAmount = Double.parseDouble(value);
					Configuration.writeReport(LogStatus.PASS, "IGSTAmount:::"+IGSTAmount+" is Verified::Passed");
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")
								&& !billJobsList.get((i - 1)).getText().contains("SGST")
								&& !billJobsList.get((i - 1)).getText().contains("CGST")){
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString1);
						}
					}
				}
			}

			double SGSTValidationAmount = (totalLabourAmount / 100) * 9;
			double CGSTValidationAmount = (totalLabourAmount / 100) * 9;
			double finalAmount = totalLabourAmount + SGSTValidationAmount + CGSTValidationAmount;
			double finalValidationAmount = finalAmount - discountAmount;
			System.out.println("finalValidationAmount::"+finalValidationAmount);
			/*if (cumulativeAmount == totalLabourAmount)
				Configuration.writeReport(LogStatus.PASS, "Total Labour Amount is Verified::Passed");
			else
				Configuration.writeReport(LogStatus.FAIL, "Total Labour Amount is incorrect::Failed");

			if (SGSTValidationAmount == SGSTAmount)
				Configuration.writeReport(LogStatus.PASS, "SGSTAmount is Verified::Passed ");
			else
				Configuration.writeReport(LogStatus.FAIL, "SGST Amount is incorrect::Failed");

			if (CGSTValidationAmount == CGSTAmount)
				Configuration.writeReport(LogStatus.PASS, "CGST Amount is Verified::Passed ");
			else
				Configuration.writeReport(LogStatus.FAIL, "CGST Amount is incorrect::Failed");

			if (finalBillAmount == finalValidationAmount)
				Configuration.writeReport(LogStatus.PASS, "Final bill Amount is Verified::Passed");
			else
				Configuration.writeReport(LogStatus.FAIL, "Final bill Amount is incorrect::Failed");

			finalBillAmt= finalValidationAmount;
			System.out.println("finalValidationAmount::"+finalValidationAmount);	
			System.out.println("finalBillAmt::"+finalBillAmt);	*/
			
			returnValue=finalValidationAmount;
		
		return returnValue;
	}
	
}

package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import java.io.IOException;
import java.util.List;

import io.appium.java_client.android.AndroidKeyCode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BreakDownStartJob extends Configuration {

	public BreakDownStartJob() {
	}

	public void startJobs(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified) throws InterruptedException {
		
			System.out.println(" in StartJobs try");
		
			Thread.sleep(4000);
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(3000);
			System.out.println("Entered Start jobs method :::");
			mechanicWait(LocatorPath.jobsCompletePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
			System.out.println("Technician: Jobs complete Passed");
			writeReport(LogStatus.PASS, "Technician: Jobs complete Passed");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			System.out.println("Entered Submit bill path ::: ");
			Thread.sleep(3000);
			//Mandatory alert for KM Reading:
			String Mandateforkm=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
			if(Mandateforkm.equalsIgnoreCase("Please enter km Reading"))
			{   
				Thread.sleep(1000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
				mechanicWait(KMReading);
				clickMechanicElement(LocatorType.XPATH, KMReading);
				//enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
				mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
				writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+Mandateforkm);
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(2000);
				hideKeyBoard(mechanicDriver);
				mechanicWait(LocatorPath.jobSubmitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.alertOkPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}else {
				Thread.sleep(1000);
				String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
				mechanicWait(KMReading);
				clickMechanicElement(LocatorType.XPATH, KMReading);
				//enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
				mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(1000);
				mechanicWait(LocatorPath.jobSubmitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.alertOkPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				}
			writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
		}

	public void BDRevisedBill(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified) throws InterruptedException {
		
			System.out.println("in RevisedBill ");
		
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(5000);
			System.out.println("Entered Job complete method :::");
			mechanicWait(LocatorPath.completeJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
			Thread.sleep(5000);

			System.out.println("Entered into Revised bill method :::");
			mechanicWait(LocatorPath.revisedBillPath);
			Thread.sleep(2000);
			WebElement revisedBill = mechanicDriver.findElement(By.xpath(LocatorPath.revisedBillPath));
			revisedBill.clear();
			revisedBill.clear();
			revisedBill.sendKeys(excelInputData.getRevisedBillValue1());

			System.out.println("excelInputData.getRevisedBillValue1():::"+excelInputData.getRevisedBillValue1());
			
			System.out.println("excelInputData.getBdLabourAmount():::"+excelInputData.getBdLabourAmount());
			
			
			/*String number1=excelInputData.getBdLabourAmount();
			
			int number2 =Integer.parseInt(number1)+Integer.parseInt(number1);
			
			if(number2 >= Integer.parseInt(excelInputData.getRevisedBillValue1()))
			{
				System.out.println("RevisedBill is double the Labour Amount");
				
			}*/
			/*
			 * Thread.sleep(1000); WebElement revisedBillElement = mechanicDriver
			 * .findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath) );
			 * revisedBillElement.clear(); revisedBillElement.clear();
			 * revisedBillElement.sendKeys(excelInputData.getRevisedBillValue2() );
			 */
			Thread.sleep(2000);
			hideKeyBoard(mechanicDriver);
			mechanicWait(LocatorPath.submitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
			Thread.sleep(2000);
			//Mandatory alert for KM Reading:
			String Mandateforkm=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
			if(Mandateforkm.equalsIgnoreCase("Please enter km Reading"))
			{   
				Thread.sleep(1000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
				mechanicWait(KMReading);
				clickMechanicElement(LocatorType.XPATH, KMReading);
				//enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
				mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
				writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+Mandateforkm);
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(2000);
				hideKeyBoard(mechanicDriver);
				mechanicWait(LocatorPath.jobSubmitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.alertOkPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}else {
				Thread.sleep(1000);
				String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
				mechanicWait(KMReading);
				clickMechanicElement(LocatorType.XPATH, KMReading);
				//enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
				mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(1000);
				mechanicWait(LocatorPath.jobSubmitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.alertOkPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				}
			
			writeReport(LogStatus.PASS, "Technician::Revised bill::Passed");
			
		
	}

	public void revisedEstimationWithParts(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified) throws InterruptedException, IOException {
		/*if(isVerified) {
			System.out.println("isVerified in revisedEstimationWithParts try"+isVerified);
		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			System.out.println("Entered Revised Estimate method :::");
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i - 1).click();
					writeReport(LogStatus.PASS, "Technician:: Selected job :-" + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(2000);

			// EnterLabour:
			//String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(LocatorPath.trackLocationLabourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getBdLabourAmount());
			Thread.sleep(1000);

			// EnterParts:
			//String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(LocatorPath.PartsPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			
			writeReport(LogStatus.PASS, "Technician::Entered Revised Estimate-Parts::" + excelInputData.getBdPartsAmount());
			isVerified=true;*/
		
		//New method:
		//==============
		
			System.out.println("in revisedEstimationWithParts " + isVerified);
			Thread.sleep(2000);
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(3000);
			System.out.println("Entered revisedEstimationWithParts-Start jobs method :::");
			mechanicWait(LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(4000);
			
				System.out.println("Entered the trackLocationWithParts method :::");
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);

				String expected = excelInputData.getBdRevisedJobSelection();
				System.out.println("expected" + expected);

				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("outerArrayLength" + outerArrayLength);
				int checkcount = 0;
				for (String OuterArrayValues1 : outerArray) {
					mechanicWait(LocatorPath.totalJobListPath);
					List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
					System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
					System.out.println("checkcount:::" + checkcount);
					System.out.println("OuterArrayValues1" + OuterArrayValues1);
					int result = Integer.parseInt(OuterArrayValues1);
					selectJobOuterList.get(result).click();
					Thread.sleep(1000);

					mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.trackLocationLabourPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
					Thread.sleep(1000);
					System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
					writeReport(LogStatus.PASS,"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());

					mechanicWait(LocatorPath.PartsPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS,"Technician::BD-Parts amount entered:- "+excelInputData.getBdPartsAmount());

					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
						clickMechanicElement(LocatorType.XPATH, addJobButton);
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
						Thread.sleep(1000);

					} else {
						mechanicWait(LocatorPath.submitRevisedEstimatePath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
						Thread.sleep(500);
						System.out.println("Technician: Submit Revised Estimate With Parts Bill");
						writeReport(LogStatus.PASS, "Technician::Revised Estimate With Parts:::Passed");
						isVerified = true;
						break;
					}
					
				}		
	}

	public void driverRevisedEstimationWithParts(ExcelInputData excelInputData, Mechanic mechanic) throws InterruptedException {

		try {
			Thread.sleep(2000);
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			

			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(1000);
			System.out.println("Clicked Add Jobs");
			
			try {
				Thread.sleep(2000);
				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				mechanicWait(YesBtn);
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				System.out.println("Clicked completeOrYes :::");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i - 1).click();
					writeReport(LogStatus.PASS, "Technician::Selected job :-" + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			//String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(LocatorPath.trackLocationLabourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getBdLabourAmount());
			Thread.sleep(1000);

			// EnterParts:
			//String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(LocatorPath.PartsPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Entered Break down parts :::");
			writeReport(LogStatus.PASS, "Technician::Entered Driver-Revised Estimate-Parts " + excelInputData.getBdPartsAmount());
			/*
			 * Utils.sleepTimeLow(); mechanicWait(LocatorPath.revisedPaymentRequestOk);
			 * clickMechanicElement(LocatorType.XPATH, LocatorPath.revisedPaymentRequestOk);
			 * Thread.sleep(3000);
			 */

		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "Technician::Driver-Revised Estimate-Parts Failed");
		}
	}

	public void revisedEstimationWithOutParts(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified) throws InterruptedException, IOException {
	/*if(isVerified) {
			System.out.println("isVerified in revisedEstimationWithOutParts try"+isVerified);

		try {
			Thread.sleep(2000);
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(3000);
			System.out.println("Entered revisedEstimationWithOutParts-Start jobs method :::");
			mechanicWait(LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(4000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
			System.out.println("jobList.size()"+jobList.size());
			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i - 1).click();
					writeReport(LogStatus.PASS, "Technician::Selected job :- " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(2000);

			// EnterLabour:
			
			mechanicWait(LocatorPath.trackLocationLabourPath);
			Thread.sleep(2000);
			clickMechanicElement(LocatorType.XPATH,LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getBdLabourAmount());
			Thread.sleep(1000);

			mechanicWait(LocatorPath.PartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, "00");
			hideKeyBoard(mechanicDriver);
			
			 * //EnterParts: String partsPath = "/
			 
				 * [@id=\
				 * "nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
				 * mechanicWait(partsPath); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, partsPath); Thread.sleep(1000);
				 * enterValueMechanic(LocatorType.XPATH, partsPath, "300");
				 
			

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Clicked submitRevisedEstimate :::");
			writeReport(LogStatus.PASS, "Technician::Revised Estimate Without parts Passed");

			
			 * Thread.sleep(2000); mechanicWait(LocatorPath.revisedPaymentRequestOk);
			 * clickMechanicElement(LocatorType.XPATH, LocatorPath.revisedPaymentRequestOk);
			 */
			
			//New method:
			//==============
			if (isVerified) {
				System.out.println("isVerified in revisedEstimationWithOutParts try" + isVerified);
				Thread.sleep(2000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(3000);
				System.out.println("Entered revisedEstimationWithOutParts-Start jobs method :::");
				mechanicWait(LocatorPath.addJobsLinkPath);
				Thread.sleep(2000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
				Thread.sleep(4000);
				try {
					System.out.println("Entered the trackLocationWithParts method :::");
					mechanicWait(LocatorPath.selectJobPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
					Thread.sleep(1000);

					String expected = excelInputData.getBdRevisedJobSelection();
					System.out.println("expected" + expected);

					String[] outerArray = expected.split(",");
					int outerArrayLength = outerArray.length;
					System.out.println("outerArrayLength" + outerArrayLength);
					int checkcount = 0;
					for (String OuterArrayValues1 : outerArray) {
						mechanicWait(LocatorPath.totalJobListPath);
						List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
						System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
						System.out.println("checkcount:::" + checkcount);
						System.out.println("OuterArrayValues1" + OuterArrayValues1);
						int result = Integer.parseInt(OuterArrayValues1);
						selectJobOuterList.get(result).click();
						Thread.sleep(1000);

						mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
						Thread.sleep(1000);
						mechanicWait(LocatorPath.trackLocationLabourPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
						enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
						Thread.sleep(1000);
						System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
						writeReport(LogStatus.PASS,"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());

						mechanicWait(LocatorPath.PartsPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
						enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, "00");
						hideKeyBoard(mechanicDriver);
						writeReport(LogStatus.PASS,"Technician::BD-Parts amount entered:- " +"00");

						checkcount = checkcount + 1;
						System.out.println("checkcount" + checkcount);
						if (checkcount != outerArrayLength) {
							Thread.sleep(1000);
							String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
							clickMechanicElement(LocatorType.XPATH, addJobButton);
							mechanicWait(LocatorPath.selectJobPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
							Thread.sleep(1000);

						} else {
							mechanicWait(LocatorPath.submitRevisedEstimatePath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
							Thread.sleep(500);
							System.out.println("Technician: Submit Revised Estimate Without Parts Bill");
							writeReport(LogStatus.PASS, "Technician::Revised Estimate without Parts:::Passed");
							isVerified = true;
							break;
						}
						isVerified=true;
					}
			
		}catch (Exception e) {
			e.printStackTrace();
			MECH_TakeClick();
			writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts:: Failed");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts:: Failed");
			isVerified=false;
			}
	}

	
}

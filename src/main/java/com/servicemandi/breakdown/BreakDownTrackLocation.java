package com.servicemandi.breakdown;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Driver;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class BreakDownTrackLocation extends Configuration {

	public void giveEstimate(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("in giveEstimate try");
			
				Thread.sleep(3000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				mechanicWait(LocatorPath.giveEstimatePath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
				System.out.println("Clicked the give Estimate method :::");
				writeReport(LogStatus.PASS, "Technician: Clicked Give Estimate method");
				
			
	}

	public void completeOrYes(boolean isVerified) throws InterruptedException, IOException {
		System.out.println("Entered completeOrYes method :::");
		
			Thread.sleep(5000);
			String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
			mechanicWait(YesBtn);
			clickMechanicElement(LocatorType.XPATH, YesBtn);
			Thread.sleep(3000);
			writeReport(LogStatus.PASS, "Technician: Clicked YES as per Dealer cut off configuiration");

		

	}

	public void trackLocationWithCompleteYesNo(ExcelInputData excelInputData, Mechanic mechanic)
			throws InterruptedException {
		try {
			System.out.println("Entered the trackLocationWithCompleteYesNo method :::");
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(3000);

			String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
			mechanicWait(YesBtn);
			clickMechanicElement(LocatorType.XPATH, YesBtn);
			Thread.sleep(2000);

			mechanicWait(LocatorPath.PartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Technician::BD-Parts amount entered:- " + excelInputData.getBdPartsAmount());

			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			Thread.sleep(500);
			/*
			 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
			 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			 */
			System.out.println("Technician: Submitted With Parts Bill");

		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Technician::TrackLocationWithCompleteYesNo method failed");
		}
	}

	public void trackLocationWithParts(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("in trackLocationWithParts");
			
				System.out.println("Entered the trackLocationWithParts method :::");
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);

				String expected = excelInputData.getBdJobSelection();
				System.out.println("expected" + expected);

				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("outerArrayLength" + outerArrayLength);
				int checkcount = 0;
				for (String OuterArrayValues1 : outerArray) {
					mechanicWait(LocatorPath.totalJobListPath);
					List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
					System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
					System.out.println("checkcount:::" + checkcount);
					System.out.println("OuterArrayValues1" + OuterArrayValues1);
					int result = Integer.parseInt(OuterArrayValues1);
					selectJobOuterList.get(result).click();
					Thread.sleep(1000);

					mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.trackLocationLabourPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getSrLabourAmount());
					Thread.sleep(1000);
					System.out.println("getBdLabourAmount" + excelInputData.getSrLabourAmount());
					writeReport(LogStatus.PASS,
							"Technician::BD-Labour amount entered:- " + excelInputData.getSrLabourAmount());

					mechanicWait(LocatorPath.PartsPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
					System.out.println("getBdPartsAmount" + excelInputData.getSrPartsAmount());
					enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getSrPartsAmount());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS,
							"Technician::BD-Parts amount entered:- " + excelInputData.getSrPartsAmount());

					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
						clickMechanicElement(LocatorType.XPATH, addJobButton);
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
						Thread.sleep(1000);

					} else {
						mechanicWait(LocatorPath.partsSubmitPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						Thread.sleep(500);
						/*
						 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
						 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						 */
						System.out.println("Technician: Submitted With Parts Bill");
						writeReport(LogStatus.PASS, "Technician: TrackLocation With Parts method:::Passed");
						isVerified = true;
						break;
					}
				}
			}

	

	// New BD-SelectJOBS method:
	public void bdSelectJobs(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {

		if (isVerified) {
			System.out.println("isVerified in trackLocationWithMultiJobs try" + isVerified);
			try {

				System.out.println("Entered the trackLocationWithParts method :::");
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
				System.out.println("jobList.size()" + jobList.size());

				for (int i = 1; i <= jobList.size(); i++) {
					Thread.sleep(1000);
					String selectedJobText = Integer.toString(i);
					String expected = excelInputData.getBdJobSelection();
					System.out.println("expected" + expected);
					String[] outerArray = expected.split(",");
					System.out.println("outerArray.length:::" + outerArray.length);
					System.out.println("outerArray:::::" + outerArray);
					for (String OuterArrayValues1 : outerArray) {
						System.out.println(OuterArrayValues1);
						if (selectedJobText.equals(OuterArrayValues1)) {
							jobList.get(i).click();
							Thread.sleep(1000);
							mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
							Thread.sleep(2000);
							// Entering Labour Amount:
							mechanicWait(LocatorPath.trackLocationLabourPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
							enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
							Thread.sleep(2000);
							System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
							writeReport(LogStatus.PASS,
									"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());
							// Entering Parts Amount:
							mechanicWait(LocatorPath.PartsPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
							System.out.println("getBdPartsAmount" + excelInputData.getBdPartsAmount());
							enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath,excelInputData.getBdPartsAmount());
							hideKeyBoard(mechanicDriver);
							writeReport(LogStatus.PASS,"Technician::BD-Parts amount entered:- " + excelInputData.getBdPartsAmount());

							if ((outerArray.length) == 0) {
								break;
							} else {
								Thread.sleep(1000);
								String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
								clickMechanicElement(LocatorType.XPATH, addJobButton);
								Thread.sleep(2000);
								mechanicWait(LocatorPath.selectJobPath);
								clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
							}
						}
					} // second for
				}

				/*
				 * if (outerArray.length == 0) { Thread.sleep(1000); String addJobButton =
				 * "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
				 * clickMechanicElement(LocatorType.XPATH, addJobButton); Thread.sleep(2000);
				 * mechanicWait(LocatorPath.selectJobPath);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				 */

				// Clicking Submit Bill button:

				
				Thread.sleep(1000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				System.out.println("Technician: Submitted Bill");
				writeReport(LogStatus.FAIL, "Technician:  Submit Bill::: Passed");

			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician: TrackLocation With Parts method Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician: TrackLocation With Parts method Failed");
			isVerified = false;
		}

	}

	
	public void trackLocationWithOutParts(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("in trackLocationWithParts");
			Thread.sleep(3000);
				System.out.println("Entered the trackLocationWithParts method :::");
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);

				String expected = excelInputData.getBdJobSelection();
				System.out.println("expected" + expected);

				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("outerArrayLength" + outerArrayLength);
				int checkcount = 0;
				for (String OuterArrayValues1 : outerArray) {
					mechanicWait(LocatorPath.totalJobListPath);
					List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
					System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
					System.out.println("checkcount:::" + checkcount);
					System.out.println("OuterArrayValues1" + OuterArrayValues1);
					int result = Integer.parseInt(OuterArrayValues1);
					selectJobOuterList.get(result).click();
					Thread.sleep(1000);

					mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(3000);
					mechanicWait(LocatorPath.trackLocationLabourPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
					Thread.sleep(1000);
					System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
					writeReport(LogStatus.PASS,"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());

					mechanicWait(LocatorPath.PartsPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, "00");
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS,"Technician::BD-Parts amount entered:- " +"00");

					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
						clickMechanicElement(LocatorType.XPATH, addJobButton);
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
						Thread.sleep(1000);

					} else {
						mechanicWait(LocatorPath.partsSubmitPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						Thread.sleep(2000);
						/*
						 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
						 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						 */
						System.out.println("Technician: Submitted Without Parts Bill");
						writeReport(LogStatus.PASS, "Technician: TrackLocation Without Parts method:::Passed");
						isVerified = true;
						break;
					}
				}

	}

	public void UpdateJobStatus(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {

		
			System.out.println(" in UpdateJobStatus" + isVerified);

		
				Thread.sleep(3000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(3000);
				System.out.println("Entered Update Job Status method :::");
				mechanicWait(LocatorPath.completeJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
				Thread.sleep(5000);
				//Thread.sleep(4000);//added due to App slowness
				mechanicWait(LocatorPath.submitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
				Thread.sleep(2000);
				
				//Mandatory alert for KM Reading:
				String Mandateforkm=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
				if(Mandateforkm.equalsIgnoreCase("Please enter km Reading"))
				{   
					Thread.sleep(1000);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
					String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
					Thread.sleep(1000);
					mechanicWait(KMReading);
					clickMechanicElement(LocatorType.XPATH, KMReading);
					mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+Mandateforkm);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
					Thread.sleep(3000);
					mechanicWait(LocatorPath.jobSubmitBillPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
					Thread.sleep(2000);
					mechanicWait(LocatorPath.alertOkPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				}else {
					Thread.sleep(1000);
					String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
					mechanicWait(KMReading);
					clickMechanicElement(LocatorType.XPATH, KMReading);
					mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
					Thread.sleep(1000);
					mechanicWait(LocatorPath.jobSubmitBillPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.alertOkPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
					}
				writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
				writeReport(LogStatus.PASS, "Technician::UpdateJobStatus Passed");
				isVerified=true;
			
	}

	public void driverUpdateJobStatus(ExcelInputData excelInputData, Mechanic mechanic, Driver driver,
			boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in driverUpdateJobStatus try" + isVerified);
			try {
				Thread.sleep(2000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				System.out.println("Entered driverUpdateJobStatus method :::");
				mechanicWait(LocatorPath.completeJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
				Thread.sleep(5000);
				// driver.pushNotification();
				mechanicWait(LocatorPath.submitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
				Thread.sleep(2000);
				/*
				 * Thread.sleep(500); mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				 * Thread.sleep(1000); clickMechanicElement(LocatorType.XPATH,
				 * LocatorPath.partsSubmitPath);
				 */
				mechanicWait(LocatorPath.paymentRequestOk);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
				writeReport(LogStatus.PASS, "Technician::Driver-UpdateJobStatus Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::Driver:UpdateJobStatus Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::Driver:UpdateJobStatus Failed");
			isVerified = false;
		}
	}

	public void UpdateJobStatusWithRevisedBill(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in UpdateJobStatusWithRevisedBill try" + isVerified);
			try {
				Thread.sleep(2000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				System.out.println("Inside UpdateJobStatusWithRevisedBill method");
				Thread.sleep(2000);
				mechanicWait(LocatorPath.addJobsLinkPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop" + selectJob);
					if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
						jobList.get(i).click();
						writeReport(LogStatus.PASS,
								"Technician: Selected job :- " + excelInputData.getBdJobSelection());
						Thread.sleep(1000);
						break;
					}
				}
				Thread.sleep(1000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);

				// EnterLabour:
				String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

				mechanicWait(labourPath);
				Thread.sleep(1000);
				clickMechanicElement(LocatorType.XPATH, labourPath);
				enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getBdLabourAmount());
				Thread.sleep(500);
				hideKeyBoard(mechanicDriver);
				/*
				 * //EnterParts: String partsPath = "/
				 *//*
					 * [@id=\
					 * "nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
					 * mechanicWait(partsPath); Thread.sleep(1000);
					 * clickMechanicElement(LocatorType.XPATH, partsPath); Thread.sleep(1000);
					 * enterValueMechanic(LocatorType.XPATH, partsPath, "300");
					 */

				mechanicWait(LocatorPath.submitRevisedEstimatePath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.paymentRequestOk);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
				writeReport(LogStatus.PASS, "Technician::UpdateJobStatusWithRevisedBill Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::UpdateJobStatusWithRevisedBill Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::UpdateJobStatusWithRevisedBill Failed");
			isVerified = false;
		}
	}

	public void withOutRetailer(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in withOutRetailer try" + isVerified);
			try {
				Utils.sleepTimeLow();
				System.out.println("Entered withOutRetailer method :::");
				mechanicWait(LocatorPath.retailersListPath);
				Thread.sleep(2000);
				mechanicClassPathWait(LocatorPath.noRetailerPath);
				clickMechanicElement(LocatorType.CLASS, LocatorPath.noRetailerPath);
				mechanicWait(LocatorPath.retailerConfirmPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
				writeReport(LogStatus.PASS, "Technician::Submit Estimate -WithOut Retailer Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				MECH_TakeClick();
				writeReport(LogStatus.FAIL, "Technician::BD:WithOutRetailer method failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::BD:WithOutRetailer method failed");
			isVerified = false;
		}
	}

	public void UpdateJobStatusRevisedBill(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		if (isVerified) {
			System.out.println("isVerified in UpdateJobStatusRevisedBill try" + isVerified);
			try {
				Thread.sleep(3000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(4000);

				mechanicWait(LocatorPath.completeJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
				Thread.sleep(3000);
				System.out.println("Entered UpdateJobStatusRevisedBill method :::");

				mechanicWait(LocatorPath.submitBillPath);
				Thread.sleep(2000);
				WebElement revisedBill = mechanicDriver.findElement(By.xpath(LocatorPath.revisedBillPath));
				revisedBill.clear();
				revisedBill.clear();
				revisedBill.sendKeys(excelInputData.getRevisedBillValue1());
				hideKeyBoard(mechanicDriver);

				/*
				 * Thread.sleep(1000); WebElement revisedBillElement = mechanicDriver
				 * .findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath) );
				 * revisedBillElement.clear(); revisedBillElement.clear();
				 * revisedBillElement.sendKeys(excelInputData.getRevisedBillValue2() );
				 */

				mechanicWait(LocatorPath.submitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
				Thread.sleep(2000);
				
				//Mandatory alert for KM Reading:
				String Mandateforkm=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
				if(Mandateforkm.equalsIgnoreCase("Please enter km Reading"))
				{   
					Thread.sleep(1000);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
					String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
					Thread.sleep(1000);
					mechanicWait(KMReading);
					clickMechanicElement(LocatorType.XPATH, KMReading);
					mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value::Alert message captured::::"+Mandateforkm);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
					Thread.sleep(1000);
					mechanicWait(LocatorPath.jobSubmitBillPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.alertOkPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
				}else {
					Thread.sleep(1000);
					String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
					mechanicWait(KMReading);
					clickMechanicElement(LocatorType.XPATH, KMReading);
					mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
					Thread.sleep(1000);
					mechanicWait(LocatorPath.jobSubmitBillPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.alertOkPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
					}
				writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
				writeReport(LogStatus.PASS, "Technician::UpdateJobStatus Revised bill::Passed");
				isVerified=true;
				
			} catch (Exception e) {
				e.printStackTrace();
				MECH_TakeClick();
				writeReport(LogStatus.FAIL, "Technician::UpdateJobStatus Revised bill::Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::UpdateJobStatus Revised bill::Failed");
			isVerified = false;
		}
	}

	public void selectRetailer(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {
		
		
			System.out.println(" in selectRetailer " );
			Thread.sleep(10000);
				System.out.println("Entered the service request retailer method :::");
				mechanicWait(LocatorPath.retailersListPath);
				List<WebElement> retailersList = mechanicDriver.findElements(By.xpath(LocatorPath.retailersListPath));
				System.out.println("retailersList.size()" + retailersList.size());
				int loopSize = retailersList.size();

				boolean isClicked = false;
				for (int i = 1; i <= loopSize; i++) {
											
					String insideRetailerPath = "//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/div[3]/ion-card[" + i + "]" + "/*";
					
					List<WebElement> retailersInsideList = mechanicDriver.findElements(By.xpath(insideRetailerPath));
					System.out.println("retailersInsideList.size();" + retailersInsideList.size());
					// if (!isClicked) {
					for (int j = 0; j < retailersInsideList.size(); j++) {
						System.out.println("Retailer name :::" + retailersInsideList.get(j).getText());
						if (retailersInsideList.get(j).getText().contains(excelInputData.getSrSelectRetailer())){
							Thread.sleep(2000);
							
							 String inneraddress="//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/div[3]/ion-card/div["+(j+1)+"]/ion-card-content/ion-row[2]/ion-col";
							 mechanicDriver.findElement(By.xpath(inneraddress)).click();
							
							Thread.sleep(2000);
							mechanicWait(LocatorPath.retailerConfirmPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
							System.out.println(
									"excelInputData.getSrSelectRetailer()" + excelInputData.getSrSelectRetailer());
							writeReport(LogStatus.PASS, "Technician: SR-Retailer selected :- "	+ excelInputData.getSrSelectRetailer());
							isVerified = true;
							
							break;
						}}
					}
						
			
	}


	public void giveEstimateReturnBack(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in giveEstimateReturnBack try" + isVerified);
			try {
				System.out.println("Entered the Give Estimate ReturnBack method :::");
				Thread.sleep(1000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				mechanicWait(LocatorPath.giveEstimatePath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
				Thread.sleep(4000);
				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				Thread.sleep(3000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.VehicleImage);
				Thread.sleep(4000);
				
				writeReport(LogStatus.PASS, "Technician::Returned from Give Estimate Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::Returned from Give Estimate method Failed ::");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::Returned from Give Estimate method Failed");
			isVerified = false;
		}
	}

	public void giveEstimateCompleteOrder(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in giveEstimateCompleteOrder try" + isVerified);
			try {
				System.out.println("Entered the Give Estimate Complete Order method :::");
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				/*
				 * mechanicWait(LocatorPath.giveEstimatePath);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
				 * Thread.sleep(5000);
				 */

				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				Thread.sleep(2000);

				/*
				 * WebElement completeText =
				 * mechanicDriver.findElement(By.xpath("//*[@id=\"alert-subhdr-0\"]"));
				 * System.out.println("CompleteText:" + completeText.getText());
				 * Thread.sleep(1000);
				 * 
				 * mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER); Thread.sleep(1000);
				 */
				writeReport(LogStatus.PASS, "Technician::GiveEstimate CompleteOrder method Passed");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::GiveEstimateCompleteOrder method Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::GiveEstimateCompleteOrder method Failed");
			isVerified = false;
		}
	}
	public void trackLocationMultiJobWithParts(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException {

	
			System.out.println(" in trackLocationMultiJobWithParts");
		
				System.out.println("Entered the track location with parts method :::");
				Thread.sleep(7000);
													
				String JobCardPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/*";
				System.out.println("JobCardPath::::" + JobCardPath);
				List<WebElement> JobCardPathSize = mechanicDriver.findElements(By.xpath(JobCardPath));
				System.out.println("JobCardPathSize:::" + JobCardPathSize.size());
				

				for (int i = 1; i <= JobCardPathSize.size(); i++) {

					String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
					WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
					partsElement.clear();
					partsElement.clear();
					partsElement.sendKeys(excelInputData.getSrPartsAmount());
					Thread.sleep(500);

				}
				hideKeyBoard(mechanicDriver);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);

				System.out.println("Technician::Clicked Submit estimate :::");
				writeReport(LogStatus.PASS,"Technician::Parts Amount::" + excelInputData.getSrPartsAmount() + " entered ");
		
	}
	
	public void trackLocationMultiJobWithoutParts(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
	
			System.out.println("in trackLocationMultiJobWithoutParts");
			
				System.out.println("Entered the track location without parts method :::");
				Thread.sleep(5000);
													
				/*String JobCardPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/*";
				System.out.println("JobCardPath::::" + JobCardPath);
				List<WebElement> JobCardPathSize = mechanicDriver.findElements(By.xpath(JobCardPath));
				System.out.println("JobCardPathSize:::" + JobCardPathSize.size());
				

				for (int i = 1; i <= JobCardPathSize.size(); i++) {
					
					String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
					WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
					partsElement.clear();
					partsElement.clear();
					partsElement.sendKeys("00");
					Thread.sleep(500);

				}
				hideKeyBoard(mechanicDriver);*/
				
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(1000);

				System.out.println("Technician::Clicked Submit estimate :::");
				writeReport(LogStatus.PASS,	"Technician::Parts Amount:::" +00 + " entered ");
		}
}

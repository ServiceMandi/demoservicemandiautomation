package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.android.AndroidKeyCode;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateBreakDownRequest extends Configuration {

	public CreateBreakDownRequest() {

	}

	public String breakDownNewRequest(ExcelInputData excelInputData,boolean isVerified) throws InterruptedException, IOException {
		
			Thread.sleep(4000);
			String orderId = "";
			System.out.println("In Create New Break Down Request method ::: ");
			fleetManagerWait(LocatorPath.liveOrderTab);
			clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			Thread.sleep(8000);
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(6000);
			System.out.println("Entered Create New Break Down Request ::: ");
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(1000);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectVehicleNo);
			System.out.println("Entered Vehicle No  ::: ");
			fleetManagerWait(LocatorPath.selectDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverNumber());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			System.out.println("Entered Driver No  ::: ");
			fleetManagerWait(LocatorPath.selectLocation);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			System.out.println("Entered Location ::: ");

			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.breakdownButton);
						
			/*String CloseIcon="//html/body/ion-app/ion-modal/div/page-book-appointment/ion-content/div[2]/ion-buttons/button";
								
			fleetManagerWait(CloseIcon);
			clickFMElement(LocatorType.XPATH,CloseIcon);*/
			
			
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			//Thread.sleep(6000);

			fleetManagerWait(LocatorPath.requestOrderIdPath);
			orderId = getFMWebElement(LocatorType.XPATH, LocatorPath.requestOrderIdPath).getText().replaceAll("[^0-9]","");

			
			fleetManagerWait(LocatorPath.manageOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
			Thread.sleep(5000);
			writeReport(LogStatus.PASS, "FM: Order Id ::" + orderId);
			writeReport(LogStatus.PASS, "FM::New Order created");
			
			return orderId;
		
		
	}

	public String createNewBreakDownInvalidVehicleNo(ExcelInputData excelInputData) throws InterruptedException {

		String orderId = "";
		try {
			Thread.sleep(5000);
			System.out.println("Entered Create NewRequest-breakdown method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			Thread.sleep(3000);

			// Vehicle Number validation:

			WebElement vehicleValidnMsgElement = fleetManagerDriver.findElement(
					By.xpath("//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/p"));

			String inValidVehicleMessage = vehicleValidnMsgElement.getText();
			System.out.println("vehicleValidnMsgElement :::::" + inValidVehicleMessage);

			if (inValidVehicleMessage.contains("Vehicle does not exist.")) {
				System.out.println("vehicleValidnMsg" + inValidVehicleMessage);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				writeReport(LogStatus.FAIL,
						"FM: Vehicle Number :::" + excelInputData.getVehicleNumber() + "  doesnt exists in DB");
			}

		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "FM::CreateNewBreakDownInvalidVehicleNo:::Failed");
			saveReport();
		}
		return orderId;
	}

	public String createNewBreakDownRequest(ExcelInputData excelInputData) throws InterruptedException {
		String orderId = "";
		try {
			Thread.sleep(5000);
			System.out.println("Entered CreateNewRequest-breakdown method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			Thread.sleep(1000);
			// hideKeyBoard(fmDriver);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.breakdownButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.breakdownButton);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			Thread.sleep(5000);
			writeReport(LogStatus.PASS,"FM: BREAKDOWN -New Order created");
			fleetManagerWait(LocatorPath.manageOrder);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,
					"FM: BREAKDOWN -New Order creation failed");
		}
		return orderId;

	}
	
	
	
	public String bdVehicleNoAlreadyExists(ExcelInputData excelInputData) throws InterruptedException {
		
		String orderId = "";
		try {
			Thread.sleep(6000);
			System.out.println("Entering already existing Vehicle Number for New order creation::: ");
			
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			Thread.sleep(3000);

			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			Thread.sleep(1000);
			// hideKeyBoard(fmDriver);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.breakdownButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.breakdownButton);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			Thread.sleep(5000);

			// Vehicle Number already in Progress validation:

			String Duplicatemsg = "/html/body/ion-app/ion-alert/div";
			fleetManagerWait(Duplicatemsg);
			String Duplicatemsg1 = fleetManagerDriver.findElement(By.xpath(Duplicatemsg)).getText();
			WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(Duplicatemsg));
			if (AlertMsg1.isDisplayed()) {
				Thread.sleep(3000);// Msg can be taken to excel
				System.out.println("Inside Alert");
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(6000);

				writeReport(LogStatus.FAIL, "FM: Captured message:::"
						+ excelInputData.getVehicleNumber() + "Already Exists. Message :: " + Duplicatemsg1 + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,
					"FM: BD -NEW REQUEST-Vehicle No " + excelInputData.getVehicleNumber() + "Already Exists");
		}
		return orderId;

	}

}

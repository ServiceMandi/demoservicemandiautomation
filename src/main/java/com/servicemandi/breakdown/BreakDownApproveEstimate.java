package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Driver;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidKeyCode;

public class BreakDownApproveEstimate extends Configuration {

	public BreakDownApproveEstimate() {

	}

	public void driverApproveEstimate(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in driverApproveEstimatewithPayCash try"+isVerified);
		try {
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM: Driver-Approve Estimate method Passed");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL, "FM: Driver-Approve Estimate method Failed");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL, "FM: Driver-Approve Estimate method Failed");
			isVerified=false;
			}
	}

	public void driverAppEstwithPayCash(ExcelInputData excelInputData, FleetManager fleetManager, Driver driver,boolean isVerified)
			throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in driverAppEstwithPayCash try"+isVerified);
		try {Thread.sleep(1000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);

			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM: Driver-PayCash method Passed");
			fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL, "FM: Driver-PayCash method Failed");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL, "FM: Driver-PayCash method Failed");
			isVerified=false;
			}
	}

	public void driverRevisedApproveEstimate(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in driverRevisedApproveEstimatewithPayCash try"+isVerified);
		try {Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			System.out.println("Clicked Approve Revised Estimate method ::");
			writeReport(LogStatus.PASS, "FM: Driver-Approve Revised Estimate Passed");
			Thread.sleep(1000);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL,  "FM: Driver-Approve Revised Estimate Failed");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL,  "FM: Driver-Approve Revised Estimate Failed");
			isVerified=false;
			}
	}

	public void approveEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println(" in approveEstimateWithPayCash ");
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			System.out.println("Clicked Approve Estimate method ::");
			writeReport(LogStatus.PASS, "FM::Approve Estimate method Passed");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			Thread.sleep(4000);
			System.out.println("Clicked PayCash method ::");
			writeReport(LogStatus.PASS, "FM::PayCash method Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		
	}

	public void approveEstimateCancelAllJobs(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		if(isVerified) {
			System.out.println("isVerified in approveEstimateCancelAllJobs try"+isVerified);
		try {
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Entered Approve Estimate method ::");
			String expected = excelInputData.getBdJobSelection();
			System.out.println("expected" + expected);

			String[] outerArray = expected.split(",");
			int outerArrayLength = outerArray.length;
			System.out.println("outerArrayLength" + outerArrayLength);
			
			for (int i = 1; i <= outerArrayLength; i++) {

				String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[" + i + "]/ion-col[4]/ion-checkbox/div";
				System.out.println("Cancel jobs path ::" + cancelCheckBox);
				WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
				Actions action = new Actions(fleetManagerDriver);
				action.moveToElement(cancelElement).click().perform();
				Thread.sleep(1000);
			}

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "FM::Approve Estimate Cancel All jobs::: Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;}
			catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "FM::Approve Estimate Cancel All jobs:: Failed");
				isVerified=false;
			}}else {
				writeReport(LogStatus.FAIL, "FM::Approve Estimate Cancel All jobs:: Failed");
				isVerified=false;
				}
		}

	public void approveEstimateCancelOneJob(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("in approveEstimateCancelOneJobs ");
	
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			
			Thread.sleep(4000);
			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			System.out.println("Cancel jobs path ::" + cancelCheckBox);
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(1000);
			
						
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			fleetManagerWait(yesBtn);
			clickFMElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			System.out.println("Clicked Cancel Order");
			Thread.sleep(3000);
			writeReport(LogStatus.PASS, "FM::Approve Estimate- Cancel OneJob method:: Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			
	}
	
	public void approveEstimateWithoutPayCash(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("In ApproveEstimateWithoutPayCash try");
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			System.out.println("Clicked Approve Estimate:");
			writeReport(LogStatus.PASS, "FM::Approve Estimate Without PayCash method Passed");
			Thread.sleep(1000);
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		
	}
	
	public void driverApproveEstimateWithoutPayCash(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver, boolean isVerified) throws InterruptedException {
		if(isVerified) {
			System.out.println("isVerified in driverApproveEstimateWithoutPayCash try"+isVerified);
		try {
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM::Driver-Approve Estimate Without PayCash method Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,"FM::Driver-Approve Estimate Without PayCash method Failed");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL, "FM::Driver-Approve Estimate Without PayCash method Failed");
			isVerified=false;
			}
	}

	public void approveEstimateCancelOrder(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("in approveEstimateCancelOrder");
		
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
			System.out.println("Entered Cancel Order method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.ApproveEstChkbox);
			clickFMElement(LocatorType.XPATH, LocatorPath.ApproveEstChkbox);
			try {
				Thread.sleep(2000);
				WebElement ApproveestChkboxElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.ApproveEstChkbox));
				Thread.sleep(2000);
				if (ApproveestChkboxElement.isSelected()) {
					System.out.println("Approve Estimate checkbox is Unchecked");
				}

			} catch (Exception e) {
				System.out.println("Approve Estimate checkbox is not Unchecked");
				//writeReport(LogStatus.FAIL, "Fleet manager Approve estimate Cancel Order checkbox click failed");
			}
			fleetManagerWait(LocatorPath.CancelOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.CancelOrder);
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			fleetManagerWait(yesBtn);
			clickFMElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.PASS, "FM::Approve Estimate CancelOrder method Passed");
			Thread.sleep(1000);
			//fleetManagerWait(LocatorPath.liveOrderTab);
			
	}

	public void approveEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in approveEstimateWithAlert try"+isVerified);
	
		try {
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			System.out.println("Clicked Approve Estimate method ::");
			writeReport(LogStatus.PASS, "FM::Approve estimate Passed");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM::No Retailer Alert clicked Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL,"FM::Approve Estimate With Alert method Failed");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "FM::Approve Estimate With Alert method Failed");
			isVerified=false;
			}
	}

	public void ApproveRevisedEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
			throws InterruptedException {
		
			System.out.println("in ApproveRevisedEstimateWithPayCash ");
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(), isVerified);
			System.out.println("Entered Approve Revised Estimate With Pay cash method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS,	"FM:::Approve Revised Estimate method Passed");
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			System.out.println("Clicked Pay cash method ::");
			writeReport(LogStatus.PASS,	"FM:::PayCash method Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		
	}


	public void ApproveRevisedEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified) throws InterruptedException, IOException {

		if(isVerified) {
			System.out.println("isVerified in ApproveRevisedEstimateWithAlert try"+isVerified);
		try {
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered ApproveRevisedEstimateWithAlert method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			System.out.println("Clicked Approve Estimate");
			writeReport(LogStatus.PASS, "FM:::Approve RevisedEstimate method Passed");
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM:::No Retailer Alert method Passed");
			//fleetManagerWait(LocatorPath.liveOrderTab);
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL,"FM:::Approve RevisedEstimate With Alert method Failed");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "FM:::Approve RevisedEstimate With Alert method Failed");
			isVerified=false;
			}
	}
	public void ApproveRevisedEstimateWithoutPay(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("isVerified in ApproveRevisedEstimateWithoutPay try"+isVerified);
		try {
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			System.out.println("Clicked ApproveRevisedEstimate method");
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "FM::Approve Revised Estimate Without Pay method:: Passed");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			FM_TakeClick();
			writeReport(LogStatus.FAIL, "FM::Approve Revised Estimate Without Pay method:: Failed");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "FM::Approve Revised Estimate Without Pay method Failed");
			isVerified=false;
			}
	}

	public void driverApproveRevisedEstimateWithoutPay(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException {
		if(isVerified) {
			System.out.println("isVerified in driverApproveRevisedEstimateWithoutPay try"+isVerified);
		try {
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			System.out.println("Clicked Approve Revised Estimate method ::");
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS,"FM::Driver-Approve Revised Estimate Without Pay method Passed");
			// driver.pushNotification();
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,"FM::Driver-Approve Revised Estimate Without Pay method Failed");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL, "FM::Driver-Approve Revised Estimate Without Pay method Failed");
			isVerified=false;
			}
	}
}

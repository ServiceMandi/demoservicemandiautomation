package com.servicemandi.workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class BreakDownFlow {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateBreakDownRequest breakDownRequest;
	private BreakDownTrackLocation trackLocation;
	private BreakDownApproveEstimate approveEstimate;
	private BreakDownStartJob startJob;
	private String mRequestOrderId = "";

	@BeforeTest
	public void setUpConfiguration() {

		try {

			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			breakDownRequest = new CreateBreakDownRequest();
			trackLocation = new BreakDownTrackLocation();
			approveEstimate = new BreakDownApproveEstimate();
			startJob = new BreakDownStartJob();
			Configuration.reportConfiguration("BDBaseFlow");
			Configuration.createNewWorkFlowReport("Login Details");
			System.out.println("AL Enter Login method ::::");
			fleetManager.login(Utils.excelInputDataList.get(1),isVerified);
			mechanic.login(Utils.excelInputDataList.get(1),isVerified);
			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	@BeforeClass
	public void beforeClass() {

	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	@Test
	public void serviceMandiDemo() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== ServiceMandi Automation Demo=====");
				
				Configuration.createNewWorkFlowReport("serviceMandiDemo");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithPartsGiveEstimateCompleteOrder() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown With Parts GiveEstimate CompleteOrder=====");
				
				
				Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimateCompleteOrder");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				mechanic.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(0), mRequestOrderId);
				mechanic.myPreviousOrder(Utils.excelInputDataList.get(0), mRequestOrderId);

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithPartsGiveEstimate() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown With Parts GiveEstimate =====");
				Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimate");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void BDFMCancelAllJobsInApproveEstimate() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown Parts Cancel All Jobs =====");
				Configuration.createNewWorkFlowReport("BDFMCancelAllJobsInApproveEstimate");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithMultiJobs(Utils.excelInputDataList.get(0));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(0), fleetManager);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {

		}
	}
	//Completed
	@Test
	public void BDWithoutPartsFMCancelJobInAproveEstimate() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BDWithoutPartsFMCancelJobInAproveEstimate =====");
				Configuration.createNewWorkFlowReport("BDWithoutPartsFMCancelJobInAproveEstimate");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(0), fleetManager);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownFMCancelAtConfirmGarage() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down FM Cancel At Confirm Garage =====");
				Configuration.createNewWorkFlowReport("BreakDownFMCancelAtConfirmGarage");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				/*
				 * fleetManager.viewBill(Utils.excelInputDataList.get(0));
				 * mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				 * mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				 * fleetManager.fleetManagerRating(Utils.excelInputDataList.get(
				 * 1));
				 */

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownWithPartsNoRetailerRevisedEstimateWithoutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts NoRetailer Revised Estimate Without Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsNoRetailerRevisedEstimateWithoutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(0), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
				System.out.println(
						"===== Break Down With Parts NoRetailer Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithoutPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Estimate With Parts 8=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedEstimateWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(5));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(5));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(5), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(5), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(5));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(5), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(5));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithoutPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Estimate Without Parts 12=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedEstimateWithOutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(4));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(4));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(4), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(4));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(4), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(4), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(4), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(4), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(4));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(4));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(4));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Bill 16=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownPartsRevisedWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Parts Revised Estimate Parts RevisedBill 22=====");
				Configuration.createNewWorkFlowReport("breakDownPartsRevisedWithPartsRevisedBill");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(7));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(7), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(7));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//Completed
	@Test
	public void breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill 22=====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(9));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(9));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(9), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(9), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(9), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(9));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown Without Parts Revised Estimate Without Parts Revised Bill 24=====");
				Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(8));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(8));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(8), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(8), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(8), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(8), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(8));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8));

			}
		} catch (Exception e) {
		}
	}
	// 3rd iteration Workflowsover-rest to be taken from SANTA.

	// Starting Rest of the Breakdown Flows from here:
	/// Duplicate flow
	//Completed
	@Test
	public void breakDownWithoutPartsRevisedEstimateWithPartsRevisedbill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill 21=====");
				Configuration.createNewWorkFlowReport("BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(10));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(10));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(10), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(10));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(10), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(10), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(10));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(10), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(10));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(10));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(10));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownWithPartsWithOutRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== break Down WithParts With OutRetailer =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsWithOutRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Bill =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedBill");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithOutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With out Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(2));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2));
				// trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(2), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(2), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With out Parts Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Estimate With out Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(6));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(6));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(6), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(6), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(6));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(6), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(6), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(6), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(6), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(6));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(6));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(6));

			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void breakDownWithPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(3));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(3));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(3), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(3), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(3));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(3), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(3));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(3));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3));

			}
		} catch (Exception e) {
		}
	}
	//Completed
	@Test
	public void breakDownMechanicRejectsOrder() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Mechanic Rejects Order =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicRejectOrder();

			}
		} catch (Exception e) {

		}
	}
	//Completed
	@Test
	public void BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 35-NS=====");
				Configuration.createNewWorkFlowReport("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 35-NS");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithMultiJobsWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//Completed
	@Test
	public void BDFMDeletesAtVehicleInGarage() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDFMDeleteAtVehicleInGarage=====");
				Configuration.createNewWorkFlowReport("BDFMDeleteAtVehicleInGarage");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}
//Completed
	@Test
	public void BDMechanicDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {

		}
	}
//Completed
	@Test
	public void BDFMDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {

		}
	}
	//Completed
	@Test
	public void BDFMDeletesAtFindGarage() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDFMDeletesAtFindGarage 38=====");
				Configuration.createNewWorkFlowReport("BDFMDeletesAtFindGarage 38");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}
}

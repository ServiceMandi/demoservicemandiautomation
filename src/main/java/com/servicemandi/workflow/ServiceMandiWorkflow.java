package com.servicemandi.workflow;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.common.Driver;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.driver.DriverCreateBreakdownRequest;
import com.servicemandi.driver.DriverCreateServiceRequest;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;
import com.sevicemandi.servicerequest.ServiceRequestApproveEstimate;
import com.sevicemandi.servicerequest.ServiceRequestStartJob;
import com.sevicemandi.servicerequest.ServicesRequestTrackLocation;

import jxl.read.biff.BiffException;

public class ServiceMandiWorkflow<ReqNumber> extends SendMail {


// ----------------------Start BDBaseFlow------------------------------
private FleetManager fleetManager;
private Mechanic mechanic;
private CreateBreakDownRequest breakDownRequest;
private BreakDownTrackLocation trackLocation;
private BreakDownApproveEstimate approveEstimate;
private BreakDownStartJob startJob;
private String mRequestOrderId;
private String ReqNumber = "";
WebElement myAccount;
private String locator ="";
private String Status="";
int j=1;
String newWorkFlow = null;
private boolean executeValue = true;
   

   
		public void basicBDBaseFlow() {
		// for(i=1;i<=2;i++){
		// if(i==2){
		// i=21;
		// } 
//		j=j+20;
//		System.out.println("J value :"+j);
		try {
		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
		ReadingExcelData.readingWorkFlowData();
		fleetManager = new FleetManager();
		mechanic = new Mechanic();
		breakDownRequest = new CreateBreakDownRequest();
		trackLocation = new BreakDownTrackLocation();
		approveEstimate = new BreakDownApproveEstimate();
		startJob = new BreakDownStartJob();
		// Configuration.reportConfiguration("BreakDownReport");
		// Configuration.createNewWorkFlowReport("Login Details");
		// fleetManager.login(Utils.excelInputDataList.get(1));
		// mechanic.login(Utils.excelInputDataList.get(1));
		// Configuration.closeWorkFlowReport();
		
		} catch (Exception e) {
		
		}
		//return j;
		}

		public void breakDownWithParts() throws Exception {
			
				/*int i=0; 
				for(i=1;i<=2;i++){
				if(i==2){
				i=21;
				}*/
				try {
				basicBDBaseFlow();
				   
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
				System.out.println("===== Break Down WithParts Normal Flow =====");
				Configuration.reportConfiguration("BreakDownWithParts");
				Configuration.createNewWorkFlowReport("Login Details");
				fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
				mechanic.login(Utils.excelInputDataList.get(1), isVerified);
				Configuration.closeWorkFlowReport();

				 Configuration.createNewWorkFlowReport("BreakDownWithParts");
				 mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
				 mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
				 fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
				 trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
				 trackLocation.completeOrYes(isVerified);
				 trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic, isVerified);
				 trackLocation.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
				 approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager,isVerified);
				 startJob.startJobs(Utils.excelInputDataList.get(1), mechanic, isVerified);
				 fleetManager.viewBill(Utils.excelInputDataList.get(1),isVerified);
				 mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
				 mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
				 mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId,isVerified);
				 mechanic.MECHLogout(Utils.excelInputDataList.get(1));
				 fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
				 fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
				 fleetManager.FMLogout(Utils.excelInputDataList.get(1));
				Status=Configuration.closeWorkFlowReport();
				}
				}
				}catch (Exception e) {
				e.printStackTrace();
				Configuration.catchcall();
				} finally {
				Configuration.saveReport(); 
				SendMail("BreakDownWithParts");
				}
				/*try{
				if(Status.equals("Pass")) {
				SendMail("BreakDownWithParts");
				break;
				}
				}catch(Exception e){
				mechanic.MECHLogoutfrmMyAccLink(Utils.excelInputDataList.get(i));
				fleetManager.FMLogoutfrmMyAccLink(Utils.excelInputDataList.get(i));
				}*/
			} 
	//}

	

	public void breakDownWithOutParts() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Break Down WithoutParts =====");
					newWorkFlow=Configuration.reportConfiguration("BreakDownWithOutParts");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithOutParts");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(2), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(2), fleetManager,
							isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(2), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(2), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithOutParts");
		}
	}

	public void BDWithPartsRevisedBill() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Break Down With Parts Revised Bill =====");
					Configuration.reportConfiguration("BreakDownWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithPartsRevisedBill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager,isVerified);
					startJob.BDRevisedBill(Utils.excelInputDataList.get(5), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(5), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithPartsRevisedBill");
		}
	}

	public void BDWithPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====Break Down With Parts Revised Estimate WithParts=====");
				Configuration.reportConfiguration("BDWithPartsRevisedEstimateWithParts");
				Configuration.createNewWorkFlowReport("Login Details");
				fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
				mechanic.login(Utils.excelInputDataList.get(1), isVerified);
				Configuration.closeWorkFlowReport();

				Configuration.createNewWorkFlowReport("BDWithPartsRevisedEstimateWithParts");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
				mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
				trackLocation.completeOrYes(isVerified);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic, isVerified);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager, isVerified);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic,isVerified);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager,
						isVerified);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic, isVerified);
				fleetManager.viewBill(Utils.excelInputDataList.get(1), isVerified);
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
				mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
				mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
				mechanic.MECHLogout(Utils.excelInputDataList.get(1));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
				fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue,mRequestOrderId1, isVerified);
				fleetManager.FMLogout(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("BDWithPartsRevisedEstimateWithParts");
		}
	}

	public void BDWithoutPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown WithoutParts RevisedEstimate WithParts 8=====");
				Configuration.reportConfiguration("BDWithoutPartsRevisedEstimateWithParts");
				Configuration.createNewWorkFlowReport("Login Details");
				fleetManager.login(Utils.excelInputDataList.get(7), isVerified);
				mechanic.login(Utils.excelInputDataList.get(7), isVerified);
				Configuration.closeWorkFlowReport();

				Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithParts");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(7), isVerified);
				mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7), isVerified);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic, isVerified);
				trackLocation.completeOrYes(isVerified);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(7), isVerified);
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(7), fleetManager,
						isVerified);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(7), mechanic, isVerified);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager,
						isVerified);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(7), mechanic, isVerified);
				fleetManager.viewBill(Utils.excelInputDataList.get(7), isVerified);
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7), isVerified);
				mechanic.mechanicRating(Utils.excelInputDataList.get(7), isVerified);
				mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(7), mRequestOrderId1, isVerified);
				mechanic.MECHLogout(Utils.excelInputDataList.get(7));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7), isVerified);
				fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(7),mechactualvalue, mRequestOrderId1, isVerified);
				fleetManager.FMLogout(Utils.excelInputDataList.get(7));
				Configuration.closeWorkFlowReport();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsRevisedEstimateWithParts");
		}
	}

	public void BDWithPartsRevisedEstimateWithOutParts() throws Exception {
		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithPartsRevisedEstimateWithOutParts =====");
					Configuration.reportConfiguration("BDWithPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithPartsRevisedEstimateWithOutParts");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(8), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(8), isVerified);
					approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(8), fleetManager,
							isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(8), mechanic, isVerified);
					approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(8), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(8), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(8), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("BDWithPartsRevisedEstimateWithOutParts");
		}
	}

	public void BDWithoutPartsRevisedEstimateWithOutParts() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown WithoutParts RevisedEstimate WithoutParts 12=====");
					Configuration.reportConfiguration("BDWithoutPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(9), isVerified);
					mechanic.login(Utils.excelInputDataList.get(9), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithOutParts");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(9), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(9), fleetManager,
							isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(9), mechanic, isVerified);
					approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(9), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(9), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(9), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(9), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(9), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(9));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(9),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(9));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsRevisedEstimateWithOutParts");
		}
	}

	public void BDWithoutPartsRevisedBill() throws Exception {

		try {
			basicBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BD WithoutParts RevisedBill 16=====");
					Configuration.reportConfiguration("BDWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(10), isVerified);
					mechanic.login(Utils.excelInputDataList.get(10), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedBill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(10), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(10), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(10), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(10), fleetManager,isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(10), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(10), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(10), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(10), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(10));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(10), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(10),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(10));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsRevisedBill");
		}
	}

	// BD11
	public void BDWithPartsRevisedWithPartsRevisedBill() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithPartsRevisedWithPartsRevisedBill 22=====");
					Configuration.reportConfiguration("BDWithPartsRevisedWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(11), isVerified);
					mechanic.login(Utils.excelInputDataList.get(11), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithPartsRevisedWithPartsRevisedBill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(11), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(11), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(11), fleetManager,
							isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(11), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(11), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(11), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(11), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(11), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(11), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(11));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(11), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(11),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(11));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithPartsRevisedWithPartsRevisedBill");
		}
	}

	// BD12
	public void BDWithoutPartsRevisedEstimateWithPartsRevisedbill() throws Exception {

		try {
			basicBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithoutPartsRevisedEstimateWithPartsRevisedbill 21=====");
					Configuration.reportConfiguration("BDWithoutPartsRevisedEstimateWithPartsRevisedbill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(12), isVerified);
					mechanic.login(Utils.excelInputDataList.get(12), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithPartsRevisedbill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(12), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(12), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(12), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(12), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(12), fleetManager,
							isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(12), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(12), isVerified);
					approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(12), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(12), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(12), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(12), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(12), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(12), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(12));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(12), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(12),mechactualvalue, mRequestOrderId1, isVerified);
				    fleetManager.FMLogout(Utils.excelInputDataList.get(12));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsRevisedEstimateWithPartsRevisedbill");
		}
	}

	// BD13
	public void BDWithPartsRevisedEstimateWithOutPartsRevisedBill() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithPartsRevisedEstimateWithOutPartsRevisedBill 22=====");
					Configuration.reportConfiguration("BDWithPartsRevisedEstimateWithOutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(13), isVerified);
					mechanic.login(Utils.excelInputDataList.get(13), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithPartsRevisedEstimateWithOutPartsRevisedBill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(13), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(13), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(13), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(13), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(13), isVerified);
					approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(13), fleetManager,
							isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(13), mechanic, isVerified);
					approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(13), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(13), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(13), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(13), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(13), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(13), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(13));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(13), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(13),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(13));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithPartsRevisedEstimateWithOutPartsRevisedBill");
		}
	}

	// BD14
	public void BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill 24=====");
					Configuration.reportConfiguration("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(14), isVerified);
					mechanic.login(Utils.excelInputDataList.get(14), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(14), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(14), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(14), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(14), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(14), fleetManager,
							isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(14), mechanic, isVerified);
					approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(14), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(14), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(14), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(14), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(14), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(14), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(14));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(14), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(14),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(14));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
		}
	}

	// BD15
	public void breakDownWithPartsGiveEstimateCompleteOrder() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown With Parts GiveEstimate CompleteOrder=====");
					Configuration.reportConfiguration("BreakDownWithPartsGiveEstimateCompleteOrder");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimateCompleteOrder");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
					mechanic.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(1), mRequestOrderId, isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithPartsGiveEstimateCompleteOrder");
		}
	}

	// BD16
	public void breakDownWithPartsGiveEstimate() throws Exception {

		try {
			basicBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown With Parts GiveEstimate =====");
					Configuration.reportConfiguration("BreakDownWithPartsGiveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimate");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager,
							isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(1), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithPartsGiveEstimate");
		}
	}

	public void BDMechanicDeletesAtTrackLoc() throws Exception {
		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
					Configuration.reportConfiguration("BDMechanicDeletesAtTrackLoc");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(3), isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDMechanicDeletesAtTrackLoc");
		}
	}

	private void moveToNextFlow(Object newWorkFlow) {
		// TODO Auto-generated method stub

	}

	// ----------------------End BDBaseFlow------------------------------

	// ----------------------Start BDCancelRejectFlow----------------------

	public void BDFMDeletesAtFindGarage() throws Exception {
		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDFMDeletesAtFindGarage 38=====");
				Configuration.reportConfiguration("BDFMDeletesAtFindGarage");
				Configuration.createNewWorkFlowReport("Login Details");
				fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
				mechanic.login(Utils.excelInputDataList.get(1), isVerified);
				Configuration.closeWorkFlowReport();

				Configuration.createNewWorkFlowReport("BDFMDeletesAtFindGarage");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(1),locator, isVerified);
				fleetManager.FMLogout(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("BDFMDeletesAtFindGarage");
		}
	}

	public void breakDownFMCancelAtConfirmGarage() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Break Down FM Cancel At Confirm Garage =====");
					Configuration.reportConfiguration("BreakDownFMCancelAtConfirmGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownFMCancelAtConfirmGarage");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(2), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(2),locator,isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(2));
					mechanic.MECHLogout(Utils.excelInputDataList.get(2));
					/*
					 * fleetManager.viewBill(Utils.excelInputDataList.get(0));
					 * mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
					 * mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					 * fleetManager.fleetManagerRating(Utils.excelInputDataList.get( 1));
					 */
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownFMCancelAtConfirmGarage");
		}
	}

	public void BDFMDeletesAtTrackLoc() throws Exception {
		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====BDFMDeletesAtTrackLoc 47=====");
					Configuration.reportConfiguration("BDFMDeletesAtTrackLoc");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDFMDeletesAtTrackLoc");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(4), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(4), isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(4),locator ,isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(4), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(4), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(4), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(4));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(4), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(4),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("BDFMDeletesAtTrackLoc");
		}
	}

	public void BDFMDeletesAtVehicleInGarage() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====BDFMDeleteAtVehicleInGarage=====");
					Configuration.reportConfiguration("BDFMDeleteAtVehicleInGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDFMDeleteAtVehicleInGarage");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(3), isVerified);
					trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(3), mechanic, isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(3),locator, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(3), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDFMDeleteAtVehicleInGarage");
		}
	}

	public void BDWithoutPartsFMCancelJobInAproveEstimate() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BDWithoutPartsFMCancelJobInAproveEstimate =====");
					Configuration.reportConfiguration("BDWithoutPartsFMCancelJobInAproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithoutPartsFMCancelJobInAproveEstimate");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(5), fleetManager,isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(5), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithoutPartsFMCancelJobInAproveEstimate");
		}
	}

	// BDCR6
	public void BDFMCancelAllJobsInApproveEstimate() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown Parts Cancel All Jobs =====");
					Configuration.reportConfiguration("BDFMCancelAllJobsInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDFMCancelAllJobsInApproveEstimate");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1),mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(1), fleetManager,isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDFMCancelAllJobsInApproveEstimate");
		}
	}

	// BDCR7
	public void breakDownMechanicRejectsOrder() throws Exception {
		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown Mechanic Rejects Order =====");
					Configuration.reportConfiguration("BreakDown Mechanic Rejects Order");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(7), isVerified);
					mechanic.login(Utils.excelInputDataList.get(7), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("Break Down Mechanic Rejects Order");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(7), isVerified);
					mechanic.mechanicRejectOrder(Utils.excelInputDataList.get(7),isVerified);
					mechanic.MECHLogoutwithoutback(Utils.excelInputDataList.get(7));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownMechanicRejectsOrder");
		}
	}
	// BDCR9

	public void BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() throws Exception {
	try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 35-NS=====");
					Configuration.reportConfiguration("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(8),isVerified);
					approveEstimate.approveEstimateCancelOneJob(Utils.excelInputDataList.get(8), fleetManager,isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(8), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(8), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue,mRequestOrderId1,isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
		}
	}
		

	// -------------------End BD CancelReject Flow-------------------------------------------

	// ------------------Start BD Negative Flow-------------------------------------------

	// NBD1
	public void breakDownWithPartsWithOutRetailer() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown WithParts WithOutRetailer =====");
					Configuration.reportConfiguration("BreakDownWithPartsWithOutRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(9), isVerified);
					mechanic.login(Utils.excelInputDataList.get(9), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithPartsWithOutRetailer");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(9), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(9), fleetManager, isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(9), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(9), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(9), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(9), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(9));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(9),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(9));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithPartsWithOutRetailer");
		}
	}

	// NBD2
	public void breakDownWithPartsNoRetailerRevisedEstimateWithoutParts() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDown WithParts NoRetailer RevisedEstimate WithoutParts =====");
					Configuration.reportConfiguration("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(10), isVerified);
					mechanic.login(Utils.excelInputDataList.get(10), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(10), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(10), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(10), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(10), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(10), fleetManager, isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(10), mechanic, isVerified);
					approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(10), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(10), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(10), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(10), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(10), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(10), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(10));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(10), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(10),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(10));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownFMCancelAtConfirmGarage");
		}
	}

	// NBD3
	public void BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
				if (isVerified) {
					System.out
							.println("===== BreakDown WithParts NoRetailer RevisedEstimate WithParts NoRetailer =====");
					Configuration.reportConfiguration("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(11), isVerified);
					mechanic.login(Utils.excelInputDataList.get(11), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(11), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(11), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(11), fleetManager, isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(11), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(11), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(11), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(11), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(11), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(11), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(11));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(11), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(11),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(11));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
		}
	}

	// NBD4
	public void breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer =====");
					Configuration.reportConfiguration("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(12), isVerified);
					mechanic.login(Utils.excelInputDataList.get(12), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
					mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(12), isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(12), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(12), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(12), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(12), fleetManager,
							isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(12), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(12), isVerified);
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(12), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(12), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(12), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(12), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(12), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(12), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(12));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(12), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(12),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(12));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");

		}
	}

	// -------------------End BD Negative Flow----------------------------

	// ***************************Start of SRBase Flow*********************

	private FleetManager fleetManager1;
	private Mechanic mechanic1;
	private Driver driver1;
	private BrowserTest browser1;

	private DriverCreateServiceRequest driverCreateServiceRequest1;
	private CreateServiceRequest createServiceRequest;
	private ServicesRequestTrackLocation trackLocation1;
	private ServiceRequestApproveEstimate approveEstimate1;
	private ServiceRequestStartJob startJob1;
	private String mRequestOrderId1 = "";
	private String mechactualvalue;
	private String fmactualvalue;
	private String finalBillAmount = "";
	private String cumulativeAmount = "";
	private ProfileCreation mProfileCreation;
	private Configuration mConfiguration;
	boolean isVerified = true;

	public void basicSRBaseFlow() throws BiffException, IOException {

		try {
			org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
			ReadingExcelData.readingWorkFlowData();
			fleetManager1 = new FleetManager();
			mechanic1 = new Mechanic();
			driver1 = new Driver();
			browser1 = new BrowserTest();

			driverCreateServiceRequest1 = new DriverCreateServiceRequest();
			createServiceRequest = new CreateServiceRequest();
			trackLocation1 = new ServicesRequestTrackLocation();
			approveEstimate1 = new ServiceRequestApproveEstimate();
			startJob1 = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			mConfiguration = new Configuration();

			/*
			 * Configuration.reportConfiguration("ServiceRequestReports");
			 * Configuration.createNewWorkFlowReport("Login Details");
			 * fleetManager1.login(Utils.excelInputDataList.get(1));
			 * mechanic1.login(Utils.excelInputDataList.get(1));
			 * Configuration.closeWorkFlowReport();
			 */

		} catch (Exception e) {

		}
	}

	// SR1:
	public void SRWithParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR With Parts Normal Flow =====");
					Configuration.reportConfiguration("SRWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					System.out.println("mechactualvalue:::"+mechactualvalue);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1), mechactualvalue,mRequestOrderId1, isVerified);
					System.out.println("fmactualvalue:::::"+fmactualvalue);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithParts");
		}
	}
	
	// SR2:
	public void SRWithoutParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR WithoutParts=====");
					Configuration.reportConfiguration("SRWithoutParts");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithoutParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(2), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(2), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2), mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithoutParts");
		}
	}
	// SR3:
	public void SRWithPartsGiveEstimate() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsGiveEstimate =====");
					Configuration.reportConfiguration("SRWithPartsGiveEstimate");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsGiveEstimate");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(3), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimateReturnBack(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					trackLocation1.trackLocationWithCompleteYesNo(Utils.excelInputDataList.get(3), mechanic1,
							isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager1,
							isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3), mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsGiveEstimate");
		}
	}
	// SR5:

	public void SRWithPartsGiveEstimateCompleteOrder() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR With Parts GiveEstimate CompleteOrder=====");
					Configuration.reportConfiguration("SRWithPartsGiveEstimateCompleteOrder");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsGiveEstimateCompleteOrder");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(4), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					mechanic1.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(4));
					fleetManager1.FMLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsGiveEstimateCompleteOrder");
		}
	}
	// SR6

	public void SRWithPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsRevisedEstimateWithParts =====");
					Configuration.reportConfiguration("SRWithPartsRevisedEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(5), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(5), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager1,	isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager1,isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsRevisedEstimateWithParts");
		}
	}

	public void SRWithoutPartsReviseEstimateWithParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithoutPartsReviseEstimateWithParts =====");
					Configuration.reportConfiguration("SRWithoutPartsReviseEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithoutPartsReviseEstimateWithParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationWithOutParts(Utils.excelInputDataList.get(1), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithoutPartsReviseEstimateWithParts");
		}
	}

	public void SRWithPartsRevisedEstimateWithOutParts() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsRevisedEstimateWithOutParts =====");
					Configuration.reportConfiguration("SRWithPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(21), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(21), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithOutParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(21), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(21), fleetManager1, isVerified);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(21), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(21), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(21), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(21), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(21), fleetManager1,
							isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(21), mechanic1, isVerified);
					approveEstimate1.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(21), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(21), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(21), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(21), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(21), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(21), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(21));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(21), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(21),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(21));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsRevisedEstimateWithOutParts");
		}
	}

	public void SRWithoutPartsRevisedWithoutParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithoutPartsRevisedWithoutParts =====");
					Configuration.reportConfiguration("SRWithoutPartsRevisedWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithoutPartsRevisedWithoutParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(8), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(8), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationWithOutParts(Utils.excelInputDataList.get(8), isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					approveEstimate1.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(8), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithoutPartsRevisedWithoutParts");
		}
	}

	public void SRWithPartsRevisedEstimateWithPartsRevisedBill() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsRevisedEstimateWithPartsRevisedBill =====");
					Configuration.reportConfiguration("SRWithPartsRevisedEstimateWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(9), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithPartsRevisedBill");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(9), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(9), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(9), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(9),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager1,isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(9), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager1,isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(9), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(9), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(9), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(9));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(9), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(9),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(9));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsRevisedEstimateWithPartsRevisedBill");
		}
	}

	public void SRWithoutPartsRevisedEstimatePartsRevisedBill() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithoutPartsRevisedEstimatePartsRevisedBill =====");
					Configuration.reportConfiguration("SRWithoutPartsRevisedEstimatePartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(10), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithoutPartsRevisedEstimatePartsRevisedBill");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(10), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(10), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(10),isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(10), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(10), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(10), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(10));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(10), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(10),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(10));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithoutPartsRevisedEstimatePartsRevisedBill");
		}
	}

	public void SRWithPartsRevisedEstimateWithoutPartsRevisedBill() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsRevisedEstimateWithoutPartsRevisedBill =====");
					Configuration.reportConfiguration("SRWithPartsRevisedEstimateWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(11), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithoutPartsRevisedBill");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(11), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(11), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(11), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(11),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(11), fleetManager1,
							isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(11), mechanic1, isVerified);
					approveEstimate1.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(11), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(11), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(11), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(11), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(11));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(11), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(11),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(11));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsRevisedEstimateWithoutPartsRevisedBill");
		}
	}

	
	public void SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill() throws Exception {

			try {
				basicSRBaseFlow();

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					if (isVerified) {
						System.out.println("===== SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill 24=====");
						Configuration.reportConfiguration("SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
						Configuration.createNewWorkFlowReport("Login Details");
						fleetManager1.login(Utils.excelInputDataList.get(12), isVerified);
						mechanic1.login(Utils.excelInputDataList.get(12), isVerified);
						Configuration.closeWorkFlowReport();

						Configuration.createNewWorkFlowReport("SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
						createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(12), isVerified);
						createServiceRequest.selectJobs1(Utils.excelInputDataList.get(12), fleetManager1, isVerified);
						mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(12), isVerified);
						mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
						trackLocation1.giveEstimate(Utils.excelInputDataList.get(12), mechanic1, isVerified);
						trackLocation1.completeOrYes(isVerified);
						trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(12),isVerified);
						startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(12), mechanic1, isVerified);
						approveEstimate1.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(12), fleetManager1, isVerified);
						trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(12), mechanic1, isVerified);
						fleetManager1.viewBill(Utils.excelInputDataList.get(12), isVerified);
						mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(12), isVerified);
						mechanic1.mechanicRating(Utils.excelInputDataList.get(12), isVerified);
						mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(12), mRequestOrderId1, isVerified);
						mechanic1.MECHLogout(Utils.excelInputDataList.get(12));
						fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(12), isVerified);
						fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(12),mechactualvalue, mRequestOrderId1, isVerified);
						fleetManager1.FMLogout(Utils.excelInputDataList.get(12));
						Configuration.closeWorkFlowReport();
					}	
				}
			} catch (Exception e) {
				e.printStackTrace();
				Configuration.catchcall();
			} finally {
				Configuration.saveReport();
				SendMail("SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
			}
		}
	// CR's::

		public void SRWithPartsDownloadPDF_CR() throws Exception {

			try {
				basicSRBaseFlow();

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					if (isVerified) {
						System.out.println("===== SRWithPartsDownloadPDF_CR =====");
						Configuration.reportConfiguration("SRWithPartsDownloadPDF_CR");
						Configuration.createNewWorkFlowReport("Login Details");
						fleetManager1.login(Utils.excelInputDataList.get(4), isVerified);
						mechanic1.login(Utils.excelInputDataList.get(4), isVerified);
						Configuration.closeWorkFlowReport();

						Configuration.createNewWorkFlowReport("SRWithPartsDownloadPDF_CR");
						createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(4), isVerified);
						createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
						mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4), isVerified);
						mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
						trackLocation1.giveEstimate(Utils.excelInputDataList.get(4), mechanic1, isVerified);
						trackLocation1.completeOrYes(isVerified);
						trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(4),isVerified);
						trackLocation1.selectRetailer(Utils.excelInputDataList.get(4), isVerified);
						approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
						startJob1.startJobs(Utils.excelInputDataList.get(4), mechanic1, isVerified);
						fleetManager1.viewBillDownloadPDF(Utils.excelInputDataList.get(4), isVerified);
						mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(4), isVerified);
						mechanic1.mechanicRating(Utils.excelInputDataList.get(4), isVerified);
						mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
						mechanic1.MECHLogout(Utils.excelInputDataList.get(4));
						fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(4), isVerified);
						fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(4),mechactualvalue,mRequestOrderId1, isVerified);
						fleetManager1.FMLogout(Utils.excelInputDataList.get(4));
						Configuration.closeWorkFlowReport();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				Configuration.catchcall();
			} finally {
				Configuration.saveReport();
				SendMail("SRWithPartsDownloadPDF_CR");
			}
		}

		public void SRWithPartsSendMail_CR() throws Exception {

			try {
				basicSRBaseFlow();

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					if (isVerified) {
						System.out.println("===== SRWithPartsSendMail_CR =====");
						Configuration.reportConfiguration("SRWithPartsSendMail_CR");
						Configuration.createNewWorkFlowReport("Login Details");
						fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
						mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
						Configuration.closeWorkFlowReport();

						Configuration.createNewWorkFlowReport("SRWithPartsSendMail_CR");
						createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(5), isVerified);
						createServiceRequest.selectJobs1(Utils.excelInputDataList.get(5), fleetManager1, isVerified);
						mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
						mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
						trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
						trackLocation1.completeOrYes(isVerified);
						trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(5), isVerified);
						trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
						approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager1,
								isVerified);
						startJob1.startJobs(Utils.excelInputDataList.get(5), mechanic1, isVerified);
						fleetManager1.viewBillSendMail(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
						browser1.pdfinMail(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
						mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
						mechanic1.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
						mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
						mechanic1.MECHLogout(Utils.excelInputDataList.get(5));
						fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
						fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
						fleetManager1.FMLogout(Utils.excelInputDataList.get(5));
						Configuration.closeWorkFlowReport();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				Configuration.catchcall();
			} finally {
				Configuration.saveReport();
				SendMail("SRWithPartsSendMail_CR");
			}
		}

		public void SRDesktopWebVersion_CR() throws Exception {

			try {
				basicSRBaseFlow();

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					if (isVerified) {
						System.out.println("===== SRDesktopWebVersion_CR =====");
						Configuration.reportConfiguration("SRDesktopWebVersion_CR");
						Configuration.createNewWorkFlowReport("Login Details");
						fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
						mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
						Configuration.closeWorkFlowReport();

						Configuration.createNewWorkFlowReport("SRDesktopWebVersion_CR");
						createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
						createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
						mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
						mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
						trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
						trackLocation1.completeOrYes(isVerified);
						trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1),isVerified);
						trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
						approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager1,
								isVerified);
						startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
						fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
						fleetManager1.viewBillForwardMail(Utils.excelInputDataList.get(1), isVerified);
						browser1.browserLaunch(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
						mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
						mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
						mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
						mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
						fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
						fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
						fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
						Configuration.closeWorkFlowReport();
					}
				}
			} catch (Exception e) {

				e.printStackTrace();
				Configuration.catchcall();
			} finally {
				Configuration.saveReport();
				SendMail("SRDesktopWebVersion_CR");
			}
		}

	// *************************End of SRBaseFlow************************

	// ********************Start SRCancelRejectFlow********************
	// SRCR1
	public void SRFMDeletesAtFindGarage() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRFMDeletesAtFindGarage 48=====");
					Configuration.reportConfiguration("SRFMDeletesAtFindGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(13), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(13), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRFMDeletesAtFindGarage");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(13), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(13), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(13), isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(13),locator, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(13));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMDeletesAtFindGarage");
		}
	}

	// SRCR2
	public void SRFMCancelAtTrackLocation() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Service Request Cancel At TrackLocation =====");
					Configuration.reportConfiguration("SR-FMCancelAtTrackLocation");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(14), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(14), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("ServiceRequestFMCancelAtTrackLocation");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(14), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(14), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(14), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(14),locator, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(14));
					mechanic1.MECHLogout(Utils.excelInputDataList.get(14));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMCancelAtTrackLocation");
		}
	}
	// SRCR3

	public void SRFMDeletesAtTrackLocation() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRFMDeletesAtTrackLoc 59=====");
					Configuration.reportConfiguration("SRFMDeletesAtTrackLoc");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(15), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(15), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRFMDeletesAtTrackLoc");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(15), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(15), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(15), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(15),locator,isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(15));
					mechanic1.MECHLogout(Utils.excelInputDataList.get(15));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMDeletesAtTrackLoc");

		}
	}
	// SRCR4

	public void SRFMDeletesAtApproveEstimate() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRFMDeletesAtApproveEstimate 61=====");
					Configuration.reportConfiguration("SRFMDeletesAtApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(16), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(16), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRFMDeletesAtApproveEstimate");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(16), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(16), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(16), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(16), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(16),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(16), isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(16),locator, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(16));
					mechanic1.MECHLogout(Utils.excelInputDataList.get(16));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMDeletesAtApproveEstimate");
		}
	}
	// SRCR5

	public void SRFMCancelOneJobInApproveEstimate() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Job Cancel In Approve Estimate =====");
					Configuration.reportConfiguration("SRFMCancelOneJobInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(17), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(17), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRFMCancelOneJobInApproveEstimate");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(17), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(17), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(17), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(17), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(17), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(17), isVerified);
					approveEstimate1.approveEstimateCancelOneJob(Utils.excelInputDataList.get(17), fleetManager1,isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(17),fleetManager,driver,isVerified); //added driverAppestWithPayCash for paycash method alone
					startJob1.startJobs(Utils.excelInputDataList.get(17), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(17), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(17), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(17), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(17), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(17));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(17), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(17),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(17));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMCancelOneJobInApproveEstimate");
		}
	}
	// SRCR6

	public void SRFMCancelAllJobsInApproveEstimate() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Service Request Cancel All Jobs in Approve Estimate =====");

					Configuration.reportConfiguration("SRFMCancelAllJobsInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(18), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(18), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRFMCancelAllJobsInApproveEstimate");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(18), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(18), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(18), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(18), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(18), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(18), isVerified);
					approveEstimate1.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(18), fleetManager1,isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(18));
					mechanic1.MECHLogout(Utils.excelInputDataList.get(18));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRFMCancelAllJobsInApproveEstimate");
		}
	}

	// SRCR7

	public void SRMultiJobsFMCancelOneJobInApproveEstimate() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRMultiJobsFMCancelOneJobInApproveEstimate 38=====");
					Configuration.reportConfiguration("SRMultiJobsFMCancelOneJobInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(19), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(19), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRMultiJobsFMCancelOneJobInApproveEstimate");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(19), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(19), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(19), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(19), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(19), isVerified);
					approveEstimate1.approveEstimateCancelOneJobsWithPayCash(Utils.excelInputDataList.get(19),
							fleetManager1, isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(19), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(19), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(19), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(19), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(19), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(19));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(19), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(19),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(19));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRMultiJobsFMCancelOneJobInApproveEstimate");
		}
	}

	public void SRbyFMEstimatewithPartsRetailerCancelJobs() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRbyFMEstimatewithPartsRetailerCancelJobs 38=====");
					Configuration.reportConfiguration("SRbyFMEstimatewithPartsRetailerCancelJobs");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(20), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(20), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRbyFMEstimatewithPartsRetailerCancelJobs");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(20), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(20), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(20), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(20), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(20), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(20), isVerified);
					approveEstimate1.approveEstimateCancelOneJobsWithPayCash(Utils.excelInputDataList.get(20),
							fleetManager1, isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(20), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(20), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(20), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(20), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(20), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(20));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(20), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(20),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(20));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRbyFMEstimatewithPartsRetailerCancelJobs");
		}
	}
	// SRCR8-starting from 1:

	public void SRDeleteOneJobAndAddOneNewJobByMechanic() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRDeleteOneJobAndAddOneNewJobByMechanic 44=====");
					Configuration.reportConfiguration("SRDeleteOneJobAndAddOneNewJobByMechanic");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRDeleteOneJobAndAddOneNewJobByMechanic");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.DeletejobwithoutSubmitEstimate(Utils.excelInputDataList.get(1), mechanic1,
							isVerified);
					trackLocation1.trackLocationWithAddOneNewJob(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateCancelOneJob(Utils.excelInputDataList.get(1), fleetManager1,
							isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRDeleteOneJobAndAddOneNewJobByMechanic");
		}
	}
	// SRCR9

	public void SRWithPartsWithOutRetailerCancelOneJob() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRWithPartsWithOutRetailerCancelOnejob 39=====");
					Configuration.reportConfiguration("SRWithPartsWithOutRetailerCancelOnejob");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsWithOutRetailerCancelOnejob");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(2), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(2), isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate1.approveEstimateCancelOneJobWithAlert(Utils.excelInputDataList.get(2),
							fleetManager1, isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsWithOutRetailerCancelOnejob");
		}
	}
	// SRCR10

	public void SRMechanicRejectsOrder() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRMechanic Rejects Order =====");
					Configuration.reportConfiguration("SRMechanicRejectsOrder");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRMechanicRejectsOrder");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(3), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3), fleetManager1, isVerified);
					createServiceRequest.findGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicRejectOrder(Utils.excelInputDataList.get(3),isVerified);
					mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRMechanicRejectsOrder");
		}
	}
	// SRCR11

	public void SRMechanicDeletesAtTrackLoc() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRMechanicDeletesAtTrackLoc 57=====");
					Configuration.reportConfiguration("SRMechanicDeletesAtTrackLoc");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRMechanicDeletesAtTrackLoc");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(4), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					mechanic1.mechanicCancelOrder(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRMechanicDeletesAtTrackLoc");
		}
	}
	// SRCR12

	public void SRDeleteOneJobByMechanic() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRDeleteOneJobbyMechanic 43 =====");
					Configuration.reportConfiguration("SRDeleteOneJobByMechanic");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRDeleteOneJobByMechanic");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(5), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(5), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithPartsWithoutSubmit(Utils.excelInputDataList.get(5),
							isVerified);
					trackLocation1.deleteJobwithSubmitEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager1,
							isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRDeleteOneJobByMechanic");
		}
	}

	// --------------------End SR CancelReject Flow-----------------------------

	// -------------------Start Negative SR Flow-----------------------------

	// NSR1
	public void SRWithPartsWithOutRetailer() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsWithOutRetailer =====");
					Configuration.reportConfiguration("SRWithPartsWithOutRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsWithOutRetailer");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3), mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsWithOutRetailer");
		}
	}

	// NSR2
	public void SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer =====");
					Configuration.reportConfiguration("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(2), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(2),isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager1,
							isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
		}
	}

	// NSR3
	public void SRWithPartsWithRetailerRevisedPartsNoRetailer() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRWithPartsWithRetailerRevisedPartsNoRetailer =====");
					Configuration.reportConfiguration("SRWithPartsWithRetailerRevisedPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRWithPartsWithRetailerRevisedPartsNoRetailer");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(3), isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3),fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(3),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager1,isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(3), fleetManager1,isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRWithPartsWithRetailerRevisedPartsNoRetailer");
		}
	}

	// NSR4
	public void SRPartsNoRetailerRevisedwithPartsWithRetailer() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR With Parts NoRetailer Revised Estimate With Parts With Retailer =====");
					Configuration.reportConfiguration("SRPartsNoRetailerRevisedwithPartsWithRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(4),isVerified);
					mechanic1.login(Utils.excelInputDataList.get(4),isVerified);
					Configuration.closeWorkFlowReport();
					  
					  Configuration.createNewWorkFlowReport("SRPartsNoRetailerRevisedwithPartsWithRetailer");
					  createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(4),isVerified);
					  createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4),fleetManager1,isVerified); 
					  mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4),isVerified);
					  mechanic1.mechanicAcceptOrder(mRequestOrderId1,isVerified);
					  trackLocation1.giveEstimate(Utils.excelInputDataList.get(4),
					  mechanic1,isVerified); trackLocation1.completeOrYes(isVerified);
					  trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(4),isVerified);
					  trackLocation1.withOutRetailer(Utils.excelInputDataList.get(4),isVerified);
					  approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(4),fleetManager1,isVerified);
					  startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(4),mechanic1,isVerified);
					  trackLocation1.selectRetailer(Utils.excelInputDataList.get(4),isVerified);
					  approveEstimate1.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(4), fleetManager1,isVerified);
					  trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(4),mechanic1,isVerified);
					  fleetManager1.viewBill(Utils.excelInputDataList.get(4),isVerified);
					  mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(4),isVerified);
					  mechanic1.mechanicRating(Utils.excelInputDataList.get(4),isVerified);
					  mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(4),mRequestOrderId1,isVerified);
					  mechanic1.MECHLogout(Utils.excelInputDataList.get(4));
					  fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(4),isVerified);
					  fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(4),mechactualvalue,mRequestOrderId1, isVerified);
					  fleetManager1.FMLogout(Utils.excelInputDataList.get(4));
					  Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("SRPartsNoRetailerRevisedwithPartsWithRetailer");
		}
	}

	// NSR5
	public void SRIDontKnowWhatsWrongWithParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR I Don't Know Whats Wrong with parts =====");
					Configuration.reportConfiguration("SR IDontKnowWhatsWrong With Parts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("ServiceRequestIDontKnowWhatsWrongWithParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(5), isVerified);
					createServiceRequest.selectUnKnowJobs(isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationForUnknownIssueWithParts(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager1,
							isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRIDontKnowWhatsWrongWithParts");
		}
	}

	// NSR6
	public void SRIDontKnowWhatsWrongWithOutParts() throws Exception {

		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SR I Don't Know Whats Wrong With out parts =====");
					Configuration.reportConfiguration("SRIDontKnowWhatsWrongWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1),isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRIDontKnowWhatsWrongWithOutParts");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1),isVerified); 
					createServiceRequest.selectUnKnowJobs(isVerified);
					mRequestOrderId1 =createServiceRequest.findGarage(Utils.excelInputDataList.get(1),isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1,isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationForUnknownIssueWithOutParts(Utils.excelInputDataList.get(1),isVerified);
					approveEstimate1.approveEstimate(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBill(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRIDontKnowWhatsWrongWithOutParts");
		}
	}

	// -------------------End Negative SR Flow--------------------------

	// -------------------Start DriverBDBase Flow---------------------------

	private Driver driver;
	private DriverCreateBreakdownRequest createDriverRequest;
	
	public void basicDBDBaseFlow() {
		try {
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			driver = new Driver();
			createDriverRequest = new DriverCreateBreakdownRequest();
			trackLocation = new BreakDownTrackLocation();
			approveEstimate = new BreakDownApproveEstimate();
			startJob = new BreakDownStartJob();
			
		} catch (Exception e) {
		}
	}

	// DBD1
	public void driverBreakDownWithoutParts() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown Without Parts 2=====");
					Configuration.reportConfiguration("DriverBreakDownWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverBreakDownWithoutParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(1),isVerified);
					System.out.println("mRequestOrderId::::::::::::"+mRequestOrderId);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(1),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);

					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(1), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithoutParts");
		}
	}

	// DBD2
	public void driverBreakDownWithParts() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown With Parts 1=====");
					Configuration.reportConfiguration("DriverBreakDownWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic.login(Utils.excelInputDataList.get(2), isVerified);
					driver.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("Driver BreakDown With Parts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2),isVerified);
					System.out.println("ReqNumber in Main method:::::"+mRequestOrderId);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(2),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(2), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(2), fleetManager,driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(2), fleetManager, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(2), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(2), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithParts");
		}
	}

	// DBD3
	public void driverBreakDownWithPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicDBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithParts Revised Estimate WithParts-5 =====");
					Configuration.reportConfiguration("DriverBreakDownWithPartsRevisedEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic.login(Utils.excelInputDataList.get(3), isVerified);
					driver.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsRevisedEstimateWithParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(3),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(3),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(3), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(3), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(3), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(3), fleetManager,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(3), fleetManager, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(3), mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate.driverRevisedApproveEstimate(Utils.excelInputDataList.get(3),fleetManager, driver, isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(3), fleetManager, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(3), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithPartsRevisedEstimateWithParts");
		}
	}

	// DBD4
	public void driverBreakDownWithoutPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithoutParts Revised Estimate WithParts-7 =====");
					Configuration.reportConfiguration("DriverBreakDownWithoutPartsRevisedEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic.login(Utils.excelInputDataList.get(4), isVerified);
					driver.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverBreakDownWithoutPartsRevisedEstimateWithParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(4),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(4),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(4), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(4), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(4), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(4), fleetManager,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(4), mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(4), isVerified);
					approveEstimate.driverRevisedApproveEstimate(Utils.excelInputDataList.get(4),fleetManager, driver, isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(4), fleetManager, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(4), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(4), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(4), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(4), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(4));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(4), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(4),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithoutPartsRevisedEstimateWithParts");
		}
	}

	// DBD5
	public void driverBreakDownWithPartsRevisedEstimateWithOutParts() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithParts RevisedEstimate WithoutParts-9 =====");
					Configuration.reportConfiguration("DriverBreakDownWithPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic.login(Utils.excelInputDataList.get(5), isVerified);
					driver.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsRevisedEstimateWithOutParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(5),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(5),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(5), fleetManager,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(5), fleetManager, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(5), mechanic,isVerified);
					approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(5),fleetManager, driver, isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(5), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(5), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithPartsRevisedEstimateWithOutParts");
		}
	}

	// DBD6
	public void driverBreakDownWithoutPartsRevisedEstimateWithOutParts() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithoutParts RevisedEstimate WithoutParts 11=====");
					Configuration.reportConfiguration("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(6), isVerified);
					mechanic.login(Utils.excelInputDataList.get(6), isVerified);
					driver.login(Utils.excelInputDataList.get(6), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(6),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(6),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(6), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(6), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(6), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(6), fleetManager,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(6), mechanic,isVerified);
					approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(6),
							fleetManager, driver, isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(6), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(6), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(6), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(6), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(6), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(6));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(6), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(6),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(6));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts");
		}
	}

	// DBD7
	public void DriverBreakDownEstimateWithPartsRevisedBill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-Break Down Estimate with Parts RevisedBill 13=====");
					Configuration.reportConfiguration("DriverBreakDownEstimateWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(7), isVerified);
					mechanic.login(Utils.excelInputDataList.get(7), isVerified);
					driver.login(Utils.excelInputDataList.get(7), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverBreakDownEstimateWithPartsRevisedBill");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(7),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(7),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(7), fleetManager,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(7), fleetManager, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					startJob.BDRevisedBill(Utils.excelInputDataList.get(7), mechanic, isVerified);
					/*trackLocation.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(7), mechanic, isVerified);*/
					fleetManager.viewBill(Utils.excelInputDataList.get(7), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(7), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(7), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(7));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(7),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(7));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownEstimateWithPartsRevisedBill");
		}
	}

	// DBD8
	public void driverbreakDownWithoutPartsRevisedBill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-Break Down Without Parts Revised Bill 15=====");
					Configuration.reportConfiguration("DriverbreakDownWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic.login(Utils.excelInputDataList.get(8), isVerified);
					driver.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverbreakDownWithoutPartsRevisedBill");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(8),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(8),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(8), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(8), fleetManager,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(8), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverbreakDownWithoutPartsRevisedBill");
		}
	}

	// DBD9
	public void driverbreakDownPartsRevisedWithPartsRevisedBill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver- BreakDown WithParts RevisedEstimateWithParts RevisedBill 17=====");
					Configuration.reportConfiguration("DriverbreakDownPartsRevisedWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(9), isVerified);
					mechanic.login(Utils.excelInputDataList.get(9), isVerified);
					driver.login(Utils.excelInputDataList.get(9), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverbreakDownPartsRevisedWithPartsRevisedBill");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(9),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(9),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(9), fleetManager,
							driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(9), fleetManager, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(9), mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(9), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(9), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(9), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(9), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(9));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(9),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(9));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverbreakDownPartsRevisedWithPartsRevisedBill");
		}
	}

	// DBD10
	public void driverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====Driver-BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill 19=====");

					Configuration.reportConfiguration("driverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(10), isVerified);
					mechanic.login(Utils.excelInputDataList.get(10), isVerified);
					driver.login(Utils.excelInputDataList.get(10), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration
							.createNewWorkFlowReport("driverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(10),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(10),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(10), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(10), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(10), fleetManager,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(10), mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(10), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(10), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(10), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(10), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(10), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(10));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(10), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(10),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(10));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverbreakDownPartsRevisedWithPartsRevisedBill");
		}
	}

	// DBD11
	public void driverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println(
							"===== Driver-BreakDown WithParts RevisedEstimate WithOutParts RevisedBill 21=====");
					Configuration.reportConfiguration("DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(11), isVerified);
					mechanic.login(Utils.excelInputDataList.get(11), isVerified);
					driver.login(Utils.excelInputDataList.get(11), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(11),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(11),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(11), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(11), mechanic, isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.driverApproveEstimate(Utils.excelInputDataList.get(11), fleetManager,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(11), fleetManager, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(11), mechanic,isVerified);
					approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(11),
							fleetManager, driver, isVerified);
					//driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(11), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(11), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(11), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(11), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(11), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(11));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(11), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(11),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(11));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
		}
	}

	// DBD12
	public void driverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println(
							"===== Driver-BreakDown WithoutParts RevisedEstimate WithoutParts RevisedBill 23=====");
					Configuration.reportConfiguration("DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(12), isVerified);
					mechanic.login(Utils.excelInputDataList.get(12), isVerified);
					driver.login(Utils.excelInputDataList.get(12), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(12),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(12),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(12), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(12), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(12), isVerified);
					approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(12), fleetManager,
							driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(12), mechanic,isVerified);
					approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(12),
							fleetManager, driver, isVerified);
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(12), mechanic, isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(12), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(12), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(12), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(12), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(12));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(12), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(12),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(12));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
		}
	}

	// DBD13
	public void driverBreakDownWithPartsWithOutRetailer() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithParts WithOut Retailer 25=====");
					Configuration.reportConfiguration("DriverBreakDownWithPartsWithOutRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(13), isVerified);
					mechanic.login(Utils.excelInputDataList.get(13), isVerified);
					driver.login(Utils.excelInputDataList.get(13), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsWithOutRetailer");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(13),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(13),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(13), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(13), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(13), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(13), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(13), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(13), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(13), fleetManager, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(13), isVerified);
					startJob.startJobs(Utils.excelInputDataList.get(13), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(13), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(13), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(13), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(13), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(13), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(13), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(13));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(13), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(13),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(13));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithPartsWithOutRetailer");
		}
	}

	// -----------------------End DriverBDBase Flow----------------------
	// ----------------------Start Driver BD CancelReject Flow------------------------
	// DBDCR1
	public void driverBDFMDeletesAtTrackLoc() throws Exception {
		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====DriverBDMechanicDeletesAtTrackLoc 48=====");
					Configuration.reportConfiguration("DriverBDFMDeletesAtTrackLoc");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverBDFMDeletesAtTrackLoc");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(1),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(1),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(1),locator,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDFMDeletesAtTrackLoc");
		}
	}

	// DBDCR2
	public void driverBDMechanicRejectsOrder() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====Driver- Break Down Mechanic Rejects Order 41=====");
					Configuration.reportConfiguration("DriverBDMechanicRejectsOrder");

					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic.login(Utils.excelInputDataList.get(2), isVerified);
					driver.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverBDMechanicRejectsOrder");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(2),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicRejectOrder(Utils.excelInputDataList.get(2),isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDMechanicRejectsOrder");
		}
	}

	// DBDCR3
	public void driverBreakDownFMDeletesAtConfirmGarage() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-Break Down FM Deletes At Confirm Garage44=====");
					Configuration.reportConfiguration("DriverBreakDownFMDeletesAtConfirmGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic.login(Utils.excelInputDataList.get(3), isVerified);
					driver.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverBreakDownFMDeletesAtConfirmGarage");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(3),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(3),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(3),locator, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(3), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownFMDeletesAtConfirmGarage");
		}
	}

	// DBDCR4
	public void driverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() throws Exception {
		try {
			basicDBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 33-NS=====");
					Configuration.reportConfiguration("DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic.login(Utils.excelInputDataList.get(4), isVerified);
					driver.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(4),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(4),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(4), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(4), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(4), isVerified);
					approveEstimate.approveEstimateCancelOneJob(Utils.excelInputDataList.get(4), fleetManager,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					//startJob.startJobs(Utils.excelInputDataList.get(4), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(4), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(4), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(4), isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(4));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(4), isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
		}
	}

	// DBDCR5
	public void driverBDFMCancelAllJobsInApproveEstimate() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {

				if (isVerified) {
					System.out.println("===== Driver-BreakDown Parts Cancel All Jobs 34 =====");
					Configuration.reportConfiguration("DriverBDFMCancelAllJobsInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic.login(Utils.excelInputDataList.get(5), isVerified);
					driver.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverBDFMCancelAllJobsInApproveEstimate");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(5),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(5),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(5),mechanic,isVerified);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(5), fleetManager,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(5), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDFMCancelAllJobsInApproveEstimate");
		}
	}

	// DBDCR6
	public void driverBreakDownMechanicDeletesAtTrackLoc() throws Exception {
		try {
			basicDBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====DriverBDMechanicDeletesAtTrackLoc 46=====");
					Configuration.reportConfiguration("DriverBreakDownMechanicDeletesAtTrackLoc");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(6), isVerified);
					mechanic.login(Utils.excelInputDataList.get(6), isVerified);
					driver.login(Utils.excelInputDataList.get(6), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverBreakDownMechanicDeletesAtTrackLoc");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(6),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(6),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(6), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(6), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownMechanicDeletesAtTrackLoc");
		}
	}

	// DBDCR7
	public void driverBDWithoutPartsFMCancelJobInAproveEstimate() throws Exception {

		try {
			basicDBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BDWithoutPartsFMCancelJobInAproveEstimate 50=====");
					Configuration.reportConfiguration("DriverBDWithoutPartsFMCancelJobInAproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(7), isVerified);
					mechanic.login(Utils.excelInputDataList.get(7), isVerified);
					driver.login(Utils.excelInputDataList.get(7), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverBDWithoutPartsFMCancelJobInAproveEstimate");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(7),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(7),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(7), fleetManager,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(7), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(7), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(7), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(7));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(7),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(7));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDWithoutPartsFMCancelJobInAproveEstimate");
		}
	}

	// DBDCR8
	public void driverBDFMDeletesAtVehicleInGarage() throws Exception {

		try {
			basicDBDBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====DriverBDFMDeleteAtVehicleInGarage 52=====");
					Configuration.reportConfiguration("DriverBDFMDeleteAtVehicleInGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic.login(Utils.excelInputDataList.get(8), isVerified);
					driver.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("DriverBDFMDeleteAtVehicleInGarage");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(8),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(8),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(8), mechanic, isVerified);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(8),locator,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					fleetManager.viewBill(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDFMDeleteAtVehicleInGarage");
		}
	}
	// ------------------End Driver BD CancelReject Flow-----------------------------

	// ---------------Start Negative DriverBD Flow-----------------------------
	// NDBD1
	public void driverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
				if (isVerified) {
					System.out.println(
							"===== Driver-BreakDown WithParts NoRetailer RevisedEstimate With Parts NoRetailer 27 =====");
					Configuration.reportConfiguration("DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration
							.createNewWorkFlowReport("DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(1),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(1),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic,isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
		}
	}

	// NDBD2
	public void driverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println(
							"===== Driver-BreakDown WithoutParts RevisedEstimate WithParts NoRetailer 29=====");
					Configuration.reportConfiguration("DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic.login(Utils.excelInputDataList.get(2), isVerified);
					driver.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration
							.createNewWorkFlowReport("driverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
					
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2),
							isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(2),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(2), fleetManager,
							driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(2), mechanic,isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(2), isVerified);
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager,
							isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(2), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(2), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
						
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
		}
	}

	// NDBD3
	public void driverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts() throws Exception {
		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown WithParts NoRetailer RevisedEstimate WithoutParts 31=====");
					Configuration.reportConfiguration("DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic.login(Utils.excelInputDataList.get(3), isVerified);
					driver.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(3),isVerified);
					createDriverRequest.fmCreateBDOrder(Utils.excelInputDataList.get(3),mRequestOrderId,fleetManager,isVerified);
					mechanic.mechanicAcceptOrder(mRequestOrderId, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(3), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(3), mechanic, isVerified);
					trackLocation.completeOrYes(isVerified);
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(3), mechanic, isVerified);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(3), fleetManager, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(3), mechanic, isVerified);
					approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(3),fleetManager, driver, isVerified);
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic, isVerified);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(3), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechactualvalue=mechanic.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fmactualvalue=fleetManager.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
		}
	}

	// NDBD4
	public void driverNegativeScenarioFMDeletes() throws Exception {

		try {
			basicDBDBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-BreakDown Negative Scenario FM Deletes 37 =====");
					Configuration.reportConfiguration("DriverNegativeScenarioFMDeletes");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverNegativeScenarioFMDeletes");
					mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(1),isVerified);
					createDriverRequest.fmDeleteBDOrder(fleetManager, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverNegativeScenarioFMDeletes");
		}
	}
	// -------------------------End Negative DriverBD Flow----------------------------------------------

	// -------------------------Start DriverSRBase Flow------------------------------------------------

	private DriverCreateServiceRequest driverCreateServiceRequest;

	public void basicDSRBaseFlow() {

		try {
			System.out.println("===== Inside try=====");
			ReadingExcelData.readingWorkFlowData();
			fleetManager1 = new FleetManager();
			mechanic1 = new Mechanic();
			driver = new Driver();
			driverCreateServiceRequest = new DriverCreateServiceRequest();
			trackLocation1 = new ServicesRequestTrackLocation();
			approveEstimate1 = new ServiceRequestApproveEstimate();
			startJob1 = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			createServiceRequest = new CreateServiceRequest();

		} catch (Exception e) {

		}
	}

	// ===================Driver SR FLOWS============================================================
	// DSR1
	public void driverSRWithParts() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR With Parts=====");
					Configuration.reportConfiguration("DriverSRWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverSRWithParts");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					System.out.println("RequestNumber in Main Method"+mRequestOrderId1);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1,mRequestOrderId1,isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(1), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(1), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithParts");
		}
	}

	// DSR2
	public void driverSRWithoutParts() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR Without Parts=====");
					Configuration.reportConfiguration("DriverSRWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					driver.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverSRWithoutParts");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(2), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(2), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(2), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(2), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(2),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithoutParts");
		}
	}

	// DSR3
	public void driverSRWithPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Break Down With Parts Revised Estimate With Parts 3=====");
					Configuration.reportConfiguration("DriverSRWithPartsRevisedEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					driver.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithParts");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(3), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(3),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(3), fleetManager1,
							driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(3), fleetManager1, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(3),
							fleetManager1, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(3), fleetManager1, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(3), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(3), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(3));
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(3),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsRevisedEstimateWithParts");
		}
	}

	// DSR4
	public void driverSRWithoutPartsRevisedEstimateWithParts() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR-Without Parts Revised Estimate With Parts 4=====");
					Configuration.reportConfiguration("DriverSRWithoutPartsRevisedEstimateWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(4), isVerified);
					driver.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedEstimateWithParts");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(4), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(4), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(4), isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(4),
							fleetManager1, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(4), fleetManager1, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(4), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(4), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(4), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(4), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(4));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(4), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(4),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(4));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithoutPartsRevisedEstimateWithParts");
		}
	}

	// DSR5
	public void driverSRWithPartsRevisedEstimateWithOutParts() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR-With Parts Revised Estimate WithOut Parts 5=====");
					Configuration.reportConfiguration("DriverSRWithPartsRevisedEstimateWithOutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
					driver.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithOutParts");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(5), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(5), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(5),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(5), fleetManager1, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(5), fleetManager1, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(5),
							fleetManager1, driver, isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(5), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(5), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(5), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(5), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(5));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(5), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(5),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(5));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsRevisedEstimateWithOutParts");
		}
	}

	// DSR6
	public void driverSRWithoutPartsRevisedWithoutParts() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver SR WithoutParts Revised Without Parts 6=====");
					Configuration.reportConfiguration("DriverSRWithoutPartsRevisedWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(6), isVerified);
					driver.login(Utils.excelInputDataList.get(6), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedWithoutParts");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(6), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(6), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(6), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(6), isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(6), mechanic1, isVerified);
					approveEstimate1.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(6), fleetManager1,
							isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(6), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(6), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(6), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(6), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(6), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(6));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(6), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(6),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(6));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithoutPartsRevisedWithoutParts");
		}
	}

	// DSR7
	public void driverSRWithPartsRevisedEstimateWithPartsRevisedBill() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver SR WithParts Revised Estimate With Parts Revised Bill 17=====");
					Configuration.reportConfiguration("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(7), isVerified);
					driver.login(Utils.excelInputDataList.get(7), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(7), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(7), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(7),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(7), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(7), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(7),fleetManager1, driver, isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(7), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(7), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(7), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(7), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(7), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(7));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(7), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(7),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(7));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill");
		}
	}

	// DSR8
	public void driverSRWithoutPartsRevisedEstimatePartsRevisedBill() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR WithoutParts Revised Without Parts 19=====");
					Configuration.reportConfiguration("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(8), isVerified);
					driver.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedEstimatePartsRevisedBill");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(8), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(8), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(8), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(8), isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(8),
							fleetManager1, driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(8), fleetManager1, driver,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(8), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(8), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(8), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(8), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(8), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(8));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(8), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(8),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(8));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill");
		}
	}

	// DSR9
	public void driverServiceRequestWithoutPartsRevisedWithoutParts() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-service Request WithoutParts Revised Without Parts 23=====");
					Configuration.reportConfiguration("DriverServiceRequestWithoutPartsRevisedWithoutParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(9), isVerified);
					driver.login(Utils.excelInputDataList.get(9), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverServiceRequestWithoutPartsRevisedWithoutParts");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(9), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(9), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(9), mechanic, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(9), isVerified);
					approveEstimate1.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(9), fleetManager,
							driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(9), mechanic1, isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(9),
							fleetManager1, driver, isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(9), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(9), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(9), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(9), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(9), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(9), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(9));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(9), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(9),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(9));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverServiceRequestWithoutPartsRevisedWithoutParts");
		}
	}

	// DSR10
	public void driverSRPartsRevisedEstimateWithoutPartsRevisedBill() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR WithParts Revised Estimate WithOut Parts Revised bill 21 =====");
					Configuration.reportConfiguration("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(10), isVerified);
					driver.login(Utils.excelInputDataList.get(10), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(10), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(10), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(10),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(10), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(10), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					startJob1.revisedEstimationWithoutParts(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(10),fleetManager1, driver, isVerified);
					trackLocation1.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(10), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(10), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(10), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(10), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(10), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(10));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(10), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(10),mechactualvalue,mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(10));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill");
		}

	}

	// DSR11
	public void driverSREstimateWithPartsRevisedBill() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver SR Estimate WithParts RevisedBill 13=====");
					Configuration.reportConfiguration("DriverSREstimateWithPartsRevisedBill");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(11), isVerified);
					driver.login(Utils.excelInputDataList.get(11), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverSREstimateWithPartsRevisedBill");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(11), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(11), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(11), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(11),isVerified); 
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(11), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(11), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(11), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(11), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(11), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(11), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(11), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(11), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(11));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(11), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(11),mechactualvalue, mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(11));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSREstimateWithPartsRevisedBill");
		}
	}

	// DSR12
	 public void driverSREstimateWithoutPartsRevisedBill() throws Exception {
	  
	  try { basicDSRBaseFlow(); 
	  if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) 
	  { if(isVerified){ 
		  System.out.println("===== driverSREstimateWithoutPartsRevisedBill 15=====");
		  Configuration.reportConfiguration("DriverSREstimateWithoutPartsRevisedBill");
		  Configuration.createNewWorkFlowReport("Login Details");
		  fleetManager1.login(Utils.excelInputDataList.get(12),isVerified);
		  mechanic1.login(Utils.excelInputDataList.get(12),isVerified);
		  driver.login(Utils.excelInputDataList.get(12),isVerified);
		  Configuration.closeWorkFlowReport(); 
		  
		  
		  Configuration.createNewWorkFlowReport("DriverSREstimateWithoutPartsRevisedBill");
		  mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(12), isVerified);
		  driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
		  createServiceRequest.selectJobs1(Utils.excelInputDataList.get(12), fleetManager1, isVerified);
		  mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(12), isVerified);
		  mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
		  driver.pushNotification(Utils.excelInputDataList.get(12), isVerified);
		  trackLocation1.giveEstimate(Utils.excelInputDataList.get(12), mechanic1,isVerified); 
		  trackLocation1.completeOrYes(isVerified);
		  driver.pushNotification(Utils.excelInputDataList.get(12),isVerified);
		  trackLocation1.trackLocationMultiJobWithoutParts(Utils.excelInputDataList.get(12),isVerified); 
		  startJob1.startJobs(Utils.excelInputDataList.get(12),mechanic1,isVerified);
		  fleetManager1.driverViewBill(Utils.excelInputDataList.get(12),driver,isVerified);
		  driver.pushNotification(Utils.excelInputDataList.get(12),isVerified);
		  mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(12),driver,isVerified);
		  driver.pushNotification(Utils.excelInputDataList.get(12),isVerified);
		  mechanic1.mechanicRating(Utils.excelInputDataList.get(12),isVerified);
		  mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(12),mRequestOrderId1,isVerified);
		  mechanic1.MECHLogout(Utils.excelInputDataList.get(12));
		  fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(12), isVerified);
		  fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(12),mechactualvalue, mRequestOrderId1, isVerified);
		  fleetManager1.FMLogout(Utils.excelInputDataList.get(12));
		  Configuration.closeWorkFlowReport(); 
	  }} } 
	  catch (Exception e) {
	  e.printStackTrace(); 
	  Configuration.catchcall(); 
	  } 
	  finally {
	  Configuration.saveReport();
	  SendMail("DriverSREstimateWithoutPartsRevisedBill"); } }
	 
	
	
	// Driver CompleteOrder CR:
	public void driverSRWithPartsGiveEstimateCompleteOrder() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver Service Request With Parts GiveEstimate CompleteOrder=====");
					Configuration.reportConfiguration("DriverSRWithPartsGiveEstimateCompleteOrder");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverSRWithPartsGiveEstimateCompleteOrder");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					mechanic1.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.myPreviousOrder(Utils.excelInputDataList.get(1), mechanic1, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsGiveEstimateCompleteOrder");
		}
	}
	// ---------------------------------End DriverSRBase
	// Flow-------------------------------------------

	// ----------------------------------Start DriverSR CancelReject
	// Flow----------------------------
	// DSRCR1
	public void driverSRFMDeletesAtTrackLocation() throws Exception {
		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SRFMDeletesAtTrackLoc 58=====");

					Configuration.reportConfiguration("DriverSRFMDeletesAtTrackLocation");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverSRFMDeletesAtTrackLocation");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(1),locator,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRFMDeletesAtTrackLocation");
		}
	}

	// DSRCR2
	public void driverSRMechanicDeletesAtTrackLoc() throws Exception {
		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver SRMechanicDeletesAtTrackLoc 56=====");
					Configuration.reportConfiguration("DriverSRMechanicDeletesAtTrackLoc");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					driver.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverSRMechanicDeletesAtTrackLoc");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(2), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicCancelOrder(Utils.excelInputDataList.get(2), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRMechanicDeletesAtTrackLoc");
		}
	}

	// DSRCR3
	public void driverSRFMDeletesAtFindGarage() throws Exception {
		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SRFMDeletesAtFindGarage 49=====");
					Configuration.reportConfiguration("DriverSRFMDeletesAtFindGarage");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					driver.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverSRFMDeletesAtFindGarage");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(3), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(3),locator,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRMechanicDeletesAtTrackLoc");
		}
	}

	// DSRCR4
	public void driverSRFMDeletesAtApproveEstimate() throws Exception {
		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SRFMDeletesAtApproveEstimate 60=====");

					Configuration.reportConfiguration("DriverSRFMDeletesAtApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(4), isVerified);
					driver.login(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRFMDeletesAtApproveEstimate");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(4), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(4), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(4), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(4), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(4), isVerified);
					fleetManager1.cancelOrder(Utils.excelInputDataList.get(4),locator, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(4), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRFMDeletesAtApproveEstimate");
		}
	}

	// DSRCR5
	public void driverSRFMCancelAllJobsInApproveEstimate() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver Service Request Cancel All Jobs in Approve Estimate 45 =====");
					Configuration.reportConfiguration("DriverSRFMCancelAllJobsInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(5), isVerified);
					driver.login(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRFMCancelAllJobsInApproveEstimate");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(5), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(5), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(5), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(5), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(5), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(5), isVerified);
					approveEstimate1.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(5), fleetManager1,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(5), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRFMCancelAllJobsInApproveEstimate");
		}
	}

	// DSRCR6
	public void driverSRDeleteOneJobAndAddOneNewJobByMechanic() throws Exception {
		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("=====SRDeleteOneJobAndAddOneNewJobByMechanic 42=====");
					Configuration.reportConfiguration("DriverSRDeleteOneJobAndAddOneNewJobByMechanic");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(6), isVerified);
					driver.login(Utils.excelInputDataList.get(6), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverSRDeleteOneJobAndAddOneNewJobByMechanic");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(6), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(6), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(6), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					trackLocation1.DeletejobwithoutSubmitEstimate(Utils.excelInputDataList.get(6), mechanic1,isVerified);
					trackLocation1.trackLocationWithAddOneNewJob(Utils.excelInputDataList.get(6), isVerified);
					approveEstimate1.approveEstimate(Utils.excelInputDataList.get(6), fleetManager1,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(6), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(6), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(6), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(6), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(6), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(6), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(6), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(6),mechactualvalue, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRDeleteOneJobAndAddOneNewJobByMechanic");
		}
	}

	// DSRCR7
	public void driverSRFMCancelOneJobInApproveEstimate() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-Job Cancel In Approve Estimate 41=====");
					Configuration.reportConfiguration("DriverSRFMCancelOneJobInApproveEstimate");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(7), isVerified);
					driver.login(Utils.excelInputDataList.get(7), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRFMCancelOneJobInApproveEstimate");
					
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(7), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(7), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(7), isVerified);
					approveEstimate1.approveEstimateCancelOneJob(Utils.excelInputDataList.get(7), fleetManager1,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(7), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(7), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(7), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(7), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(7), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(7), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(7), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(7),mechactualvalue, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRFMCancelOneJobInApproveEstimate");
		}
	}

	// DSRCR8
	public void driverSRMechanicRejectsOrder() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver SR Mechanic Rejects Order 51 =====");
					Configuration.reportConfiguration("driverSRMechanicRejectsOrder");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(8), isVerified);
					driver.login(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRMechanicRejectsOrder");
					
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(8), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(8), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(8), isVerified);
					mechanic1.mechanicRejectOrder(Utils.excelInputDataList.get(8),isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(8), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("driverSRMechanicRejectsOrder");
		}
	}

	// DSRCR9
	public void driverSRFMDeletes() throws Exception {
		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SRFMDeletes 47 =====");
					Configuration.reportConfiguration("DriverSRFMDeletes");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					Configuration.createNewWorkFlowReport("driverSRFMDeletes");
					
					
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					fleetManager1.deleteFMOrder(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();

		} finally {
			Configuration.saveReport();
			SendMail("DriverSRFMDeletes");
		}
	}

	// DSRCR10
	public void driverSRWithPartsWithOutRetailerCancelOneJob() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SRWithPartsWithOutRetailerCancelOnejob 36=====");
					Configuration.reportConfiguration("DriverSRWithPartsWithOutRetailerCancelOneJob");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRWithPartsWithOutRetailerCancelOneJob");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverApproveEstimateCancelOneJobsWithPayCashWithAlert(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsWithOutRetailerCancelOneJob");
		}
	}

	// ----------------------End DriverSR CancelReject Flow----------------------

	// ----------------------Start Negative DriverSRFlow---------------------------
	// NDSR1
	public void driverServiceRequestIDontKnowWhatsWrongWithParts() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-Service Request I Don't Know Whats Wrong with parts 33 =====");
					Configuration.reportConfiguration("DriverServiceRequestIDontKnowWhatsWrongWithParts");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverServiceRequestIDontKnowWhatsWrongWithParts");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectUnKnowJobs(isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationForUnknownIssueWithParts(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(1), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(1), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverServiceRequestIDontKnowWhatsWrongWithParts");
		}
	}

	// NDSR2
	public void driverSRPartsNoRetailerRevisedwithPartsWithRetailer() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println(
							"===== Driver-SR With Parts NoRetailer Revised Estimate With Parts With Retailer 31 =====");
					Configuration.reportConfiguration("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1),isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(1),fleetManager1, driver, isVerified);
					approveEstimate1.driverRevisedApproveEstwithPayCash(Utils.excelInputDataList.get(1), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer");
		}
	}

	// NDSR3
	public void driverSRWithPartsWithRetailerRevisedPartsNoRetailer() throws Exception {

		try {
			basicDSRBaseFlow();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-service Request WithParts Revised Estimate With Parts no retailer 29 =====");
					Configuration.reportConfiguration("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();
					
					Configuration.createNewWorkFlowReport("driverSRWithPartsWithRetailerRevisedPartsNoRetailer");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1),isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverApproveEstimate(Utils.excelInputDataList.get(1), fleetManager1,driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.driverAppEstwithPayCash(Utils.excelInputDataList.get(1), fleetManager1, driver,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue,mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer");
		}
	}

	// NDSR4
	public void driverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR With Parts NoRetailer Revised Estimate With Parts No Retailer -27 =====");
					Configuration.reportConfiguration("DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
					mRequestOrderId1 = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,isVerified);
					trackLocation1.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(0), isVerified);
					mechanic1.driverConfirmCashReceipt(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(0), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue, mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
		}
	}

	// NDSR5
	public void driverSRWithPartsNoRetailer() throws Exception {

		try {
			basicDSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== Driver-SR WithParts NoRetailer -25 =====");
					Configuration.reportConfiguration("DriverSRWithPartsNoRetailer");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					driver.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("driverSRWithPartsNoRetailer");
					mRequestOrderId1 = driverCreateServiceRequest
							.driverServiceRepairsRequest(Utils.excelInputDataList.get(1), isVerified);
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager1, mRequestOrderId1, isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1), fleetManager1, isVerified);
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.completeOrYes(isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					trackLocation1.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					trackLocation1.withOutRetailer(Utils.excelInputDataList.get(1), isVerified);
					approveEstimate1.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager1,
							isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.driverViewBill(Utils.excelInputDataList.get(1), driver, isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					driver.pushNotification(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fmactualvalue=fleetManager1.FMcheckMyPreviousOrder(Utils.excelInputDataList.get(1),mechactualvalue,mRequestOrderId1, isVerified);
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("DriverSRWithPartsNoRetailer");
		}
	}
	// ------------------End Negative
	// DriverSRFlow----------------------------------------------------------
	// ------------------End DriverSRBase
	// Flow--------------------------------------------------------------

	// ---------------Start Negative validations----------------------------------

	public void loginNegativeFMApp() {

		try {
			ReadingExcelData.readingNegativeData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			breakDownRequest = new CreateBreakDownRequest();
			createServiceRequest = new CreateServiceRequest();
			trackLocation1 = new ServicesRequestTrackLocation();
			approveEstimate1 = new ServiceRequestApproveEstimate();
			startJob1 = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			/*Configuration.createNewWorkFlowReport("Login Details");
			fleetManager.login(Utils.excelInputDataList.get(1));
			mechanic.login(Utils.excelInputDataList.get(1));
			driver.login(Utils.excelInputDataList.get(1));
			Configuration.closeWorkFlowReport();*/

		} catch (Exception e) {
		}
	}

	public void invalidMobileNumber() throws Exception {
		try {
			System.out.println("Inside Common Workflow");
			loginNegativeFMApp();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {	
				System.out.println("===== SR With Invalid Mobile Number =====");
					Configuration.reportConfiguration("InvalidMobileNumberInvalidGarage");
					Configuration.createNewWorkFlowReport("InvalidMobileNumberInvalidGarage");
					fleetManager.fmLoginWithIncorrectMobileNo(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1), isVerified);
					createServiceRequest.NegselectJobs(Utils.excelInputDataList.get(1), fleetManager,isVerified);
					createServiceRequest.findNoGarage(Utils.excelInputDataList.get(1),isVerified);
					Configuration.closeWorkFlowReport();
				
			}}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("NegativeFlowReports-InvalidMobileNumber&InvalidGarage");
		}
	}

	public void breakdownWithPartsInvalidVehicleNo() throws Exception {
		try {
			loginNegativeFMApp();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				Thread.sleep(3000);
				Configuration.reportConfiguration("BD-Invalid Vehicle Number");
				Configuration.createNewWorkFlowReport("BD-Invalid Vehicle Number");
				
				fleetManager.negativefmLogin(Utils.excelInputDataList.get(1),isVerified);
				breakDownRequest.createNewBreakDownInvalidVehicleNo(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("NegativeFlowReports-BD-Invalid Vehicle Number");
		}
	}

	public void bdVehicleNoAlreadyExists() throws Exception {
		try {
			loginNegativeFMApp();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				Thread.sleep(3000);
				Configuration.reportConfiguration("BDVehicleNoAlreadyExists");
				Configuration.createNewWorkFlowReport("BDVehicleNoAlreadyExists");
				
				fleetManager.negativefmLogin(Utils.excelInputDataList.get(1),isVerified);
				breakDownRequest.createNewBreakDownRequest(Utils.excelInputDataList.get(1));
				breakDownRequest.bdVehicleNoAlreadyExists(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("NegativeFlowReports-BDVehicleNoAlreadyExists");
		}
	}

	// --------------End Negative Validations---------------------------------------

	// --------------Common Scenarios
	// Starts------------------------------------------
	public void CommonScenariosSetUp() {

		try {
			System.out.println("Inside Common Workflow");
			ReadingExcelData.readingWorkFlowData();

			fleetManager1 = new FleetManager();
			mechanic1 = new Mechanic();
			driver = new Driver();
			breakDownRequest = new CreateBreakDownRequest();
			createServiceRequest = new CreateServiceRequest();
			trackLocation1 = new ServicesRequestTrackLocation();
			approveEstimate1 = new ServiceRequestApproveEstimate();
			startJob1 = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			// Configuration.createNewWorkFlowReport("Login Details");
			// fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
			// mechanic.login(Utils.excelInputDataList.get(1));
			// driver.login(Utils.excelInputDataList.get(1));
			// Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	public void FleetManagerUserEdit() throws Exception {

		try {
			CommonScenariosSetUp();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager User Edit =====");
				Configuration.reportConfiguration("FleetManagerUserEdit");
				Configuration.createNewWorkFlowReport("Login Details");
				fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
				Configuration.closeWorkFlowReport();
				Configuration.createNewWorkFlowReport("FleetManagerUserEdit");
				mProfileCreation.editUserProfile();
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("FleetManagerUserEdit");
		}
	}

	public void createNewFMProfile() throws Exception {                         

		try {
			CommonScenariosSetUp();

			System.out.println("Checking excel data size :::" + Utils.excelInputDataList);
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {

				System.out.println("===== FleetManager User creation  =====");
				Configuration.reportConfiguration("FleetManager User creation");
				Configuration.createNewWorkFlowReport("FleetManager User creation");
				mProfileCreation.fleetManageUserCreation(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
				Utils.sleepTimeLow();

				Configuration.createNewWorkFlowReport("Create New Vehicle");
				Thread.sleep(4000);
				mProfileCreation.createNewVehicle(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();

				Configuration.createNewWorkFlowReport("Create New Driver");
				Utils.sleepTimeLow();
				mProfileCreation.addNewDriver(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("CreateNewFMProfile");
		}
	}

	public void createMechanicProfile() throws Exception {

		try {
			CommonScenariosSetUp();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Mechanic User creation  =====");
				Configuration.reportConfiguration("CreateMechanicProfile");
				Configuration.createNewWorkFlowReport("CreateMechanicProfile");
				mProfileCreation.mechanicUserCreation(Utils.excelInputDataList.get(1),isVerified);
				mechanic1.Profilelogin(Utils.excelInputDataList.get(1),isVerified);
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("CreateMechanicProfile");
		}
	}

	public void FleetManagerAddNewVehicle() throws Exception {

		try {
			CommonScenariosSetUp();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager Add New Vehicle =====");
				Configuration.reportConfiguration("FleetManagerAddNewVehicle");
				Configuration.createNewWorkFlowReport("FleetManagerAddNewVehicle");
				fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
				mProfileCreation.addNewVehicle();
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("FleetManagerAddNewVehicle");
		}
	}

	public void mechanicRateCardChange() throws Exception {

		try {
			CommonScenariosSetUp();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== MechanicRateCardChange =====");
				Configuration.reportConfiguration("MechanicRateCardChange");
				Configuration.createNewWorkFlowReport("Login Details");
				mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
				Configuration.closeWorkFlowReport();
				Configuration.createNewWorkFlowReport("MechanicRateCardChange");
				mechanic1.mechanicRateCard();
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("MechanicRateCardChange");
		}
	}

	public void newDriverCreation() throws Exception {

		try {
			CommonScenariosSetUp();
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== NewDriverCreation =====");
				Configuration.reportConfiguration("NewDriverCreation");
				Configuration.createNewWorkFlowReport("NewDriverCreation");
				fleetManager1.login(Utils.excelInputDataList.get(1), isVerified);
				driver.newDriverCreation(Utils.excelInputDataList.get(1));
				Configuration.closeWorkFlowReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("NewDriverCreation");
		}
	}
	// --------------Common Scenarios Ends------------------------------------------

	// ---------------------NEW CR's-------------------------------------
	public void SRRetailerInvoice_CR() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRRetailerInvoice_CR =====");
					Configuration.reportConfiguration("SRRetailerInvoice_CR");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(1),isVerified);
					mechanic1.login(Utils.excelInputDataList.get(1), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRRetailerInvoice_CR");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1),isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1),fleetManager1,isVerified); 
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(1),isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1,isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(1),mechanic1,isVerified); 
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(1),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(1),isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(1),fleetManager1,isVerified);
					startJob1.startJobsWithAddImage(Utils.excelInputDataList.get(1), mechanic1, isVerified);
					fleetManager1.viewBillRetailerInvoice(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(1), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(1), isVerified);
					mechactualvalue=mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(1));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(1), isVerified);
					fleetManager1.FMcheckMyPrevOrderCheckImage(Utils.excelInputDataList.get(1), mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(1));
					Configuration.closeWorkFlowReport();
					
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRRetailerInvoice_CR");
		}
	}
	
	public void SRRetailerInvoiceNoUploadImage_CR() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRRetailerInvoiceNoUploadImage_CR =====");
					Configuration.reportConfiguration("SRRetailerInvoiceNoUploadImage_CR");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(2),isVerified);
					mechanic1.login(Utils.excelInputDataList.get(2), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRRetailerInvoiceNoUploadImage_CR");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(2),isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(2),fleetManager1,isVerified); 
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(2),isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1,isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(2),mechanic1,isVerified); 
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(2),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(2),isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(2),fleetManager1,isVerified);
					startJob1.startJobs(Utils.excelInputDataList.get(2), mechanic1, isVerified);
					fleetManager1.viewBillRetailerInvoiceNoImageFound(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(2), isVerified);
					mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(2));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(2), isVerified);
					fleetManager1.FMcheckMyPrevOrderCheckImage(Utils.excelInputDataList.get(2), mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(2));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRRetailerInvoiceNoUploadImage_CR");
		}
	}

	public void SRRetailerInvoiceCheckImages_CR() throws Exception {
		try {
			basicSRBaseFlow();

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				if (isVerified) {
					System.out.println("===== SRRetailerInvoiceCheckImages_CR =====");
					Configuration.reportConfiguration("SRRetailerInvoiceCheckImages_CR");
					Configuration.createNewWorkFlowReport("Login Details");
					fleetManager1.login(Utils.excelInputDataList.get(3),isVerified);
					mechanic1.login(Utils.excelInputDataList.get(3), isVerified);
					Configuration.closeWorkFlowReport();

					Configuration.createNewWorkFlowReport("SRRetailerInvoiceCheckImages_CR");
					createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(3),isVerified);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(3),fleetManager1,isVerified); 
					mRequestOrderId1 = createServiceRequest.findGarage(Utils.excelInputDataList.get(3),isVerified);
					mechanic1.mechanicAcceptOrder(mRequestOrderId1,isVerified);
					trackLocation1.giveEstimate(Utils.excelInputDataList.get(3),mechanic1,isVerified); 
					trackLocation1.completeOrYes(isVerified);
					trackLocation1.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(3),isVerified);
					trackLocation1.selectRetailer(Utils.excelInputDataList.get(3),isVerified);
					approveEstimate1.approveEstimateWithPayCash(Utils.excelInputDataList.get(3),fleetManager1,isVerified);
					startJob1.startJobsWithRecursiveImages(Utils.excelInputDataList.get(3), mechanic1, isVerified);
					fleetManager1.viewBillRetailerInvoice(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.confirmCashReceipt(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.mechanicRating(Utils.excelInputDataList.get(3), isVerified);
					mechanic1.MechMyPreviousOrder(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					mechanic1.MECHLogout(Utils.excelInputDataList.get(3));
					fleetManager1.fleetManagerRating(Utils.excelInputDataList.get(3), isVerified);
					fleetManager1.FMcheckMyPrevOrderCheckImage(Utils.excelInputDataList.get(3), mRequestOrderId1, isVerified);
					fleetManager1.FMLogout(Utils.excelInputDataList.get(3));
					Configuration.closeWorkFlowReport();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Configuration.catchcall();
		} finally {
			Configuration.saveReport();
			SendMail("SRRetailerInvoiceCheckImages_CR");
		}
	}
	
	
}

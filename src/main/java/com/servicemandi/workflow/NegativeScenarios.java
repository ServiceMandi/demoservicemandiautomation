package com.servicemandi.workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;
import com.sevicemandi.servicerequest.ServiceRequestApproveEstimate;
import com.sevicemandi.servicerequest.ServiceRequestStartJob;
import com.sevicemandi.servicerequest.ServicesRequestTrackLocation;

public class NegativeScenarios {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateBreakDownRequest breakDownRequest;
	private CreateServiceRequest createServiceRequest;
	private ServicesRequestTrackLocation trackLocation;
	private ServiceRequestApproveEstimate approveEstimate;
	private ServiceRequestStartJob startJob;
	private String mRequestOrderId = "";
	private ProfileCreation mProfileCreation;

	@BeforeTest
	public void setUpConfiguration() {

		try {
			ReadingExcelData.readingNegativeData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			breakDownRequest = new CreateBreakDownRequest();
			createServiceRequest = new CreateServiceRequest();
			trackLocation = new ServicesRequestTrackLocation();
			approveEstimate = new ServiceRequestApproveEstimate();
			startJob = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
		//	Configuration.reportConfiguration();

		} catch (Exception e) {

		}
	}

	@AfterTest
	public void afterTest() throws InterruptedException {
		Configuration.saveReport();
	}

	@AfterSuite
	public void moveFile() {

		/*
		 * File file = new File(System.getProperty("user.dir") + File.separator
		 * + "Reports.html"); String fileName = new
		 * SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date()); if
		 * (file.renameTo(new File( System.getProperty("user.dir") +
		 * File.separator + "Reports" + File.separator + fileName + ".html"))) {
		 * // if file copied successfully then delete the original file
		 * System.out.println("File moved successfully"); } else {
		 * System.out.println("Failed to move the file"); }
		 */

	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	@Test
	public void invalidMobileNumber() throws InterruptedException {
		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				Thread.sleep(3000);
				System.out.println("===== Service Request With Invalid Mobile Number =====");
				Configuration.createNewWorkFlowReport("Invalid MobileNumber & Invalid Garage");
				fleetManager.fmLoginWithIncorrectMobileNo(Utils.excelInputDataList.get(0));
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				createServiceRequest.findNoGarage(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void breakdownWithPartsInvalidVehicleNo() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				Thread.sleep(3000);
				Configuration.createNewWorkFlowReport("BD-Invalid Vehicle Number");
				fleetManager.negativefmLogin(Utils.excelInputDataList.get(0));
				breakDownRequest.createNewBreakDownInvalidVehicleNo(Utils.excelInputDataList.get(0));


			}
		} catch (Exception e) {

		}
	}

	@Test
	public void breakdownWithPartsVehicleNoAlreadyExists() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				Thread.sleep(3000);
				Configuration.createNewWorkFlowReport("BDVehicleNoAlreadyExists");
				fleetManager.negativefmLogin(Utils.excelInputDataList.get(1));
				breakDownRequest.createNewBreakDownRequest(Utils.excelInputDataList.get(1));
				breakDownRequest.bdVehicleNoAlreadyExists(Utils.excelInputDataList.get(1));


			}
		} catch (Exception e) {

		}
	}
}

package com.servicemandi.workflow;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.servicemandi.common.Configuration;

public class Main extends BreakDownFlow1  {

	public static Scanner s = null;
	JLabel jl = new JLabel();
	private static JFrame frame;
	JCheckBox jcb = new JCheckBox("BreakDownWithParts");

	JCheckBox jcb1 = new JCheckBox("BreakDownWithOutParts");
	JCheckBox jcb2 = new JCheckBox("BreakDownWithPartsGiveEstimate");

	JCheckBox jcb3 = new JCheckBox("BreakDownMechanicRejectsOrder");
	JCheckBox jcb4 = new JCheckBox("BDMechanicDeletesAtTrackLoc");
	JCheckBox jcb5 = new JCheckBox("BDFMDeletesAtTrackLoc");

	JCheckBox jcb6 = new JCheckBox("BreakDownWithPartsRevisedBill");
	JCheckBox jcb7 = new JCheckBox("BreakDownWithPartsWithOutRetailer");
	JCheckBox jcb8 = new JCheckBox("BreakDownWithoutPartsRevisedBill");

	JCheckBox jcb9 = new JCheckBox("BreakDownFMCancelAtConfirmGarage");
	JCheckBox jcb10 = new JCheckBox("BDFMCancelAllJobsInApproveEstimate");
	JCheckBox jcb11 = new JCheckBox("BreakDownWithPartsRevisedEstimateWithOutParts");

	JCheckBox jcb12 = new JCheckBox("BreakDownWithPartsGiveEstimateCompleteOrder");
	JCheckBox jcb13 = new JCheckBox("BDWithoutPartsFMCancelJobInAproveEstimate");
	JCheckBox jcb14 = new JCheckBox("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
	JCheckBox jcb15 = new JCheckBox("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
	JCheckBox jcb16 = new JCheckBox("BreakDownWithoutPartsRevisedEstimateWithParts");
	JCheckBox jcb17 = new JCheckBox("BreakDownWithoutPartsRevisedEstimateWithOutParts");
	JCheckBox jcb18 = new JCheckBox("BreakDownPartsRevisedWithPartsRevisedBill");
	JCheckBox jcb19 = new JCheckBox("BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
	JCheckBox jcb20 = new JCheckBox("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
	JCheckBox jcb21 = new JCheckBox("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");

	
	JCheckBox a = new JCheckBox("Select/DeSelect All CheckBox");
	final JTextArea textArea = new JTextArea("BreakDown Flow Status");
	JComboBox<String> breakDownStatus = new JComboBox<String>();
	BreakDownFlow1 b1;
	Configuration s1;
	JTextField f;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {

				new Main();

			}
		});


	}

	public Main() {
		initComponents();
		b1=new BreakDownFlow1();
	   


	}

	void initComponents() {
		try{
			frame = new JFrame();
			f = new JTextField("BreakDownFlow Status");
			int offset = 7;
			frame.setTitle("Service MANDI-Automation");
			frame.getContentPane().add(jl);
			jl.setIcon(new ImageIcon("D:/App/ServiceaMand_Logo1.jpg"));
			frame.validate();
			frame.setBounds(200, 200, 578, 448);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.dispose();
			//System.exit(0);

			frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
//			BachGroundImg b2=new BachGroundImg();
//			frame.add(b2);

			//System.exit(0); // stop program
			frame.dispose(); // close window
			frame.setVisible(false); // hide window

			JButton breakDownFlow;
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			JPanel footerPanel = new JPanel();
			breakDownFlow = new JButton("BreakDownFlow");

			//		JCheckBox checkBox =  new JCheckBox("CheckBox");
			//        checkBox.setToolTipText("checkbox tool tip");
			//		//checkBox.addFocusListener(fa  );
			//        add( checkBox );


			JButton btnNewButton = new JButton("Start Servers");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					// =================================
					// Runtime rt = Runtime.getRuntime();
					// rt.exec(new String[]{"cmd.exe","/c","start"});
					String command = "cmd.exe /c start "
							+ "appium --address 0.0.0.0 -p 4723 --chromedriver-executable=D:/App/chromedriver_win32/chromedriver.exe";
					String command1 = "cmd.exe /c start " + "appium --address 127.0.0.1 -p 4273";
					// String command2 = "cmd.exe /c start " + "appium --address
					// 127.0.0.1 -p 4274";

					try {
						Process child = Runtime.getRuntime().exec(command);
						Process child1 = Runtime.getRuntime().exec(command1);
						// Process child2 = Runtime.getRuntime().exec(command2);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});

			JButton btnSaveReport = new JButton("SaveReport");
			btnSaveReport.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// afterTest();
					Configuration.saveReport();
				}
			});

			JPanel contentPane = new JPanel();
			// contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			// contentPane.setLayout(new BorderLayout(5, 5));
			//
			// // checkBox = new JCheckBox[data.length];
			// JPanel centerPanel = new JPanel();
			// centerPanel.setLayout(new GridLayout(0, 4, 5, 5));

			contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(5, 5));

			// checkBox = new JCheckBox[data.length];
			JPanel centerPanel = new JPanel();
			centerPanel.setLayout(new GridLayout(0, 3, 4, 4));

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb.isSelected()) {
						
						breakDownWithParts();
						//breakDownStatus.addItem("BreakDownWithParts Folw Completed");
//						textArea.setText("BreakDownWithParts Flow Completed");
//						jcb.setToolTipText("BreakDownWithParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb1.isSelected()) {
						breakDownWithOutParts();
//						breakDownStatus.addItem("BreakDownWithOutParts Folw Completed");
//						textArea.setText("BreakDownWithOutParts Flow Completed");
//						jcb1.setToolTipText("BreakDownWithOutParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithOutParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithOutParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb2.isSelected()) {
						breakDownWithPartsGiveEstimate();
//						breakDownStatus.addItem("BreakDownWithPartsGiveEstimate Folw Completed");
//						textArea.setText("BreakDownWithPartsGiveEstimate Flow Completed");
//						jcb2.setToolTipText("BreakDownWithPartsGiveEstimate Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsGiveEstimate Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsGiveEstimate method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb3.isSelected()) {
						breakDownMechanicRejectsOrder();
//						breakDownStatus.addItem("BreakDownMechanicRejectsOrder Folw Completed");
//						textArea.setText("BreakDownMechanicRejectsOrder Flow Completed");
//						jcb3.setToolTipText("BreakDownMechanicRejectsOrder Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownMechanicRejectsOrder Flow Completed");				
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsGiveEstimate method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb4.isSelected()) {
						BDMechanicDeletesAtTrackLoc();
//						breakDownStatus.addItem("BDMechanicDeletesAtTrackLoc Folw Completed");
//						textArea.setText("BDMechanicDeletesAtTrackLoc Flow Completed");
//						jcb4.setToolTipText("BDMechanicDeletesAtTrackLoc Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BDMechanicDeletesAtTrackLoc Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BDMechanicDeletesAtTrackLoce method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb5.isSelected()) {
						BDFMDeletesAtTrackLoc();
//						breakDownStatus.addItem("BDFMDeletesAtTrackLoc Folw Completed");
//						textArea.setText("BDFMDeletesAtTrackLoc Flow Completed");
//						jcb5.setToolTipText("BDFMDeletesAtTrackLoc Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BDFMDeletesAtTrackLoc Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BDFMDeletesAtTrackLoc method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb6.isSelected()) {
						breakDownWithPartsRevisedBill();
//						breakDownStatus.addItem("BreakDownWithPartsRevisedBill Folw Completed");
//						textArea.setText("BreakDownWithPartsRevisedBill Flow Completed");
//						jcb6.setToolTipText("BreakDownWithPartsRevisedBill Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsRevisedBill Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsRevisedBill method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb7.isSelected()) {
						breakDownWithPartsWithOutRetailer();
//						breakDownStatus.addItem("BreakDownWithPartsWithOutRetailer Folw Completed");
//						textArea.setText("BreakDownWithPartsWithOutRetailer Flow Completed");
//						jcb7.setToolTipText("BreakDownWithPartsWithOutRetailer Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsWithOutRetailer Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsWithOutRetailer method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb8.isSelected()) {
						breakDownWithoutPartsRevisedBill();
//						breakDownStatus.addItem("BreakDownWithoutPartsRevisedBill Folw Completed");
//						textArea.setText("BreakDownWithoutPartsRevisedBill Flow Completed");
//						jcb8.setToolTipText("BreakDownWithoutPartsRevisedBill Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithoutPartsRevisedBill Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithoutPartsRevisedBill method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb9.isSelected()) {
						breakDownFMCancelAtConfirmGarage();
//						breakDownStatus.addItem("BreakDownFMCancelAtConfirmGarage Folw Completed");
//						textArea.setText("BreakDownFMCancelAtConfirmGarage Flow Completed");
//						jcb9.setToolTipText("BreakDownFMCancelAtConfirmGarage Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownFMCancelAtConfirmGarage Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownFMCancelAtConfirmGarage() method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb10.isSelected()) {
						BDFMCancelAllJobsInApproveEstimate();
//						breakDownStatus.addItem("BDFMCancelAllJobsInApproveEstimate Folw Completed");
//						textArea.setText("BDFMCancelAllJobsInApproveEstimate Flow Completed");
//						jcb10.setToolTipText("BDFMCancelAllJobsInApproveEstimate Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BDFMCancelAllJobsInApproveEstimate Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BDFMCancelAllJobsInApproveEstimate method unselected");
					}
				}
			});
			//BreakDownWithPartsGiveEstimateCompleteOrder
			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb11.isSelected()) {
						breakDownWithPartsRevisedEstimateWithOutParts();
//						breakDownStatus.addItem("BDFMCancelAllJobsInApproveEstimate Folw Completed");
//						textArea.setText("BDFMCancelAllJobsInApproveEstimate Flow Completed");
//						jcb11.setToolTipText("BDFMCancelAllJobsInApproveEstimate Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsRevisedEstimateWithOutParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsRevisedEstimateWithOutParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb12.isSelected()) {
						breakDownWithPartsGiveEstimateCompleteOrder();
//						breakDownStatus.addItem("BreakDownWithPartsGiveEstimateCompleteOrder Folw Completed");
//						textArea.setText("BreakDownWithPartsGiveEstimateCompleteOrder Flow Completed");
//						jcb12.setToolTipText("BreakDownWithPartsGiveEstimateCompleteOrder Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsGiveEstimateCompleteOrder Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsGiveEstimateCompleteOrder method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb13.isSelected()) {
						BDWithoutPartsFMCancelJobInAproveEstimate();
//						breakDownStatus.addItem("BDWithoutPartsFMCancelJobInAproveEstimate Folw Completed");
//						textArea.setText("BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
//						jcb13.setToolTipText("BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BDWithoutPartsFMCancelJobInAproveEstimate method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb14.isSelected()) {
						breakDownWithPartsNoRetailerRevisedEstimateWithoutParts();
//						breakDownStatus.addItem("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Folw Completed");
//						textArea.setText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
//						jcb14.setToolTipText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts method unselected");
					}
				}
			});
			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb15.isSelected()) {
						breakDownWithPartsNoRetailerRevisedEstimateWithoutParts();
//						breakDownStatus.addItem("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Folw Completed");
//						textArea.setText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
//						jcb15.setToolTipText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb16.isSelected()) {
						breakDownWithoutPartsRevisedEstimateWithParts();
//						breakDownStatus.addItem("BreakDownWithoutPartsRevisedEstimateWithParts Folw Completed");
//						textArea.setText("BreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
//						jcb16.setToolTipText("BreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithoutPartsRevisedEstimateWithParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb17.isSelected()) {
						breakDownWithoutPartsRevisedEstimateWithOutParts();
//						breakDownStatus.addItem("BreakDownWithoutPartsRevisedEstimateWithOutParts Folw Completed");
//						textArea.setText("BreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
//						jcb17.setToolTipText("BreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithoutPartsRevisedEstimateWithOutParts method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb18.isSelected()) {
						breakDownPartsRevisedWithPartsRevisedBill();
//						breakDownStatus.addItem("BreakDownPartsRevisedWithPartsRevisedBill Folw Completed");
//						textArea.setText("BreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
//						jcb18.setToolTipText("BreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownPartsRevisedWithPartsRevisedBill method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb19.isSelected()) {
					breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill();
						breakDownStatus.addItem("BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Folw Completed");
//						textArea.setText("BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
//						jcb19.setToolTipText("BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb20.isSelected()) {
						BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill();
//						breakDownStatus.addItem("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Folw Completed");
//						textArea.setText("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
//						jcb20.setToolTipText("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill method unselected");
					}
				}
			});

			breakDownFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					System.out.println("AE Value::" + ae);
					if (jcb21.isSelected()) {
						breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer();
//						breakDownStatus.addItem("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Folw Completed");
//						textArea.setText("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
//						jcb21.setToolTipText("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
//						JOptionPane.showMessageDialog(frame, "BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
					} else {
						// JOptionPane.showMessageDialog(frame, "JCheckBox is NOT
						// selected");breakDownWithOutParts()
						System.out.println("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer method unselected");
					}
				}
			});

			//		centerPanel.add(checkBox); 
			centerPanel.add(a);
			centerPanel.add(jcb);
			centerPanel.add(jcb1);
			centerPanel.add(jcb2);
			centerPanel.add(jcb3);
			centerPanel.add(jcb4);
			centerPanel.add(jcb5);
			centerPanel.add(jcb6);
			centerPanel.add(jcb7);
			centerPanel.add(jcb8);
			centerPanel.add(jcb9);
			centerPanel.add(jcb10);
			centerPanel.add(jcb11);

			centerPanel.add(jcb12);
			centerPanel.add(jcb13);
			centerPanel.add(jcb14);
			centerPanel.add(jcb15);
			centerPanel.add(jcb16);
			centerPanel.add(jcb17);
			centerPanel.add(jcb18);
			centerPanel.add(jcb19);
			centerPanel.add(jcb20);
			centerPanel.add(jcb21);

			JButton selectAllCheckBox = new JButton("Select All CheckBox");

			a.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == a) {
						// Set all JCheckBox to select when JCheckBox a is select
						if (a.isSelected() == true) {
							jcb.setSelected(true);
							jcb1.setSelected(true);
							jcb2.setSelected(true);
							jcb3.setSelected(true);
							jcb4.setSelected(true);
							jcb5.setSelected(true);
							jcb6.setSelected(true);
							jcb7.setSelected(true);
							jcb8.setSelected(true);
							jcb9.setSelected(true);
							jcb10.setSelected(true);
							jcb11.setSelected(true);

							jcb12.setSelected(true);
							jcb13.setSelected(true);
							jcb14.setSelected(true);
							jcb15.setSelected(true);
							jcb16.setSelected(true);
							jcb17.setSelected(true);
							jcb18.setSelected(true);
							jcb19.setSelected(true);
							jcb20.setSelected(true);
							jcb21.setSelected(true);

						} else {
							jcb.setSelected(false);
							jcb1.setSelected(false);
							jcb2.setSelected(false);
							jcb3.setSelected(false);
							jcb4.setSelected(false);
							jcb5.setSelected(false);
							jcb6.setSelected(false);
							jcb7.setSelected(false);
							jcb8.setSelected(false);
							jcb9.setSelected(false);
							jcb10.setSelected(false);
							jcb11.setSelected(false);
							jcb12.setSelected(false);
							jcb13.setSelected(false);
							jcb14.setSelected(false);
							jcb15.setSelected(false);
							jcb16.setSelected(false);
							jcb17.setSelected(false);
							jcb18.setSelected(false);
							jcb19.setSelected(false);
							jcb20.setSelected(false);
							jcb21.setSelected(false);

						}

					}
				}
			});


			contentPane.add(centerPanel, BorderLayout.CENTER);

			// footerPanel.add(selectAllCheckBox);textArea.setText("your Text here");breakDownStatus
			footerPanel.add(a);
			footerPanel.add(btnNewButton);
			footerPanel.add(breakDownFlow);
			footerPanel.add(btnSaveReport);
			//footerPanel.add(f);
			footerPanel.add(textArea);
			footerPanel.add(breakDownStatus);
			contentPane.add(footerPanel, BorderLayout.PAGE_END);

			frame.setContentPane(contentPane);
			frame.pack();
			frame.setLocationByPlatform(true);
			frame.setVisible(true);
			f.setVisible(true);

			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			//System.exit(0);

			Configuration.saveReport();
			System.exit(0);
		}catch(Exception e){

		}finally{
			//Configuration.saveReport();
		}

	}

	private void add(JCheckBox checkBox) {
		// TODO Auto-generated method stub

	}
}





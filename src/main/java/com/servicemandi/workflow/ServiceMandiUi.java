package com.servicemandi.workflow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

import org.springframework.util.FileSystemUtils;

import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;

public class ServiceMandiUi extends ServiceMandiWorkflow{

	private static final int EXIT_ON_CLOSE = 0;
	ImageIcon ii;
	private static JFrame frame;
	JPanel centerPanel = new JPanel();
	JPanel footerPanel = new JPanel();
	JPanel belowfooterPanel = new JPanel();

	final JLabel copyRightLabel = new JLabel("Copyrights@ServiceMandi_2018, All rights reserved");

	final JTextArea textArea = new JTextArea("BreakDown Flow Status");
	JComboBox<String> breakDownStatus = new JComboBox<String>();
	JCheckBox selectDeselect = new JCheckBox("Select/DeSelect All CheckBox");
	JComboBox<String> bDBaseFlow = new JComboBox<String>();
	JCheckBox wf1;
	//BreakDownFlow1 b1;
	JList places;
	JLabel SMworkFlowLabel = new JLabel("AllCheckBox::");
	JLabel JL1 = new JLabel();
	JLabel JL2 = new JLabel();
	JLabel JL3 = new JLabel();
	JLabel JL4 = new JLabel();
	JLabel JL11 = new JLabel();
	JLabel JL5 = new JLabel("<html><font color='black'>SERVICES & REPAIRS FLOWS</font></html>");
	JLabel JL6 = new JLabel();
	JLabel JL7 = new JLabel();
	JLabel JL8 = new JLabel("<html><font color='black'>BREAKDOWN FLOWS</font></html>");
	JLabel JL9 = new JLabel();
	JLabel JL10 = new JLabel();
	JLabel JL12 = new JLabel("<html><font color='black'> FlowStatus :</font><size=20></html>");
	JLabel JL13 = new JLabel();
	
	JLabel JL51 = new JLabel();
	JLabel JL52 = new JLabel();
	JLabel JL15 = new JLabel();
	
	JCheckBox BDAll = new JCheckBox("BD");
	JCheckBox BDCRAll = new JCheckBox("BDCReject");
	JCheckBox NBDAll = new JCheckBox("BDNegative");
	JCheckBox SRAll = new JCheckBox("SR");
	JCheckBox SRCRAll = new JCheckBox("SRCReject");
	JCheckBox NSRAll = new JCheckBox("SRNegative");
	JCheckBox DBDAll = new JCheckBox("DriverBD");
	JCheckBox DBDCRAll = new JCheckBox("DBDCRej");
	JCheckBox NDBDAll = new JCheckBox("DBDNeg");
	JCheckBox DSRAll = new JCheckBox("DriverSR");
	JCheckBox DSRCRAll = new JCheckBox("DSRCRej");
	JCheckBox NDSRAll = new JCheckBox("DSRNeg");
	JCheckBox NEGAll = new JCheckBox("Negative");

	// BDBase Flow Checkbox
	JCheckBox BD1 = new JCheckBox("BreakDownWithParts");
	JCheckBox BD2 = new JCheckBox("BreakDownWithOutParts");
	JCheckBox BD3 = new JCheckBox("BDMechanicDeletesAtTrackLoc");
	JCheckBox BD4 = new JCheckBox("BDFMDeletesAtTrackLoc");
	JCheckBox BD5 = new JCheckBox("BDWithPartsRevisedBill");
	JCheckBox BD6 = new JCheckBox("BDWithPartsRevisedEstimateWithParts");
	JCheckBox BD7 = new JCheckBox("BDWithoutPartsRevisedEstimateWithParts");
	JCheckBox BD8 = new JCheckBox("BDWithPartsRevisedEstimateWithOutParts");
	JCheckBox BD9 = new JCheckBox("BDWithoutPartsRevisedEstimateWithOutParts");
	JCheckBox BD10 = new JCheckBox("BDWithoutPartsRevisedBill");
	JCheckBox BD11 = new JCheckBox("BDWithPartsRevisedWithPartsRevisedBill");
	JCheckBox BD12 = new JCheckBox("BDWithoutPartsRevisedEstimateWithPartsRevisedbill");
	JCheckBox BD13 = new JCheckBox("BDWithPartsRevisedEstimateWithOutPartsRevisedBill");
	JCheckBox BD14 = new JCheckBox("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
	
	// BD CancelReject Flow Checkbox
	JCheckBox BDCR1 = new JCheckBox("BDFMDeletesAtFindGarage");
	JCheckBox BDCR2 = new JCheckBox("BreakDownFMCancelAtConfirmGarage");
	JCheckBox BDCR3 = new JCheckBox("BDFMDeletesAtTrackLoc");
	JCheckBox BDCR4 = new JCheckBox("BDFMDeletesAtVehicleInGarage");
	JCheckBox BDCR5 = new JCheckBox("BDWithoutPartsFMCancelJobInAproveEstimate");
	JCheckBox BDCR6 = new JCheckBox("BDFMCancelAllJobsInApproveEstimate");
	JCheckBox BDCR7 = new JCheckBox("BreakDownMechanicRejectsOrder");
	JCheckBox BDCR8 = new JCheckBox("BDMechanicDeletesAtTrackLoc");
	JCheckBox BDCR9 = new JCheckBox("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");

	// Negative BD Flow Checkbox
	JCheckBox NBD1 = new JCheckBox("BreakDownWithPartsWithOutRetailer");
	JCheckBox NBD2 = new JCheckBox("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
	JCheckBox NBD3 = new JCheckBox("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
	JCheckBox NBD4 = new JCheckBox("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");

	// SRBase Flow Checkbox
	JCheckBox SR1 = new JCheckBox("SRWithParts");
	JCheckBox SR2 = new JCheckBox("SRWithoutParts");
	JCheckBox SR3 = new JCheckBox("SRWithPartsGiveEstimate");
	
	JCheckBox SR5 = new JCheckBox("SRWithPartsGiveEstimateCompleteOrder");
	JCheckBox SR6 = new JCheckBox("SRWithPartsRevisedEstimateWithParts");
	JCheckBox SR7 = new JCheckBox("SRWithoutPartsReviseEstimateWithParts");
	JCheckBox SR8 = new JCheckBox("SRWithPartsRevisedEstimateWithOutParts");
	JCheckBox SR9 = new JCheckBox("SRWithoutPartsRevisedWithoutParts");
	JCheckBox SR10 = new JCheckBox("SRWithPartsRevisedEstimateWithPartsRevisedBill");
	JCheckBox SR11 = new JCheckBox("SRWithoutPartsRevisedEstimatePartsRevisedBill");
	JCheckBox SR12 = new JCheckBox("SRWithPartsRevisedEstimateWithoutPartsRevisedBill");
	JCheckBox SR13 = new JCheckBox("SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
	
	// SR CancelReject Flow Checkbox
	JCheckBox SRCR1 = new JCheckBox("SRFMDeletesAtFindGarage");
	JCheckBox SRCR2 = new JCheckBox("SRFMCancelAtTrackLocation");
	JCheckBox SRCR3 = new JCheckBox("SRFMDeletesAtTrackLocation");
	JCheckBox SRCR4 = new JCheckBox("SRFMDeletesAtApproveEstimate");
	JCheckBox SRCR5 = new JCheckBox("SRFMCancelOneJobInApproveEstimate");
	JCheckBox SRCR6 = new JCheckBox("SRFMCancelAllJobsInApproveEstimate");
	JCheckBox SRCR7 = new JCheckBox("SRMultiJobsFMCancelOneJobInApproveEstimate");
	JCheckBox SRCR8 = new JCheckBox("SRDeleteOneJobAndAddOneNewJobByMechanic");
	JCheckBox SRCR9 = new JCheckBox("SRWithPartsWithOutRetailerCancelOneJob");
	JCheckBox SRCR10 = new JCheckBox("SRMechanicRejectsOrder");
	JCheckBox SRCR11 = new JCheckBox("SRMechanicDeletesAtTrackLoc");
	JCheckBox SRCR12 = new JCheckBox("SRDeleteOneJobByMechanic");
		
	// Negative SR Flow Checkbox
	JCheckBox NSR1 = new JCheckBox("SRWithPartsWithOutRetailer");
	JCheckBox NSR2 = new JCheckBox("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
	JCheckBox NSR3 = new JCheckBox("SRWithPartsWithRetailerRevisedPartsNoRetailer");
	JCheckBox NSR4 = new JCheckBox("SRPartsNoRetailerRevisedwithPartsWithRetailer");
	JCheckBox NSR5 = new JCheckBox("SRIDontKnowWhatsWrongWithParts");
	JCheckBox NSR6 = new JCheckBox("SRIDontKnowWhatsWrongWithOutParts");

	// DriverBDBase Flow Checkbox
	JCheckBox DBD1 = new JCheckBox("DriverBreakDownWithoutParts");
	JCheckBox DBD2 = new JCheckBox("DriverBreakDownWithParts");
	JCheckBox DBD3 = new JCheckBox("DriverBreakDownWithPartsRevisedEstimateWithParts");
	JCheckBox DBD4 = new JCheckBox("DriverBreakDownWithoutPartsRevisedEstimateWithParts");
	JCheckBox DBD5 = new JCheckBox("DriverBreakDownWithPartsRevisedEstimateWithOutParts");
	JCheckBox DBD6 = new JCheckBox("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts");
	JCheckBox DBD7 = new JCheckBox("DriverBreakDownEstimateWithPartsRevisedBill");
	JCheckBox DBD8 = new JCheckBox("DriverbreakDownWithoutPartsRevisedBill");
	JCheckBox DBD9 = new JCheckBox("DriverbreakDownPartsRevisedWithPartsRevisedBill");
	JCheckBox DBD10 = new JCheckBox("DriverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");
	JCheckBox DBD11 = new JCheckBox("DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
	JCheckBox DBD12 = new JCheckBox("DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
	JCheckBox DBD13 = new JCheckBox("DriverBreakDownWithPartsWithOutRetailer");
	
	// DriverBD CancelReject Flow Checkbox
	JCheckBox DBDCR1 = new JCheckBox("DriverBDFMDeletesAtTrackLoc");
	JCheckBox DBDCR2 = new JCheckBox("DriverBDMechanicRejectsOrder");
	JCheckBox DBDCR3 = new JCheckBox("DriverBreakDownFMDeletesAtConfirmGarage");
	JCheckBox DBDCR4 = new JCheckBox("DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate");
	JCheckBox DBDCR5 = new JCheckBox("DriverBDFMCancelAllJobsInApproveEstimate");
	JCheckBox DBDCR6 = new JCheckBox("DriverBreakDownMechanicDeletesAtTrackLoc");
	JCheckBox DBDCR7 = new JCheckBox("DriverBDWithoutPartsFMCancelJobInAproveEstimate");
	JCheckBox DBDCR8 = new JCheckBox("DriverBDFMDeletesAtVehicleInGarage");
	
	//Negative DriverBD Flow Checkbox
	JCheckBox NDBD1 = new JCheckBox("DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
	JCheckBox NDBD2 = new JCheckBox("DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
	JCheckBox NDBD3 = new JCheckBox("DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
	JCheckBox NDBD4 = new JCheckBox("DriverNegativeScenarioFMDeletes");
	
	//DriverSRBase Flow Checkbox
	JCheckBox DSR1 = new JCheckBox("DriverSRWithParts");
	JCheckBox DSR2 = new JCheckBox("DriverSRWithoutParts");
	JCheckBox DSR3 = new JCheckBox("DriverSRWithPartsRevisedEstimateWithParts");
	JCheckBox DSR4 = new JCheckBox("DriverSRWithoutPartsRevisedEstimateWithParts");
	JCheckBox DSR5 = new JCheckBox("DriverSRWithPartsRevisedEstimateWithOutParts");
	JCheckBox DSR6 = new JCheckBox("DriverSRWithoutPartsRevisedWithoutParts");
	JCheckBox DSR7 = new JCheckBox("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill");
	JCheckBox DSR8 = new JCheckBox("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill");
	//JCheckBox DSR9 = new JCheckBox("DriverSRWithoutPartsRevisedWithoutParts");
	JCheckBox DSR10 = new JCheckBox("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill");
	JCheckBox DSR11 = new JCheckBox("DriverSREstimateWithPartsRevisedBill");
	JCheckBox DSR12 = new JCheckBox("DriverSREstimateWithoutPartsRevisedBill");
	
	//DriverSR CancelReject Flow Checkbox
	JCheckBox DSRCR1 = new JCheckBox("DriverSRFMDeletesAtTrackLocation");
	JCheckBox DSRCR2 = new JCheckBox("DriverSRMechanicDeletesAtTrackLoc");
	JCheckBox DSRCR3 = new JCheckBox("DriverSRFMDeletesAtFindGarage");
	JCheckBox DSRCR4 = new JCheckBox("DriverSRFMDeletesAtApproveEstimate");
	JCheckBox DSRCR5 = new JCheckBox("DriverSRFMCancelAllJobsInApproveEstimate");
	JCheckBox DSRCR6 = new JCheckBox("DriverSRDeleteOneJobAndAddOneNewJobByMechanic");
	JCheckBox DSRCR7 = new JCheckBox("DriverSRFMCancelOneJobInApproveEstimate");
	JCheckBox DSRCR8 = new JCheckBox("DriverSRMechanicRejectsOrder");
	JCheckBox DSRCR9 = new JCheckBox("DriverSRFMDeletes");
	JCheckBox DSRCR10 = new JCheckBox("DriverSRWithPartsWithOutRetailerCancelOneJob");
	
	// Negative DriverSR Flow Checkbox
	JCheckBox NDSR1 = new JCheckBox("DriverServiceRequestIDontKnowWhatsWrongWithParts");
	JCheckBox NDSR2 = new JCheckBox("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer");
	JCheckBox NDSR3 = new JCheckBox("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer");
	JCheckBox NDSR4 = new JCheckBox("DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
	JCheckBox NDSR5 = new JCheckBox("DriverSRWithPartsNoRetailer");
	
	//SR Payment Scenarios Checkbox
	JCheckBox SRPS1 = new JCheckBox("SRRetailerInvoice_CR");
	JCheckBox SRPS2= new JCheckBox("SRWithPartsDownloadPDF_CR");
	JCheckBox SRPS3 = new JCheckBox("SRWithPartsSendMail_CR");
	JCheckBox SRPS4 = new JCheckBox("SRDesktopWebVersion_CR"); //Desktop webversion CR
	JCheckBox SRPS5 = new JCheckBox("SRRetailerInvoiceNoUploadImage_CR");
	JCheckBox SRPS6 = new JCheckBox("SRRetailerInvoiceCheckImages_CR");

	
	// Negative Validations Checkbox
	JCheckBox Neg1 = new JCheckBox("InvalidMobileNumber");
	JCheckBox Neg2 = new JCheckBox("BreakdownWithPartsInvalidVehicleNo");
	JCheckBox Neg3 = new JCheckBox("BreakdownWithPartsVehicleNoAlreadyExists");
	//JCheckBox Neg4 = new JCheckBox("SRWithPartsWithOutRetailer");
	//JCheckBox Neg5 = new JCheckBox("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
	
	// Common Scenarios Checkbox
	JCheckBox Cs1 = new JCheckBox("FleetManagerUserEdit");
	JCheckBox Cs2= new JCheckBox("CreateNewFMProfile");
	JCheckBox Cs3 = new JCheckBox("CreateMechanicProfile");
	JCheckBox Cs4 = new JCheckBox("FleetManagerAddNewVehicle");
	JCheckBox Cs5 = new JCheckBox("MechanicRateCardChange");
	JCheckBox Cs6 = new JCheckBox("NewDriverCreation");
	
	
	
	protected String newWorkFlow;
	

	public static void main(String[] args) throws Exception {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				new ServiceMandiUi();
			}
		});
	}

	public ServiceMandiUi() {
		initComponents();
	}

	void initComponents() {
		try {
			frame = new JFrame("main");
			frame.setTitle("Service MANDI-Automation");
			JPanel contentPane = new JPanel();
			SendMailAttachment sm= new SendMailAttachment();
			contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(5, 5));
			centerPanel.setLayout(new GridLayout(0, 3, 4, 4));
			footerPanel.setLayout(new GridLayout(0, 14, 4, 4));
			JButton runWorkFlow = new JButton("RunFlows");

			//JButton runWorkFlow = new JButton("Execute");
			//MultipleWorkflows
			//SendEmailMultipleWorkflows
			/*JButton sendMultipleEmail = new JButton("Zip&Email");
			sendMultipleEmail.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						sm.SendMailAttachment(newWorkFlow);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});*/
			/*JButton saveReport = new JButton("SaveReport");
			saveReport.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					Configuration.saveReport();
				}
			});*/

			JButton uploadExcelSheet = new JButton("UploadExcel");
			uploadExcelSheet.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					File source = new File(
							"D:\\SM_EXE\\Service_Mandi_Input_new.xls");
					File dest = new File("D:\\Myfolder\\Service_Mandi_Input_new.xls"); //D:\\SM_EXE\\Service_Mandi_Input_new.xls
					try {
						// FileUtils.copyDirectory(source, dest);
						FileSystemUtils.copyRecursively(source, dest);
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				}
			});

			JButton startServerButton = new JButton("Servers");
			startServerButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// =================================
					// Runtime rt = Runtime.getRuntime();
					// rt.exec(new String[]{"cmd.exe","/c","start"});
					// String command = "cmd.exe /c start "
					// + "appium --address 0.0.0.0 -p 4723
					// --chromedriver-executable=D:/App/chromedriver_win32/chromedriver.exe";
					
					//Deepa System
					String command = "cmd.exe /c start "+ "appium --address 0.0.0.0 -p 4723";
					String command1 = "cmd.exe /c start " + "appium --address 127.0.0.1 -p 4273";
					String command2 = "cmd.exe /c start " + "appium --address 127.0.0.1 -p 4274 --chromedriver-executable=D:/SM_EXE/chromedriver_win32/chromedriver.exe";
					
					// Priya System
					//==========
					//FM
					//String command = "cmd.exe /c start " + "appium --address 0.0.0.0 -p 4723";
					//Mech
					//String command1 = "cmd.exe /c start " + "appium --address 127.0.0.1 -p 4273 --chromedriver-executable=D:\\Driver\\chromedriver_win32\\chromedriver.exe";
					//Driver        
					//String command2 = "cmd.exe /c start " + "appium --address 127.0.0.1 -p 4274 --chromedriver-executable= D:\\chromedriver_win32\\chromedriver.exe";
					
					try {
						Process child = Runtime.getRuntime().exec(command);
						Process child1 = Runtime.getRuntime().exec(command1);
						Process child2 = Runtime.getRuntime().exec(command2);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
			
			Vector v = new Vector();
			v.add("BD BaseFlows");
			//v.add(BDAll);
			v.add(BD1);
			v.add(BD2);
			v.add(BD3);
			v.add(BD4);
			v.add(BD5);
			v.add(BD6);
			v.add(BD7);
			v.add(BD8);
			v.add(BD9);
			v.add(BD10);
			v.add(BD11);
			v.add(BD12);
			v.add(BD13);
			v.add(BD14);

			Vector v2 = new Vector();
			v2.add("SR BaseFlows");
			// v2.add(SRAll);
			v2.add(SR1);
			v2.add(SR2);
			v2.add(SR3);
			//v2.add(SR4);
			v2.add(SR5);
			v2.add(SR6);
			v2.add(SR7);
			v2.add(SR8);
			v2.add(SR9);
			v2.add(SR10);
			v2.add(SR11);
			v2.add(SR12);
			v2.add(SR13);

			Vector v4 = new Vector();
			v4.add("Driver BD BaseFlows ");
			// v4.add(DBDAll);
			v4.add(DBD1);
			v4.add(DBD2);
			v4.add(DBD3);
			v4.add(DBD4);
			v4.add(DBD5);
			v4.add(DBD6);
			v4.add(DBD7);
			v4.add(DBD8);
			v4.add(DBD9);
			v4.add(DBD10);
			v4.add(DBD11);
			v4.add(DBD12);
			v4.add(DBD13);

			Vector v5 = new Vector();
			v5.add("Driver SR BaseFlows");
			// v5.add(DSRAll);
			v5.add(DSR1);
			v5.add(DSR2);
			v5.add(DSR3);
			v5.add(DSR4);
			v5.add(DSR5);
			v5.add(DSR6);
			v5.add(DSR7);
			v5.add(DSR8);
			//v5.add(DSR9);
			v5.add(DSR10);
			v5.add(DSR11);
			v5.add(DSR12);

			Vector v6 = new Vector();
			v6.add("Negative Validations");
			// v6.add(NEGAll);
			v6.add(Neg1);
			v6.add(Neg2);
			v6.add(Neg3);
			//v6.add(Neg4);
		//	v6.add(Neg5);

			Vector v7 = new Vector();
			v7.add("BD Negative Scenarios");
			v7.add(NBD1);
			v7.add(NBD2);
			v7.add(NBD3);
			v7.add(NBD4);

			Vector v8 = new Vector();
			v8.add("SR Negative Scenarios");
			v8.add(NSR1);
			v8.add(NSR2);
			v8.add(NSR3);
			v8.add(NSR4);
			v8.add(NSR5);
			v8.add(NSR6);
			Vector v9 = new Vector();
			v9.add("BD CancelReject Scenarios");
			v9.add(BDCR1);
			v9.add(BDCR2);
			v9.add(BDCR3);
			v9.add(BDCR4);
			v9.add(BDCR5);
			v9.add(BDCR6);
			v9.add(BDCR7);
			v9.add(BDCR8);
			v9.add(BDCR9);
			Vector v10 = new Vector();
			v10.add("SR CancelReject Scenarios");
			v10.add(SRCR1);
			v10.add(SRCR2);
			v10.add(SRCR3);
			v10.add(SRCR4);
			v10.add(SRCR5);
			v10.add(SRCR6);
			v10.add(SRCR7);
			v10.add(SRCR8);
			v10.add(SRCR9);
			v10.add(SRCR10);
			v10.add(SRCR11);
			v10.add(SRCR12);
			Vector v11 = new Vector();
			v11.add("DriverBD CancelReject Scenarios");
			v11.add(DBDCR1);
			v11.add(DBDCR2);
			v11.add(DBDCR3);
			v11.add(DBDCR4);
			v11.add(DBDCR5);
			v11.add(DBDCR6);
			v11.add(DBDCR7);
			v11.add(DBDCR8);
			Vector v12 = new Vector();
			v12.add("DriverBD Negative Scenarios");
			v12.add(NDBD1);
			v12.add(NDBD2);
			v12.add(NDBD3);
			v12.add(NDBD4);
			Vector v13 = new Vector();
			v13.add("DriverSR CancelReject Scenarios");
			v13.add(DSRCR1);
			v13.add(DSRCR2);
			v13.add(DSRCR3);
			v13.add(DSRCR4);
			v13.add(DSRCR5);
			v13.add(DSRCR6);
			v13.add(DSRCR7);
			v13.add(DSRCR8);
			v13.add(DSRCR9);
			v13.add(DSRCR10);
			Vector v14 = new Vector();
			v14.add("DriverSR Negative Scenarios");
			v14.add(NDSR1);
			v14.add(NDSR2);
			v14.add(NDSR3);
			v14.add(NDSR4);
			v14.add(NDSR5);
			
			Vector v16 = new Vector();
			 v16.add("SR Payment Scenarios");
			 v16.add(SRPS1);
			 v16.add(SRPS2);
			 v16.add(SRPS3);
			 v16.add(SRPS4);
			 v16.add(SRPS5);
			 v16.add(SRPS6);
			
			
			Vector v15=new Vector();
			v15.add("Common Scenarios");
			v15.add(Cs1);
			v15.add(Cs2);
			v15.add(Cs3);
			v15.add(Cs4);
			v15.add(Cs5);
			v15.add(Cs6);
			
			
			/* Vector v17 = new Vector();
			 v17.add("BD Payment Scenarios");
			 v17.add(BDPS1);
			 v17.add(BDPS2);
			 v17.add(BDPS3);
			 v17.add(BDPS4);*/
			

			// --------------------------------------------BDBase Flow-----------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD1.isSelected()) {
						try {
							breakDownWithParts();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownWithParts Flow Completed");
						textArea.setText("BreakDownWithParts Flow Completed");
						BD1.setToolTipText("BreakDownWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD2.isSelected()) {
						try {
							breakDownWithOutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownWithOutParts Flow Completed");
						textArea.setText("BreakDownWithOutParts Flow Completed");
						BD2.setToolTipText("BreakDownWithOutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakDownWithOutParts method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD3.isSelected()) {
						try {
							BDMechanicDeletesAtTrackLoc();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BDMechanicDeletesAtTrackLoc Flow Completed");
						textArea.setText("BDMechanicDeletesAtTrackLoc Flow Completed");
						BD3.setToolTipText("BDMechanicDeletesAtTrackLoc Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BDMechanicDeletesAtTrackLoc
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD4.isSelected()) {
						try {
							BDFMDeletesAtTrackLoc();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BDFMDeletesAtTrackLoc Flow Completed");
						textArea.setText("BDFMDeletesAtTrackLoc Flow Completed");
						BD4.setToolTipText("BDFMDeletesAtTrackLoc Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BDFMDeletesAtTrackLoc method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD5.isSelected()) {
						try {
							BDWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownWithPartsRevisedBill Flow Completed");
						textArea.setText("BreakDownWithPartsRevisedBill Flow Completed");
						BD5.setToolTipText("BreakDownWithPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD6.isSelected()) {
						try {
							BDWithPartsRevisedEstimateWithParts();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("BDWithPartsRevisedEstimateWithParts Flow Completed");
						BD6.setToolTipText("BDWithPartsRevisedEstimateWithParts Flow Completed");
					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD7.isSelected()) {
						try {
							BDWithoutPartsRevisedEstimateWithParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("BDWithoutPartsRevisedEstimateWithParts Flow Completed");
						BD7.setToolTipText("BDWithoutPartsRevisedEstimateWithParts Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD8.isSelected()) {
						try {
							BDWithPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("BDWithPartsRevisedEstimateWithOutParts Flow Completed");
						BD8.setToolTipText("BDWithPartsRevisedEstimateWithOutParts Flow Completed");
					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD9.isSelected()) {
						try {
							BDWithoutPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("BDWithoutPartsRevisedEstimateWithOutParts Flow Completed");
						BD9.setToolTipText("BDWithoutPartsRevisedEstimateWithOutParts Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD10.isSelected()) {
						try {
							BDWithoutPartsRevisedBill();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsRevisedBill Flow Completed");
						textArea.setText("BDWithoutPartsRevisedBill Flow Completed");
						BD10.setToolTipText("BDWithoutPartsRevisedBill Flow Completed");
					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD11.isSelected()) {
						try {
							BDWithPartsRevisedWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithPartsRevisedWithPartsRevisedBill Flow Completed");
						textArea.setText("BDWithPartsRevisedWithPartsRevisedBill Flow Completed");
						BD11.setToolTipText("BDWithPartsRevisedWithPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD12.isSelected()) {
						try {
							BDWithoutPartsRevisedEstimateWithPartsRevisedbill();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						textArea.setText("BDWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						BD12.setToolTipText("BDWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						
					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD13.isSelected()) {
						try {
							BDWithPartsRevisedEstimateWithOutPartsRevisedBill();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
						textArea.setText("BDWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
						BD13.setToolTipText("BDWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BD14.isSelected()) {
						try {
							BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						BD14.setToolTipText("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			// -------------------------------------------------------BD CancelReject
			// Flow---------------------------------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR1.isSelected()) {
						try {
							BDFMDeletesAtFindGarage();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDFMDeletesAtFindGarage Flow Completed");
						textArea.setText("BDFMDeletesAtFindGarage Flow Completed");
						BDCR1.setToolTipText("BDFMDeletesAtFindGarage Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR2.isSelected()) {
						try {
							breakDownFMCancelAtConfirmGarage();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownFMCancelAtConfirmGarage Flow Completed");
						textArea.setText("BreakDownFMCancelAtConfirmGarage Flow Completed");
						BDCR2.setToolTipText("BreakDownFMCancelAtConfirmGarage Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR3.isSelected()) {
						try {
							BDFMDeletesAtTrackLoc();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDFMDeletesAtTrackLoc Flow Completed");
						textArea.setText("BDFMDeletesAtTrackLoc Flow Completed");
						BDCR3.setToolTipText("BDFMDeletesAtTrackLoc Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR4.isSelected()) {
						try {
							BDFMDeletesAtVehicleInGarage();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDFMDeletesAtVehicleInGarage Flow Completed");
						textArea.setText("BDFMDeletesAtVehicleInGarage Flow Completed");
						BDCR4.setToolTipText("BDFMDeletesAtVehicleInGarage Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR5.isSelected()) {
						try {
							BDWithoutPartsFMCancelJobInAproveEstimate();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
						textArea.setText("BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
						BDCR5.setToolTipText("BDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR6.isSelected()) {
						try {
							BDFMCancelAllJobsInApproveEstimate();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDFMCancelAllJobsInApproveEstimate Flow Completed");
						textArea.setText("BDFMCancelAllJobsInApproveEstimate Flow Completed");
						BDCR6.setToolTipText("BDFMCancelAllJobsInApproveEstimate Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR7.isSelected()) {
						try {
							breakDownMechanicRejectsOrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownMechanicRejectsOrder Flow Completed");
						textArea.setText("BreakDownMechanicRejectsOrder Flow Completed");
						BDCR7.setToolTipText("BreakDownMechanicRejectsOrder Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR8.isSelected()) {
						try {
							BDMechanicDeletesAtTrackLoc();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BDMechanicDeletesAtTrackLoc Flow Completed");
						textArea.setText("BDMechanicDeletesAtTrackLoc Flow Completed");
						BDCR8.setToolTipText("BDMechanicDeletesAtTrackLoc Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (BDCR9.isSelected()) {
						try {
							BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");
						textArea.setText("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");
						BDCR9.setToolTipText("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			// ---------------------Negative BD Flow---------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NBD1.isSelected()) {
						try {
							breakDownWithPartsWithOutRetailer();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakDownWithPartsWithOutRetailer Flow Completed");
						textArea.setText("BreakDownWithPartsWithOutRetailer Flow Completed");
						NBD1.setToolTipText("BreakDownWithPartsWithOutRetailer Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NBD2.isSelected()) {
						try {
							breakDownWithPartsNoRetailerRevisedEstimateWithoutParts();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
						textArea.setText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
						NBD2.setToolTipText("BreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NBD3.isSelected()) {
						try {
							BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						NBD3.setToolTipText("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");

					} else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
	  				if (NBD4.isSelected()) {
						try {
							breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
						NBD4.setToolTipText("BreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
					}else {
						// System.out.println("BreakDownWithPartsRevisedBill
						// method unselected");
					}
				}
			});

			// -----------------------------SRBaseFlow----------------------------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR1.isSelected()) {
						try {
							SRWithParts();
						
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("ServiceRequestWithParts Flow Completed");
						textArea.setText("ServiceRequestWithParts Flow Completed");
						SR1.setToolTipText("ServiceRequestWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("ServiceRequestWithParts method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR2.isSelected()) {
						try {
							SRWithoutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("ServiceRequestWithoutParts Flow Completed");
						textArea.setText("ServiceRequestWithoutParts Flow Completed");
						SR2.setToolTipText("ServiceRequestWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("ServiceRequestWithParts method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR3.isSelected()) {
						try {
							SRWithPartsGiveEstimate();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsGiveEstimate Flow Completed");
						textArea.setText("SRWithPartsGiveEstimate Flow Completed");
						SR3.setToolTipText("SRWithPartsGiveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("SRWithPartsGiveEstimate method
						// unselected");
					}
				}
			});

			/*runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR4.isSelected()) {
						try {
			
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem(" Flow Completed");
						textArea.setText(" Flow Completed");
						SR4.setToolTipText(" Flow Completed");

					} else {
						// System.out.println("SRCreateNewRequestFromMyVehicle
						// method unselected");
					}
				}
			});*/

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR5.isSelected()) {
						try {
						SRWithPartsGiveEstimateCompleteOrder();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsGiveEstimateCompleteOrder Flow Completed");
						textArea.setText("SRWithPartsGiveEstimateCompleteOrder Flow Completed");
						SR5.setToolTipText("SRWithPartsGiveEstimateCompleteOrder Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR6.isSelected()) {
						try {
						SRWithPartsRevisedEstimateWithParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("SRWithPartsRevisedEstimateWithParts Flow Completed");
						SR6.setToolTipText("SRWithPartsRevisedEstimateWithParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR7.isSelected()) {
						try {
							SRWithoutPartsReviseEstimateWithParts();
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithoutPartsReviseEstimateWithParts Flow Completed");
						textArea.setText("SRWithoutPartsReviseEstimateWithParts Flow Completed");
						SR7.setToolTipText("SRWithoutPartsReviseEstimateWithParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR8.isSelected()) {
						try {
							SRWithPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("SRWithPartsRevisedEstimateWithOutParts Flow Completed");
						SR8.setToolTipText("SRWithPartsRevisedEstimateWithOutParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR9.isSelected()) {
						try {
							SRWithoutPartsRevisedWithoutParts();
						
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithoutPartsRevisedWithoutParts Flow Completed");
						textArea.setText("SRWithoutPartsRevisedWithoutParts Flow Completed");
						SR9.setToolTipText("SRWithoutPartsRevisedWithoutParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR10.isSelected()) {
						try {
							SRWithPartsRevisedEstimateWithPartsRevisedBill();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");
						textArea.setText("SRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");
						SR10.setToolTipText("SRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR11.isSelected()) {
						try {
							SRWithoutPartsRevisedEstimatePartsRevisedBill();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");
						textArea.setText("SRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");
						SR11.setToolTipText("SRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR12.isSelected()) {
						try {
							SRWithPartsRevisedEstimateWithoutPartsRevisedBill();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("SRWithPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						SR12.setToolTipText("SRWithPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			
			
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SR13.isSelected()) {
						try {
							SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("SRWithPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						SR13.setToolTipText("SRWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			// ---------------------SR CancelReject Flow----------------
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR1.isSelected()) {
						try {
							SRFMDeletesAtFindGarage();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMDeletesAtFindGarage Flow Completed");
						textArea.setText("SRFMDeletesAtFindGarage Flow Completed");
						SRCR1.setToolTipText("SRFMDeletesAtFindGarage Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR2.isSelected()) {
						try {
							SRFMCancelAtTrackLocation();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMCancelAtTrackLocation Flow Completed");
						textArea.setText("SRFMCancelAtTrackLocation Flow Completed");
						SRCR2.setToolTipText("SRFMCancelAtTrackLocation Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR3.isSelected()) {
						try {
							SRFMDeletesAtTrackLocation();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMDeletesAtTrackLocation Flow Completed");
						textArea.setText("SRFMDeletesAtTrackLocation Flow Completed");
						SRCR3.setToolTipText("SRFMDeletesAtTrackLocation Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR4.isSelected()) {
						try {
							SRFMDeletesAtApproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMDeletesAtApproveEstimate Flow Completed");
						textArea.setText("SRFMDeletesAtApproveEstimate Flow Completed");
						SRCR4.setToolTipText("SRFMDeletesAtApproveEstimate Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR5.isSelected()) {
						try {
							SRFMCancelOneJobInApproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMCancelOneJobInApproveEstimate Flow Completed");
						textArea.setText("SRFMCancelOneJobInApproveEstimate Flow Completed");
						SRCR5.setToolTipText("SRFMCancelOneJobInApproveEstimate Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR6.isSelected()) {
						try {
							SRFMCancelAllJobsInApproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRFMCancelAllJobsInApproveEstimate Flow Completed");
						textArea.setText("SRFMCancelAllJobsInApproveEstimate Flow Completed");
						SRCR6.setToolTipText("SRFMCancelAllJobsInApproveEstimate Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR7.isSelected()) {
						try {
							SRMultiJobsFMCancelOneJobInApproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRMultiJobsFMCancelOneJobInApproveEstimate Flow Completed");
						textArea.setText("SRMultiJobsFMCancelOneJobInApproveEstimate Flow Completed");
						SRCR7.setToolTipText("SRMultiJobsFMCancelOneJobInApproveEstimate Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR8.isSelected()) {
						try {
							SRDeleteOneJobAndAddOneNewJobByMechanic();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");
						textArea.setText("SRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");
						SRCR8.setToolTipText("SRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR9.isSelected()) {
						try {
							SRWithPartsWithOutRetailerCancelOneJob();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsWithOutRetailerCancelOneJob Flow Completed");
						textArea.setText("SRWithPartsWithOutRetailerCancelOneJob Flow Completed");
						SRCR9.setToolTipText("SRWithPartsWithOutRetailerCancelOneJob Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR10.isSelected()) {
						try {
							SRMechanicRejectsOrder();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRMechanicRejectsOrder Flow Completed");
						textArea.setText("SRMechanicRejectsOrder Flow Completed");
						SRCR10.setToolTipText("SRMechanicRejectsOrder Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR11.isSelected()) {
						try {
							SRMechanicDeletesAtTrackLoc();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRMechanicDeletesAtTrackLoc Flow Completed");
						textArea.setText("SRMechanicDeletesAtTrackLoc Flow Completed");
						SRCR11.setToolTipText("SRMechanicDeletesAtTrackLoc Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRCR12.isSelected()) {
						try {
							SRDeleteOneJobByMechanic();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRDeleteOneJobByMechanic Flow Completed");
						textArea.setText("SRDeleteOneJobByMechanic Flow Completed");
						SRCR12.setToolTipText("SRDeleteOneJobByMechanic Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});

			// -------------------------------Negative SR Flow---------------------------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR1.isSelected()) {
						try {
							SRWithPartsWithOutRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("ServiceRequestWithPartsWithOutRetailer Flow Completed");
						textArea.setText("ServiceRequestWithPartsWithOutRetailer Flow Completed");
						NSR1.setToolTipText("ServiceRequestWithPartsWithOutRetailer Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR2.isSelected()) {
						try {
							SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						NSR2.setToolTipText("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR3.isSelected()) {
						try {
							SRWithPartsWithRetailerRevisedPartsNoRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");
						textArea.setText("SRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");
						NSR3.setToolTipText("SRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR4.isSelected()) {
						try {
							SRPartsNoRetailerRevisedwithPartsWithRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("SRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");
						textArea.setText("SRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");
						NSR4.setToolTipText("SRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR5.isSelected()) {
						try {
							SRIDontKnowWhatsWrongWithParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("ServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");
						textArea.setText("ServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");
						NSR5.setToolTipText("ServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NSR6.isSelected()) {
						try {
							SRIDontKnowWhatsWrongWithOutParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("ServiceRequestIDontKnowWhatsWrongWithOutParts Flow Completed");
						textArea.setText("ServiceRequestIDontKnowWhatsWrongWithOutParts Flow Completed");
						NSR6.setToolTipText("ServiceRequestIDontKnowWhatsWrongWithOutParts Flow Completed");

					} else {
						// System.out.println("SRWithPartsGiveEstimateCompleteOrder
						// method unselected");
					}
				}
			});
			// ----------------DriverBDBase Flow-------------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD1.isSelected()) {
						try {
							driverBreakDownWithoutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithoutParts Flow Completed");
						textArea.setText("DriverBreakDownWithoutParts Flow Completed");
						DBD1.setToolTipText("DriverBreakDownWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownWithoutParts
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD2.isSelected()) {
						try {
							driverBreakDownWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithParts Flow Completed");
						textArea.setText("DriverBreakDownWithParts Flow Completed");
						DBD2.setToolTipText("DriverBreakDownWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownWithParts method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD3.isSelected()) {
						try {
							driverBreakDownWithPartsRevisedEstimateWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("DriverBreakDownWithPartsRevisedEstimateWithParts Flow Completed");
						DBD3.setToolTipText("DriverBreakDownWithPartsRevisedEstimateWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBDFMDeletesAtTrackLoc
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD4.isSelected()) {
						try {
							driverBreakDownWithoutPartsRevisedEstimateWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("DriverBreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
						DBD4.setToolTipText("DriverBreakDownWithoutPartsRevisedEstimateWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverbreakDownMechanicRejectsOrder
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD5.isSelected()) {
						try {
							driverBreakDownWithPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("DriverBreakDownWithPartsRevisedEstimateWithOutParts Flow Completed");
						DBD5.setToolTipText("DriverBreakDownWithPartsRevisedEstimateWithOutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD6.isSelected()) {
						try {
							driverBreakDownWithoutPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus
								.addItem("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
						DBD6.setToolTipText("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD7.isSelected()) {
						try {
							DriverBreakDownEstimateWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownEstimateWithPartsRevisedBill Flow Completed");
						textArea.setText("DriverBreakDownEstimateWithPartsRevisedBill Flow Completed");
						DBD7.setToolTipText("DriverBreakDownEstimateWithPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD8.isSelected()) {
						try {
							driverbreakDownWithoutPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverbreakDownWithoutPartsRevisedBill Flow Completed");
						textArea.setText("DriverbreakDownWithoutPartsRevisedBill Flow Completed");
						DBD8.setToolTipText("DriverbreakDownWithoutPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD9.isSelected()) {
						try {
							driverbreakDownPartsRevisedWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverbreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
						textArea.setText("DriverbreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
						DBD9.setToolTipText("DriverbreakDownPartsRevisedWithPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD10.isSelected()) {
						try {
							driverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						textArea.setText(
								"DriverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						DBD10.setToolTipText(
								"DriverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD11.isSelected()) {
						try {
							driverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
						textArea.setText(
								"DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
						DBD11.setToolTipText(
								"DriverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD12.isSelected()) {
						try {
							driverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus
								.addItem("DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						DBD12.setToolTipText(
								"DriverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBD13.isSelected()) {
						try {
							driverBreakDownWithPartsWithOutRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownWithPartsWithOutRetailer Flow Completed");
						textArea.setText("DriverBreakDownWithPartsWithOutRetailer Flow Completed");
						DBD13.setToolTipText("DriverBreakDownWithPartsWithOutRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			// --------------------------------------------------------DriverBD CancelReject
			// Flow------------------------------------------------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR1.isSelected()) {
						try {
							driverBDFMDeletesAtTrackLoc();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBDFMDeletesAtTrackLoc Flow Completed");
						textArea.setText("DriverBDFMDeletesAtTrackLoc Flow Completed");
						DBDCR1.setToolTipText("DriverBDFMDeletesAtTrackLoc Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR2.isSelected()) {
						try {
							driverBDMechanicRejectsOrder();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverbreakDownMechanicRejectsOrder Flow Completed");
						textArea.setText("DriverbreakDownMechanicRejectsOrder Flow Completed");
						DBDCR2.setToolTipText("DriverbreakDownMechanicRejectsOrder Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR3.isSelected()) {
						try {
							driverBreakDownFMDeletesAtConfirmGarage();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownFMDeletesAtConfirmGarage Flow Completed");
						textArea.setText("DriverBreakDownFMDeletesAtConfirmGarage Flow Completed");
						DBDCR3.setToolTipText("DriverBreakDownFMDeletesAtConfirmGarage Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR4.isSelected()) {
						try {
							driverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus
								.addItem("DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");
						textArea.setText("DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");
						DBDCR4.setToolTipText(
								"DriverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR5.isSelected()) {
						try {
							driverBDFMCancelAllJobsInApproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBDFMCancelAllJobsInApproveEstimate Flow Completed");
						textArea.setText("DriverBDFMCancelAllJobsInApproveEstimate Flow Completed");
						DBDCR5.setToolTipText("DriverBDFMCancelAllJobsInApproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR6.isSelected()) {
						try {
							driverBreakDownMechanicDeletesAtTrackLoc();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBreakDownMechanicDeletesAtTrackLoc Flow Completed");
						textArea.setText("DriverBreakDownMechanicDeletesAtTrackLoc Flow Completed");
						DBDCR6.setToolTipText("DriverBreakDownMechanicDeletesAtTrackLoc Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR7.isSelected()) {
						try {
							driverBDWithoutPartsFMCancelJobInAproveEstimate();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
						textArea.setText("DriverBDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
						DBDCR7.setToolTipText("DriverBDWithoutPartsFMCancelJobInAproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DBDCR8.isSelected()) {
						try {
							driverBDFMDeletesAtVehicleInGarage();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverBDFMDeletesAtVehicleInGarage Flow Completed");
						textArea.setText("DriverBDFMDeletesAtVehicleInGarage Flow Completed");
						DBDCR8.setToolTipText("DriverBDFMDeletesAtVehicleInGarage Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			// --------------------------------------------------------Negative DriverBD
			// Flow------------------------------------------------------------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDBD1.isSelected()) {
						try {
							driverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText(
								"DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						NDBD1.setToolTipText(
								"DriverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDBD2.isSelected()) {
						try {
							driverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText(
								"DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
						NDBD2.setToolTipText(
								"DriverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDBD3.isSelected()) {
						try {
							driverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
						textArea.setText(
								"DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
						NDBD3.setToolTipText(
								"DriverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDBD4.isSelected()) {
						try {
							driverNegativeScenarioFMDeletes();
						} catch (Exception e) {
							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverNegativeScenarioFMDeletes Flow Completed");
						textArea.setText("DriverNegativeScenarioFMDeletes Flow Completed");
						NDBD4.setToolTipText("DriverNegativeScenarioFMDeletes Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverBreakDownFMDeletesAtConfirmGarage
						// method unselected");
					}
				}
			});
			// --------------------------------------------------------DriverSRBase
			// Flow------------------------------------------------------------------------------------------/*

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR1.isSelected()) {
						try {
							driverSRWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithParts Flow Completed");
						textArea.setText("DriverSRWithParts Flow Completed");
						DSR1.setToolTipText("DriverSRWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRWithParts method
						// unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR2.isSelected()) {
						try {
							driverSRWithoutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithoutParts Flow Completed");
						textArea.setText("DriverSRWithoutParts Flow Completed");
						DSR2.setToolTipText("DriverSRWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRWithoutParts method
						// unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR3.isSelected()) {
						try {
							driverSRWithPartsRevisedEstimateWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("DriverSRWithPartsRevisedEstimateWithParts Flow Completed");
						DSR3.setToolTipText("DriverSRWithPartsRevisedEstimateWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRWithoutParts method
						// unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR4.isSelected()) {
						try {
							driverSRWithoutPartsRevisedEstimateWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithoutPartsRevisedEstimateWithParts Flow Completed");
						textArea.setText("DriverSRWithoutPartsRevisedEstimateWithParts Flow Completed");
						DSR4.setToolTipText("DriverSRWithoutPartsRevisedEstimateWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRMechanicDeletesAtTrackLoc
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR5.isSelected()) {
						try {
							driverSRWithPartsRevisedEstimateWithOutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsRevisedEstimateWithOutParts Flow Completed");
						textArea.setText("DriverSRWithPartsRevisedEstimateWithOutParts Flow Completed");
						DSR5.setToolTipText("DriverSRWithPartsRevisedEstimateWithOutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR6.isSelected()) {
						try {
							driverSRWithoutPartsRevisedWithoutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithoutPartsRevisedWithoutParts Flow Completed");
						textArea.setText("DriverSRWithoutPartsRevisedWithoutParts Flow Completed");
						DSR6.setToolTipText("DriverSRWithoutPartsRevisedWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR7.isSelected()) {
						try {
							driverSRWithPartsRevisedEstimateWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");
						textArea.setText("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");
						DSR7.setToolTipText("DriverSRWithPartsRevisedEstimateWithPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR8.isSelected()) {
						try {
							driverSRWithoutPartsRevisedEstimatePartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");
						textArea.setText("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");
						DSR8.setToolTipText("DriverSRWithoutPartsRevisedEstimatePartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			/*runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR9.isSelected()) {
						try {
							driverSRWithoutPartsRevisedWithoutParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverServiceRequestWithoutPartsRevisedWithoutParts Flow Completed");
						textArea.setText("DriverServiceRequestWithoutPartsRevisedWithoutParts Flow Completed");
						DSR9.setToolTipText("DriverServiceRequestWithoutPartsRevisedWithoutParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});*/
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR10.isSelected()) {
						try {
							driverSRPartsRevisedEstimateWithoutPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						DSR10.setToolTipText("DriverSRPartsRevisedEstimateWithoutPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR11.isSelected()) {
						try {
							driverSREstimateWithPartsRevisedBill();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSREstimateWithPartsRevisedBill Flow Completed");
						textArea.setText("DriverSREstimateWithPartsRevisedBill Flow Completed");
						DSR11.setToolTipText("DriverSREstimateWithPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSR12.isSelected()) {
						try {
							driverSREstimateWithoutPartsRevisedBill();
							
							
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("driverSREstimateWithoutPartsRevisedBill Flow Completed");
						textArea.setText("driverSREstimateWithoutPartsRevisedBill Flow Completed");
						DSR12.setToolTipText("driverSREstimateWithoutPartsRevisedBill Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			// -------------------------------------------------------DriverSR CancelReject
			// Flow--------------------------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR1.isSelected()) {
						try {
							driverSRFMDeletesAtTrackLocation();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMDeletesAtTrackLocation Flow Completed");
						textArea.setText("DriverSRFMDeletesAtTrackLocation Flow Completed");
						DSRCR1.setToolTipText("DriverSRFMDeletesAtTrackLocation Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR2.isSelected()) {
						try {
							driverSRMechanicDeletesAtTrackLoc();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRMechanicDeletesAtTrackLoc Flow Completed");
						textArea.setText("DriverSRMechanicDeletesAtTrackLoc Flow Completed");
						DSRCR2.setToolTipText("DriverSRMechanicDeletesAtTrackLoc Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRMechanicDeletesAtTrackLoc
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR3.isSelected()) {
						try {
							driverSRFMDeletesAtFindGarage();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMDeletesAtFindGarage Flow Completed");
						textArea.setText("DriverSRFMDeletesAtFindGarage Flow Completed");
						DSRCR3.setToolTipText("DriverSRFMDeletesAtFindGarage Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR4.isSelected()) {
						try {
							driverSRFMDeletesAtApproveEstimate();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMDeletesAtApproveEstimate Flow Completed");
						textArea.setText("DriverSRFMDeletesAtApproveEstimate Flow Completed");
						DSRCR4.setToolTipText("DriverSRFMDeletesAtApproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR5.isSelected()) {
						try {
							driverSRFMCancelAllJobsInApproveEstimate();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMCancelAllJobsInApproveEstimate Flow Completed");
						textArea.setText("DriverSRFMCancelAllJobsInApproveEstimate Flow Completed");
						DSRCR5.setToolTipText("DriverSRFMCancelAllJobsInApproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR6.isSelected()) {
						try {
							driverSRDeleteOneJobAndAddOneNewJobByMechanic();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");
						textArea.setText("DriverSRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");
						DSRCR6.setToolTipText("DriverSRDeleteOneJobAndAddOneNewJobByMechanic Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR7.isSelected()) {
						try {
							driverSRFMCancelOneJobInApproveEstimate();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMCancelOneJobInApproveEstimate Flow Completed");
						textArea.setText("DriverSRFMCancelOneJobInApproveEstimate Flow Completed");
						DSRCR7.setToolTipText("DriverSRFMCancelOneJobInApproveEstimate Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR8.isSelected()) {
						try {
							driverSRMechanicRejectsOrder();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverServiceRequestMechanicRejectsOrder Flow Completed");
						textArea.setText("DriverServiceRequestMechanicRejectsOrder Flow Completed");
						DSRCR8.setToolTipText("DriverServiceRequestMechanicRejectsOrder Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR9.isSelected()) {
						try {
							driverSRFMDeletes();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRFMDeletes Flow Completed");
						textArea.setText("DriverSRFMDeletes Flow Completed");
						DSRCR9.setToolTipText("DriverSRFMDeletes Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (DSRCR10.isSelected()) {
						try {
							driverSRWithPartsWithOutRetailerCancelOneJob();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsWithOutRetailerCancelOneJob Flow Completed");
						textArea.setText("DriverSRWithPartsWithOutRetailerCancelOneJob Flow Completed");
						DSRCR10.setToolTipText("DriverSRWithPartsWithOutRetailerCancelOneJob Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			// ------------------------------------------------------- Negative DriverSR
			// Flow----------------------------------------------------------------
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDSR1.isSelected()) {
						try {
							driverServiceRequestIDontKnowWhatsWrongWithParts();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");
						textArea.setText("DriverServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");
						NDSR1.setToolTipText("DriverServiceRequestIDontKnowWhatsWrongWithParts Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDSR2.isSelected()) {
						try {
							driverSRPartsNoRetailerRevisedwithPartsWithRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");
						textArea.setText("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");
						NDSR2.setToolTipText("DriverSRPartsNoRetailerRevisedwithPartsWithRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDSR3.isSelected()) {
						try {
							driverSRWithPartsWithRetailerRevisedPartsNoRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");
						textArea.setText("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");
						NDSR3.setToolTipText("DriverSRWithPartsWithRetailerRevisedPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDSR4.isSelected()) {
						try {
							driverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem(
								"DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText(
								"DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						NDSR4.setToolTipText(
								"DriverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (NDSR5.isSelected()) {
						try {
							driverSRWithPartsNoRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("DriverSRWithPartsNoRetailer Flow Completed");
						textArea.setText("DriverSRWithPartsNoRetailer Flow Completed");
						NDSR5.setToolTipText("DriverSRWithPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("DriverSRFMDeletesAtFindGarage
						// method unselected");
					}
				}
			});
			// --------------------------------------------------------Negative Scenario
			// Flow----------------------------------------------------------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Neg1.isSelected()) {
						try {
							invalidMobileNumber();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("InvalidMobileNumber Flow Completed");
						textArea.setText("InvalidMobileNumber Flow Completed");
						Neg1.setToolTipText("InvalidMobileNumber Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("InvalidMobileNumber method
						// unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Neg2.isSelected()) {
						try {
							breakdownWithPartsInvalidVehicleNo();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakdownWithPartsInvalidVehicleNo Flow Completed");
						textArea.setText("BreakdownWithPartsInvalidVehicleNo Flow Completed");
						Neg2.setToolTipText("BreakdownWithPartsInvalidVehicleNo Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsInvalidVehicleNo
						// method unselected");
					}
				}
			});

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Neg3.isSelected()) {
						try {
							bdVehicleNoAlreadyExists();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("BreakdownWithPartsVehicleNoAlreadyExists Flow Completed");
						textArea.setText("BreakdownWithPartsVehicleNoAlreadyExists Flow Completed");
						Neg3.setToolTipText("BreakdownWithPartsVehicleNoAlreadyExists Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			/*runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Neg4.isSelected()) {
						try {
							SRWithPartsWithOutRetailer();
						} catch (Exception e) {

							e.printStackTrace();
						}
						breakDownStatus.addItem("SRWithPartsWithOutRetailer Flow Completed");
						textArea.setText("SRWithPartsWithOutRetailer Flow Completed");
						Neg4.setToolTipText("SRWithPartsWithOutRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Neg5.isSelected()) {
						try {
						SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						textArea.setText("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						Neg5.setToolTipText("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});*/

			//Common Sceanrios:			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs1.isSelected()) {
						try {
							FleetManagerUserEdit();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("FleetManagerUserEdit Flow Completed");
						textArea.setText("FleetManagerUserEdit Flow Completed");
						Cs1.setToolTipText("FleetManagerUserEdit Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs2.isSelected()) {
						try {
							createNewFMProfile();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus.addItem("CreateNewFMProfile Flow Completed");
						textArea.setText("CreateNewFMProfile Flow Completed");
						Cs2.setToolTipText("CreateNewFMProfile Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs3.isSelected()) {
						try {
							createMechanicProfile();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("CreateMechanicProfile Flow Completed");
						textArea.setText("CreateMechanicProfile Flow Completed");
						Cs3.setToolTipText("CreateMechanicProfile Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs4.isSelected()) {
						try {
							FleetManagerAddNewVehicle();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("FleetManagerAddNewVehicle Flow Completed");
						textArea.setText("FleetManagerAddNewVehicle Flow Completed");
						Cs4.setToolTipText("FleetManagerAddNewVehicle Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs5.isSelected()) {
						try {
							mechanicRateCardChange();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("MechanicRateCardChange Flow Completed");
						textArea.setText("MechanicRateCardChange Flow Completed");
						Cs5.setToolTipText("MechanicRateCardChange Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (Cs6.isSelected()) {
						try {
							newDriverCreation();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("NewDriverCreation Flow Completed");
						textArea.setText("NewDriverCreation Flow Completed");
						Cs6.setToolTipText("NewDriverCreation Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
//=========================================================================================================
			//SR:Payment Scenarios with CR's:
			//============================
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS1.isSelected()) {
						try {
							SRRetailerInvoice_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS1.setToolTipText("SRAddImageCR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS5.isSelected()) {
						try {
							SRRetailerInvoiceNoUploadImage_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS5.setToolTipText("SRRetailerInvoiceNoUploadImage_CR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS6.isSelected()) {
						try {
							SRRetailerInvoiceCheckImages_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS6.setToolTipText("SRRetailerInvoiceCheckImages_CR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			
			
			
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS2.isSelected()) {
						try {
							SRWithPartsDownloadPDF_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS2.setToolTipText("SRWithPartsDownloadPDF_CR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS3.isSelected()) {
						try {
							SRWithPartsSendMail_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS3.setToolTipText("SRWithPartsSendMail_CR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (SRPS4.isSelected()) {
						try {
							SRDesktopWebVersion_CR();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						breakDownStatus
								.addItem("SRAddImageCR Flow Completed");
						textArea.setText("SRAddImageCR Flow Completed");
						SRPS4.setToolTipText("SRWithPartsForwardMail_CR Flow Completed");
						// JOptionPane.showMessageDialog(frame,
						// "BreakDownWithParts Flow Completed");
					} else {
						// System.out.println("BreakdownWithPartsVehicleNoAlreadyExists
						// method unselected");
					}
				}
			});
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
// -----------------------------------------------------------------------------------------------------------------------------------
			BDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == BDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (BDAll.isSelected() == true) {
							BD1.setSelected(true);
							BD2.setSelected(true);
							BD3.setSelected(true);
							BD4.setSelected(true);
							BD5.setSelected(true);
							BD6.setSelected(true);
							BD7.setSelected(true);
							BD8.setSelected(true);
							BD9.setSelected(true);
							BD10.setSelected(true);
							BD11.setSelected(true);
							BD12.setSelected(true);
							BD13.setSelected(true);
							BD14.setSelected(true);
						} else {
							BD1.setSelected(false);
							BD2.setSelected(false);
							BD3.setSelected(false);
							BD4.setSelected(false);
							BD5.setSelected(false);
							BD6.setSelected(false);
							BD7.setSelected(false);
							BD8.setSelected(false);
							BD9.setSelected(false);
							BD10.setSelected(false);
							BD11.setSelected(false);
							BD12.setSelected(false);
							BD13.setSelected(false);
							BD14.setSelected(false);
						}
					}
				}
			});
			
			BDCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == BDCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (BDCRAll.isSelected() == true) {
							BDCR1.setSelected(true);
							BDCR2.setSelected(true);
							BDCR3.setSelected(true);
							BDCR4.setSelected(true);
							BDCR5.setSelected(true);
							BDCR6.setSelected(true);
							BDCR7.setSelected(true);
							BDCR8.setSelected(true);
							BDCR9.setSelected(true);

						} else {
							BDCR1.setSelected(false);
							BDCR2.setSelected(false);
							BDCR3.setSelected(false);
							BDCR4.setSelected(false);
							BDCR5.setSelected(false);
							BDCR6.setSelected(false);
							BDCR7.setSelected(false);
							BDCR8.setSelected(false);
							BDCR9.setSelected(false);

						}
					}
				}
			});
			NBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NBDAll.isSelected() == true) {
							NBD1.setSelected(true);
							NBD2.setSelected(true);
							NBD3.setSelected(true);
							NBD4.setSelected(true);
						} else {
							NBD1.setSelected(false);
							NBD2.setSelected(false);
							NBD3.setSelected(false);
							NBD4.setSelected(false);
						}
					}
				}
			});

			SRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == SRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (SRAll.isSelected() == true) {
							SR1.setSelected(true);
							SR2.setSelected(true);
							SR3.setSelected(true);
							//SR4.setSelected(true);
							SR5.setSelected(true);
							SR6.setSelected(true);
							SR7.setSelected(true);
							SR8.setSelected(true);
							SR9.setSelected(true);
							SR10.setSelected(true);
							SR11.setSelected(true);
							SR12.setSelected(true);

						} else {
							SR1.setSelected(false);
							SR2.setSelected(false);
							SR3.setSelected(false);
							//SR4.setSelected(false);
							SR5.setSelected(false);
							SR6.setSelected(false);
							SR7.setSelected(false);
							SR8.setSelected(false);
							SR9.setSelected(false);
							SR10.setSelected(false);
							SR11.setSelected(false);
							SR12.setSelected(false);
						}
					}
				}
			});
			SRCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == SRCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (SRCRAll.isSelected() == true) {
							SRCR1.setSelected(true);
							SRCR2.setSelected(true);
							SRCR3.setSelected(true);
							SRCR4.setSelected(true);
							SRCR5.setSelected(true);
							SRCR6.setSelected(true);
							SRCR7.setSelected(true);
							SRCR8.setSelected(true);
							SRCR9.setSelected(true);
							SRCR10.setSelected(true);
							SRCR11.setSelected(true);
							SRCR12.setSelected(true);
						} else {
							SRCR1.setSelected(false);
							SRCR2.setSelected(false);
							SRCR3.setSelected(false);
							SRCR4.setSelected(false);
							SRCR5.setSelected(false);
							SRCR6.setSelected(false);
							SRCR7.setSelected(false);
							SRCR8.setSelected(false);
							SRCR9.setSelected(false);
							SRCR10.setSelected(false);
							SRCR11.setSelected(false);
							SRCR12.setSelected(false);
						}
					}
				}
			});
			NSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NSRAll.isSelected() == true) {
							NSR1.setSelected(true);
							NSR2.setSelected(true);
							NSR3.setSelected(true);
							NSR4.setSelected(true);
							NSR5.setSelected(true);
							NSR6.setSelected(true);
						} else {
							NSR1.setSelected(false);
							NSR2.setSelected(false);
							NSR3.setSelected(false);
							NSR4.setSelected(false);
							NSR5.setSelected(false);
							NSR6.setSelected(false);
						}
					}
				}
			});

			DBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DBDAll.isSelected() == true) {
							DBD1.setSelected(true);
							DBD2.setSelected(true);
							DBD3.setSelected(true);
							DBD4.setSelected(true);
							DBD5.setSelected(true);
							DBD6.setSelected(true);
							DBD7.setSelected(true);
							DBD8.setSelected(true);
							DBD9.setSelected(true);
							DBD10.setSelected(true);
							DBD11.setSelected(true);
							DBD12.setSelected(true);
							DBD13.setSelected(true);

						} else {
							DBD1.setSelected(false);
							DBD2.setSelected(false);
							DBD3.setSelected(false);
							DBD4.setSelected(false);
							DBD5.setSelected(false);
							DBD6.setSelected(false);
							DBD7.setSelected(false);
							DBD8.setSelected(false);
							DBD9.setSelected(false);
							DBD10.setSelected(false);
							DBD11.setSelected(false);
							DBD12.setSelected(false);
							DBD13.setSelected(false);
						}
					}
				}
			});
			DBDCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DBDCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DBDCRAll.isSelected() == true) {
							DBDCR1.setSelected(true);
							DBDCR2.setSelected(true);
							DBDCR3.setSelected(true);
							DBDCR4.setSelected(true);
							DBDCR5.setSelected(true);
							DBDCR6.setSelected(true);
							DBDCR7.setSelected(true);
							DBDCR8.setSelected(true);

						} else {
							DBDCR1.setSelected(false);
							DBDCR2.setSelected(false);
							DBDCR3.setSelected(false);
							DBDCR4.setSelected(false);
							DBDCR5.setSelected(false);
							DBDCR6.setSelected(false);
							DBDCR7.setSelected(false);
							DBDCR8.setSelected(false);
						}
					}
				}
			});
			NDBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NDBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NDBDAll.isSelected() == true) {
							NDBD1.setSelected(true);
							NDBD2.setSelected(true);
							NDBD3.setSelected(true);
							NDBD4.setSelected(true);

						} else {
							NDBD1.setSelected(false);
							NDBD2.setSelected(false);
							NDBD3.setSelected(false);
							NDBD4.setSelected(false);
						}
					}
				}
			});

			DSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DSRAll.isSelected() == true) {
							DSR1.setSelected(true);
							DSR2.setSelected(true);
							DSR3.setSelected(true);
							DSR4.setSelected(true);
							DSR5.setSelected(true);
							DSR6.setSelected(true);
							DSR7.setSelected(true);
							DSR8.setSelected(true);
							//DSR9.setSelected(true);
							DSR10.setSelected(true);
							DSR11.setSelected(true);
							DSR12.setSelected(true);

						} else {
							DSR1.setSelected(false);
							DSR2.setSelected(false);
							DSR3.setSelected(false);
							DSR4.setSelected(false);
							DSR5.setSelected(false);
							DSR6.setSelected(false);
							DSR7.setSelected(false);
							DSR8.setSelected(false);
							//DSR9.setSelected(false);
							DSR10.setSelected(false);
							DSR11.setSelected(false);
							DSR12.setSelected(false);
						}
					}
				}
			});
			DSRCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DSRCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DSRCRAll.isSelected() == true) {
							DSRCR1.setSelected(true);
							DSRCR2.setSelected(true);
							DSRCR3.setSelected(true);
							DSRCR4.setSelected(true);
							DSRCR5.setSelected(true);
							DSRCR6.setSelected(true);
							DSRCR7.setSelected(true);
							DSRCR8.setSelected(true);
							DSRCR9.setSelected(true);
							DSRCR10.setSelected(true);
						} else {
							DSRCR1.setSelected(false);
							DSRCR2.setSelected(false);
							DSRCR3.setSelected(false);
							DSRCR4.setSelected(false);
							DSRCR5.setSelected(false);
							DSRCR6.setSelected(false);
							DSRCR7.setSelected(false);
							DSRCR8.setSelected(false);
							DSRCR9.setSelected(false);
							DSRCR10.setSelected(false);
						}
					}
				}
			});
			NDSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NDSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NDSRAll.isSelected() == true) {
							NDSR1.setSelected(true);
							NDSR2.setSelected(true);
							NDSR3.setSelected(true);
							NDSR4.setSelected(true);
							NDSR5.setSelected(true);
						} else {
							NDSR1.setSelected(false);
							NDSR2.setSelected(false);
							NDSR3.setSelected(false);
							NDSR4.setSelected(false);
							NDSR5.setSelected(false);
						}
					}
				}
			});

			NEGAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NEGAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NEGAll.isSelected() == true) {
							Neg1.setSelected(true);
							Neg2.setSelected(true);
							Neg3.setSelected(true);
							//Neg4.setSelected(true);
							//Neg5.setSelected(true);
						} else {
							Neg1.setSelected(false);
							Neg2.setSelected(false);
							Neg3.setSelected(false);
							//Neg4.setSelected(false);
							//Neg5.setSelected(false);
						}
					}
				}
			});
			centerPanel.add(JL6);
			centerPanel.add(JL5);
			centerPanel.add(JL7);
			centerPanel.add(new JComboCheckBox(v2));
			centerPanel.add(new JComboCheckBox(v10));
			centerPanel.add(new JComboCheckBox(v8));
			centerPanel.add(new JComboCheckBox(v5));
			centerPanel.add(new JComboCheckBox(v13));
			centerPanel.add(new JComboCheckBox(v14));
			centerPanel.add(new JComboCheckBox(v16));
			centerPanel.add(JL51);
			centerPanel.add(JL52);
			centerPanel.add(JL9);
			centerPanel.add(JL8);
			centerPanel.add(JL10);
			centerPanel.add(new JComboCheckBox(v));
			centerPanel.add(new JComboCheckBox(v9));
			centerPanel.add(new JComboCheckBox(v7));
			centerPanel.add(new JComboCheckBox(v4));
			centerPanel.add(new JComboCheckBox(v11));
			centerPanel.add(new JComboCheckBox(v12));
			centerPanel.add(new JComboCheckBox(v6));
			centerPanel.add(new JComboCheckBox(v15));
			//centerPanel.add(new JComboCheckBox(v16));
			//centerPanel.add(new JComboCheckBox(v17));
			
			contentPane.add(centerPanel, BorderLayout.PAGE_START);
			
			footerPanel.add(SMworkFlowLabel);
			footerPanel.add(SRAll);
			footerPanel.add(SRCRAll);
			footerPanel.add(NSRAll);
			footerPanel.add(DSRAll);
			footerPanel.add(DSRCRAll);
			footerPanel.add(NDSRAll);
			footerPanel.add(BDAll);
			footerPanel.add(BDCRAll);
			footerPanel.add(NBDAll);
			footerPanel.add(DBDAll);
			footerPanel.add(DBDCRAll);
			footerPanel.add(NDBDAll);
			footerPanel.add(NEGAll);
			footerPanel.add(JL1);
			footerPanel.add(JL2);
			footerPanel.add(JL3);
			//footerPanel.add(JL4);v15
			footerPanel.add(JL11);
			
			
			footerPanel.add(uploadExcelSheet);
			footerPanel.add(startServerButton);
			footerPanel.add(runWorkFlow);
		//	footerPanel.add(sendMultipleEmail);
			footerPanel.add(JL12);
			footerPanel.add(breakDownStatus);
			contentPane.add(footerPanel, BorderLayout.CENTER);
			// contentPane.add(afterCenterPanel, BorderLayout.CENTER);
			// contentPane.add(afterCenterPanel, BorderLayout.set);

			// belowfooterPanel, copyRightLabel
			belowfooterPanel.add(copyRightLabel);
			contentPane.add(belowfooterPanel, BorderLayout.PAGE_END);
			frame.getContentPane().add(new CheckCombo().getContent());
			frame.setContentPane(contentPane);
			frame.pack();
			frame.setVisible(true);
			// CYAN
			frame.getContentPane().setBackground(new java.awt.Color(255,140,0));
			centerPanel.setBackground(new java.awt.Color(255,140,0));
			footerPanel.setBackground(new java.awt.Color(255,140,0));
			belowfooterPanel.setBackground(new java.awt.Color(255,140,0));
			//ImageIcon ii = new ImageIcon("D:\\R\\sm\\Image\\ServiceaMand_Logo2New_Latest.png");  //Priya
			//ImageIcon ii = new ImageIcon("C:\\Users\\14402\\git\\demoservicemandiautomation\\Image\\ServiceaMand_Logo2New_Latest.png"); //Rakesh
			ImageIcon ii = new ImageIcon("D:\\Deepa\\GitFolder\\GitPull\\Capture1.PNG");       //Deepa
			JLabel lable = new JLabel(ii);
			JScrollPane jsp = new JScrollPane(lable);
			frame.getContentPane().add(jsp);
			frame.setSize(1000, 700);
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			frame.setMaximizedBounds(env.getMaximumWindowBounds());
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		} catch (Exception e) {
		}
	}

	private void add(JLabel jL122) {
		// TODO Auto-generated method stub
	}
}

	class JComboCheckBox extends JComboBox {
	public JComboCheckBox() {
		init();
	}

	public JComboCheckBox(JCheckBox[] items) {
		super(items);
		init();
	}

	public JComboCheckBox(Vector items) {
		super(items);
		init();
	}

	public JComboCheckBox(ComboBoxModel aModel) {
		super(aModel);
		init();
	}

	private void init() {
		setRenderer(new ComboBoxRenderer());
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				itemSelected();
			}
		});
	}

	private void itemSelected() {
		if (getSelectedItem() instanceof JCheckBox) {
			JCheckBox jcb = (JCheckBox) getSelectedItem();
			jcb.setSelected(!jcb.isSelected());
		}
	}

	class ComboBoxRenderer implements ListCellRenderer {
		private JLabel label;

		public ComboBoxRenderer() {
			setOpaque(true);
		}

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			if (value instanceof Component) {
				Component c = (Component) value;
				if (isSelected) {
					c.setBackground(list.getSelectionBackground());
					c.setForeground(list.getSelectionForeground());
				} else {
					c.setBackground(list.getBackground());
					c.setForeground(list.getForeground());
				}

				return c;
			} else {
				if (label == null) {
					label = new JLabel(value.toString());
				} else {
					label.setText(value.toString());
				}

				return label;
			}
		}
	}
}
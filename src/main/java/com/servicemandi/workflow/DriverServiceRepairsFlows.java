
package com.servicemandi.workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Driver;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.driver.DriverCreateServiceRequest;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;
import com.sevicemandi.servicerequest.ServiceRequestApproveEstimate;
import com.sevicemandi.servicerequest.ServiceRequestStartJob;
import com.sevicemandi.servicerequest.ServicesRequestTrackLocation;

/**
 * Created by Deepa on 13-03-2018.
 */

public class DriverServiceRepairsFlows {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private Driver driver;
	private DriverCreateServiceRequest driverCreateServiceRequest;
	private CreateServiceRequest createServiceRequest;
	private ServicesRequestTrackLocation trackLocation;
	private ServiceRequestApproveEstimate approveEstimate;
	private ServiceRequestStartJob startJob;
	private String mRequestOrderId = "";
	private ProfileCreation mProfileCreation;
	private Configuration mConfiguration;

	@BeforeTest
	public void setUpConfiguration() {

		try {
			System.out.println("===== Inside try=====");
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			driver = new Driver();
			driverCreateServiceRequest = new DriverCreateServiceRequest();
			trackLocation = new ServicesRequestTrackLocation();
			approveEstimate = new ServiceRequestApproveEstimate();
			startJob = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			createServiceRequest=new CreateServiceRequest();
			Configuration.reportConfiguration("DriverSRBaseFlow");
			Configuration.createNewWorkFlowReport("Loging Details");
			fleetManager.login(Utils.excelInputDataList.get(0));
			mechanic.login(Utils.excelInputDataList.get(0));
			driver.login(Utils.excelInputDataList.get(0));
			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	@BeforeClass
	public void beforeClass() {

	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}
	@Test
	public void driverSRWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-SR With Parts=====1");
				Configuration.createNewWorkFlowReport("driverSRWithParts 1");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverSRWithoutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-SR Without Parts=====2");
				Configuration.createNewWorkFlowReport("driverSRWithoutParts 2");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager,driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverSRWithPartsRevisedEstimateWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Estimate With Parts 3=====");
				Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithParts 3");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverSRWithoutPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-SR-Without Parts Revised Estimate With Parts 4=====");
				Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedEstimateWithParts 4");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverSRWithPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-SR-With Parts Revised Estimate WithOut Parts 5=====");
				Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithOutParts 5");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(0), mechanic);
				approveEstimate.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	
	@Test
	public void driverSRWithoutPartsRevisedWithoutParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver SR WithoutParts Revised Without Parts 6=====");
				Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedWithoutParts 6");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(4), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(4), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "ServiceRequestWithoutPartsRevisedWithoutParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
	
	
	@Test
	public void driverSRWithPartsRevisedEstimateWithPartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver SR WithParts Revised Estimate With Parts Revised Bill 17=====");
				Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithPartsRevisedBill 17");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "driverSRWithPartsRevisedEstimateWithPartsRevisedBill  - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
	
	@Test
	public void driverSRWithoutPartsRevisedEstimatePartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-SR WithoutParts Revised Without Parts 19=====");
				Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedEstimatePartsRevisedBill 19");
				mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
				driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithoutPartsRevisedEstimatePartsRevisedBill - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
	
	
		public void driverSRFMDeletesAtApproveEstimate() throws InterruptedException {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SRFMDeletesAtApproveEstimate 60=====");
					Configuration.createNewWorkFlowReport("driverSRFMDeletesAtApproveEstimate 60");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRFMDeletesAtApproveEstimate - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRFMDeletesAtTrackLocation() throws InterruptedException {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SRFMDeletesAtTrackLoc 58=====");
					Configuration.createNewWorkFlowReport("driverSRFMDeletesAtTrackLocation 58");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRFMDeletesAtTrackLocation - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRMechanicDeletesAtTrackLoc() throws InterruptedException {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver SRMechanicDeletesAtTrackLoc 56=====");
					Configuration.createNewWorkFlowReport("driverSRMechanicDeletesAtTrackLoc 56");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					//Configuration.saveReport();
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRMechanicDeletesAtTrackLoc - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRFMDeletesAtFindGarage() throws InterruptedException {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SRFMDeletesAtFindGarage 49=====");
					Configuration.createNewWorkFlowReport("driverSRFMDeletesAtFindGarage 49");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRFMDeletesAtFindGarage - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRFMCancelAllJobsInApproveEstimate() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver Service Request Cancel All Jobs in Approve Estimate 45 =====");
					Configuration.createNewWorkFlowReport("driverSRFMCancelAllJobsInApproveEstimate 45");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.tackLocationMultiJobWithParts(Utils.excelInputDataList.get(0));
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRFMCancelAllJobsInApproveEstimate - Fail");
			}
		}
		
		@Test
		public void driverSRDeleteOneJobAndAddOneNewJobByMechanic() throws InterruptedException {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("=====SRDeleteOneJobAndAddOneNewJobByMechanic 42=====");
					Configuration.createNewWorkFlowReport("driverSRDeleteOneJobAndAddOneNewJobByMechanic 42");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.DeletejobwithoutSubmitEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.trackLocationWithAddOneNewJob(Utils.excelInputDataList.get(0));
					approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
					//Configuration.saveReport();

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRDeleteOneJobAndAddOneNewJobByMechanic - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRFMCancelOneJobInApproveEstimate() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-Job Cancel In Approve Estimate 41=====");
					Configuration.createNewWorkFlowReport("driverSRFMCancelOneJobInApproveEstimate 41");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRFMCancelOneJobInApproveEstimate - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverServiceRequestMechanicRejectsOrder() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver Service Request Mechanic Rejects Order 51 =====");
					Configuration.createNewWorkFlowReport("driverServiceRequestMechanicRejectsOrder 51");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicRejectOrder();
					driver.pushNotification(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverServiceRequestMechanicRejectsOrder - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		//By - Rakesh - 47
	    //Date 14-March-2018
		@Test
		public void driverSRFMDeletes() {
			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SRFMDeletes 47 =====");
					Configuration.createNewWorkFlowReport("driverSRFMDeletes 47");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					fleetManager.deleteFMOrder();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					//Configuration.saveReport();
				}
			} catch (Exception e) {

			}
		}
		
		@Test
		public void driverSRWithPartsWithOutRetailerCancelOneJob() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SRWithPartsWithOutRetailerCancelOnejob 36=====");
					Configuration.createNewWorkFlowReport("driverSRWithPartsWithOutRetailerCancelOneJob 36");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.tackLocationMultiJobWithParts(Utils.excelInputDataList.get(0));
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverApproveEstimateCancelOneJobsWithPayCashWithAlert(Utils.excelInputDataList.get(0),fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
					//Configuration.saveReport();
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRWithPartsWithOutRetailerCancelOneJob - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverServiceRequestIDontKnowWhatsWrongWithParts() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-Service Request I Don't Know Whats Wrong with parts 33 =====");
					Configuration.createNewWorkFlowReport("driverServiceRequestIDontKnowWhatsWrongWithParts 33");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectUnKnowJobs();
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					//driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationForUnknownIssueWithParts(Utils.excelInputDataList.get(0));
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverServiceRequestIDontKnowWhatsWrongWithParts - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRPartsNoRetailerRevisedwithPartsWithRetailer() throws InterruptedException {

			try {

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SR With Parts NoRetailer Revised Estimate With Parts With Retailer 31 =====");
					Configuration.createNewWorkFlowReport("driverSRPartsNoRetailerRevisedwithPartsWithRetailer 31");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					approveEstimate.driverRevisedApproveEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "SRPartsNoRetailerRevisedwithPartsWithRetailer - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRWithPartsWithRetailerRevisedPartsNoRetailer() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-service Request WithParts Revised Estimate With Parts no retailer 29 =====");
					Configuration.createNewWorkFlowReport("driverSRWithPartsWithRetailerRevisedPartsNoRetailer 29");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRPartsNoRetailerRevisedwithPartsWithRetailer - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws InterruptedException {

			try {

				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SR With Parts NoRetailer Revised Estimate With Parts No Retailer -27 =====");
					Configuration.createNewWorkFlowReport("driverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer 27");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
		
		@Test
		public void driverServiceRequestWithoutPartsRevisedWithoutParts() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-service Request WithoutParts Revised Without Parts 23=====");
					Configuration.createNewWorkFlowReport("driverServiceRequestWithoutPartsRevisedWithoutParts 23");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
//					approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager,driver);
//					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(0), mechanic);
					approveEstimate.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverServiceRequestWithoutPartsRevisedWithoutParts - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
	
		@Test
		public void driverSRPartsRevisedEstimateWithoutPartsRevisedBill() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-service Request WithParts Revised Estimate WithOut Parts Revised bill 21 =====");
					Configuration.createNewWorkFlowReport("driverSRPartsRevisedEstimateWithoutPartsRevisedBill 21");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(0), mechanic);
					approveEstimate.driverRevisedApproveEstimatewithoutPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRPartsRevisedEstimateWithoutPartsRevisedBill - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}

		}

		

		@Test
		public void driverSRWithPartsNoRetailer() throws InterruptedException {

		try {

		if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
		System.out.println("===== Driver-SR WithParts NoRetailer -25 =====");
		Configuration.createNewWorkFlowReport("driverSRWithPartsNoRetailer 25");
		mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
		driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
		createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
		mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
		
		mechanic.mechanicAcceptOrder(mRequestOrderId);
		trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
		trackLocation.completeOrYes();
		driver.pushNotification(Utils.excelInputDataList.get(0));
		
		trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
		trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
		approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
		driver.pushNotification(Utils.excelInputDataList.get(0));
		
		startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
		fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
		driver.pushNotification(Utils.excelInputDataList.get(0));
		mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
		driver.pushNotification(Utils.excelInputDataList.get(0));
		mechanic.mechanicRating(Utils.excelInputDataList.get(0));
		fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

		}
		} catch (Exception e) {
		Configuration.writeReport(LogStatus.FAIL, "driverSRWithPartsNoRetailer - Fail");
		mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
		mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
		}

		@Test
		public void driverSREstimateWithPartsRevisedBill() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver SR Estimate WithParts RevisedBill 13=====");
					Configuration.createNewWorkFlowReport("driverSRWithPartsRevisedEstimateWithPartsRevisedBill 13");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
					approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
					
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "driverSRWithPartsRevisedEstimateWithPartsRevisedBill  - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
	
	
		@Test
		public void driverSREstimateWithoutPartsRevisedBill() throws InterruptedException {

			try {
				if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
					System.out.println("===== Driver-SR WithoutParts Revised Without Parts 15=====");
					Configuration.createNewWorkFlowReport("driverSRWithoutPartsRevisedEstimatePartsRevisedBill 15");
					mRequestOrderId = driverCreateServiceRequest.driverServiceRepairsRequest(Utils.excelInputDataList.get(0));
					driverCreateServiceRequest.fmCreateServiceRepairsOrder(fleetManager);
					createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
					mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					mechanic.mechanicAcceptOrder(mRequestOrderId);
					trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
					trackLocation.completeOrYes();
					driver.pushNotification(Utils.excelInputDataList.get(0));
					
					trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));

					startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
					
					trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
					fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.driverConfirmCashReceipt(Utils.excelInputDataList.get(0), driver);
					driver.pushNotification(Utils.excelInputDataList.get(0));
					mechanic.mechanicRating(Utils.excelInputDataList.get(0));
					fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

				}
			} catch (Exception e) {
				Configuration.writeReport(LogStatus.FAIL, "SRWithoutPartsRevisedEstimatePartsRevisedBill - Fail");
				mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
				mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
			}
		}
	
	
	
}
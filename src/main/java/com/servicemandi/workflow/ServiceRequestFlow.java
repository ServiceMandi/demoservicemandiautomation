package com.servicemandi.workflow;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;
import com.sevicemandi.servicerequest.ServiceRequestApproveEstimate;
import com.sevicemandi.servicerequest.ServiceRequestStartJob;
import com.sevicemandi.servicerequest.ServicesRequestTrackLocation;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ServiceRequestFlow {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateServiceRequest createServiceRequest;
	private ServicesRequestTrackLocation trackLocation;
	private ServiceRequestApproveEstimate approveEstimate;
	private ServiceRequestStartJob startJob;
	private String mRequestOrderId = "";
	private ProfileCreation mProfileCreation;
	private Configuration mConfiguration;

	@BeforeTest
	public void setUpConfiguration() {

		try {
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			createServiceRequest = new CreateServiceRequest();
			trackLocation = new ServicesRequestTrackLocation();
			approveEstimate = new ServiceRequestApproveEstimate();
			startJob = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			mConfiguration = new Configuration();
			Configuration.reportConfiguration("SRBaseFlow");
			Configuration.createNewWorkFlowReport("Login Details");
			fleetManager.login(Utils.excelInputDataList.get(0));
			mechanic.login(Utils.excelInputDataList.get(0));
			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterSuite
	public void moveFile() {
		/*
		 * File file = new File(System.getProperty("user.dir") + File.separator
		 * + "Reports.html"); String fileName = new
		 * SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date()); if
		 * (file.renameTo(new File( System.getProperty("user.dir") +
		 * File.separator + "Reports" + File.separator + fileName + ".html"))) {
		 * // if file copied successfully then delete the original file
		 * System.out.println("File moved successfully"); } else {
		 * System.out.println("Failed to move the file"); }
		 */
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	
	@Test
	public void sRWithPartsGiveEstimate() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request With Parts Give Estimate =====");
				Configuration.createNewWorkFlowReport("sRWithPartsGiveEstimate");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
	}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "Service Request With Parts ");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
	
	
	
	@Test
	public void sRWithPartsGiveEstimateCompleteOrder() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request With Parts GiveEstimate CompleteOrder=====");
				Configuration.createNewWorkFlowReport("srWithPartsGiveEstimateCompleteOrder");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				mechanic.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(0), mRequestOrderId);
				mechanic.myPreviousOrder(Utils.excelInputDataList.get(0), mRequestOrderId);
	}	
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "Service Request With Parts ");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
		
	
	@Test
	public void serviceRequestWithoutParts() throws InterruptedException {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Without Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(2));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(2));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2));
				startJob.startJobs(Utils.excelInputDataList.get(2), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "Service Request With out Parts ");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestWithParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request With Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				//createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0), fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "Service Request With Parts ");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRWithPartsWithRetailerRevisedPartsNoRetailer() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate With Parts no retailer =====");
				Configuration.createNewWorkFlowReport("SRWithPartsWithRetailerRevisedPartsNoRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithPartsWithRetailerRevisedPartsNoRetailer - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestWithPartsRevisedEstimateWithParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsRevisedEstimateWithParts");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(3));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(3));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(3), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(3), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(3));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(3));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(3), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(3), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(3));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(3));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(3));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithPartsRevisedEstimateWithParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestWithoutPartsReviseEstimateWithParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request Without Parts Revise Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutPartsReviseEstimateWithParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithoutPartsReviseEstimateWithParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestWithoutPartsRevisedWithoutParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutPartsRevisedWithoutParts");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating(Utils.excelInputDataList.get(1));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithoutPartsRevisedWithoutParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRPartsNoRetailerRevisedwithPartsWithRetailer() throws InterruptedException {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== SR With Parts NoRetailer Revised Estimate With Parts With Retailer =====");
				Configuration.createNewWorkFlowReport("SRPartsNoRetailerRevisedwithPartsWithRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRPartsNoRetailerRevisedwithPartsWithRetailer - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() throws InterruptedException {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== SR With Parts NoRetailer Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRPartsRevisedEstimateWithoutPartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate WithOut Parts Revised bill =====");
				Configuration.createNewWorkFlowReport("SRPartsRevisedEstimateWithoutPartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(8));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(8));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(8), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(8));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(8), fleetManager);
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(8), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(8), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(8), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(8));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8));
				mechanic.mechanicRating(Utils.excelInputDataList.get(1));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRPartsRevisedEstimateWithoutPartsRevisedBill - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}

	}

	@Test
	public void serviceRequestWithPartsRevisedEstimateWithOutParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate WithOut Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsRevisedEstimateWithOutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(1),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating(Utils.excelInputDataList.get(1));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithPartsRevisedEstimateWithOutParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestFMCancelAtConfirmGarage() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Cancel At ConfirmGarage =====");
				Configuration.createNewWorkFlowReport("serviceRequestFMCancelAtConfirmGarage");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestFMCancelAtConfirmGarage - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestWithPartsWithOutRetailer() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Reques tWithParts WithOut Retailer =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsWithOutRetailer");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithPartsWithOutRetailer - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestIDontKnowWhatsWrongWithParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request I Don't Know Whats Wrong with parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestIDontKnowWhatsWrong");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectUnKnowJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationForUnknownIssueWithParts(Utils.excelInputDataList.get(0));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestIDontKnowWhatsWrongWithParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestIDontKnowWhatsWrongWithOutParts() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request I Don't Know Whats Wrong With out parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestIDontKnowWhatsWrongWithOutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectUnKnowJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationForUnknownIssueWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimate(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestIDontKnowWhatsWrongWithOutParts - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void serviceRequestMechanicRejectsOrder() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Mechanic Rejects Order =====");
				Configuration.createNewWorkFlowReport("serviceRequestMechanicRejectsOrder");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicRejectOrder();

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestMechanicRejectsOrder - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	public void serviceRequestWithoutPartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutPartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "serviceRequestWithoutPartsRevisedBill - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRWithoutPartsRevisedEstimatePartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("SRWithoutPartsRevisedEstimatePartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(9));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(9));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(9));
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(9));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(9), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(9));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithoutPartsRevisedEstimatePartsRevisedBill - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}

	}

	@Test
	public void SRWithPartsRevisedEstimateWithPartsRevisedBill() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate With Parts Revised Bill =====");
				Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithPartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(7));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(7));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(7), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(7));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithPartsRevisedEstimateWithPartsRevisedBill - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRFMCancelAllJobsInApproveEstimate() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Cancel All Jobs in Approve Estimate =====");
				Configuration.createNewWorkFlowReport("SRFMCancelAllJobsInApproveEstimate");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(0));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(0), fleetManager);

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRFMCancelAllJobsInApproveEstimate - Fail");
		}
	}

	@Test
	public void SRCreateNewRequestFromMyVehicle() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager Add New Vehicle with new request =====");
				Configuration.createNewWorkFlowReport("fleetManagerCreateNewRequestFromMyVehicle");
				mProfileCreation.addNewVehicle();
				createServiceRequest.createServiceRequestFromMyVehicle(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRCreateNewRequestFromMyVehicle - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRFMCancelOneJobInApproveEstimate() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Job Cancel In Approve Estimate =====");
				Configuration.createNewWorkFlowReport("SRFMCancelOneJobInApproveEstimate");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(0), fleetManager);

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRFMCancelOneJobInApproveEstimate - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRFMDeletesAtTrackLocation() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtTrackLoc 59=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtTrackLoc 59");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRFMDeletesAtTrackLocation - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRMechanicDeletesAtTrackLoc() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRMechanicDeletesAtTrackLoc 57=====");
				Configuration.createNewWorkFlowReport("SRMechanicDeletesAtTrackLoc 57");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(0));
				Configuration.saveReport();
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRMechanicDeletesAtTrackLoc - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRFMDeletesAtFindGarage() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtFindGarage 48=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtFindGarage 48");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRFMDeletesAtFindGarage - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRFMDeletesAtApproveEstimate() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtApproveEstimate 61=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtApproveEstimate 61");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectJobs1(Utils.excelInputDataList.get(0),fleetManager);
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRFMDeletesAtApproveEstimate - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRDeleteOneJobByMechanic() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRDeleteOneJobbyMechanic 43 =====");
				Configuration.createNewWorkFlowReport("SRDeleteOneJobByMechanic 43");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationMultiJobWithPartsWithoutSubmit(Utils.excelInputDataList.get(0));
				trackLocation.deleteJobwithSubmitEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				Configuration.saveReport();

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRDeleteOneJobByMechanic - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRDeleteOneJobAndAddOneNewJobByMechanic() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRDeleteOneJobAndAddOneNewJobByMechanic 44=====");
				Configuration.createNewWorkFlowReport("SRDeleteOneJobAndAddOneNewJobByMechanic 44");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.DeletejobwithoutSubmitEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationWithAddOneNewJob(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				Configuration.saveReport();

			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRDeleteOneJobAndAddOneNewJobByMechanic - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRMultiJobsFMCancelOneJobInApproveEstimate() throws InterruptedException {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRbyFMEstimatewithPartsRetailerCancelJobs 38=====");
				Configuration.createNewWorkFlowReport("SRMultiJobsFMCancelOneJobInApproveEstimate 38");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(0));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOneJobsWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				Configuration.saveReport();
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRMultiJobsFMCancelOneJobInApproveEstimate - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}

	@Test
	public void SRWithPartsWithOutRetailerCancelOneJob() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRWithPartsWithOutRetailerCancelOnejob 39=====");
				Configuration.createNewWorkFlowReport("SRWithPartsWithOutRetailerCancelOnejob 39");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(0));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(0));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(0));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.trackLocationMultiJobWithParts(Utils.excelInputDataList.get(0));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOneJobsWithPayCashWithAlert(Utils.excelInputDataList.get(0),
						fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				Configuration.saveReport();
			}
		} catch (Exception e) {
			Configuration.writeReport(LogStatus.FAIL, "SRWithPartsWithOutRetailerCancelOneJob - Fail");
			mConfiguration.clickFMElement(LocatorType.XPATH, LocatorPath.backImagePath);
			mConfiguration.clickMechanicElement(LocatorType.XPATH, LocatorPath.backImagePath);
		}
	}
}

package com.servicemandi.workflow;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BreakDownFlow1 {
	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateBreakDownRequest breakDownRequest;
	private BreakDownTrackLocation trackLocation;
	private BreakDownApproveEstimate approveEstimate;
	private BreakDownStartJob startJob;
	private String mRequestOrderId = "";
	WebElement myAccount;

	@BeforeTest
	public void setUpConfiguration() {

		try {

//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0), false);
//			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	@BeforeClass
	public void beforeClass() {

	}

	@AfterTest
	public void afterTest() throws InterruptedException {
		Configuration.saveReport();
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}
//
//	@Test(enabled = false)
//	public void breakDownWithPartsGiveEstimateCompleteOrder() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0), false);
//			mechanic.login(Utils.excelInputDataList.get(0), false);
//			Configuration.closeWorkFlowReport();
//
////			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
////				System.out.println("===== BreakDown With Parts GiveEstimat CompleteOrder=====");
////				Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimateCompleteOrder");
////				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
////				mechanic.mechanicAcceptOrder(mRequestOrderId);
////				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
////				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
////				mechanic.onGoingOrderCompleteOrder(Utils.excelInputDataList.get(0), mRequestOrderId);
////				mechanic.myPreviousOrder(Utils.excelInputDataList.get(0), mRequestOrderId);
////				Configuration.closeWorkFlowReport();
////			}
//		} catch (Exception e) {
//		}
//	}
	
//	@Test
//	public void breakDownWithPartsGiveEstimate() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== BreakDown With Parts GiveEstimate =====");
//				Configuration.createNewWorkFlowReport("BreakDownWithPartsGiveEstimate");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//
//			}
//		} catch (Exception e) {
//		}
//	}

	@Test
	public void leykartDemo() {

		try {
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
			//fleetManager.loginLeykat(Utils.excelInputDataList.get(0), false);
			//mechanic.login(Utils.excelInputDataList.get(0), false);
			Configuration.closeWorkFlowReport();

			
		} catch (Exception e) {
		}
	}
//
//	@Test
//	public void breakDownWithParts() {
//
//		try {
//
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With Parts Normal Flow =====");
//				Configuration.createNewWorkFlowReport("BreakDown With Parts");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		} finally {
//
//			// Configuration.closeWorkFlowReport();
//		}
//	}
//
//	@Test
//	public void BDFMCancelAllJobsInApproveEstimate() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== BreakDown Parts Cancel All Jobs =====");
//				Configuration.createNewWorkFlowReport("BDFMCancelAllJobsInApproveEstimate");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithMultiJobs(Utils.excelInputDataList.get(0));
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(0), fleetManager);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//
//		}
//	}
//
//	@Test
//	public void BDWithoutPartsFMCancelJobInAproveEstimate() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== BDWithoutPartsFMCancelJobInAproveEstimat=====");
//				Configuration.createNewWorkFlowReport("BDWithoutPartsFMCancelJobInAproveEstimate");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(0), fleetManager);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownFMCancelAtConfirmGarage() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down FM Cancel At Confirm Garage =====");
//				Configuration.createNewWorkFlowReport("BreakDownFMCancelAtConfirmGarage");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//				/*
//				 * fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				 * mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				 * mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				 * fleetManager.fleetManagerRating(Utils.excelInputDataList.get( 1));
//				 */
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsNoRetailerRevisedEstimateWithoutParts() {
//
//		try {
//
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With Parts NoRetailer RevisedEstimate Without Parts =====");
//				Configuration.createNewWorkFlowReport("breakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(0), mechanic);
//				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(0), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
//				System.out
//						.println("===== Break Down With Parts NoRetailer Revised Estimate With Parts NoRetailer =====");
//				Configuration.createNewWorkFlowReport("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithoutPartsRevisedEstimateWithParts() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down Without Parts Revised Estimate WithParts 8=====");
//				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedEstimateWithParts");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(5));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(5));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(5), mechanic);
//				trackLocation.completeOrYes();
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(5));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(5), fleetManager);
//				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(5), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(5));
//				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(5), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(5), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(5));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(5));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(5));
//				Configuration.closeWorkFlowReport();
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithoutPartsRevisedEstimateWithOutParts() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down Without Parts Revised EstimateWithout Parts 12=====");
//				Configuration.createNewWorkFlowReport("BreakDown WithoutPartsRevisedEstimate WithoutParts");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
//				trackLocation.completeOrYes();
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
//				// startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1),
//				// mechanic);
//				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(1), mechanic, trackLocation);
//				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(1));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(1));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithoutPartsRevisedBill() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down Without Parts Revised Bill 16=====");
//				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedBill");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
//				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownPartsRevisedWithPartsRevisedBill() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down Parts Revised Estimate PartsRevisedBill 22=====");
//				Configuration.createNewWorkFlowReport("breakDownPartsRevisedWithPartsRevisedBill");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(7));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
//				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(7), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
//				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
//				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(7), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(7));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(7));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(7));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("=====breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill 22=====");
//				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(9));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic);
//				trackLocation.completeOrYes();
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(9), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(9));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(9), fleetManager);
//				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(9), mechanic);
//				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(9), fleetManager);
//				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(9), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(9));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(9));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(9));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== BreakDown Without Parts Revised EstimateWithout Parts Revised Bill 24=====");
//				Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(8));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(8));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(8), mechanic);
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(8));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(8), fleetManager);
//				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(8), mechanic);
//				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(8), fleetManager);
//				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(8), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(8));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(8));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(8));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithoutPartsRevisedEstimateWithPartsRevisedbill() {
//
//		try {
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("=====BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill 21=====");
//				Configuration.createNewWorkFlowReport("BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(10));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(10));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(10), mechanic);
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(10));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(10), fleetManager);
//				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(10), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(10));
//				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(10), fleetManager);
//				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(10), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(10));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(10));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(10));
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsWithOutRetailer() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== BD WithParts NO Retailer =====");
//				Configuration.createNewWorkFlowReport("BD WithParts NO Retailer");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsRevisedBill() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With Parts Revised Bill =====");
//				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedBill");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithOutParts() {
//
//		try {
//
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			// fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			// if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0)
//			// {
//			// System.out.println("===== Break Down With out Parts Normal Flow =====");
//			// Configuration.createNewWorkFlowReport("BreakDown WithOut Parts");
//			//
//			// mRequestOrderId =
//			// breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//			// mechanic.mechanicAcceptOrder(mRequestOrderId);
//			// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//			// trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//			// trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
//			// // trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//			// approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0),
//			// fleetManager);
//			// startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//			// fleetManager.viewBill(Utils.excelInputDataList.get(0));
//			// mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//			// mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//			// fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//			// Configuration.closeWorkFlowReport();
//			// }
//		} catch (Exception e) {
//		} finally {
//			// Configuration.closeWorkFlowReport();
//
//		}
//	}
//
//	@Test
//	public void breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With out Parts Revised Estimate WithParts No Retailer =====");
//				Configuration.createNewWorkFlowReport("breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
//				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
//				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsRevisedEstimateWithOutParts() {
//
//		try {
//
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With Parts Revised Estimate With out Parts =====");
//				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutParts");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(6));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(6));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(6), mechanic);
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(6), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(6));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(6), fleetManager);
//				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(6), mechanic);
//				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(6), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(6), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(6));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(6));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(6));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownWithPartsRevisedEstimateWithParts() {
//
//		try {
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down With Parts Revised Estimate WithParts =====");
//				Configuration.createNewWorkFlowReport("BreakDownWithPartsRevisedEstimateWithParts");
//
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
//				trackLocation.completeOrYes();
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
//				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
//				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(1), mechanic, trackLocation);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
//				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
//				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
//				fleetManager.viewBill(Utils.excelInputDataList.get(1));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(1));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
//
//			}
//		} catch (Exception e) {
//		}
//	}
//
//	@Test
//	public void breakDownMechanicRejectsOrder() {
//
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("===== Break Down Mechanic Rejects Order =====");
//				Configuration.createNewWorkFlowReport("breakDownWithParts");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRejectOrder();
//				Configuration.closeWorkFlowReport();
//
//			}
//		} catch (Exception e) {
//
//		}
//	}
//
//	// @Test
//	// public void BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() {
//	// try {
//	// if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() >
//	// 0) {
//	// System.out.println("=====BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate
//	// 35-NS=====");
//	// Configuration.createNewWorkFlowReport("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate
//	// 35-NS");
//	// mRequestOrderId =
//	// breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//	// mechanic.mechanicAcceptOrder(mRequestOrderId);
//	// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//	// trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//	// trackLocation.trackLocationWithMultiJobsWithOutParts(Utils.excelInputDataList.get(0));
//	// approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(0),
//	// fleetManager);
//	// startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//	// fleetManager.viewBill(Utils.excelInputDataList.get(0));
//	// mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//	// mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//	// fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//	// }
//	// } catch (Exception e) {
//	// e.printStackTrace();
//	// }
//	// }
//	//
//	// @Test
//	// public void BDFMDeletesAtVehicleInGarage() throws InterruptedException {
//	//
//	// try {
//	// if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() >
//	// 0) {
//	// System.out.println("=====BDFMDeleteAtVehicleInGarage=====");
//	// Configuration.createNewWorkFlowReport("BDFMDeleteAtVehicleInGarage");
//	// mRequestOrderId =
//	// breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//	// mechanic.mechanicAcceptOrder(mRequestOrderId);
//	// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//	// trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0),
//	// mechanic);
//	// fleetManager.cancelOrder();
//	// fleetManager.viewBill(Utils.excelInputDataList.get(0));
//	// mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//	// mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//	// fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
//	// }
//	// } catch (Exception e) {
//	// }
//	// }
//
//	@Test
//	public void BDMechanicDeletesAtTrackLoc() {
//		try {
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
//				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//
//		}
//	}
//
//	@Test
//	public void BDFMDeletesAtTrackLoc() {
//		try {
//
//			ReadingExcelData.readingWorkFlowData();
//			fleetManager = new FleetManager();
//			mechanic = new Mechanic();
//			breakDownRequest = new CreateBreakDownRequest();
//			trackLocation = new BreakDownTrackLocation();
//			approveEstimate = new BreakDownApproveEstimate();
//			startJob = new BreakDownStartJob();
//			Configuration.reportConfiguration("BDBaseFlow");
//			Configuration.createNewWorkFlowReport("Login Details");
//			fleetManager.login(Utils.excelInputDataList.get(0));
//			mechanic.login(Utils.excelInputDataList.get(0));
//			Configuration.closeWorkFlowReport();
//
//			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
//				System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
//				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
//				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
//				Configuration.closeWorkFlowReport();
//			}
//		} catch (Exception e) {
//
//		}
//	}
//
//	// @Test
//	// public void BDFMDeletesAtFindGarage() {
//	// try {
//	// if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() >
//	// 0) {
//	// System.out.println("=====BDFMDeletesAtFindGarage 38=====");
//	// Configuration.createNewWorkFlowReport("BDFMDeletesAtFindGarage 38");
//	// mRequestOrderId =
//	// breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(0));
//	// fleetManager.cancelOrder();
//	// Configuration.saveReport();
//	// }
//	// } catch (Exception e) {
//	//
//	// }
//	// }

}

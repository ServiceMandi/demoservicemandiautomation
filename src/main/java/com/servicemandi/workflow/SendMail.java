package com.servicemandi.workflow;

import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;

import java.nio.file.FileVisitResult;

import java.nio.file.Files;

import java.nio.file.Path;

import java.nio.file.Paths;

import java.nio.file.SimpleFileVisitor;

import java.nio.file.attribute.BasicFileAttributes;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.Properties;

import java.util.zip.ZipEntry;

import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;

import javax.activation.DataSource;

import javax.activation.FileDataSource;

import javax.mail.BodyPart;

import javax.mail.Message;

import javax.mail.MessagingException;

import javax.mail.Multipart;

import javax.mail.PasswordAuthentication;

import javax.mail.Session;

import javax.mail.Transport;

import javax.mail.internet.InternetAddress;

import javax.mail.internet.MimeBodyPart;

import javax.mail.internet.MimeMessage;

import javax.mail.internet.MimeMultipart;

//extends ZipFolder

public class SendMail {

	public static void SendMail(String newWorkFlow) throws Exception {

		System.out.println("Report WorkflowName::::" + newWorkFlow);

		//DateFormat df = new SimpleDateFormat("dd MM yyyy");

		// hinduja_ramu@ashokleyland.com
		// String To="rakesh.singh@hindujatech.com";
		//Hinduja_Priyadharshi@ashokleyland.com
		// String To = "hinduja_priyadharshi@ashokleyland.com";
		// String To="rakesh.singh@hindujatech.com";

		String From = "HTL_Mohana@ashokleyland.com"; // HTL_Mohana@ashokleyland.com
		String To = "HTL_Mohana@ashokleyland.com ,mohanadeepa.chandran@hindujatech.com";
		// String CC= "jagadabi.roopasri@hindujatech.com,rakesh.singh@hindujatech.com";

		Properties prop = new Properties();

		prop.put("mail.smtp.auth", "true");

		//prop.put("mail.smtp.starttls.enable", "true"); //"mail.smtp.starttls.enable",
		// "true");

		prop.put("mail.smtp.port", 587); // 587

		prop.put("mail.smtp.host", "mailrelay.ashokleyland.com"); // AL Mails
		//prop.put("mail.smtp.host", "smtp.office365.com");       //"smtp.office365.com" for Hinduja mails

		Session session = Session.getDefaultInstance(prop, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication("HTL_Mohana@ashokleyland.com", "Hinduja@6");
				//return new PasswordAuthentication("mohanadeepa.chandran@hindujatech.com", "Automation1");
			}

		});

		try {

			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(From));

			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(To));

			// message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(CC));

			message.setSubject("Service MANDI Automation Report-" + newWorkFlow);

			BodyPart messageBodyPart = new MimeBodyPart();

			String newline = System.getProperty("line.separator");

			messageBodyPart.setText("Dear User," + newline + " PFA the Execution report of::: " + newWorkFlow);

			Multipart multipart = new MimeMultipart();

			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();

			Thread.sleep(2000);

			// ZipFolder();

			// ZipFolder zf = new ZipFolder();

			// Use the following paths for windows

			// String folderToZip = "D:\\SM_EXE\\SMReport";

			// String zipName = "D:\\SM_EXE\\SMReport\\SMReports.zip";

			// zipFolder(Paths.get(folderToZip), Paths.get(zipName));

			// System.out.println("Folder Zipped Successfully");

			// String filename = "D:\\SM_EXE\\SMReport\\" + newWorkFlow + df.format(new
			// Date()) + ".html";

			String filename = "D:\\SM_EXE\\SMReport\\"+newWorkFlow+".html";
			
			System.out.println("filename:::"+filename);

			// String filename="D:\\SM_EXE\\SMReport\\Reports"+".zip";

			DataSource source = new FileDataSource(filename);

			messageBodyPart.setDataHandler(new DataHandler(source));

			messageBodyPart.setFileName(filename);

			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);

			Thread.sleep(5000);

			Transport.send(message);

			System.out.println("::::::Email Sent:::::::::");

		} catch (MessagingException e) {

			e.printStackTrace();

		}

	}

	// Uses java.util.zip to create zip file

	public void zipFolder(Path sourceFolderPath, Path zipPath) throws Exception {

		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));

		Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {

			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

				zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));

				Files.copy(file, zos);

				zos.closeEntry();

				return FileVisitResult.CONTINUE;

			}

		});

		zos.close();

	}

}
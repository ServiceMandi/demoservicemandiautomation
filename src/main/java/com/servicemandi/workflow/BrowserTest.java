package com.servicemandi.workflow;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

public class BrowserTest extends Configuration {

	public void browserLaunch(ExcelInputData excelInputData, String mRequestOrderId1, boolean isVerified)
			throws InterruptedException, IOException {
		System.out.println("Inside Browserlaunch");
		
		System.out.println("mRequestOrderId1::::::::::::" + mRequestOrderId1);
		System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver.exe"); // Old

		WebDriver driver = new ChromeDriver();

		driver.get("https://outlook.office.com/owa/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// LOGIN DETAILS:
		// =================
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@type='email']")).click();
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("useralip@gmail.com");
		String EmailNext = "//input[@value='Next']";
		Thread.sleep(1000);
		driver.findElement(By.xpath(EmailNext)).click();
		Thread.sleep(5000);
		String PswdInput = "//input[@name='passwd']";

		driver.findElement(By.xpath(PswdInput)).click();
		driver.findElement(By.xpath("//input[@name='passwd']")).sendKeys("Cherry@2");
		String PswdSubmit = "//input[@type='submit']";

		Thread.sleep(5000);
		driver.findElement(By.xpath(PswdSubmit)).click();
		Thread.sleep(4000);
		// Do you want stay signed in alert:
		driver.findElement(By.xpath("//input[@value='Yes']")).click();

		// Search for Order id:
		// ====================
		Thread.sleep(8000);
		// WebElement wb=driver.findElement(By.xpath("//button[@class='_n_j ms-bgc-tl-h
		// _n_k ms-bgc-tlr o365button ms-border-color-themeLighter']"));
		WebElement wb = driver.findElement(By.xpath(
				"//div[4]/div/div[1]/div/div[1]/div[1]/div/div/div[1]/div[1]/div[1]/div[2]/div[2]/button/span[2]"));
		Thread.sleep(2000);
		wb.click();

		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@autoid='_is_3']")).sendKeys(mRequestOrderId1);

		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@class='_is_w o365button']")).click();

		Thread.sleep(4000);

		driver.findElement(By.xpath("//div[@autoid='_lvv_3']")).click();
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText("https://test.ccavenue.com")).click();

		Thread.sleep(8000);
		// CC-AVENUE PAGE:
		// =================
		waitForNewWindowAndSwitchToIt(driver);
		try {
			Thread.sleep(8000);
			String windowTitle = driver.getTitle();
			if (windowTitle.equalsIgnoreCase("CCAvenue: Billing Shipping")) {
				System.out.println("windowTitle:::" + windowTitle);
				//eLogger.log(LogStatus.PASS, "CC Avenue Page Displayed::: PASSED");
			}else {
				System.out.println("windowTitle:::" + windowTitle);
				//eLogger.log(LogStatus.FAIL, "System error::"  +windowTitle +"FAILED");
				//System.exit(0);
			}

			Thread.sleep(2000);
			// Entering CreditCardNumber:
			driver.findElement(By.cssSelector("input[id*='creditCardNumber']")).click();
			driver.findElement(By.cssSelector("input[id*='creditCardNumber']")).sendKeys("4111111111111111");
			driver.findElement(By.cssSelector("input[id*='creditCardNumber']")).sendKeys(Keys.TAB);
			Thread.sleep(1000);

			WebElement month = driver.findElement(By.cssSelector("select[id*='expiryMonthCreditCard']"));
			Select Monthdropdown = new Select(month);
			Thread.sleep(1000);
			// Select Expiry Month:
			Monthdropdown.selectByVisibleText("Feb (02)");

			// Monthdropdown.selectByIndex(1);
			driver.findElement(By.cssSelector("select[id*='expiryMonthCreditCard']")).sendKeys(Keys.TAB);
			// Select Expiry Year:
			Thread.sleep(1000);

			Select Yeardropdown = new Select(driver.findElement(By.cssSelector("select[id*='expiryYearCreditCard']")));
			// Select Expiry Month:
			Yeardropdown.selectByVisibleText("2020");
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("input[id*='CVVNumberCreditCard']")).click();
			driver.findElement(By.cssSelector("input[id*='CVVNumberCreditCard']")).sendKeys("123");
			Thread.sleep(1000);
			driver.findElement(By.partialLinkText("Make Payment")).click();
			// TakesScreenshot:
			Thread.sleep(4000);
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			Thread.sleep(2000);
			DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh");
			String DestFile = System.getProperty("user.dir") +"_PaymentRecieptScreen_"+ df1.format(new Date())+".png";
					
			Thread.sleep(2000);
			System.out.println("Date Format::::" + df1.format(new Date()));
			FileUtils.copyFile(SrcFile, new File(DestFile));
			Thread.sleep(2000);
			eLogger.log(LogStatus.PASS, "Transaction Screen::: " + eLogger.addScreenCapture(DestFile));
			Thread.sleep(2000);
			// Return To the Merchant Site::
			driver.findElement(By.xpath("//input[@value='Return To the Merchant Site']")).click();
			Thread.sleep(5000);
			String CompletedText = driver.findElement(By.xpath("//html/body/p")).getText();
			System.out.println("CompletedText");
			eLogger.log(LogStatus.PASS, "SuccessFull Payment:::" + CompletedText);
		} catch (Exception e) {
			e.printStackTrace();
			eLogger.log(LogStatus.FAIL, "Browser failure issue");
		}

	}

	public static void waitForNewWindowAndSwitchToIt(WebDriver driver) throws InterruptedException {
		String cHandle = driver.getWindowHandle();
		String newWindowHandle = null;
		Set<String> allWindowHandles = driver.getWindowHandles();

		// Wait for 20 seconds for the new window and throw exception if not found
		for (int i = 0; i < 20; i++) {
			if (allWindowHandles.size() > 1) {
				for (String allHandlers : allWindowHandles) {
					if (!allHandlers.equals(cHandle))
						newWindowHandle = allHandlers;
				}
				driver.switchTo().window(newWindowHandle);
				break;
			} else {
				Thread.sleep(1000);
			}
		}
		if (cHandle == newWindowHandle) {
			throw new RuntimeException("Time out - No window found");
		}
	}

	
	public static void pdfinMail(ExcelInputData excelInputData, String mRequestOrderId1, boolean isVerified)
			throws InterruptedException, IOException {
		System.out.println("Inside PDFinMail");
		
		//System.out.println("mRequestOrderId1::::::::::::" + mRequestOrderId1);
		System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver.exe"); // Old

		WebDriver driver = new ChromeDriver();

		driver.get("https://outlook.office.com/owa/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// LOGIN DETAILS:
		// =================
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@type='email']")).click();
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("mohanadeepa.chandran@hindujatech.com");
		String EmailNext = "//input[@value='Next']";
		Thread.sleep(1000);
		driver.findElement(By.xpath(EmailNext)).click();
		Thread.sleep(5000);
		String PswdInput = "//input[@name='passwd']";

		driver.findElement(By.xpath(PswdInput)).click();
		driver.findElement(By.xpath("//input[@name='passwd']")).sendKeys("Deepa@1977");
		String PswdSubmit = "//input[@type='submit']";

		Thread.sleep(4000);
		driver.findElement(By.xpath(PswdSubmit)).click();
		Thread.sleep(4000);
		// Do you want stay signed in alert:
		driver.findElement(By.xpath("//input[@value='Yes']")).click();

		// Search for Order id:
		// ====================
		Thread.sleep(8000);
		// WebElement wb=driver.findElement(By.xpath("//button[@class='_n_j ms-bgc-tl-h
		// _n_k ms-bgc-tlr o365button ms-border-color-themeLighter']"));
		WebElement wb = driver.findElement(By.xpath("//div[4]/div/div[1]/div/div[1]/div[1]/div/div/div[1]/div[1]/div[1]/div[2]/div[2]/button/span[2]"));
		Thread.sleep(2000);
		wb.click();

		Thread.sleep(1000);
		//driver.findElement(By.xpath("//input[@autoid='_is_3']")).sendKeys("Invoice No ::"+mRequestOrderId1);
		
		driver.findElement(By.xpath("//input[@autoid='_is_3']")).sendKeys("Invoice No ::24564");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='_is_w o365button']")).click();

		Thread.sleep(4000);

		driver.findElement(By.xpath("//div[@autoid='_lvv_3']")).click();
		Thread.sleep(2000);
		//Download from mail:
			
		driver.findElement(By.partialLinkText("Invoice")).click();
		Thread.sleep(8000);
		driver.findElement(By.xpath("//span[@id='_ariaId_227']")).click(); //(By.cssSelector("input[id*='CVVNumberCreditCard']")).click();
		Thread.sleep(2000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		/*js.executeScript("window.scrollTo(0, 1000)");*/
		
		((JavascriptExecutor) js).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		/*// TakesScreenshot:
			Thread.sleep(8000);
			
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			Thread.sleep(2000);
			DateFormat df1 = new SimpleDateFormat(" dd MM yyyy hh");
			String DestFile = System.getProperty("user.dir") +"_InvoicePDF_"+ df1.format(new Date())+".png";
					
			Thread.sleep(1000);
			System.out.println("Date Format::::" + df1.format(new Date()));
			FileUtils.copyFile(SrcFile, new File(DestFile));
			Thread.sleep(1000);
			eLogger.log(LogStatus.PASS, "InvoicePDF taken from MAIL::: " + eLogger.addScreenCapture(DestFile));
			Thread.sleep(2000);

			((JavascriptExecutor) driver).executeScript("window.scrollTo(300, 500");
			Thread.sleep(2000);
			
			
			TakesScreenshot scrShot1 = ((TakesScreenshot) driver);
			File SrcFile1 = scrShot1.getScreenshotAs(OutputType.FILE);
			Thread.sleep(2000);
			DateFormat df11 = new SimpleDateFormat(" dd MM yyyy hh");
			String DestFile1 = System.getProperty("user.dir") +"_InvoicePDF1_"+ df1.format(new Date())+".png";
					
			Thread.sleep(2000);
			System.out.println("Date Format::::" + df11.format(new Date()));
			FileUtils.copyFile(SrcFile1, new File(DestFile1));
			Thread.sleep(2000);
			eLogger.log(LogStatus.PASS, "InvoicePDF taken from MAIL::: " + eLogger.addScreenCapture(DestFile1));
			*/
	}
	
	

	
}

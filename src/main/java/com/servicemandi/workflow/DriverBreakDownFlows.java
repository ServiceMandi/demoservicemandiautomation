
package com.servicemandi.workflow;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Driver;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.driver.DriverCreateBreakdownRequest;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

/**
 * Created by Deepa on 13-03-2018.
 */

public class DriverBreakDownFlows {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private Driver driver;
	private DriverCreateBreakdownRequest createDriverRequest;
	private BreakDownTrackLocation trackLocation;
	private BreakDownApproveEstimate approveEstimate;
	private BreakDownStartJob startJob;
	private String mRequestOrderId = "";
	public static String suiteName;
	
	@BeforeSuite
	public static void setUp(ITestContext ctx) {
		suiteName = ctx.getCurrentXmlTest().getSuite().getName();
	}
	
	@BeforeTest
	public void setUpConfiguration() {

		try {
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			driver = new Driver();
			createDriverRequest = new DriverCreateBreakdownRequest();
			trackLocation = new BreakDownTrackLocation();
			approveEstimate = new BreakDownApproveEstimate();
			startJob = new BreakDownStartJob();
	    	Configuration.reportConfiguration("DriverBDBaseFlow");
			Configuration.createNewWorkFlowReport("Login Details");
			fleetManager.login(Utils.excelInputDataList.get(0));
			mechanic.login(Utils.excelInputDataList.get(0));
			driver.login(Utils.excelInputDataList.get(0));
			Configuration.closeWorkFlowReport();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public void beforeClass() {

	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	@Test
	public void driverBreakDownWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown With Parts 1=====");
				Configuration.createNewWorkFlowReport("Driver BreakDown With Parts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
//				createDriverRequest.fmCreateBDOrder(fleetManager);
//				mechanic.mechanicAcceptOrder(mRequestOrderId);
//				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
//				driver.pushNotification(Utils.excelInputDataList.get(0));
//				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.completeOrYes();
//				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
//				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
//				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
//				driver.pushNotification(Utils.excelInputDataList.get(0));
//				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
//				driver.pushNotification(Utils.excelInputDataList.get(0));
//				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
//				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
//				driver.pushNotification(Utils.excelInputDataList.get(0));
//				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
//				driver.pushNotification(Utils.excelInputDataList.get(0));
//				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
//				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithoutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown Without Parts 2=====");
				Configuration.createNewWorkFlowReport("Driver BreakDown Without Parts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager,
						driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithParts Revised Estimate WithParts-5 =====");
				Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsRevisedEstimateWithParts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));

				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(0), mechanic, trackLocation);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager,
						driver);
				// driver.pushNotification(Utils.excelInputDataList.get(0));

				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithoutPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithoutParts Revised Estimate WithParts-7 =====");
				Configuration.createNewWorkFlowReport("DriverBreakDownWithoutPartsRevisedEstimateWithParts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));

				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(0), mechanic, trackLocation);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverRevisedApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager,
						driver);

				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithParts RevisedEstimate WithoutParts-9 =====");
				Configuration.createNewWorkFlowReport("DriverBreakDownWithPartsRevisedEstimateWithOutParts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));

				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(0), mechanic, trackLocation);
				approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(0), fleetManager,
						driver);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithoutPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithoutParts RevisedEstimate WithoutParts 11=====");
				Configuration.createNewWorkFlowReport("DriverBreakDownWithoutPartsRevisedEstimateWithOutParts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));

				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(0), mechanic, trackLocation);
				approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(0), fleetManager,
						driver);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownPartsRevisedWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-Break Down Parts Revised Estimate Parts RevisedBill 13=====");
				Configuration.createNewWorkFlowReport("DriverBreakDownPartsRevisedWithPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(0), mechanic, trackLocation);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void driverbreakDownWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-Break Down Without Parts Revised Bill 15=====");
				Configuration.createNewWorkFlowReport("driverbreakDownWithoutPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverbreakDownPartsRevisedWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver- BreakDown WithParts RevisedEstimateWithParts RevisedBill 17=====");
				Configuration.createNewWorkFlowReport("driverbreakDownPartsRevisedWithPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(7));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(7), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(0), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(7), mechanic, trackLocation);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(7));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void driverbreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====Driver-BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill 19=====");
				Configuration.createNewWorkFlowReport("driverbreakDownPartsRevisedWithPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(7), mechanic, trackLocation);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(7), fleetManager);

				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithParts RevisedEstimate WithOutParts RevisedBill 21=====");
				Configuration.createNewWorkFlowReport("driverBreakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(9));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(9));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(9), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(9));

				approveEstimate.driverApproveEstimatewithPayCash(Utils.excelInputDataList.get(9), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				approveEstimate.driverAppEstwithPayCash(Utils.excelInputDataList.get(9), fleetManager, driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(9), mechanic, trackLocation);
				approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(9), fleetManager,
						driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));

				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println(
						"===== Driver-BreakDown WithoutParts RevisedEstimate WithoutParts RevisedBill 23=====");
				Configuration.createNewWorkFlowReport("driverBDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2));
				approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(2), fleetManager,
						driver);
				driver.pushNotification(Utils.excelInputDataList.get(2));

				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(2), mechanic, trackLocation);
				approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(2), fleetManager,
						driver);

				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(2), mechanic);
				// fleetManager.driverViewRevisedBill(Utils.excelInputDataList.get(2),
				// driver);//for greater amount
				fleetManager.viewBill(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithPartsWithOutRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithParts WithOut Retailer 25=====");
				Configuration.createNewWorkFlowReport("driverBreakDownWithPartsWithOutRetailer");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
				System.out.println(
						"===== Driver-BreakDown WithParts NoRetailer RevisedEstimate With Parts NoRetailer 27 =====");
				Configuration.createNewWorkFlowReport("driverBDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.completeOrYes();

				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(2));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(21));
				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(2), mechanic, trackLocation);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(2));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(2), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(2), driver);
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithoutParts RevisedEstimate WithParts NoRetailer 29=====");
				Configuration.createNewWorkFlowReport("driverBreakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.completeOrYes();

				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(2));
				approveEstimate.driverApproveEstimateWithoutPayCash(Utils.excelInputDataList.get(2), fleetManager,
						driver);
				driver.pushNotification(Utils.excelInputDataList.get(2));
				startJob.revisedEstimationWithPartsYesNo(Utils.excelInputDataList.get(2), mechanic, trackLocation);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(2));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(2), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(2), driver);
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts() {
		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown WithParts NoRetailer RevisedEstimate WithoutParts 31=====");
				Configuration.createNewWorkFlowReport("driverBreakDownWithPartsNoRetailerRevisedEstimateWithoutParts");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(2));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.completeOrYes();

				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(2), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(2));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(2), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(2));
				startJob.revisedEstimationWithOutPartsYesNo(Utils.excelInputDataList.get(2), mechanic, trackLocation);
				approveEstimate.driverApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(2), fleetManager,
						driver);

				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(2), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(2), driver);
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(2));
				driver.pushNotification(Utils.excelInputDataList.get(2));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(2));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 33-NS=====");
				Configuration
						.createNewWorkFlowReport("driverBDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 33-NS");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithMultiJobsWithOutParts(Utils.excelInputDataList.get(0));

				approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void driverBDFMCancelAllJobsInApproveEstimate() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown Parts Cancel All Jobs 34 =====");
				Configuration.createNewWorkFlowReport("driverBDFMCancelAllJobsInApproveEstimate");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithMultiJobs(Utils.excelInputDataList.get(0));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {

		}
	}
	
	
	@Test
	public void driverNegativeScenarioFMDeletes() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BreakDown Negative Scenario FM Deletes 37 =====");
				Configuration.createNewWorkFlowReport("driverNegativeScenarioFMDeletes -37");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmDeleteBDOrder(fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
				
			}
		} catch (Exception e) {

		}
	}
	
	
	

	@Test
	public void driverbreakDownMechanicRejectsOrder() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====Driver- BreakDown Mechanic Reject Order 41=====");
				Configuration.createNewWorkFlowReport("driverbreakDownMechanicRejectsOrder");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicRejectOrder();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void driverBreakDownFMDeletesAtConfirmGarage() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-Break Down FM Deletes At Confirm Garage 44=====");
				Configuration.createNewWorkFlowReport("driverBreakDownFMDeletesAtConfirmGarage");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void driverBreakDownMechanicDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====DriverBDMechanicDeletesAtTrackLoc 46=====");
				Configuration.createNewWorkFlowReport("driverBreakDownMechanicDeletesAtTrackLoc 46");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicCancelOrder(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				
			}
		} catch (Exception e) {

		}
	}
	

	@Test
	public void driverBDFMDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====DriverBDMechanicDeletesAtTrackLoc 48=====");
				Configuration.createNewWorkFlowReport("driverBDFMDeletesAtTrackLoc 48");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void driverBDWithoutPartsFMCancelJobInAproveEstimate() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Driver-BDWithoutPartsFMCancelJobInAproveEstimate	50=====");
				Configuration.createNewWorkFlowReport("driverBDWithoutPartsFMCancelJobInAproveEstimate 50");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(0), mechanic);
				trackLocation.completeOrYes();
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(0));
				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(0), fleetManager);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				fleetManager.driverViewBill(Utils.excelInputDataList.get(0), driver);
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {
		}
	}
	@Test
	public void driverBDFMDeletesAtVehicleInGarage() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====DriverBDFMDeleteAtVehicleInGarage 52=====");
				Configuration.createNewWorkFlowReport("DriverBDFMDeleteAtVehicleInGarage");
				mRequestOrderId = createDriverRequest.driverBreakdownRequest(Utils.excelInputDataList.get(0));
				createDriverRequest.fmCreateBDOrder(fleetManager);
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.cancelOrder(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				driver.pushNotification(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating(Utils.excelInputDataList.get(0));
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
			}
		} catch (Exception e) {
		}
	}	

}




package com.servicemandi.webapps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.webapps.utils.FarmerUtils;
import com.servicemandi.webapps.utils.ReadingExcelData;
import com.servicemandi.webapps.utils.Utils;
import com.servicemandi.webapps.workflows.createOrder;
import com.servicemandi.workflow.SendMail;
import com.servicemandi.common.LocatorPath2;

public class WebAppWorkFlows extends SendMail{
	
	private static String RequestOrder;
	private static String OrderID;
	private static createOrder createOrder;
	private static createNewFM createNewFM;	
	private static com.servicemandi.webapps.workflows.hunterResources hunterResources;
	private static com.servicemandi.webapps.createNewVehicle createNewVehicle;
	private static com.servicemandi.webapps.createNewDriver createNewDriver;
	private static com.servicemandi.webapps.validation negval;
	private static com.servicemandi.webapps.workflows.farmerBDOrder farmerBDOrder;
	static DateFormat df = new SimpleDateFormat("ddMMyyyy_hhmmss");
	private static String DateTime=df.format(new Date());
	private static String WorkflowName;
	public static void init() throws InterruptedException {

		try {
		
//		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
		ReadingExcelData.readingWorkFlowData();
		createOrder = new createOrder();
		createNewFM = new createNewFM();
		
		createNewVehicle= new com.servicemandi.webapps.createNewVehicle();
		createNewDriver= new com.servicemandi.webapps.createNewDriver();
		
		} catch (Exception e) {
		
		}
	}
	
	public static void initValidations() throws InterruptedException {

		try {
		
//		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
		ReadingExcelData.readingWorkFlowData2();
		negval=new validation();
		createOrder = new createOrder();
		createNewFM = new createNewFM();
		createNewVehicle= new com.servicemandi.webapps.createNewVehicle();
		createNewDriver= new com.servicemandi.webapps.createNewDriver();
				
		} catch (Exception e) {
		
		}
		
	}
	
	public static void initResources() throws InterruptedException {

		try {
		//org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
		ReadingExcelData.readingWorkFlowData3();
		createOrder = new createOrder();
		createNewFM = new createNewFM();
		hunterResources=new com.servicemandi.webapps.workflows.hunterResources();
		createNewVehicle= new com.servicemandi.webapps.createNewVehicle();
		createNewDriver= new com.servicemandi.webapps.createNewDriver();
				
		} catch (Exception e) {
		
		}
	}
	
	public static void initFarmer() throws InterruptedException {

		try {
		//org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
		ReadingExcelData.readingWorkFlowData4();
		farmerBDOrder = new com.servicemandi.webapps.workflows.farmerBDOrder();
				
		} catch (Exception e) {
		
		}
	}
	
	//HUNTER-BD:

	public static void CreateOnlineBDOrder() throws Exception {

	try {
		 init();
		 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
				System.out.println("===== Create Online BD Orders =====");
				WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineBDOrder"+DateTime);
				WebConfiguration.createNewWorkFlowReport("CreateOnlineBDOrder"+DateTime);
				createOrder.webLogin(Utils.ExcelInputDataList.get(1));
				createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
				OrderID=createOrder.InputLocationBDVerification(Utils.ExcelInputDataList.get(1));
				WebConfiguration.closeWorkFlowReport();
				}
		}catch(Exception e) {	
		e.printStackTrace();
		WebConfiguration.TakeSnapshot();
	} finally{
		WebConfiguration.saveReport();
		SendMail(WorkflowName);
	}
}
	
	public static void CreateOnlineBDOrderAddNotes() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOnlineBDOrderAddNotes =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineBDOrderAddNotes"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOnlineBDOrderAddNotes"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
					OrderID=createOrder.InputLocationBDAddNotes(Utils.ExcelInputDataList.get(1));
					createOrder.AddNotes(Utils.ExcelInputDataList.get(1),OrderID);
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateOnlineBDOrderVerifyDetailsinResources() throws Exception {

		try {
			initResources();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOnlineBDOrderVerifyDetailsinResources =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineBDOrderVerifyDetailsinResources"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOnlineBDOrderVerifyDetailsinResources"+DateTime);
					hunterResources.webLogin(Utils.ExcelInputDataList.get(1));
					hunterResources.createBDOrder(Utils.ExcelInputDataList.get(1));
					OrderID = hunterResources.BDInputLocation(Utils.ExcelInputDataList.get(1));
					System.out.println("OrderID:::"+OrderID);
					hunterResources.VerifyDetailsinResources(Utils.ExcelInputDataList.get(1),OrderID);
					WebConfiguration.closeWorkFlowReport();
				}
		}catch(Exception e) {	
		e.printStackTrace();
		WebConfiguration.TakeSnapshot();
	} finally{
		WebConfiguration.saveReport();
		SendMail(WorkflowName);
	}
}
					
	public static void CreateOnlineBDOrderVerifyWorkshopLocatorinResources() throws Exception {

		try {
			initResources();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOnlineBDOrderVerifyWorkshopLocatorinResources =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineBDOrderVerifyWorkshopLocatorinResources"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOnlineBDOrderVerifyWorkshopLocatorinResources"+DateTime);
					hunterResources.webLogin(Utils.ExcelInputDataList.get(1));
					hunterResources.createBDOrder(Utils.ExcelInputDataList.get(1));
					OrderID = hunterResources.BDInputLocation(Utils.ExcelInputDataList.get(1));
					//OrderID = createOrder.InputLocationBDAddNotes(Utils.ExcelInputDataList.get(1));
					System.out.println("OrderID:::"+OrderID);
					hunterResources.WorkshopLocator(Utils.ExcelInputDataList.get(1), OrderID);
					WebConfiguration.closeWorkFlowReport();
				}
		}catch(Exception e) {	
		e.printStackTrace();
		WebConfiguration.TakeSnapshot();
	} finally{
		WebConfiguration.saveReport();
		SendMail(WorkflowName);
	}
}				
					
	public static void CreateOnlineBDOrderVerifyPriceEstimatorinResources() throws Exception {

		try {
			initResources();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOnlineBDOrderVerifyPriceEstimatorinResources =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineBDOrderVerifyPriceEstimatorinResources"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOnlineBDOrderVerifyPriceEstimatorinResources"+DateTime);
					hunterResources.webLogin(Utils.ExcelInputDataList.get(1));
					hunterResources.PriceEstimator(Utils.ExcelInputDataList.get(1), OrderID);
					hunterResources.SRSelectJobsWithoutSubmit(Utils.ExcelInputDataList.get(1));
					hunterResources.ResourcesVerifySelectedSRJobs(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
				}
		}catch(Exception e) {	
		e.printStackTrace();
		WebConfiguration.TakeSnapshot();
	} finally{
		WebConfiguration.saveReport();
		SendMail(WorkflowName);
	}
}					
				
		
	public static void CreateNewFMBDFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewFMBDFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewFMBDFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewFMBDFlow"+DateTime);
					createNewFM.webLogin(Utils.ExcelInputDataList.get(1));
					createNewFM.createOrder(Utils.ExcelInputDataList.get(1));
					createNewFM.vehicleRegister(Utils.ExcelInputDataList.get(1));
					createNewFM.UpcomingOrders(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}

	public static void CreateNewVehicleBDFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewVehicleBDFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewVehicleBDFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewVehicleBDFlow"+DateTime);
					createNewVehicle.webLogin(Utils.ExcelInputDataList.get(1));
					createNewVehicle.createOrder(Utils.ExcelInputDataList.get(1));
					OrderID=createNewVehicle.vehicleRegister(Utils.ExcelInputDataList.get(1));
					createNewVehicle.UpcomingOrders(Utils.ExcelInputDataList.get(1),OrderID);
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateNewDriverBDFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewDriverBDFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewDriverBDFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewDriverBDFlow"+DateTime);
   					createNewDriver.webLogin(Utils.ExcelInputDataList.get(1));
					createNewDriver.createOrder(Utils.ExcelInputDataList.get(1));
					createNewDriver.UpcomingOrders(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateOrderExistingFMWithNewVehicleNewDriverBDFlow() throws Exception {
		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOrderExistingFMWithNewVehicleNewDriverBDFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow"+DateTime);
					createNewVehicle.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createOrderwithNewVehicleNewDriver(Utils.ExcelInputDataList.get(1));
					createNewVehicle.vehicleRegister(Utils.ExcelInputDataList.get(1));
					//createNewVehicle.UpcomingOrders(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
		
	}
		
	public static void DeleteBDOrderAtConfirmOrder() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteBDOrderAtConfirmOrder =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteBDOrderAtConfirmOrder"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteBDOrderAtConfirmOrder"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
					createOrder.DeleteAtConfirmOrder(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void DeleteBDOrderAtAssignOrder() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteBDOrderAtAssignOrder =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteBDOrderAtAssignOrder"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteBDOrderAtAssignOrder"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
					OrderID=createOrder.DeleteAtAssignOrdertoWorkshop(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void DeleteBDOrderAtConfirmWorkshop() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteBDOrderAtConfirmWorkshop =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteBDOrderAtConfirmWorkshop"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteBDOrderAtConfirmWorkshop"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
					createOrder.DeleteBDOrderAtConfirmWorkshop(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
		
	public static void DeleteBDOrderAtConfirmVehicle() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteBDOrderAtConfirmVehicle =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteBDOrderAtConfirmVehicle"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteBDOrderAtConfirmVehicle"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createBDOrder(Utils.ExcelInputDataList.get(1));
					createOrder.DeleteBDOrderAtConfirmVehicle(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	
	//HUNTER-SR:

	public static void CreateOnlineSROrder() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOnlineSROrder =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOnlineSROrder"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOnlineSROrder"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerification(Utils.ExcelInputDataList.get(1));
					createOrder.selectWorkshop(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateSROrderIDontKnowWhatsWrong() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateSROrderIDontKnowWhatsWrong =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateSROrderIDontKnowWhatsWrong"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateSROrderIDontKnowWhatsWrong"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerificationIDontKnowWhatsWrong(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateNewFMSRFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewFMSRFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewFMSRFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewFMSRFlow"+DateTime);
					createNewFM.webLogin(Utils.ExcelInputDataList.get(1));
					createNewFM.createOrder(Utils.ExcelInputDataList.get(1));
					createNewVehicle.vehicleRegisterSR(Utils.ExcelInputDataList.get(1));
					createNewFM.UpcomingOrders(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
		
	public static void CreateNewVehicleSRFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewVehicleSRFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewVehicleSRFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewVehicleSRFlow"+DateTime);
					createNewVehicle.webLogin(Utils.ExcelInputDataList.get(1));
					createNewVehicle.createOrder(Utils.ExcelInputDataList.get(1));
					createNewVehicle.vehicleRegisterSR(Utils.ExcelInputDataList.get(1));
					createOrder.selectWorkshop(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateNewDriverSRFlow() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateNewDriverSRFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateNewDriverSRFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateNewDriverSRFlow"+DateTime);
   					createNewDriver.webLogin(Utils.ExcelInputDataList.get(1));
					createNewDriver.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.selectWorkshop(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void CreateOrderExistingFMWithNewVehicleNewDriverSRFlow() throws Exception {
		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== CreateOrderExistingFMWithNewVehicleNewDriverSRFlow =====");
					WorkflowName=WebConfiguration.reportConfiguration("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow"+DateTime);
					WebConfiguration.createNewWorkFlowReport("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow"+DateTime);
					createNewVehicle.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createOrderwithNewVehicleNewDriver(Utils.ExcelInputDataList.get(1));
					createNewVehicle.vehicleRegisterSR(Utils.ExcelInputDataList.get(1));
					createNewVehicle.UpcomingOrdersWithNewFMandExistingVehicleAndDriver(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
		
	}
	
	public static void DeleteSROrderAtConfirmOrder() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteSROrderAtConfirmOrder =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteSROrderAtConfirmOrder"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteSROrderAtConfirmOrder"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerification(Utils.ExcelInputDataList.get(1));
					createOrder.SRSelectWorkshopDelAtConfirmOrder(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			 
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void DeleteSROrderAtConfirmVehicle() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteSROrderAtConfirmVehicle =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteSROrderAtConfirmVehicle"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteSROrderAtConfirmVehicle"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerification(Utils.ExcelInputDataList.get(1));
					createOrder.SRSelectWorkshopDelAtConfirmVehicle(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			 
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void SRSelectWorkshopDelAtConfirmVehicleinWorkshop() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== SRSelectWorkshopDelAtConfirmVehicleinWorkshop =====");
					WorkflowName=WebConfiguration.reportConfiguration("SRSelectWorkshopDelAtConfirmVehicleinWorkshop"+DateTime);
					WebConfiguration.createNewWorkFlowReport("SRSelectWorkshopDelAtConfirmVehicleinWorkshop"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerification(Utils.ExcelInputDataList.get(1));
					RequestOrder=createOrder.SRSelectWorkshopDelAtConfirmVehicleinWorkshp(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			 
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void DeleteSROrderAtAssignOrder() throws Exception {

		try {
			 init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== DeleteSROrderAtAssignOrder =====");
					WorkflowName=WebConfiguration.reportConfiguration("DeleteSROrderAtAssignOrder"+DateTime);
					WebConfiguration.createNewWorkFlowReport("DeleteSROrderAtAssignOrder"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.createSROrder(Utils.ExcelInputDataList.get(1));
					createOrder.InputLocationSRVerification(Utils.ExcelInputDataList.get(1));
					createOrder.SRSelectWorkshopDelAtAssignOrder(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
			}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
			
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}


	//Validations:
	public static void VerifyMandatoryAlertsBDFlowCreateNewFM() throws Exception {

		try {
			initValidations();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== VerifyMandatoryAlertsBDFlowCreateNewFM =====");
					WorkflowName=WebConfiguration.reportConfiguration("VerifyMandatoryAlertsBDFlowCreateNewFM"+DateTime);
					WebConfiguration.createNewWorkFlowReport("VerifyMandatoryAlertsBDFlowCreateNewFM"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					negval.VerifyMandatoryAlerts(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FieldLevelValidations() throws Exception {

		try {
			initValidations();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== FieldLevelValidations =====");
					WorkflowName=WebConfiguration.reportConfiguration("FieldLevelValidations"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FieldLevelValidations"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					negval.FieldLevelValidations(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void OrderIdProcessing() throws Exception {

		try {
			init();
			 if (Utils.ExcelInputDataList != null && Utils.ExcelInputDataList.size() > 0) {
					System.out.println("===== OrderIdProcessing =====");
					WorkflowName=WebConfiguration.reportConfiguration("OrderIdProcessing"+DateTime);
					WebConfiguration.createNewWorkFlowReport("OrderIdProcessing"+DateTime);
					createOrder.webLogin(Utils.ExcelInputDataList.get(1));
					createOrder.PendingActions(Utils.ExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	//FARMER-BD Flows:
	public static void FarmerBDWithPartsProcessing() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FarmerBDWithPartsProcessing =====");
					WorkflowName =WebConfiguration.reportConfiguration("FarmerBDWithPartsProcessing"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FarmerBDWithPartsProcessing"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.SparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e) {	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FarmerBDWithoutPartsProcessing() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FarmerBDWithoutPartsProcessing =====");
					WorkflowName =WebConfiguration.reportConfiguration("FarmerBDWithoutPartsProcessing"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FarmerBDWithoutPartsProcessing"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	public static void FarmerBDWithPartsNoRetailer() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FarmerBDWithPartsNoRetailer =====");
					WorkflowName =WebConfiguration.reportConfiguration("FarmerBDWithPartsNoRetailer"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FarmerBDWithPartsNoRetailer"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithPartsNoRetail(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FRBDWithPartsRevEstWithParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsRevEstWithParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsRevEstWithParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsRevEstWithParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.SparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}

	public static void FRBDWithOutPartsRevEstWithParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithOutPartsRevEstWithParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithOutPartsRevEstWithParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithOutPartsRevEstWithParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	public static void FRBDWithPartsRevEstWithOutParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsRevEstWithOutParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsRevEstWithOutParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsRevEstWithOutParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.SparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}

	public static void FRBDWithOutPartsRevEstWithOutParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithOutPartsRevEstWithOutParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithOutPartsRevEstWithOutParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithOutPartsRevEstWithOutParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	//No Retailer:
	public static void FRBDWithPartsNoRetailerRevEstWithParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsNoRetailerRevEstWithParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsNoRetailerRevEstWithParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsNoRetailerRevEstWithParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithPartsNoRetail(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FRBDWithPartsRevEstWithPartsNoRetailer() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsRevEstWithPartsNoRetailer =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsRevEstWithPartsNoRetailer"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsRevEstWithPartsNoRetailer"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithParts(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.SparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithPartsNoRetailer(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));					
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	
	public static void FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithPartsNoRetail(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithPartsNoRetailer(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	
	public static void FRBDWithOutPartsRevEstWithPartsNoRetailer() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithOutPartsRevEstWithPartsNoRetailer =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithOutPartsRevEstWithPartsNoRetailer"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithOutPartsRevEstWithPartsNoRetailer"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithPartsNoRetailer(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.RevisedJobSparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FRBDWithPartsNoRetailerRevEstWithOutParts() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDWithPartsNoRetailerRevEstWithOutParts =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDWithPartsNoRetailerRevEstWithOutParts"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDWithPartsNoRetailerRevEstWithOutParts"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithPartsNoRetail(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddRevisedJobWithoutParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveAdditionalJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}			 
			
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	public static void FRBDAutoInvoice() throws Exception {

		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRBDAutoInvoice =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRBDAutoInvoice"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRBDAutoInvoice"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDCancelJob(FarmerUtils.FarmerExcelInputDataList.get(1)); //check by executing
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e){	
			e.printStackTrace();
			WebConfiguration.TakeSnapshot();
		}finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	
	public static void FRReallocateRetailer() throws Exception {
	
		try {
			initFarmer();
			 if (FarmerUtils.FarmerExcelInputDataList!= null && FarmerUtils.FarmerExcelInputDataList.size() > 0) {
					System.out.println("===== FRReallocateRetailer =====");
					WorkflowName =WebConfiguration.reportConfiguration("FRReallocateRetailer"+DateTime);
					WebConfiguration.createNewWorkFlowReport("FRReallocateRetailer"+DateTime);
					farmerBDOrder.farmerLogin(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.BDAddJobsWithParts(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ApproveJobs(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.SparePayments(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.ConfirmJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateJobStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.AddBillStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdatePaymentStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					farmerBDOrder.UpdateRatingStatus(FarmerUtils.FarmerExcelInputDataList.get(1));
					WebConfiguration.closeWorkFlowReport();
					}
		}catch(Exception e) {	
			e.printStackTrace();
			System.out.println("Workflow done");
			WebConfiguration.TakeSnapshot();
		} finally{
			WebConfiguration.saveReport();
			SendMail(WorkflowName);
		}
	}
	
	
	
	
}

	



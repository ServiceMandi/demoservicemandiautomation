package com.servicemandi.webapps;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.LocatorPath2;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.common.WebConfiguration.LocatorType;
import com.servicemandi.webapps.utils.ExcelInputData;



public class validation extends WebConfiguration{
	private String today;
	
	public validation() {

	}
	
	public void VerifyMandatoryAlerts(ExcelInputData excelInputData) throws InterruptedException {
		
		
		System.out.println("Inside CreateOrders");

		Thread.sleep(8000);
		//CreateOrder Link click:
		DriverWait("//*[@id='layout_1']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();
		//CreateOrder Button click:
		DriverWait("//*[@id='createOrder']");
		driver.findElement(By.xpath("//*[@id='createOrder']")).click();
		
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		Thread.sleep(2000);
		String GetErrorText = alert.getText();
		alert.accept();
		System.out.println("Alert message displays::::" + GetErrorText);
		writeReport(LogStatus.PASS, "Mandatory Alert for no FM name Number displays::"+GetErrorText);
		
		//Verifying FMNameNo field:
		driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();
		Thread.sleep(1000);
		clickElement(LocatorType.XPATH, LocatorPath2.FMentrypath);
		enterValue(LocatorType.XPATH, LocatorPath2.FMentrypath,"Create New");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.FMentrypath)).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		//Verifying AddFMNameNo field:
		Thread.sleep(3000);
		Alert AddFMNameNoalert = driver.switchTo().alert();
		Thread.sleep(2000);
		String AddFMNameNoText = AddFMNameNoalert.getText();
		AddFMNameNoalert.accept();
		System.out.println("Alert message displays::::" + AddFMNameNoText);
		writeReport(LogStatus.PASS, "Mandatory Alert for Add FM name Number displays::"+AddFMNameNoText);
		
		
		clickElement(LocatorType.XPATH, LocatorPath2.AddFMNameNumber);
		enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getAddNewFM());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.AddFMNameNumber)).sendKeys(Keys.RETURN);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		//Verifying StateCode field:
		Thread.sleep(3000);
		Alert StateCodeAlert = driver.switchTo().alert();
		Thread.sleep(2000);
		String StateCodeText = StateCodeAlert.getText();
		StateCodeAlert.accept();
		System.out.println("Alert message displays::::" + StateCodeText);
		writeReport(LogStatus.PASS, "Mandatory Alert for State Code displays::"+StateCodeText);
		
		Thread.sleep(2000);
		clickElement(LocatorType.XPATH, LocatorPath2.AddState);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect_chosen']/div/div/input")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect_chosen']/div/div/input")).sendKeys(excelInputData.getAddState());
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect_chosen']/div/div/input")).sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		//Verifying Vehicle Registration No field:
		Thread.sleep(5000);
		Alert VehRegNoAlert = driver.switchTo().alert();
		Thread.sleep(5000);
		String VehRegNoText = VehRegNoAlert.getText();
		VehRegNoAlert.accept();
		System.out.println("Alert message displays::::" + VehRegNoText);
		writeReport(LogStatus.PASS, "Mandatory Alert for Vehicle Registration Number displays::"+VehRegNoText);

		Thread.sleep(3000);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleRegNo);
		enterValue(LocatorType.XPATH, LocatorPath2.VehicleRegNo, excelInputData.getAddVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		//Verifying Driver Name-Number Field:
		Thread.sleep(5000);
		Alert DriverNameNoAlert = driver.switchTo().alert();
		Thread.sleep(5000);
		String DriverNameNotxt = DriverNameNoAlert.getText();
		DriverNameNoAlert.accept();
		System.out.println("Alert message displays::::" + DriverNameNotxt);
		writeReport(LogStatus.PASS, "Mandatory Alert for Driver Name-Number displays::"+DriverNameNotxt);
		
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNo);
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNo, excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNo)).sendKeys(Keys.TAB);
		
		
		clickElement(LocatorType.XPATH, LocatorPath2.LicenseExpDate);
		WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		List<WebElement> columns = LEDate.findElements(By.tagName("td"));
		for (WebElement cell: columns) {
	            /*
	            //If you want to click 18th Date
	            if (cell.getText().equals("18")) {
	            */
	            //Select Date
	            if (cell.getText().equals(excelInputData.getLicenseExpDate())) {//Change date
	                cell.click();
	                Thread.sleep(4000);
	                break;
	            }
	        }
		
		 Thread.sleep(2000);
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 Select selectRating=new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
		 selectRating.selectByVisibleText(excelInputData.getStarRating());
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);
		
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
				
		//Verification of Vehicle creation:
		//====================================
		Thread.sleep(9000);
		DriverWait(LocatorPath2.InputLocBtn);
		clickElement(LocatorType.XPATH,LocatorPath2.InputLocBtn);
		
		for(int j=4;j<=10;j++)
		{
		 Thread.sleep(2000);
		 if(j==7) {
				clickElement(LocatorType.XPATH, LocatorPath2.VehicleModelYr);
				clickElement(LocatorType.XPATH, LocatorPath2.VehicleModelYr);
				Select VehicleModelYr=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleModelYr)));
				VehicleModelYr.selectByVisibleText(excelInputData.getVehicleModelYr());
				writeReport(LogStatus.PASS, "VehicleModelYr:::"+excelInputData.getVehicleModelYr()+"   is selected");
				driver.findElement(By.xpath(LocatorPath2.VehicleModelYr)).sendKeys(Keys.TAB);
			 }
			 if(j==8) {				
				clickElement(LocatorType.XPATH, LocatorPath2.VehicleKMReading);
				enterValue(LocatorType.XPATH, LocatorPath2.VehicleKMReading, excelInputData.getVehicleKMReading());
				writeReport(LogStatus.PASS,"VehicleKMReading::"+excelInputData.getVehicleKMReading()+"   is selected");
				driver.findElement(By.xpath(LocatorPath2.VehicleKMReading)).sendKeys(Keys.TAB);
			 }
			 if(j==11) {
				 break;
			 }
			if((j==4)||(j==5)||(j==6)||(j==9)||(j==10)) {
			 DriverWait("//*[@id='makemodelform']/div["+j+"]/div/div[2]");
			 String mandatoryAlert1= driver.findElement(By.xpath("//*[@id='makemodelform']/div["+j+"]/div/div[2]")).getText();
			 System.out.println("mandatoryAlert1:::"+mandatoryAlert1);
			 writeReport(LogStatus.PASS, "Mandatory Alert displays::"+mandatoryAlert1);
			}	 
		}
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleMake);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleMake);
		Select selectVehicleMake=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleMake)));
		selectVehicleMake.selectByVisibleText(excelInputData.getVehicleMake());
		driver.findElement(By.xpath(LocatorPath2.VehicleMake)).sendKeys(Keys.TAB);
		writeReport(LogStatus.PASS, "VehicleMake::"+excelInputData.getVehicleMake()+"   is selected");
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleCategory);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleCategory);
		Select selectCategory=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleCategory)));
		selectCategory.selectByVisibleText(excelInputData.getVehicleCategory());
		driver.findElement(By.xpath(LocatorPath2.VehicleCategory)).sendKeys(Keys.TAB);
		writeReport(LogStatus.PASS, "VehicleCategory::"+excelInputData.getVehicleCategory()+"   is selected");
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleLoadRange);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleLoadRange);
		Select loadRange=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleLoadRange)));
		loadRange.selectByVisibleText(excelInputData.getVehicleLoadRange());
		driver.findElement(By.xpath(LocatorPath2.VehicleLoadRange)).sendKeys(Keys.TAB);
		writeReport(LogStatus.PASS, "VehicleLoadRange::"+excelInputData.getVehicleLoadRange()+"   is selected");
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleType);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleType);
		Select VehicleType=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleType)));
		VehicleType.selectByVisibleText(excelInputData.getVehicleType());
		driver.findElement(By.xpath(LocatorPath2.VehicleType)).sendKeys(Keys.TAB);
		writeReport(LogStatus.PASS, "VehicleType::"+excelInputData.getVehicleType()+"   is selected");
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleTyres);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleTyres);
		Select VehicleTyres=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleTyres)));
		VehicleTyres.selectByVisibleText(excelInputData.getVehicleTyres());
		driver.findElement(By.xpath(LocatorPath2.VehicleTyres)).sendKeys(Keys.TAB);
		writeReport(LogStatus.PASS, "No of Tyres::"+excelInputData.getVehicleTyres()+"   is selected");
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehInsExpDate);
		WebElement LEDate1 = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns1 = LEDate1.findElements(By.tagName("td"));
		 for (WebElement cell: columns1) {
	            
	                        
	            //Select Today's Date
	            if (cell.getText().equals(excelInputData.getLicenseExpDate())) {
	                cell.click();
	                writeReport(LogStatus.PASS, "VehInsExpDate::"+today+"    is selected");
	                break;
	            }
	        }
		 
		clickElement(LocatorType.XPATH,LocatorPath2.FCValidityDate);
		WebElement FCDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> column = FCDate.findElements(By.tagName("td"));
		 for (WebElement cell1: column) {
	            
	           	            
	            //Select Today's Date
	            if (cell1.getText().equals(excelInputData.getFCValidityDate())) {
	                cell1.click();
	                writeReport(LogStatus.PASS,  "FCValidityDate::"+excelInputData.getFCValidityDate()+"   is selected");
	                break;
	            }
	        }
		clickElement(LocatorType.XPATH,LocatorPath2.InputLocBtn);
		writeReport(LogStatus.PASS, "Vehicle Registration details verified");
		Thread.sleep(7000);
		
	
}

	public void FieldLevelValidations(ExcelInputData excelInputData) throws InterruptedException {
		
		
		System.out.println("Inside CreateOrders");

		Thread.sleep(8000);
		//CreateOrder Link click:
		DriverWait("//*[@id='layout_1']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();
		//CreateOrder Button click:
		DriverWait("//*[@id='createOrder']");
		

		//Verifying FMNameNo field:
		driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();
		Thread.sleep(1000);
		clickElement(LocatorType.XPATH, LocatorPath2.FMentrypath);
		enterValue(LocatorType.XPATH, LocatorPath2.FMentrypath,"Create New");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.FMentrypath)).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		
		//Verifying AddFMNameNo field:
		clickElement(LocatorType.XPATH, LocatorPath2.AddFMNameNumber);
		enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getInvalidFMNameNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.AddFMNameNumber)).sendKeys(Keys.RETURN);
				
		//Verifying StateCode field:
		Thread.sleep(8000);
		clickElement(LocatorType.XPATH, LocatorPath2.AddState);
		Thread.sleep(1000);
		Select selectState=new Select(driver.findElement(By.name("_Hunter_Create_order_newFMStateCodeSelect")));
		Thread.sleep(2000);
		selectState.selectByVisibleText(excelInputData.getAddState());
		/*WebElement state=driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect_chosen']/div/div/input"));
		state.sendKeys(excelInputData.getAddState());
		state.sendKeys(Keys.ENTER);*/
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		
		Thread.sleep(3000);
		Alert AddFMNameNoalert = driver.switchTo().alert();
		Thread.sleep(2000);
		String AddFMNameNoText = AddFMNameNoalert.getText();
		AddFMNameNoalert.accept();
		System.out.println("Alert Invalid Add FM Name Number displays::::" + AddFMNameNoText);
		writeReport(LogStatus.PASS, "Alert for Invalid Add FM Name Number displays:::"+AddFMNameNoText);
		
		//Entering Valid FMNameNumber:
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.AddFMNameNumber);
		driver.findElement(By.xpath(LocatorPath2.AddFMNameNumber)).clear();
		Thread.sleep(1000);
		enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getAddNewFM());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.AddFMNameNumber)).sendKeys(Keys.RETURN);
		
			
		//Verifying Vehicle Registration No field:
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleRegNo);
		enterValue(LocatorType.XPATH, LocatorPath2.VehicleRegNo, excelInputData.getInvalidVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		Thread.sleep(3000);
		Alert VehRegNoAlert = driver.switchTo().alert();
		Thread.sleep(2000);
		String VehRegNoText = VehRegNoAlert.getText();
		VehRegNoAlert.accept();
		System.out.println("Alert Invalid Vehicle Registration Number displays::::" + VehRegNoText);
		writeReport(LogStatus.PASS, "Alert for Invalid Vehicle Registration Number displays::"+VehRegNoText);
		
		//Entering Valid VehicleRegNo:
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleRegNo);
		driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).clear();
		enterValue(LocatorType.XPATH, LocatorPath2.VehicleRegNo, excelInputData.getAddVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		
		//Entering InValid Driver Name-Number Field:
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNo);
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNo, excelInputData.getInvalidDriverNameNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNo)).sendKeys(Keys.TAB);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		
		Thread.sleep(3000);
		Alert DriverNameNoAlert = driver.switchTo().alert();
		Thread.sleep(2000);
		String DriverNameNotxt = DriverNameNoAlert.getText();
		DriverNameNoAlert.accept();
		System.out.println("Alert for Invalid Driver Name-Number displays::::" + DriverNameNotxt);
		writeReport(LogStatus.PASS, "Alert for Invalid Driver Name-Number displays::"+DriverNameNotxt);
		
		
		//Entering Valid VehicleRegNo:
		Thread.sleep(10000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNo);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNo)).clear();
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNo, excelInputData.getAddDriverNameNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNo)).sendKeys(Keys.TAB);
		
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehInsExpDate);
		WebElement LEDate1 = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns1 = LEDate1.findElements(By.tagName("td"));
		 for (WebElement cell: columns1) {
	            
	                        
	            //Select Today's Date
	            if (cell.getText().equals(excelInputData.getLicenseExpDate())) {
	                cell.click();
	                writeReport(LogStatus.PASS, "VehInsExpDate::"+today+"    is selected");
	                break;
	            }
	        }
		 
		clickElement(LocatorType.XPATH,LocatorPath2.FCValidityDate);
		WebElement FCDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> column = FCDate.findElements(By.tagName("td"));
		 for (WebElement cell1: column) {
	            
	           	            
	            //Select Today's Date
	            if (cell1.getText().equals(excelInputData.getFCValidityDate())) {
	                cell1.click();
	                writeReport(LogStatus.PASS,  "FCValidityDate::"+excelInputData.getFCValidityDate()+"   is selected");
	                break;
	            }
	        }
		clickElement(LocatorType.XPATH,LocatorPath2.InputLocBtn);
		writeReport(LogStatus.PASS, "Vehicle Registration details verified");
		
		
		Thread.sleep(10000);
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 Select selectRating=new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
		 selectRating.selectByVisibleText(excelInputData.getStarRating());
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);
		
		 Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		writeReport(LogStatus.PASS, "Order Created successfully");
		
	}
}




	
	


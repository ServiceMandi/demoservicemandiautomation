package com.servicemandi.webapps.workflows;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.LocatorPath2;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.webapps.utils.ExcelInputData;
import com.servicemandi.webapps.utils.ReadingExcelData;
import com.servicemandi.webapps.utils.Utils;

import io.appium.java_client.android.AndroidKeyCode;

public class hunterResources extends WebConfiguration {

	public hunterResources() {

	}

	public void webLogin(ExcelInputData excelInputData) throws InterruptedException {
		
		webLaunch();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]"))
				.sendKeys(excelInputData.getResEmailID());

		driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]")).click();
		WebElement password = driver
				.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]"));
		password.sendKeys(excelInputData.getResPassword());
		password.sendKeys(Keys.TAB);
		password.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		writeReport(LogStatus.PASS, "Hunter App Launched and Logged in::::Passed");
		
	}

	public void createBDOrder(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("Inside createBDOrder");

        Thread.sleep(10000);
        System.out.println("excelInputData.getFMNameNumber():::" + excelInputData.getResFMNameNo());
        driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();

        driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();// "//*[@id="fleetSearchId_chosen"]/a/span"
        Thread.sleep(1000);

        clickElement(LocatorType.XPATH, LocatorPath2.FMentrypath);
        enterValue(LocatorType.XPATH, LocatorPath2.FMentrypath, excelInputData.getResFMNameNo());
        Thread.sleep(1000);
        WebElement FMentrypath = driver
                     .findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/div/div/input"));
        FMentrypath.sendKeys(Keys.ENTER);

        Thread.sleep(10000);
        driver.findElement(By.xpath("//*[@id='vehicleRegistrationNumberSelect_chosen']/a/span")).click();
        Thread.sleep(3000);
        WebElement VehicleEntrypath = driver.findElement(By.xpath("//*[@id='vehicleRegistrationNumberSelect_chosen']/div/div/input"));
        VehicleEntrypath.click();
        VehicleEntrypath.sendKeys(excelInputData.getResVehicleRegNo());
        Thread.sleep(2000);
        VehicleEntrypath.sendKeys(Keys.ENTER);
        Thread.sleep(10000);
        driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/a/span")).click();
        driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input"))
                     .sendKeys(excelInputData.getResDriverNameNo());
        Thread.sleep(6000);
        driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input")).sendKeys(Keys.ENTER);
        Thread.sleep(9000);
        DriverWait("//*[@id='createOrder']");
        driver.findElement(By.xpath("//*[@id='createOrder']")).click();
        Thread.sleep(7000);

        writeReport(LogStatus.PASS, "Created BD Order::::Passed");
 }
	
	public void createSROrder(ExcelInputData excelInputData) throws InterruptedException {
		
			System.out.println("Inside SR createOrder");

			Thread.sleep(10000);
			System.out.println("excelInputData.getFMNameNumber():::" + excelInputData.getFMNameNumber());
			driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();

			driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();// "//*[@id="fleetSearchId_chosen"]/a/span"
			Thread.sleep(2000);

			clickElement(LocatorType.XPATH, LocatorPath2.FMentrypath);
			enterValue(LocatorType.XPATH, LocatorPath2.FMentrypath, excelInputData.getFMNameNumber());
			Thread.sleep(1000);
			WebElement FMentrypath = driver
					.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/div/div/input"));
			FMentrypath.sendKeys(Keys.RETURN);

			Thread.sleep(10000);
			driver.findElement(By.xpath("//*[@id='vehicleRegistrationNumberSelect_chosen']/a/span")).click();
			Thread.sleep(2000);
			WebElement VehicleEntrypath = driver.findElement(By.xpath("//*[@id='vehicleRegistrationNumberSelect_chosen']/div/div/input"));
			VehicleEntrypath.click();
			VehicleEntrypath.sendKeys(excelInputData.getVehicleRegNo());
			Thread.sleep(2000);
			VehicleEntrypath.sendKeys(Keys.RETURN);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/a/span")).click();
			driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input")).click();
			driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input"))
					.sendKeys(excelInputData.getDriverNameNumber());
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id='driverNameNumberSelect_chosen']/div/div/input"))
					.sendKeys(Keys.RETURN);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='createOrder']")).click();
			Thread.sleep(7000);

			writeReport(LogStatus.PASS, "Create SR Order::::Passed");
		
	}

	public void createOrderwithNewVehicleNewDriver(ExcelInputData excelInputData) throws InterruptedException{
		
			System.out.println("Inside createOrderwithNewVehicleNewDriver");
			// Get Today's date
			String today = getCurrentDay();
			System.out.println("Today's date: " + today + "\n");

			Thread.sleep(10000);
			// System.out.println("excelInputData.getFMNameNumber():::"+excelInputData.getFMNameNumber());
			driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();

			driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();// "//*[@id="fleetSearchId_chosen"]/a/span"
			Thread.sleep(1000);

			clickElement(LocatorType.XPATH, LocatorPath2.EnterFMNameNo);
			enterValue(LocatorType.XPATH, LocatorPath2.EnterFMNameNo, excelInputData.getFMNameNumber());
			Thread.sleep(1000);
			driver.findElement(By.xpath(LocatorPath2.EnterFMNameNo)).sendKeys(Keys.RETURN);

			Thread.sleep(10000);
			clickElement(LocatorType.XPATH, LocatorPath2.VehicleRegcombo);
			clickElement(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo);
			enterValue(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo, "Create New");
			Thread.sleep(7000);
			driver.findElement(By.xpath(LocatorPath2.EnterVehicleRegNo)).sendKeys(Keys.TAB);

			Thread.sleep(10000);
			clickElement(LocatorType.XPATH, LocatorPath2.NewVehicleNo);
			enterValue(LocatorType.XPATH, LocatorPath2.NewVehicleNo, excelInputData.AddVehicleRegNo);
			Thread.sleep(1000);
			driver.findElement(By.xpath(LocatorPath2.NewVehicleNo)).sendKeys(Keys.TAB);

			Thread.sleep(10000);
			clickElement(LocatorType.XPATH, "//*[@id='driverNameNumberSelect_chosen']/a/span");
			clickElement(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo);
			enterValue(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo, "Create New");
			Thread.sleep(1000);
			driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.ENTER);
			Thread.sleep(3000);
			driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.TAB);

			Thread.sleep(10000);
			clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNumber);
			enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNumber, excelInputData.AddDriverNameNo);
			Thread.sleep(1000);
			driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.ENTER);
			driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.TAB);

			Thread.sleep(5000);
			clickElement(LocatorType.XPATH, LocatorPath2.DriverLicExpiryDate);
			WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
			List<WebElement> columns = LEDate.findElements(By.tagName("td"));
			for (WebElement cell : columns) {

				// Select Today's Date
				if (cell.getText().equals(today)) {
					cell.click();
					Thread.sleep(3000);
					break;
				}
			}

			Thread.sleep(2000);
			driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
			driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
			Select selectRating = new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
			selectRating.selectByVisibleText(excelInputData.getStarRating());
			driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);

			Thread.sleep(3000);
			System.out.println(driver.findElement(By.xpath("//*[@id='createOrderOffline']")).isEnabled());
			WebElement CreateOfflineOrders=driver.findElement(By.xpath("//*[@id='createOrderOffline']"));
			new Actions(driver).moveToElement(CreateOfflineOrders).click().perform();
		//CreateOfflineOrders.click();
					
			Thread.sleep(10000);

			writeReport(LogStatus.PASS,"Create Order:=>New Vehicle:" + excelInputData.AddVehicleRegNo + " :::Created"+"Create Order:=>New Driver:" + excelInputData.AddDriverNameNo + " :::Created");
		
		
	}

	public void InputLocationBDVerification(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("InputLocationVerification");
		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

			if (createorderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
			}
			if (createorderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
			}

			if (createorderListtext.equalsIgnoreCase("Input Location")) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
				// This will scroll the page till the element is found
				js.executeScript("arguments[0].scrollIntoView();", Element);
				Element.click();
				writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
				break;
			}
		}

		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(6000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(4000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
		Thread.sleep(2000);
		List<WebElement> RequestOrderDetailsList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/*/*"));
		System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

		for (int i = 1; i <= RequestOrderDetailsList.size(); i++) {
			String ReqOrderDetailsListtext = RequestOrderDetailsList.get(i).getText();
			System.out.println("RequestOrderDetailsList:::" + i + ":::::" + ReqOrderDetailsListtext);
			if (ReqOrderDetailsListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getDriverNameNumber())) {
				System.out.println("DriverNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "DriverNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}

			// Click ConfirmOrder:

			if (ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
				driver.findElement(By
						.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/div[7]/div/button"))
						.click();
				writeReport(LogStatus.PASS, "Clicked Confirm Order::::Passed");
				Thread.sleep(5000);
				break;
			}
		}

		// Upcoming Orders:
		// ---------------
		
		DriverWait("//*[@id='layout_3']/a/span[1]");
		// Clicking Upcoming Orders:
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		Thread.sleep(6000);
		// Clicking FM:
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		// Assign Order to Workshop button:
		List<WebElement> GetVehicleNoList = driver.findElements(By.xpath("//*[@id='requestCard_1']/*/*"));
		Thread.sleep(2000);
		System.out.println("GetVehicleNoList::" + GetVehicleNoList.size());
		for (int i = 1; i <= GetVehicleNoList.size(); i++) {
			String GetVehicleNoListtext = GetVehicleNoList.get(i).getText();
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetVehicleNoListtext);
			if (GetVehicleNoListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetVehicleNoListtext + "Passed");
				//writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "matches::::Passed");
			}
			if (GetVehicleNoListtext.contains(excelInputData.getDriverNameNumber())) {
				System.out.println("DriverNameNumber::::" + GetVehicleNoListtext + "Passed");
				// writeReport(LogStatus.PASS, "DriverNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetVehicleNoListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetVehicleNoListtext + "Passed");
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}

			if (GetVehicleNoListtext.contains("Assign Order to Workshop")) {
				// System.out.println("Vehicle Number matches");
				driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
				writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
				Thread.sleep(3000);
				break;

			}

		}

		// ConfirmWorkshop:
		// -----------------
			DriverWait("//*[@id='g_form_submit']/div[1]/*");
			Thread.sleep(3000);
			List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
			System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());
	
			for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
				String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
				System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);
	
				if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
					System.out.println("Workshop matches");
					ConfirmWorkshopList.get(i).click();
	
					break;
				} else {
					System.out.println("Workshop doesnt match");
					// writeReport(LogStatus.FAIL, "Workshop doesnt match::::Failed");
				}
			}
			// Clicking Confirm Garage button:
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
			writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
			Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop with FM::::Passed");
		Thread.sleep(3000);
		// }
		// Confirm Vehicle with Technician:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
		writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop::::Passed");
		Thread.sleep(5000);
		// }

		// Verifying the Order is displayed:
		// Clicking FM:
		System.out.println("getNoResults::");
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText = driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults = getresultsMatchText.getText();
		System.out.println("getNoResults::" + getNoResults);
		Thread.sleep(1000);
		if (getNoResults.startsWith("No results match")) {
			System.out.println("Order Successfully moved from Hunter to Farmer::::Passed");
			writeReport(LogStatus.PASS, "Order Successfully moved from Hunter to Farmer::::Passed");
		}
			
	}

	public void SRSelectJobs(ExcelInputData excelInputData) throws InterruptedException 
	{
		System.out.println("Inside SR-Job Selection");
		// SR::::Job Selection
		Thread.sleep(2000);
		String selectJob = "//*[@id='accordionParent']/*";
		DriverWait(selectJob);
		List<WebElement> selectJobOuterList = driver.findElements(By.xpath(selectJob));
		Thread.sleep(1000);
		System.out.println("Job List Size :" + selectJobOuterList.size());

		for (int i = 0; i <= selectJobOuterList.size(); i++) {
			Thread.sleep(1000);
			String selectJobText = selectJobOuterList.get(i).getText();
			System.out.println("selectJobText:::::::" + selectJobText);

			String expected = excelInputData.getSROuterJob();
			String[] outerArray = expected.split(",");

			for (String OuterArrayValues1 : outerArray) {
				System.out.println(OuterArrayValues1);
				int result = Integer.parseInt(OuterArrayValues1);

				Thread.sleep(1000);

				selectJobOuterList.get(result).click();

				// String OuterText=selectJobOuterList.get(result).getText();
				// System.out.println("OuterText::"+OuterText);
				Thread.sleep(1000);

				List<WebElement> selectInnerJobList = driver
						.findElements(By.xpath("//div[@class='panel-collapse collapse in']/*/*"));

				/*
				 * String
				 * OuterText=driver.findElement(By.xpath("//*[@id='accordionParent']/div["+
				 * OuterArrayValues1+"]/div[1]/h4/a")).getAttribute("href");
				 * System.out.println("OuterText::"+OuterText);
				 */

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//*[@id='Steering']/div/*"));//*[@id="Steering"]/div

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//div[@class='panel-body']"));
				// //selectJobOuterList.get(result)
				System.out.println("selectInnerJobList.size()::::" + selectInnerJobList.size());

				String OuterText = driver.findElement(By.xpath("//*[@class='panel-collapse collapse in']"))
						.getAttribute("id");
				System.out.println("OuterText:" + OuterText);
				String expectedInnerjob = excelInputData.getSRInnerJob();
				for (int j = 0; j < Integer.parseInt(expectedInnerjob); j++) {
					Thread.sleep(1000);

					System.out.println("selectInnerJobList.get(j).getText();:::" + selectInnerJobList.get(j).getText());

					if (j < Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						
						
						writeReport(LogStatus.PASS, "SR jobs::" + selectInnerJobList.get(j).getText() + ":::: selected");
						driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" + (j + 1)
								+ "]/label/following-sibling::input[@type='checkbox']")).click();
					}
					if (j == Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						// driver.findElement(By.xpath("//*[@id='accordionParent']/div["+result+"]/div[1]/h4/a")).click();
						break;
					}

				} // InnerFor

				// }else { break;}
			}
			driver.findElement(By.xpath("//button[@id='submitJobsButton']")).click();
			writeReport(LogStatus.PASS, "Clicked Submit button in SR: Select Jobs::::Passed");
			break;
		}
	}
	
	public void SRSelectJobsWithoutSubmit(ExcelInputData excelInputData) throws InterruptedException, IOException 
	{
		System.out.println("Inside SR-Job Selection");
		// SR::::Job Selection
		Thread.sleep(2000);
		String selectJob = "//*[@id='accordionParent']/*";
		DriverWait(selectJob);
		List<WebElement> selectJobOuterList = driver.findElements(By.xpath(selectJob));
		Thread.sleep(1000);
		System.out.println("Job List Size :" + selectJobOuterList.size());

		for (int i = 0; i <= selectJobOuterList.size(); i++) {
			Thread.sleep(1000);
			String selectJobText = selectJobOuterList.get(i).getText();
			System.out.println("selectJobText:::::::" + selectJobText);

			String expected = excelInputData.getSROuterJob();
			String[] outerArray = expected.split(",");

			for (String OuterArrayValues1 : outerArray) {
				System.out.println(OuterArrayValues1);
				int result = Integer.parseInt(OuterArrayValues1);

				Thread.sleep(1000);

				selectJobOuterList.get(result).click();

				// String OuterText=selectJobOuterList.get(result).getText();
				// System.out.println("OuterText::"+OuterText);
				Thread.sleep(1000);

				List<WebElement> selectInnerJobList = driver
						.findElements(By.xpath("//div[@class='panel-collapse collapse in']/*/*"));

				/*
				 * String
				 * OuterText=driver.findElement(By.xpath("//*[@id='accordionParent']/div["+
				 * OuterArrayValues1+"]/div[1]/h4/a")).getAttribute("href");
				 * System.out.println("OuterText::"+OuterText);
				 */

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//*[@id='Steering']/div/*"));//*[@id="Steering"]/div

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//div[@class='panel-body']"));
				// //selectJobOuterList.get(result)
				System.out.println("selectInnerJobList.size()::::" + selectInnerJobList.size());

				String OuterText = driver.findElement(By.xpath("//*[@class='panel-collapse collapse in']"))
						.getAttribute("id");
				System.out.println("OuterText:" + OuterText);
				String expectedInnerjob = excelInputData.getSRInnerJob();
				for (int j = 0; j < Integer.parseInt(expectedInnerjob); j++) {
					Thread.sleep(1000);

					System.out.println("selectInnerJobList.get(j).getText();:::" + selectInnerJobList.get(j).getText());

					ArrayList<String> FirstJobList=new ArrayList<String>();
					FirstJobList.add(selectInnerJobList.get(j).getText());
					
					
					
					if (j < Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						
						
						writeReport(LogStatus.PASS, "SR jobs::" + selectInnerJobList.get(j).getText() + ":::: selected");
						driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" + (j + 1)
								+ "]/label/following-sibling::input[@type='checkbox']")).click();
					}
					if (j == Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						
						// driver.findElement(By.xpath("//*[@id='accordionParent']/div["+result+"]/div[1]/h4/a")).click();
						break;
					}
					System.out.println("FirstJobList::::"+FirstJobList);
				} // InnerFor

				// }else { break;}
			}
			
			TakePositiveSnapshot();
			break;
		}
	}
	
	public void ResourcesVerifySelectedSRJobs(ExcelInputData excelInputData) throws InterruptedException{
		
		System.out.println("ResourcesVerifySelectedSRJobs");
		DriverWait("//*[@id='priceestimatortab']/div/div[3]/div/div/div/table");
		List<WebElement> JobList = driver.findElements(By.xpath("//*[@id='priceestimatortab']/div/div[3]/div/div/div/table/*/*"));
		System.out.println("JobList.size :::" +JobList.size());
		for(int k=0;k<JobList.size();k++){
		String JobListItem=JobList.get(k).getText();
		System.out.println("JobListItems:::"+JobListItem);
		if(k>0) {
		writeReport(LogStatus.PASS, "Selected JobList Items are::"+k+"::::::"+JobListItem);
		}
		}
		
		
	}

	public void InputLocationSRVerification(ExcelInputData excelInputData) throws InterruptedException {
	
		System.out.println("InputLocationSRVerification");
		Thread.sleep(8000);

		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
		// This will scroll the page till the element is found
		js.executeScript("arguments[0].scrollIntoView();", Element);
		Element.click();
		writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
		Thread.sleep(10000);
		// Entering Location in Map:
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(4000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click SR button:
		WebElement srElement = driver.findElement(By.xpath("//*[@id='servicerepair_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", srElement);
		Thread.sleep(1000);
		srElement.click();
		Thread.sleep(4000);
		writeReport(LogStatus.PASS, "Created SR order::::Passed");

		// SR::::Job Selection
		Thread.sleep(2000);
		String selectJob = "//*[@id='accordionParent']/*";
		DriverWait(selectJob);
		List<WebElement> selectJobOuterList = driver.findElements(By.xpath(selectJob));
		Thread.sleep(1000);
		System.out.println("Job List Size :" + selectJobOuterList.size());

		for (int i = 0; i <= selectJobOuterList.size(); i++) {
			Thread.sleep(1000);
			String selectJobText = selectJobOuterList.get(i).getText();
			System.out.println("selectJobText:::::::" + selectJobText);

			String expected = excelInputData.getSROuterJob();
			String[] outerArray = expected.split(",");

			for (String OuterArrayValues1 : outerArray) {
				System.out.println(OuterArrayValues1);
				int result = Integer.parseInt(OuterArrayValues1);

				Thread.sleep(1000);

				selectJobOuterList.get(result).click();

				// String OuterText=selectJobOuterList.get(result).getText();
				// System.out.println("OuterText::"+OuterText);
				Thread.sleep(1000);

				List<WebElement> selectInnerJobList = driver
						.findElements(By.xpath("//div[@class='panel-collapse collapse in']/*/*"));

				/*
				 * String
				 * OuterText=driver.findElement(By.xpath("//*[@id='accordionParent']/div["+
				 * OuterArrayValues1+"]/div[1]/h4/a")).getAttribute("href");
				 * System.out.println("OuterText::"+OuterText);
				 */

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//*[@id='Steering']/div/*"));//*[@id="Steering"]/div

				// List<WebElement> selectInnerJobList =
				// driver.findElements(By.xpath("//div[@class='panel-body']"));
				// //selectJobOuterList.get(result)
				System.out.println("selectInnerJobList.size()::::" + selectInnerJobList.size());

				String OuterText = driver.findElement(By.xpath("//*[@class='panel-collapse collapse in']"))
						.getAttribute("id");
				System.out.println("OuterText:" + OuterText);
				String expectedInnerjob = excelInputData.getSRInnerJob();
				for (int j = 0; j < Integer.parseInt(expectedInnerjob); j++) {
					Thread.sleep(1000);

					System.out.println("selectInnerJobList.get(j).getText();:::" + selectInnerJobList.get(j).getText());

					if (j < Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						
						
						writeReport(LogStatus.PASS, "SR jobs::" + selectInnerJobList.get(j).getText() + ":::: selected");
						driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" + (j + 1)
								+ "]/label/following-sibling::input[@type='checkbox']")).click();
					}
					if (j == Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						// driver.findElement(By.xpath("//*[@id='accordionParent']/div["+result+"]/div[1]/h4/a")).click();
						break;
					}

				} // InnerFor

				// }else { break;}
			}
			driver.findElement(By.xpath("//button[@id='submitJobsButton']")).click();
			writeReport(LogStatus.PASS, "Clicked Submit button in SR: Select Jobs::::Passed");
			break;
		}
	
	}
			
	public void InputLocationSRVerificationIDontKnowWhatsWrong(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("InputLocationSRVerificationIDontKnowWhatsWrong");
		Thread.sleep(8000);

		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
		// This will scroll the page till the element is found
		js.executeScript("arguments[0].scrollIntoView();", Element);
		Element.click();
		writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
		Thread.sleep(10000);
		// Entering Location in Map:
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(4000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click SR button:
		WebElement srElement = driver.findElement(By.xpath("//*[@id='servicerepair_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", srElement);
		Thread.sleep(1000);
		srElement.click();
		Thread.sleep(4000);
		writeReport(LogStatus.PASS, "Created SR order::::Passed");

		// SR::::Job Selection
		Thread.sleep(2000);
		String DonnoWhatsWrong = "//*[@id='checkbox_0']";
		DriverWait(DonnoWhatsWrong);
		driver.findElement(By.xpath("//*[@id='checkbox_0']")).click();
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "Clicked I don't know whats wrong in SR Jobs::::Passed");
		
		driver.findElement(By.xpath("//button[@id='submitJobsButton']")).click();
		writeReport(LogStatus.PASS, "Clicked Submit button in SR: Select Jobs::::Passed");
		
		Thread.sleep(9000);
		DriverWait("//button[@class='btn btn-default']");
		driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
		Thread.sleep(4000);

		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop with FM::::Passed");
		Thread.sleep(3000);

		// Confirm Vehicle with Technician:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(15000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
		writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop::::Passed");
		Thread.sleep(8000);
		// }

		// Verifying the Order is displayed:
		// Clicking FM:
		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText = driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults = getresultsMatchText.getText();
		System.out.println("getNoResults::" + getNoResults);
		Thread.sleep(1000);
		if (getNoResults.startsWith("No results match")) {
			System.out.println("Order Successfully moved from Hunter to Farmer::::Passed");
			writeReport(LogStatus.PASS, "Order Successfully moved from Hunter to Farmer::::Passed");
		}
		
		
	}
		
	public void BDJobSelection(ExcelInputData excelInputData) throws InterruptedException
	{	// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(6000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(4000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		Thread.sleep(15000);
		DriverWait("//*[@id='p_p_id_Hunter_pending_action_']/div/div/div/div/div/div");
		//DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form"); - was before
		Thread.sleep(2000);
		List<WebElement> RequestOrderDetailsList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_pending_action_']/div/div/div/div/div/div/*/*"));
		System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

		for (int j = 1; j <= RequestOrderDetailsList.size(); j++) {
			String ReqOrderDetailsListtext = RequestOrderDetailsList.get(j).getText();
			System.out.println("RequestOrderDetailsList:::" + j + ":::::" + ReqOrderDetailsListtext);
			
			// Click ConfirmOrder:
			if (ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
				driver.findElement(By.cssSelector("button[class='btn btn-default'][type='submit']")).click();
				writeReport(LogStatus.PASS, "Clicked Confirm Order::::Passed");
				Thread.sleep(5000);
				break;
			}
		}						
				
	}
	
	public void SRJobSelection(ExcelInputData excelInputData) throws InterruptedException
	{
		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(6000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(4000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click SR button:
		WebElement srElement = driver.findElement(By.xpath("//*[@id='servicerepair_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", srElement);
		Thread.sleep(1000);
		srElement.click();
		Thread.sleep(4000);
		writeReport(LogStatus.PASS, "Created SR order::::Passed");

		// SR::::Job Selection
		Thread.sleep(2000);
		String selectJob = "//*[@id='accordionParent']/*";
		DriverWait(selectJob);
		List<WebElement> selectJobOuterList = driver.findElements(By.xpath(selectJob));
		Thread.sleep(1000);
		System.out.println("Job List Size :" + selectJobOuterList.size());

		for (int i = 0; i <= selectJobOuterList.size(); i++) {
			Thread.sleep(1000);
			String selectJobText = selectJobOuterList.get(i).getText();
			System.out.println("selectJobText:::::::" + selectJobText);

			String expected = excelInputData.getSROuterJob();
			String[] outerArray = expected.split(",");

			for (String OuterArrayValues1 : outerArray) {
				System.out.println(OuterArrayValues1);
				int result = Integer.parseInt(OuterArrayValues1);

				Thread.sleep(1000);

				selectJobOuterList.get(result).click();

				// String OuterText=selectJobOuterList.get(result).getText();
				// System.out.println("OuterText::"+OuterText);
				Thread.sleep(1000);

				List<WebElement> selectInnerJobList = driver.findElements(By.xpath("//div[@class='panel-collapse collapse in']/*/*"));
			
				System.out.println("selectInnerJobList.size()::::" + selectInnerJobList.size());

				String OuterText = driver.findElement(By.xpath("//*[@class='panel-collapse collapse in']"))
						.getAttribute("id");
				System.out.println("OuterText:" + OuterText);
				String expectedInnerjob = excelInputData.getSRInnerJob();
				for (int j = 0; j < Integer.parseInt(expectedInnerjob); j++) {
					Thread.sleep(1000);

					System.out.println("selectInnerJobList.get(j).getText();:::" + selectInnerJobList.get(j).getText());

					if (j < Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						
						
						writeReport(LogStatus.PASS, "SR jobs::" + selectInnerJobList.get(j).getText() + ":::: selected");
						driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" + (j + 1) + "]/label/following-sibling::input[@type='checkbox']")).click();
					}
					if (j == Integer.parseInt(expectedInnerjob)) {
						System.out.println("Inside InnerJob If");
						Thread.sleep(1000);
						// driver.findElement(By.xpath("//*[@id='accordionParent']/div["+result+"]/div[1]/h4/a")).click();
						break;
					}

				} // InnerFor

				// }else { break;}
			}
			driver.findElement(By.xpath("//button[@id='submitJobsButton']")).click();
			writeReport(LogStatus.PASS, "Clicked Submit button in SR: Select Jobs::::Passed");
			break;
		}
		
		
		
	}
	
	public String BDInputLocation(ExcelInputData excelInputData) throws InterruptedException
	{
		String OrderID = "";
		System.out.println("BDInputLocation");
			List<WebElement> createorderList = driver
					.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
			System.out.println("CreateOrderList size :::" + createorderList.size());

			for (int i = 1; i <= createorderList.size(); i++) {
				String createorderListtext = createorderList.get(i).getText();
				System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

				if (createorderListtext.contains(excelInputData.getResFMNameNo())) {
					System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
					writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
				}
				if (createorderListtext.contains(excelInputData.getResVehicleRegNo())) {
					System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
					writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
				}

				if (createorderListtext.equalsIgnoreCase("Input Location")) {
					JavascriptExecutor js = (JavascriptExecutor) driver;
					WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
					// This will scroll the page till the element is found
					js.executeScript("arguments[0].scrollIntoView();", Element);
					Element.click();
					writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
					break;
				}
			}

			// Entering Location in Map:
			DriverWait("//*[@id='pac-input']");
			Thread.sleep(3000);
			Actions action = new Actions(driver);
			WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
			// Double click
			action.doubleClick(location).perform();
			location.clear();
			location.sendKeys(excelInputData.getResLocation());
			Thread.sleep(1000);
			location.sendKeys(Keys.ENTER);
			Thread.sleep(5000);
			// Scrolling and click BD button:
			WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
			Thread.sleep(1000);
			bdElement.click();
			writeReport(LogStatus.PASS, "Created BD order::::Passed");

			
			//Commented for testing purpose:
			//===================================
			DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
			Thread.sleep(2000);
			List<WebElement> RequestOrderDetailsList = driver
					.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/*/*"));
			System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

			for (int i = 0; i <= RequestOrderDetailsList.size(); i++) {
				String ReqOrderDetailsListtext = RequestOrderDetailsList.get(i).getText();
				System.out.println("RequestOrderDetailsList:::" + i + ":::::" + ReqOrderDetailsListtext);
				
				if (ReqOrderDetailsListtext.startsWith("Request")) {
				OrderID = RequestOrderDetailsList.get(0).getText().replaceAll("[^0-9]","");
				System.out.println("%%%%%%%%%%%% OrderID %%%%%%%%%%%%%:::"+OrderID);
				System.out.println("Request OrderNo::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "Request OrderNo:::::" + ReqOrderDetailsListtext + "  matches::::Passed");
				}
				if (ReqOrderDetailsListtext.contains(excelInputData.getResFMNameNo())) {
					System.out.println("FMNameNumber::::" + ReqOrderDetailsListtext + "Passed");
					writeReport(LogStatus.PASS, "FMNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
				}
				if (ReqOrderDetailsListtext.contains(excelInputData.getResDriverNameNo())) {
					System.out.println("DriverNameNumber::::" + ReqOrderDetailsListtext + "Passed");
					writeReport(LogStatus.PASS, "DriverNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
				}
				if (ReqOrderDetailsListtext.contains(excelInputData.getResVehicleRegNo())) {
					System.out.println("VehicleRegNo::::" + ReqOrderDetailsListtext + "Passed");
					writeReport(LogStatus.PASS, "VehicleRegNo::::" + ReqOrderDetailsListtext + "  matches::::Passed");
				}

				// Click ConfirmOrder:
				if (ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
					driver.findElement(By
							.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/div[7]/div/button"))
							.click();
					writeReport(LogStatus.PASS, "Clicked Confirm Order::::Passed");
					Thread.sleep(5000);
					break;
				}
			}

			return OrderID;
			
			/*// Upcoming Orders:
			// ---------------
			DriverWait("//*[@id='layout_3']/a/span[1]");
			
			
			// Clicking Upcoming Orders:
			driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
			writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
			Thread.sleep(6000);
			// Clicking FM:
			driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
			driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
			driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
					.sendKeys(excelInputData.getFMNameNumber());
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
			driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
					.sendKeys(excelInputData.getVehicleRegNo());
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
			driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
					.sendKeys(excelInputData.getDriverNameNumber());
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
			Thread.sleep(15000);

			// Assign Order to Workshop button:
			List<WebElement> GetVehicleNoList = driver.findElements(By.xpath("//*[@id='requestCard_1']/*"));
			Thread.sleep(2000);
			System.out.println("GetVehicleNoList::" + GetVehicleNoList.size());
			for (int i = 1; i < GetVehicleNoList.size(); i++) {
				String GetVehicleNoListtext = GetVehicleNoList.get(i).getText();
				System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetVehicleNoListtext);
				if (GetVehicleNoListtext.contains(excelInputData.getFMNameNumber())) {
					System.out.println("FMNameNumber::::" + GetVehicleNoListtext + "Passed");
					//writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "matches::::Passed");
				}
				if (GetVehicleNoListtext.contains(excelInputData.getDriverNameNumber())) {
					System.out.println("DriverNameNumber::::" + GetVehicleNoListtext + "Passed");
					// writeReport(LogStatus.PASS, "DriverNameNumber::::"+GetVehicleNoListtext+ "
					// matches::::Passed");
				}
				if (GetVehicleNoListtext.contains(excelInputData.getVehicleRegNo())) {
					System.out.println("VehicleRegNo::::" + GetVehicleNoListtext + "Passed");
					// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
					// matches::::Passed");
				}

				if (GetVehicleNoListtext.contains("Assign Order to Workshop")) {
					// System.out.println("Vehicle Number matches");
					driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
					writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
					Thread.sleep(3000);
					break;
				}
			}*/
	}
			
	public void DeleteAtConfirmOrder(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("DeleteAtConfirmOrder");
		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

			if (createorderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
			}
			if (createorderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
			}

			if (createorderListtext.equalsIgnoreCase("Input Location")) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
				// This will scroll the page till the element is found
				js.executeScript("arguments[0].scrollIntoView();", Element);
				Element.click();
				writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
				break;
			}
		}

		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(1000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
		Thread.sleep(2000);
		
		//Skipping to Upcoming Orders without clicking Confirm Order:
		Thread.sleep(6000);
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		//Clicking FM: 
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000); 
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);			
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);  
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		//Delete At Confirm Order with FleetManager:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(18000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "  is selected::::Passed");
				Thread.sleep(9000);
				break;
				
			}
		
		}
		
		Thread.sleep(8000);
		
		//Verifying the Order is displayed:
		//Clicking FM: 
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		
		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults=getresultsMatchText.getText();				
		System.out.println("getNoResults::"+getNoResults);
		Thread.sleep(1000);
		if(getNoResults.startsWith("No results match"))
		{	System.out.println("Order is Successfully Cancelled::::Passed");
			writeReport(LogStatus.PASS, "Order is Successfully Cancelled::::Passed");
		}
		
			
	}		

	public void DeleteAtAssignOrdertoWorkshop(ExcelInputData excelInputData) throws InterruptedException {
		
		
		System.out.println("DeleteAtConfirmOrder");
		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

			if (createorderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
			}
			if (createorderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
			}

			if (createorderListtext.equalsIgnoreCase("Input Location")) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
				// This will scroll the page till the element is found
				js.executeScript("arguments[0].scrollIntoView();", Element);
				Element.click();
				writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
				break;
			}
		}

		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(1000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
		Thread.sleep(2000);
		List<WebElement> RequestOrderDetailsList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/*/*"));
		System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

		for (int i = 1; i <= RequestOrderDetailsList.size(); i++) {
			String ReqOrderDetailsListtext = RequestOrderDetailsList.get(i).getText();
			System.out.println("RequestOrderDetailsList:::" + i + ":::::" + ReqOrderDetailsListtext);
			if (ReqOrderDetailsListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getDriverNameNumber())) {
				System.out.println("DriverNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "DriverNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains("Inspection Cost")) {
				System.out.println("Inspection Cost::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "Inspection Cost::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if(ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
			Thread.sleep(1000);
			DriverWait("//button[@class='btn btn-default']");
			driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
			Thread.sleep(7000);
			break;
			}	
		}
		
		//Upcoming Orders
		Thread.sleep(6000);
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		//Clicking FM: 
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000); 
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);			
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);  
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		//Delete At Assigned Order to Workshop:
				DriverWait("//i[@class='fa fa-trash-o']");
				driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
				Thread.sleep(2000);
				
				if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
				System.out.println("FM is already selected");
				WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
				element.click();
				Thread.sleep(5000);
				}
				else {		
				driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
				Thread.sleep(2000);
				WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
				element.click();
				Thread.sleep(5000);
				//Cancel Reason:
				DriverWait("//*[@id='cancellationReasons']");
				List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
				for (int z = 1; z <= CancelReasonList.size(); z++) {
					System.out.println("CancelReasonList:::"+CancelReasonList.size());
				
					String CancelReasonListtext = CancelReasonList.get(z).getText();
					System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
					System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
					if (z == Integer.parseInt(excelInputData.getCancelReason())) {
						System.out.println("CancelReasonListtext::"+CancelReasonListtext);
						driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
						//Submit button:
						driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
						writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "  is selected::::Passed");
						Thread.sleep(5000);
						break;
						
					}
				
				}
				
				Thread.sleep(8000);
				
				//Verifying the Order is displayed:
				//Clicking FM: 
				DriverWait("//*[@id='layout_3']/a/span[1]");
				driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
				
				System.out.println("getNoResults::");
				DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(2000);
				WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
				String getNoResults=getresultsMatchText.getText();				
				System.out.println("getNoResults::"+getNoResults);
				Thread.sleep(1000);
				if(getNoResults.startsWith("No results match"))
				{	System.out.println("Order is Successfully Cancelled::::Passed");
					writeReport(LogStatus.PASS, "Order is Successfully Cancelled::::Passed");
				}
		
		
	}		
			
	public void DeleteBDOrderAtConfirmWorkshop(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("DeleteBDOrderAtConfirmWorkshop");
		List<WebElement> createorderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

			if (createorderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
			}
			if (createorderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
			}

			if (createorderListtext.equalsIgnoreCase("Input Location")) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
				// This will scroll the page till the element is found
				js.executeScript("arguments[0].scrollIntoView();", Element);
				Element.click();
				writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
				break;
			}
		}

		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(1000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
		Thread.sleep(2000);
		List<WebElement> RequestOrderDetailsList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/*/*"));
		System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

		for (int i = 1; i <= RequestOrderDetailsList.size(); i++) {
			String ReqOrderDetailsListtext = RequestOrderDetailsList.get(i).getText();
			System.out.println("RequestOrderDetailsList:::" + i + ":::::" + ReqOrderDetailsListtext);
			if (ReqOrderDetailsListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getDriverNameNumber())) {
				System.out.println("DriverNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "DriverNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains("Inspection Cost")) {
				System.out.println("Inspection Cost::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "Inspection Cost::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if(ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
			Thread.sleep(1000);
			DriverWait("//button[@class='btn btn-default']");
			driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
			Thread.sleep(7000);
			break;
			}	
		}
		
		//Upcoming Orders
		Thread.sleep(6000);
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		//Clicking FM: 
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000); 
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);			
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);  
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		
		driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
		writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
		Thread.sleep(3000);
			
		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		
		//**********************************************************************
		//Delete At Confirm Vehicle in Workshop:
		DriverWait("//*[contains(@name,'Confirm Workshop with Fleet Manager')]");
		//Delete At Assigned Order to Workshop:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(5000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "  is selected::::Passed");
				Thread.sleep(5000);
				break;
				
			}
		
		}
		
		
		Thread.sleep(8000);
		
		//Verifying the Order is displayed:
		//Clicking FM: 
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		
		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults=getresultsMatchText.getText();				
		System.out.println("getNoResults::"+getNoResults);
		Thread.sleep(1000);
		if(getNoResults.startsWith("No results match"))
		{	System.out.println("Order is Successfully Cancelled::::Passed");
			writeReport(LogStatus.PASS, "Order is Successfully Cancelled::::Passed");
		}
	
	
	}
	
	public void DeleteBDOrderAtConfirmVehicle(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("DeleteBDOrderAtConfirmVehicle");
		List<WebElement> createorderList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());

		for (int i = 1; i < createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::" + i + ":::::" + createorderListtext);

			if (createorderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + createorderListtext + "  matches::::Passed");
			}
			if (createorderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + createorderListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + createorderListtext + "  matches::::Passed");
			}

			if (createorderListtext.equalsIgnoreCase("Input Location")) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
				// This will scroll the page till the element is found
				js.executeScript("arguments[0].scrollIntoView();", Element);
				Element.click();
				writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
				break;
			}
		}

		// Entering Location in Map:
		DriverWait("//*[@id='pac-input']");
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
		// Double click
		action.doubleClick(location).perform();
		location.clear();
		location.sendKeys(excelInputData.getLocation());
		Thread.sleep(1000);
		location.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		// Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000);
		bdElement.click();
		writeReport(LogStatus.PASS, "Created BD order::::Passed");

		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form");
		Thread.sleep(2000);
		List<WebElement> RequestOrderDetailsList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/form/*/*"));
		System.out.println("RequestOrderDetailsList size :::" + RequestOrderDetailsList.size());

		for (int i = 1; i <= RequestOrderDetailsList.size(); i++) {
			String ReqOrderDetailsListtext = RequestOrderDetailsList.get(i).getText();
			System.out.println("RequestOrderDetailsList:::" + i + ":::::" + ReqOrderDetailsListtext);
			if (ReqOrderDetailsListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "FMNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getDriverNameNumber())) {
				System.out.println("DriverNameNumber::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "DriverNameNumber::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "VehicleRegNo::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if (ReqOrderDetailsListtext.contains("Inspection Cost")) {
				System.out.println("Inspection Cost::::" + ReqOrderDetailsListtext + "Passed");
				writeReport(LogStatus.PASS, "Inspection Cost::::" + ReqOrderDetailsListtext + "  matches::::Passed");
			}
			if(ReqOrderDetailsListtext.equalsIgnoreCase("Confirm Order")) {
			Thread.sleep(1000);
			DriverWait("//button[@class='btn btn-default']");
			driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
			Thread.sleep(7000);
			break;
			}	
		}
		
		//Upcoming Orders
		Thread.sleep(6000);
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		//Clicking FM: 
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000); 
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);			
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);  
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		
		driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
		writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
		Thread.sleep(3000);
			
		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);
		
		//**********************************************************************
		//Delete At Confirm Vehicle in Workshop:
		DriverWait("//*[contains(@name,'Confirm Vehicle in Workshop')]");
			
		//Delete At Assigned Order to Workshop:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(10000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "   is selected::::Passed");
				Thread.sleep(5000);
				break;
				
			}
		
		}
		
		
		Thread.sleep(8000);
		
		//Verifying the Order is displayed:
		//Clicking FM: 
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		
		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults=getresultsMatchText.getText();				
		System.out.println("getNoResults::"+getNoResults);
		Thread.sleep(1000);
		if(getNoResults.startsWith("No results match"))
		{	System.out.println("Order is Successfully Cancelled::::Passed");
			writeReport(LogStatus.PASS, "Order is Successfully Cancelled::::Passed");
		}
		
	}
		
	public void selectWorkshop(ExcelInputData excelInputData) throws InterruptedException {
	
		Thread.sleep(1000);
		List<WebElement> GetRequestOrderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div[2]/div/*"));
		Thread.sleep(1000);
		System.out.println("GetRequestOrderList::" + GetRequestOrderList.size());
		for (int i = 1; i <= GetRequestOrderList.size(); i++) {
			String GetRequestOrderListtext = GetRequestOrderList.get(i).getText();
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetRequestOrderListtext);
			if (GetRequestOrderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetRequestOrderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
				break;
			}

		}

		// Verifying table details:
		List<WebElement> GetTableDetailsList = driver.findElements(By.xpath("//*[@id='jobTableBody']/*"));
		System.out.println("GetTableDetailsList::" + GetTableDetailsList.size());
		for (int i = 0; i <= GetTableDetailsList.size(); i++) {
			String GetTableDetailsListtext = GetTableDetailsList.get(i).getText();
			System.out.println("GetTableDetailsList:::" + i + ":::::" + GetTableDetailsListtext);
			if (i == GetTableDetailsList.size() - 1) {
				break;
			}
		}
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
		Thread.sleep(4000);

		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop with FM::::Passed");
		Thread.sleep(3000);

		// Confirm Vehicle with Technician:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(15000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
		writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop::::Passed");
		Thread.sleep(8000);
		// }

		// Verifying the Order is displayed:
		// Clicking FM:

		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText = driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults = getresultsMatchText.getText();
		System.out.println("getNoResults::" + getNoResults);
		Thread.sleep(1000);
		if (getNoResults.startsWith("No results match")) {
			System.out.println("Order Successfully moved from Hunter to Farmer::::Passed");
			writeReport(LogStatus.PASS, "Order Successfully moved from Hunter to Farmer::::Passed");
		}
		
	}

	public void AddNotes(ExcelInputData excelInputData, String OrderID) throws InterruptedException {
		
		System.out.println("Add Notes");
		driver.findElement(By.linkText("Add")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='notes']")).click();
		System.out.println("Add Notes text:::"+excelInputData.getAddNotes());
		driver.findElement(By.xpath("//*[@id='notes']")).sendKeys(excelInputData.getAddNotes());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='addNotesForm']/div[2]/button[2]")).click();
		Thread.sleep(10000);
		
				
		//Verifying the Added Notes:
		
		DriverWait("//*[@id='"+OrderID+"lastNoteVal']");
		System.out.println("Order id:::"+OrderID);
		String GetAddedNotes=driver.findElement(By.xpath("//div[@id='"+OrderID+"lastNoteVal']")).getText();
		System.out.println("GetAddedNotes::"+GetAddedNotes);
		writeReport(LogStatus.PASS, "Notes Added:::::"+GetAddedNotes+":::Passed");
		
	}
		
	public void VerifyDetailsinResources(ExcelInputData excelInputData, String OrderID) throws InterruptedException{
			
		System.out.println("ReqOrderID::"+OrderID);
		System.out.println("VerifyDetailsinResources");
		driver.findElement(By.xpath("//*[@id='layout_4']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Resource menu clicked:::Passed");
		Thread.sleep(5000);
		DriverWait("//a[@href='#orderhistorytab']");
		driver.findElement(By.xpath("//a[@href='#orderhistorytab']")).click();
		Thread.sleep(3000);
		writeReport(LogStatus.PASS, "Order History tab clicked:::Passed");
		WebElement OrderID1=driver.findElement(By.xpath("//*[@id='_Hunter_resources_orderId']"));
		OrderID1.click();
		OrderID1.sendKeys(OrderID);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		writeReport(LogStatus.PASS, "Searched with Order ID::::"+OrderID+":::Passed");
		Thread.sleep(10000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		DriverWait("//*[@id='orderHistory']/div[4]/table");
		List<WebElement> tableDetails=driver.findElements(By.xpath("//*[@id='orderHistory']/div[4]/table/tbody/tr/*"));
		System.out.println("tableDetails.size():::"+tableDetails.size());
				
		for(int i=0;i<=tableDetails.size();i++) {
			String OrderTableText=tableDetails.get(i).getText();
			System.out.println("OrderTableText::"+OrderTableText+"I value:::"+i);
			if(OrderTableText.equals(OrderID))
			{System.out.println("Order Id::"+OrderTableText+":::matches:::Passed");
			 writeReport(LogStatus.PASS, "Order Id::"+OrderTableText+":::matches:::Passed");
			}
			if(OrderTableText.contains(excelInputData.getVehicleRegNo()))
			{System.out.println("Vehicle RegisterationNo::"+OrderTableText+":::matches:::Passed");
			 writeReport(LogStatus.PASS, "VehicleRegisterationNo::"+OrderTableText+":::matches:::Passed");
			}	
			if(OrderTableText.contains("ORDER_ASSIGNED"))
			{System.out.println("Order State::"+OrderTableText+":::matches:::Passed");
			 writeReport(LogStatus.PASS, "Order State::"+OrderTableText+":::matches:::Passed");
			 break;
			}	
		}
}
	
	public void WorkshopLocator(ExcelInputData excelInputData, String OrderID) throws InterruptedException, IOException{
		
		System.out.println("ReqOrderID::"+OrderID);
		System.out.println("WorkshopLocatorVerification");
		driver.findElement(By.xpath("//*[@id='layout_4']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Resource menu clicked:::Passed");
		Thread.sleep(5000);
		DriverWait("//a[@href='#garagelocatortab']");
		driver.findElement(By.xpath("//a[@href='#garagelocatortab']")).click();
		writeReport(LogStatus.PASS, "Workshop Locator tab clicked:::Passed");
		Thread.sleep(3000);
		WebElement searchLocation=driver.findElement(By.xpath("//*[@id='txtPlaces']"));
		searchLocation.click();
		Thread.sleep(1000);
		searchLocation.sendKeys(excelInputData.getResLocation());
		Thread.sleep(2000);
		writeReport(LogStatus.PASS, "Searched with Location::::"+excelInputData.getResLocation()+":::Passed");
		searchLocation.sendKeys(Keys.TAB);
		//Enter PinCodeNo:
		WebElement searchPinCode=driver.findElement(By.xpath("//*[@id='resourcePincode']"));
		searchPinCode.click();
		Thread.sleep(1000);
		searchPinCode.sendKeys(excelInputData.getResPinCodeNo());
		writeReport(LogStatus.PASS, "Searched with Pincode::::"+excelInputData.getResPinCodeNo()+":::Passed");
		driver.findElement(By.xpath("//*[@id='select-fleet']/div/div/div/span/button")).click(); //click search icon
			
		Thread.sleep(25000);
		DriverWait("//*[@id='garageInfo']");
		List<WebElement> garageList=driver.findElements(By.xpath("//*[@id='garageInfo']/*"));
		System.out.println("garageList.size():::"+garageList.size());
		String WorkshopName=excelInputData.getResWorkshopName()+ " Online";
		while (garageList.contains("HTL AT Garage Online"))
		{
			System.out.println("Inside garage if"+excelInputData.getResWorkshopName());
			writeReport(LogStatus.PASS,"Garage:::"+WorkshopName+" is Found in Workshop Locator:::::Passed");
			break;
		}
		System.out.println("Garage:::"+WorkshopName+"  is not Found in Workshop Locator:::::Failed");
		writeReport(LogStatus.FAIL,"Garage:::"+WorkshopName+"  is not Found in Workshop Locator:::::Failed");
		TakeSnapshot();
//		for(int m=0;m<garageList.size();m++) {
//			String GarageText=garageList.get(m).getText();
//			System.out.println("m:::::"+m+"     Garage Text:::"+GarageText);
//			String WorkshopName=excelInputData.getWorkshopName()+ " Online";
//			System.out.println("WorkshopName::::"+WorkshopName);
//			while(GarageText.equalsIgnoreCase(WorkshopName))
//			{
//			System.out.println("Inside garage if"+excelInputData.getWorkshopName());
//			writeReport(LogStatus.PASS,"Garage:::"+WorkshopName+"is Found in Workshop Locator:::::Passed");
//			break;
//			}
//			
//			
//		}
	}
	
	public void PriceEstimator(ExcelInputData excelInputData, String OrderID) throws InterruptedException{
				
			System.out.println("ReqOrderID::"+OrderID);
			System.out.println("PriceEstimator");
			driver.findElement(By.xpath("//*[@id='layout_4']/a/span[1]")).click();
			writeReport(LogStatus.PASS, "Resource menu clicked:::Passed");
			Thread.sleep(5000);
			DriverWait("//a[@href='#priceestimatortab']");
			driver.findElement(By.xpath("//a[@href='#priceestimatortab']")).click();
			writeReport(LogStatus.PASS, "Price Estimator tab clicked:::Passed");
			Thread.sleep(3000);
			//Select Make:
			Select selectMake=new Select(driver.findElement(By.xpath("//*[@id='makeId']")));
			Thread.sleep(1000);
			selectMake.selectByVisibleText(excelInputData.getResVehicleMake());
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "MAKE selected::::"+excelInputData.getResVehicleMake()+"::::Passed");
			//Select Model:
			Select selectModel=new Select(driver.findElement(By.xpath("//*[@id='modelId']")));
			selectModel.selectByVisibleText(excelInputData.getResVehicleLoadRange());
			writeReport(LogStatus.PASS, "MODEL selected::::"+excelInputData.getResVehicleLoadRange()+"::::Passed");
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='selectSubmit']")).click();
			Thread.sleep(10000);
			
			
	}

	public void SRSelectWorkshopDelAtConfirmOrder(ExcelInputData excelInputData) throws InterruptedException 
	{
		try {
		Thread.sleep(1000);
		List<WebElement> GetRequestOrderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div[2]/div/*"));
		Thread.sleep(1000);
		System.out.println("GetRequestOrderList::" + GetRequestOrderList.size());
		for (int i = 1; i <= GetRequestOrderList.size(); i++) {
			String GetRequestOrderListtext = GetRequestOrderList.get(i).getText();
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetRequestOrderListtext);
			if (GetRequestOrderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetRequestOrderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
				
			}
			if(GetRequestOrderListtext.equalsIgnoreCase("Select Workshop"))
			{
				DriverWait("//button[@class='btn btn-default']");
				driver.findElement(By.xpath("//button[@class='btn btn-default']")).click(); //Select Workshop
				Thread.sleep(10000);
				break;
			}

		}
		/*// Verifying table details:
		List<WebElement> GetTableDetailsList = driver.findElements(By.xpath("//*[@id='jobTableBody']/*"));
		System.out.println("GetTableDetailsList::" + GetTableDetailsList.size());
		for (int i = 0; i <= GetTableDetailsList.size(); i++) {
			String GetTableDetailsListtext = GetTableDetailsList.get(i).getText();
			System.out.println("GetTableDetailsList:::" + i + ":::::" + GetTableDetailsListtext);
			if (i == GetTableDetailsList.size() - 1) {
				break;
			}
		}
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
		Thread.sleep(4000);*/

		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		DriverWait("//*[contains(@name,'Confirm Workshop with Fleet Manager')]");
		
		
		//Delete:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(18000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "   is selected::::Passed");
				Thread.sleep(9000);
				break;
				
			}
		}}catch(Exception e) {	
			e.printStackTrace();
		}
	}
	
	public void SRSelectWorkshopDelAtConfirmVehicle(ExcelInputData excelInputData) throws InterruptedException 
	{
		
		Thread.sleep(1000);
		
		List<WebElement> GetRequestOrderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div[2]/div/*"));
		Thread.sleep(1000);
		System.out.println("GetRequestOrderList::" + GetRequestOrderList.size());
		for (int i = 1; i <= GetRequestOrderList.size(); i++) {
			String GetRequestOrderListtext = GetRequestOrderList.get(i).getText();
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetRequestOrderListtext);
			if (GetRequestOrderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetRequestOrderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
				
			}
			if(GetRequestOrderListtext.equalsIgnoreCase("Select Workshop"))
			{
				DriverWait("//button[@class='btn btn-default']");
				driver.findElement(By.xpath("//button[@class='btn btn-default']")).click(); //Select Workshop
				Thread.sleep(10000);
				break;
			}
		}

		/*// Verifying table details:
		List<WebElement> GetTableDetailsList = driver.findElements(By.xpath("//*[@id='jobTableBody']/*"));
		System.out.println("GetTableDetailsList::" + GetTableDetailsList.size());
		for (int i = 0; i <= GetTableDetailsList.size(); i++) {
			String GetTableDetailsListtext = GetTableDetailsList.get(i).getText();
			System.out.println("GetTableDetailsList:::" + i + ":::::" + GetTableDetailsListtext);
			if (i == GetTableDetailsList.size() - 1) {
				break;
			}
		}
		Thread.sleep(10000);*/
		

		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();//Confirm Workshop
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		//Confirm Order with FleetManager:
		DriverWait("//*[contains(@name,'Confirm Workshop with Fleet Manager')]");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		Thread.sleep(20000);
		//Delete At Confirm vehicle in Workshop:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(18000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "  is selected::::Passed");
				Thread.sleep(9000);
				break;
				
			}
		}
	}

	public String SRSelectWorkshopDelAtConfirmVehicleinWorkshp(ExcelInputData excelInputData) throws InterruptedException 
	{
		String RequestOrder = "";
			Thread.sleep(1000);
		
		List<WebElement> GetRequestOrderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div[2]/div/*"));
		Thread.sleep(1000);
		System.out.println("GetRequestOrderList::" + GetRequestOrderList.size());
		for (int i = 0; i <= GetRequestOrderList.size(); i++) {
			String GetRequestOrderListtext = GetRequestOrderList.get(i).getText();
			if(i==0){
				RequestOrder = GetRequestOrderList.get(i).getText().replaceAll("[^0-9]","");
				System.out.println("%%%%%%%%%%%% RequestOrder%%%%%%%%%%%%%:::"+RequestOrder);
			}
			
			
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetRequestOrderListtext);
			if (GetRequestOrderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetRequestOrderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
				
			}
			if(GetRequestOrderListtext.equalsIgnoreCase("Select Workshop"))
			{
				DriverWait("//button[@class='btn btn-default']");
				driver.findElement(By.xpath("//button[@class='btn btn-default']")).click(); //Select Workshop
				Thread.sleep(10000);
				break;
			}

		}

		/*// Verifying table details:
		List<WebElement> GetTableDetailsList = driver.findElements(By.xpath("//*[@id='jobTableBody']/*"));
		System.out.println("GetTableDetailsList::" + GetTableDetailsList.size());
		for (int i = 0; i <= GetTableDetailsList.size(); i++) {
			String GetTableDetailsListtext = GetTableDetailsList.get(i).getText();
			System.out.println("GetTableDetailsList:::" + i + ":::::" + GetTableDetailsListtext);
			if (i == GetTableDetailsList.size() - 1) {
				break;
			}
		}
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='btn btn-default']")).click(); //Select Workshop
		Thread.sleep(4000);*/

		// ConfirmWorkshop:
		// -----------------
		DriverWait("//*[@id='g_form_submit']/div[1]/*");
		Thread.sleep(3000);
		List<WebElement> ConfirmWorkshopList = driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
		System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());

		for (int i = 1; i <= ConfirmWorkshopList.size(); i++) {
			String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::" + i + ":::::" + ConfirmWorkshopListtext);

			if (ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName())) {
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();

				break;
			} else {
				System.out.println("Workshop doesnt match");
			}
		}
		// Clicking Confirm Garage button:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();//Confirm Workshop
		writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
		Thread.sleep(5000);

		// Confirm Workshop with FM:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(15000);

		/*
		 * String
		 * GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		 * Thread.sleep(2000); if(GetVehicleNumber.contains("TN12341")) {
		 */
		// System.out.println("Vehicle Number matches");
		//Confirm Order with FleetManager:
		DriverWait("//*[contains(@name,'Confirm Workshop with Fleet Manager')]");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		
		
		//Delete At Confirm Vehicle in Workshop:
		Thread.sleep(10000);
		DriverWait("//*[contains(@name,'Confirm Vehicle in Workshop')]");
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(5000);
		//Cancel Reason:
		Thread.sleep(15000);
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "  is selected::::Passed");
				Thread.sleep(10000);
				break;
				
			}
				
		}
				
		
		Thread.sleep(8000);
		
		//Verifying the Order is displayed:
		//Clicking FM: 
		DriverWait("//*[@id='layout_3']/a/span[1]");
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		
		System.out.println("getNoResults::");
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(2000);
		WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults=getresultsMatchText.getText();				
		System.out.println("getNoResults::"+getNoResults);
		Thread.sleep(1000);
		if(getNoResults.startsWith("No results match"))
		{	System.out.println("Order is Successfully Cancelled::::Passed");
			writeReport(LogStatus.PASS, "Order is Successfully Cancelled::::Passed");
		}
		return RequestOrder;				
		
	}
	
	public void SRSelectWorkshopDelAtAssignOrder(ExcelInputData excelInputData) throws InterruptedException 
	{
		
		Thread.sleep(1000);
		List<WebElement> GetRequestOrderList = driver
				.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div[2]/div/*"));
		Thread.sleep(1000);
		System.out.println("GetRequestOrderList::" + GetRequestOrderList.size());
		for (int i = 1; i <= GetRequestOrderList.size(); i++) {
			String GetRequestOrderListtext = GetRequestOrderList.get(i).getText();
			System.out.println("GetVehicleNoListtext:::" + i + ":::::" + GetRequestOrderListtext);
			if (GetRequestOrderListtext.contains(excelInputData.getFMNameNumber())) {
				System.out.println("FMNameNumber::::" + GetRequestOrderListtext + "    Passed");
				// writeReport(LogStatus.PASS, "FMNameNumber::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
			}
			if (GetRequestOrderListtext.contains(excelInputData.getVehicleRegNo())) {
				System.out.println("VehicleRegNo::::" + GetRequestOrderListtext + "    Passed");
				break;
				// writeReport(LogStatus.PASS, "VehicleRegNo::::"+GetVehicleNoListtext+ "
				// matches::::Passed");
				
			}
		}
		//Upcoming Orders:
		//---------------
		//Clicking Upcoming Orders:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		Thread.sleep(8000);			

		// Assign Order to Workshop:
		// Clicking FM:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getFMNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input"))
				.sendKeys(excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(10000);

	
		DriverWait("//*[contains(@name,'Assign Order to Workshop')]");
			
		//Delete:
		DriverWait("//i[@class='fa fa-trash-o']");
		driver.findElement(By.xpath("//i[@class='fa fa-trash-o']")).click();
		Thread.sleep(2000);
		
		if(driver.findElement(By.xpath("//*[@id='deleteForFM']")).isSelected()) {
		System.out.println("FM is already selected");
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));
		element.click();
		Thread.sleep(5000);
		}
		else {		
		driver.findElement(By.xpath("//*[@id='deleteForFM']")).click();}
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//*[@id='myModal_delete_for']/div/div/div[2]/div[3]/button[2]"));//"//button[text()='Submit']"));
		element.click();
		Thread.sleep(18000);
		//Cancel Reason:
		DriverWait("//*[@id='cancellationReasons']");
		List<WebElement> CancelReasonList =driver.findElements(By.xpath("//*[@id='cancellationReasons']/*"));
		for (int z = 1; z <= CancelReasonList.size(); z++) {
			System.out.println("CancelReasonList:::"+CancelReasonList.size());
		
			String CancelReasonListtext = CancelReasonList.get(z).getText();
			System.out.println("CancelReasonListtext:::" + z + ":::::" + CancelReasonListtext);
			System.out.println("excelInputData.getCancelReason():::;"+excelInputData.getCancelReason());
			if (z == Integer.parseInt(excelInputData.getCancelReason())) {
				System.out.println("CancelReasonListtext::"+CancelReasonListtext);
				driver.findElement(By.xpath("//*[@id='cancellationReasons']/div["+z+"]/div/label/input")).click();
				//Submit button:
				driver.findElement(By.xpath("//*[@id='deleteForm']/div[2]/button[2]")).click();
				writeReport(LogStatus.PASS, "Cancel Reason::"+CancelReasonListtext+ "   is selected::::Passed");
				Thread.sleep(9000);
				break;
				
			}
		}
		
	
	}
		
	public void PendingActions(ExcelInputData excelInputData) throws InterruptedException{
		
		System.out.println(excelInputData.getOrderID());
		String OID=excelInputData.getOrderID();
		System.out.println("Order ID::::"+OID);
		//Pending actions:
		//---------------
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id='layout_2']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Pending Actions");
		Thread.sleep(5000);			

		//Clicking OrderId:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		WebElement OrderId=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
		OrderId.click();
		Thread.sleep(1000);
		//String OID=excelInputData.getOrderId();
		System.out.println("Order ID::::"+OID);
		OrderId.sendKeys(OID);
		OrderId.sendKeys(Keys.ENTER);
		Thread.sleep(500);
		try{
		if(driver.findElement(By.xpath("//li[@class='no-results']")).isDisplayed())
		{
		  	System.out.println("Order ID doesnt exists in Pending Actions");
		  	writeReport(LogStatus.FAIL, "Order ID doesnt exists in Pending Actions");
		  	TakeSnapshot();
		  	//Click Upcoming orders:
		  	Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
			writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
			Thread.sleep(8000);
			
			//Search Order ID:
			// Clicking OrderId:
			DriverWait("//*[@id='orderSearchId_chosen']/a/span");
			driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
			Thread.sleep(1000);
			WebElement Order=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
			Order.click();
			Thread.sleep(1000);
			String OrdrID=excelInputData.getOrderID();
			System.out.println("Order ID::::"+OrdrID);
			Order.sendKeys(OrdrID);
			Order.sendKeys(Keys.ENTER);
			Thread.sleep(500);
			try {
					if(driver.findElement(By.xpath("//*[@id='searchSubmit']")).isDisplayed())
					{
						//Search result:
						if(driver.findElement(By.xpath("//*[@id='"+OrdrID+"pendingOrderParentDiv']")).isDisplayed()) {
						DriverWait("//*[@id='"+OrdrID+"pendingOrderParentDiv']");
						List<WebElement> OrderDetails1 =driver.findElements(By.xpath("//*[@id='"+OrdrID+"pendingOrderParentDiv']/*"));
						System.out.println("OrderDetails.size()"+OrderDetails1.size());
						/*for(int i=0;i<=OrderDetails.size();i++) {
							System.out.println("OrderDetails::::"+i+"::::"+OrderDetails.get(i).getText());
							if()
							}
						Thread.sleep(2000);*/
						String OrderState1=driver.findElement(By.xpath("//button[@class='btn btn-order']")).getText();
						System.out.println("OrderState:::"+OrderState1);
						Thread.sleep(2000);
						if(OrderState1.equalsIgnoreCase("Confirm Workshop with Fleet Manager"))
						{
							driver.findElement(By.cssSelector("button[@name='Confirm Workshop with Fleet Manager'][type='button'])")).click();
							ConfirmVehicleWithTechnician(Utils.ExcelInputDataList.get(1));
						
						}
						if(OrderState1.equalsIgnoreCase("Assign Order to Workshop"))
						{
							//driver.findElement(By.xpath("button[@name='Assign Order to Workshop'])")).click();
							Thread.sleep(1000);
							driver.findElement(By.cssSelector("button[name='Assign Order to Workshop'][type='button']")).click();
							AssignOrderConfirmWorkshop(Utils.ExcelInputDataList.get(1));
							ConfirmVehicleWithTechnician(Utils.ExcelInputDataList.get(1));
						
						}
					}}else{
						if(driver.findElement(By.xpath("//*[@id='p_p_id_Hunter_upcoming_orders_']/div/div/div/div[4]/div[2]/text()")).isDisplayed())
						{
							System.out.println("Order ID doesnt exists in Upcoming Orders");
							writeReport(LogStatus.FAIL, "Order ID doesnt exists in Upcoming Orders");
							TakeSnapshot();
						}
					}
					}catch(Exception e){
					if(driver.findElement(By.xpath("//li[@class='no-results']")).isDisplayed())
					{
					  	System.out.println("Order ID doesnt exists in Upcoming Orders");
					  	writeReport(LogStatus.FAIL, "Order ID doesnt exists in Upcoming Orders");
					  	TakeSnapshot();
					}
		}
		}}catch(Exception e)
		{
			System.out.println("Inside Catch Block");
			if(driver.findElement(By.xpath("//*[@id='searchSubmit']")).isDisplayed()) { //main if
				
			driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
			Thread.sleep(2000);

			//Search result:
			DriverWait("//*[@id='"+OID+"pendingOrderParentDiv']");
			
			}
			Thread.sleep(2000);
			
			String OrderState=driver.findElement(By.xpath("//button[@class='btn btn-order']")).getText();
			System.out.println("OrderState:::"+OrderState);
			Thread.sleep(1000);
			if(OrderState.equalsIgnoreCase("Confirm Workshop with Fleet Manager"))
			{		
				Thread.sleep(1000);
				driver.findElement(By.xpath("//button[@class='btn btn-order']")).click();
				Thread.sleep(10000);
				ConfirmVehicleWithTechnician(Utils.ExcelInputDataList.get(1));
			
			}
			if(OrderState.equalsIgnoreCase("Confirm Order With Fleet Manager"))
			{
				//driver.findElement(By.xpath("//button(@name='Assign Order to Workshop'))")).click();
				Thread.sleep(1000);
				List<WebElement> OrderDetails =driver.findElements(By.xpath("//*[@id='"+OID+"pendingOrderParentDiv']/*/*"));
				System.out.println("OrderDetails.size()"+OrderDetails.size());
				for(int i=0;i<=OrderDetails.size();i++) {
				System.out.println("OrderDetails::::"+i+"::::"+OrderDetails.get(i).getText());
							
				if(OrderDetails.get(i).getText().contains("Breakdown")){
					driver.findElement(By.cssSelector("button[name='Confirm Order With Fleet Manager'][type='button']")).click();
					Thread.sleep(10000);
					BDJobSelection(excelInputData);
					break;
				}//BD button click ends here-----
				else if(OrderDetails.get(i).getText().contains("Service & Repair")){//SR Job Slection
					driver.findElement(By.cssSelector("button[name='Confirm Order With Fleet Manager'][type='button']")).click();
					Thread.sleep(10000);
					SRJobSelection(excelInputData);
					break;
				}
				}
				Thread.sleep(5000);
				AssignOrderToWorkshop(Utils.ExcelInputDataList.get(1));
				AssignOrderConfirmWorkshop(Utils.ExcelInputDataList.get(1));
				ConfirmVehicleWithTechnician(Utils.ExcelInputDataList.get(1));
			
			}
			if(OrderState.equalsIgnoreCase("Assign Order to Workshop"))
			{
				//driver.findElement(By.xpath("//button(@name='Assign Order to Workshop'))")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("button[name='Assign Order to Workshop'][type='button']")).click();
				driver.findElement(By.xpath("//button[@class='btn btn-order']")).click();
				Thread.sleep(10000);
				AssignOrderConfirmWorkshop(Utils.ExcelInputDataList.get(1));
				ConfirmVehicleWithTechnician(Utils.ExcelInputDataList.get(1));
			
			}
			if(OrderState.equalsIgnoreCase("Confirm Vehicle in Workshop"))
			{
				//driver.findElement(By.xpath("//button[@name='Assign Order to Workshop'])")).click();
				driver.findElement(By.xpath("//button[@class='btn btn-order']")).click();
				Thread.sleep(15000);
				driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				WebElement OrderId1=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
				OrderId1.click();
				Thread.sleep(1000);
				String OrdID=excelInputData.getOrderID();
				System.out.println("Order ID::::"+OrdID);
				OrderId1.sendKeys(OrdID);
				Thread.sleep(2000);
				if(driver.findElement(By.xpath("//li[@class='no-results']")).isDisplayed())
				{
				  System.out.println("Order ID::"+OrdID+"is Confirmed in Workshop");
				  writeReport(LogStatus.PASS, "Order ID::"+OrdID+"is Confirmed in Workshop");
				}
			}
		}
	}
			
	
	//Upcoming orders:
	
	public void ConfirmVehicleWithTechnician(ExcelInputData excelInputData) throws InterruptedException{
		/*//Clicking Upcoming Orders:
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
		writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
		Thread.sleep(6000);*/
		//Confirm Vehicle with Technician:
		//Clicking OrderID:
		Thread.sleep(18000);
		DriverWait("//*[@id='orderSearchId_chosen']/a/span");
		
		driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		WebElement OrderId1=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
		OrderId1.click();
		Thread.sleep(1000);
		String OrdID=excelInputData.getOrderID();
		System.out.println("Order ID::::"+OrdID);
		OrderId1.sendKeys(OrdID);
		OrderId1.sendKeys(Keys.ENTER);
		Thread.sleep(1000);		
		
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(4000);
		
		/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
		Thread.sleep(2000);
		if(GetVehicleNumber.contains("TN12341"))
		{*/
			//System.out.println("Vehicle Number matches");
			driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
			writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop");
			Thread.sleep(15000);
		//}
			
	//Verifying the Order is displayed:
		//Clicking FM: 
		System.out.println("getNoResults::");
		DriverWait("//*[@id='orderSearchId_chosen']/a/span");
		
		driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		WebElement OrderId2=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
		OrderId2.click();
		Thread.sleep(1000);
		String Ord=excelInputData.getOrderID();
		System.out.println("Order ID::::"+Ord);
		OrderId2.sendKeys(Ord);
		OrderId2.sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(5000);
		
		WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
		String getNoResults=getresultsMatchText.getText();				
		System.out.println("getNoResults::"+getNoResults);
		Thread.sleep(1000);
		if(getNoResults.startsWith("No results match"))
		{	System.out.println("Order is moved from HUNTER to FARMER::::Passed");
			writeReport(LogStatus.PASS, "Order is moved from HUNTER to FARMER::::Passed");
		}	
		
	
	}
		
	public void AssignOrderConfirmWorkshop(ExcelInputData excelInputData) throws InterruptedException{
	//ConfirmWorkshop:
	//-----------------
	Thread.sleep(5000);
	System.out.println("After Assign Order ConfirmWorkshop::");
	//Edit Workshop:
	// Entering Location in Map:
			DriverWait("//*[@id='pac-input']");
			Thread.sleep(1000);
			Actions action = new Actions(driver);
			WebElement location = driver.findElement(By.xpath("//*[@id='pac-input']"));
			// Double click
			action.doubleClick(location).perform();
			location.clear();
			location.sendKeys(excelInputData.getLocation());
			Thread.sleep(4000);
			location.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='confirmlocation']")).click();
			Thread.sleep(18000);
		
			DriverWait("//*[@id='g_form_submit']/div[1]/*");
			List<WebElement> ConfirmWorkshopList =driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
			System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());
			
			for(int i = 1; i <= ConfirmWorkshopList.size(); i++) {
				String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
				System.out.println("ConfirmWorkshopList:::"+i+":::::"+ConfirmWorkshopListtext);
			
			if(ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName()))
			{
				System.out.println("Workshop matches");
				ConfirmWorkshopList.get(i).click();
				writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
				break;
			}
			else {
				System.out.println("Workshop doesnt match");}
				}
			//Clicking Confirm Garage button:
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
			Thread.sleep(5000);
	

	//Confirm Workshop with FM:
	//Clicking Upcoming Orders:
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
	writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
	Thread.sleep(6000);
	
	DriverWait("//*[@id='orderSearchId_chosen']/a/span");
	driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
	Thread.sleep(1000);
	WebElement OrderId1=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
	OrderId1.click();
	Thread.sleep(1000);
	String OrdID=excelInputData.getOrderID();
	System.out.println("Order ID::::"+OrdID);
	OrderId1.sendKeys(OrdID);
	OrderId1.sendKeys(Keys.ENTER);
	Thread.sleep(1000);		
	
	driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
	Thread.sleep(9000);
	
	/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
	Thread.sleep(2000);
	if(GetVehicleNumber.contains("TN12341"))
	{*/
		//System.out.println("Vehicle Number matches");
		driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
		writeReport(LogStatus.PASS, "Confirm Workshop with Fleet Manager::::Passed");
		Thread.sleep(3000);
	//}
}

	public void AssignOrderToWorkshop(ExcelInputData excelInputData) throws InterruptedException{
	
		Thread.sleep(10000);
		//Clicking OrderId:
		DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
		driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/a/span")).click();
		Thread.sleep(1000);
		WebElement OrderId=driver.findElement(By.xpath("//*[@id='orderSearchId_chosen']/div/div/input"));
		OrderId.click();
		Thread.sleep(1000);
		String OID=excelInputData.getOrderID();
		System.out.println("Order ID::::"+OID);
		OrderId.sendKeys(OID);
		OrderId.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
		Thread.sleep(9000);
		
		driver.findElement(By.xpath("//button[@name='Assign Order to Workshop'][type='button'])")).click();
		Thread.sleep(12000);
		writeReport(LogStatus.PASS, "Order ID::"+OID+"Assigned Order to Workshop");
	}
}

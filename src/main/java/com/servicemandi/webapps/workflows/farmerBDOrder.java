package com.servicemandi.webapps.workflows;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.webapps.utils.ExcelInputData;
import com.servicemandi.webapps.utils.FarmerExcelInputData;

import io.appium.java_client.android.AndroidKeyCode;

public class farmerBDOrder extends WebConfiguration {

	public farmerBDOrder() {

	}

	public void searchOrderID(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside searchOrderID");
		Thread.sleep(15000);
		DriverWait("//*[@id='order']");
		driver.findElement(By.xpath("//*[@id='order']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='order']")).clear();
		driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='order']")).sendKeys(Keys.TAB);	
		//driver.findElement(By.xpath("//*[@id='style-1']/li")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();
		Thread.sleep(15000);
		//Thread.sleep(6000);
		
	}
	
	public void farmerLogin(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		
		farmerWebLaunch();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"username\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys(farmerExcelInputData.getFarmerMailID());
		
		driver.findElement(By.xpath("//*[@id=\"pass\"]")).click();
		WebElement password = driver.findElement(By.xpath("//*[@id=\"pass\"]"));
  		password.sendKeys(farmerExcelInputData.getPassword());
		Thread.sleep(2000);
//		password.sendKeys(Keys.TAB);
//		password.sendKeys(Keys.ENTER);
		//Login:
		driver.findElement(By.xpath("/html/body/app-root/app-login/div[2]/div/div/div/button")).click();
		Thread.sleep(18000);
		DriverWait("//*[@id='order']");
		System.out.println("Farmer App Launched and Logged in as::::"+farmerExcelInputData.getFarmerMailID()+"::Passed");
		writeReport(LogStatus.PASS, "Farmer App Launched and Logged in as::::"+farmerExcelInputData.getFarmerMailID()+"::Passed");
		writeReport(LogStatus.PASS,"Order ID::::"+farmerExcelInputData.getOrderID());
		
	}
		
	public void BDAddJobsWithParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside AddJobsWithPartsTest");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(5000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in test Add Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Add Jobs State");
			AddJobsWithParts(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(10000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(5000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Add Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Add Jobs");
				AddJobsWithParts(farmerExcelInputData);
			}
		}
		
	}	
	
	public void BDCancelJob(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside BDCancelJob");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(5000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Update Estimate");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Estimate State");
			CancelJob(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(10000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(5000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Estimate");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Estimate State");
				CancelJob(farmerExcelInputData);
			}
		}
		
	}	
	
	public void CancelJob(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside CancelJob");
			
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[2]/span/a/i")).click();//Cancel Job
		writeReport(LogStatus.PASS, "Cancel Job button clicked::::Passed");
		Thread.sleep(6000);
		
		//Delete as FleetManager:
		driver.findElement(By.xpath("//*[@id=\"orderdeleteRole\"]/div/div/div[3]/button[1]")).click();
		new WebDriverWait(driver, 4).until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		Thread.sleep(5000);
		//Cancel reason:
		List<WebElement> CancelList=driver.findElements(By.xpath("//*[@id=\"orderCancelReasons\"]/div/div/div[2]/div/*"));
		System.out.println("CancelList.size():::"+CancelList.size());
		
		for (int i = 0; i <= CancelList.size(); i++) {
			String CancelListText = CancelList.get(i).getText();
			System.out.println("CancelListText:::" + i + ":::::" + CancelListText);
			int getCancelReason=Integer.parseInt(farmerExcelInputData.getFRCancelReason());
			System.out.println("getCancelReason::"+getCancelReason);
			if(i==0) {
				driver.findElement(By.xpath("//*[@id='orderCancelReasons']/div/div/div[2]/div/label["+ (i + 1)+"]")).click();
				Thread.sleep(4000);
				System.out.println("Cancel Reason:::"+farmerExcelInputData.getFRCancelReason());
				writeReport(LogStatus.PASS, "Cancel Reason:::"+farmerExcelInputData.getFRCancelReason()+"::::Passed");
				break;
			}
			
		}
		//Confirm Deletion:
		driver.findElement(By.xpath("//*[@id=\"orderCancelReasons\"]/div/div/div[3]/button")).click();
		Thread.sleep(6000);		
		writeReport(LogStatus.PASS, "Confirm Deletion button clicked::::Passed");
		
	}
	
	public void BDAddJobsWithPartsNoRetail(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside BDAddJobsWithPartsNoRetail");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in test Add Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Add Jobs State");
			AddJobsWithPartsNoRetailer(farmerExcelInputData);
		
		}}catch(Exception e){
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Add Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Add Jobs");
				AddJobsWithPartsNoRetailer(farmerExcelInputData);
			}
		}
		
	}	
	
	public void AddJobsWithPartsNoRetailer(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
			
		System.out.println("Inside AddJobsWithPartsNoRetailer");
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();//Update Estimate
		writeReport(LogStatus.PASS, "Confirm Job Start button clicked::::Passed");
		Thread.sleep(5000);
		
		//Click AddJobs:
		Thread.sleep(8000);
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[1]/a")).click(); 
		DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div");
		Thread.sleep(10000);
		
			String expected = farmerExcelInputData.getFarmerJobs();	
			if(expected.contains(",")) {
			String[] outerArray = expected.split(",");
			int outerArrayLength = outerArray.length;
			System.out.println("OuterArrayLength:::" + outerArrayLength);
			int checkcount = 0;
			int count =0;
			for (String OuterArrayValues1 : outerArray){
				System.out.println(OuterArrayValues1);
				System.out.println("count:::"+count);
				
				List<WebElement> JobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr/td[1]/select/*"));
				System.out.println("JobList.size():::"+JobList.size());
				
				for (int i = 1; i <= JobList.size(); i++) {
					String JobListText = JobList.get(i).getText();
					System.out.println("JobListText:::" + i + ":::::" + JobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[1]/select"));
					SelJob.click();  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement LabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[2]/input"));
					LabourAmt.click();								
					LabourAmt.sendKeys(farmerExcelInputData.getFarmerLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
					
					WebElement PartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[3]/input"));
					PartsAmt.click();
					PartsAmt.sendKeys(farmerExcelInputData.getFarmerPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getFarmerPartsAmt() + "  entered::::Passed");
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
	
			//Add Retailer button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[4]/button[1]")).click();
			Thread.sleep(20000);
			DriverWait("//*[@id='myModal']");
			
			//I couldn find any Retailer:
			driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div[1]/label")).click();
			writeReport(LogStatus.PASS, "I couldn find any Retailer checkbox clicked:::::Passed");							
			}
			//Confirm Button:
			driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
			Thread.sleep(10000);
			System.out.println("Pending Action::::Passed");
		}

	public void AddJobsWithParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
						
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();//Update Estimate
		writeReport(LogStatus.PASS, "Update Estimate button clicked::::Passed");
		Thread.sleep(5000);
		
		//Click AddJobs:
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[1]/a")).click(); 
		DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div");
		Thread.sleep(10000);
		
			String expected = farmerExcelInputData.getFarmerJobs();	
			if(expected.contains(",")){
			String[] outerArray = expected.split(",");
			int outerArrayLength = outerArray.length;
			System.out.println("OuterArrayLength:::" + outerArrayLength);
			int checkcount = 0;
			int count =0;
			for (String OuterArrayValues1 : outerArray){
				System.out.println(OuterArrayValues1);
				System.out.println("count:::"+count);
				
				List<WebElement> JobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr/td[1]/select/*"));
				System.out.println("JobList.size():::"+JobList.size());
				
				for (int i = 1; i <= JobList.size(); i++) {
					String JobListText = JobList.get(i).getText();
					System.out.println("JobListText:::" + i + ":::::" + JobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[1]/select"));
					SelJob.click();  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement LabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[2]/input"));
					LabourAmt.click();								
					LabourAmt.sendKeys(farmerExcelInputData.getFarmerLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
					
					WebElement PartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[3]/input"));
					PartsAmt.click();
					PartsAmt.sendKeys(farmerExcelInputData.getFarmerPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getFarmerPartsAmt() + "  entered::::Passed");
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}
						
						}
					
					}
				}
			}
	
			//Add Retailer button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[4]/button[1]")).click();
			Thread.sleep(22000);
			DriverWait("//*[@id='myModal']");
			
			//Retailer:
			List<WebElement> RetailerList=driver.findElements(By.xpath("//*[@id='myModal']/*/*/*"));
			System.out.println("RetailerList.size():::"+RetailerList.size());
			for(int j=2;j<RetailerList.size();j++)
			{String RetailerListText = RetailerList.get(j).getText();
			System.out.println("RetailerListText:::" + j + ":::::" + RetailerListText);
			if(RetailerListText.contains(farmerExcelInputData.getRetailer()))
			{
			driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div["+(j-2)+"]/div[2]/label")).click();
			writeReport(LogStatus.PASS, "Retailer selected:::::Passed");
			break;  
			}
			
			}
			//Confirm Button:
			driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
			Thread.sleep(10000);
			System.out.println("Pending Action::::Passed");
				
	}
		

	
	public void ApproveJobs(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside ApproveJobs");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Approve Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Approve Jobs State");
			getJobApproval(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Approve Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Approve Jobs");
				getJobApproval(farmerExcelInputData);
			}
		}
		
		
	}
	
	
	public void ApproveJobsCheckRetailer(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside ApproveJobsCheckRetailer");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Approve Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Approve Jobs State");
			getJobApproval(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Approve Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Approve Jobs");
				getJobApproval(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void ApprovalJobsVerifyRetailer(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		
		System.out.println("Inside ApprovalJobsVerifyRetailer");
		
		//Click GetJobApproval:
		DriverWait("//*[@id=\"section-two\"]/div/div/div[1]");
		Thread.sleep(2000);
				
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		Thread.sleep(18000);
		
		//GetRetailer Name:
		driver.findElement(By.xpath(""));
	
	
	
	
	
	}
	
	
	
	public void getJobApproval(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		
		System.out.println("Inside getJobApproval");
		
		//Click GetJobApproval:
		DriverWait("//*[@id=\"section-two\"]/div/div/div[1]");
		Thread.sleep(2000);
				
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("APPROVE JOBS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "APPROVE JOBS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Approve Jobs Link click:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[2]/a")).click();
		Thread.sleep(25000);
		
			
		//Verifying the Jobs and LabourParts:
		List<WebElement> EstimateListItems=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[2]/*/*/*"));
		System.out.println("EstimateListItems.size():::"+EstimateListItems.size());
		for (int i = 1; i <= EstimateListItems.size(); i++) {
			String EstimateListItemsText = EstimateListItems.get(i).getText();
			System.out.println("ListItemsText:::" + i + ":::::" + EstimateListItemsText);
			if(i==(EstimateListItems.size()-1))
			{
				break;	
			}
			
		}
		//Confirm button click:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[3]/button")).click();
		Thread.sleep(10000);
		System.out.println("Get Job Approval::::Passed");
		
		
	}//Closing method brace

	public void SparePayments(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside SparePayments");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Approve Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Approve Jobs State");
			getSparesPayment(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Approve Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Approve Jobs");
				getSparesPayment(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void getSparesPayment(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		
		System.out.println("Inside getSparesPayment");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Get Spares for Payment button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("SPARE PAYMENTS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "SPARE PAYMENTS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}

		//Spare Payments click:
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[3]/a")).click();
		Thread.sleep(22000);
				
		//DriverPaidCash checkbox:
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 
		WebElement element = driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[3]/input"));
		
		js.executeScript("arguments[0].click();", element);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[4]/button")).click();
		System.out.println("Get Spares Payment::::Passed");
				
		
 }
	
	public void ConfirmJobStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside ConfirmJobStatus");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Approve Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Approve Jobs State");
			startJobs(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Approve Jobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Approve Jobs");
				startJobs(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void startJobs(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside StartJobs");
				
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Confirm Job Status button clicked::::Passed");
		Thread.sleep(25000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("START JOBS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "START JOBS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
		}				
		
		//Click Start Jobs Link:
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(20000);
		
			
		
		//Confirm Job Start Status:
		DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-start-jobs/div/div[4]/button");
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-start-jobs/div/div[4]/button")).click();
		Thread.sleep(15000);
		System.out.println("Confirm Job Status::::Passed");
		
		
	}
		
	public void UpdateJobStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main
		System.out.println("Inside UpdateJobStatus");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in UpdateJob");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Job State");
			UpdateJob(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Job");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Job");
				UpdateJob(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void UpdateJob(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside UpdateJob");
						
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
		Thread.sleep(23000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking Update Job Status Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
		Thread.sleep(18000);
				
		//Click JobsCompleted button: 
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[3]")).click();
		writeReport(LogStatus.PASS, "Jobs Completed button clicked::::Passed");
		Thread.sleep(10000);
		List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[3]/table/*")); //tbody/*
		System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
		
		for (int i = 1; i <= VerifyJobsList.size(); i++) {
			String VerifyJobsListText = VerifyJobsList.get(i).getText();
			System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
			break;
		}
		Thread.sleep(10000);
		//Confirm Job Completion:
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[4]/button")).click();
		writeReport(LogStatus.PASS, "Confirm Job Completion button clicked::::Passed");
		Thread.sleep(10000);
		System.out.println("Update Job Status::::Passed");
				
	}

	public void AddRevisedJobWithParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main method
		
	System.out.println("Inside AddRevisedJobWithParts");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in UpdateJob");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Job State");
			RevisedJobWithParts(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Job");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Job State");
				RevisedJobWithParts(farmerExcelInputData);
			}
		}
			
	}

	public void RevisedJobWithParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //sub method
		System.out.println("Inside RevisedJobWithParts");
						
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}		
				//Clicking Update Job Status Link:
				driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
				Thread.sleep(30000);
			
				//Click Add New Job:
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[1]")).click();
				Thread.sleep(25000);
				
				DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select");
				
				String expected = farmerExcelInputData.getRevFarmerJobs();	
				if(expected.contains(",")) {
				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("OuterArrayLength:::" + outerArrayLength);
				int checkcount = 0;
				int count =0;
				for (String OuterArrayValues1 : outerArray){
					System.out.println(OuterArrayValues1);
					System.out.println("count:::"+count);
						
				List<WebElement> RevisedJobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select/*"));
				System.out.println("RevisedJobList.size():::"+RevisedJobList.size());
				
				for (int i = 1; i <= RevisedJobList.size(); i++) {
					String RevisedJobListText = RevisedJobList.get(i).getText();
					System.out.println("RevisedJobListText:::" + i + ":::::" + RevisedJobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[1]/select"));
					System.out.println(SelJob);
					SelJob.click();  							  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement RevLabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[2]/input"));
					System.out.println(RevLabourAmt);
					RevLabourAmt.click();								
					RevLabourAmt.sendKeys(farmerExcelInputData.getRevLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getRevLabAmt() + "  entered::::Passed");
					
					WebElement RevPartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[3]/input"));
					System.out.println(RevPartsAmt);
					RevPartsAmt.click();
					RevPartsAmt.sendKeys(farmerExcelInputData.getRevPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getRevPartsAmt() + "  entered::::Passed");
					
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);		
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
			Thread.sleep(3000);
			//Add Retailer button:		
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[4]/button[1]")).click();
			Thread.sleep(22000);
			DriverWait("//*[@id='myModal']");
			
			//Retailer:
			List<WebElement> RetailerList=driver.findElements(By.xpath("//*[@id='myModal']/*/*/*"));
			System.out.println("RetailerList.size():::"+RetailerList.size());
			for(int j=2;j<RetailerList.size();j++)
			{String RetailerListText = RetailerList.get(j).getText();
			System.out.println("RetailerListText:::" + j + ":::::" + RetailerListText);
			if(RetailerListText.contains(farmerExcelInputData.getRetailer()))
			{
			driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div["+(j-2)+"]/div[2]/label")).click();
			writeReport(LogStatus.PASS, "Retailer selected:::::Passed");
			break;  
			}
			
			}
			//Confirm Button:
			driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
			Thread.sleep(10000);
			
		}
			else {
			System.out.println("Else block");
			}
	}
	
	public void AddRevisedJobWithPartsNoRetailer(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main method
		
		System.out.println("Inside AddRevisedJobWithParts");
			
			searchOrderID(farmerExcelInputData);
		try {
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
				System.out.println("Clicked::"+PendingActionText+"link in UpdateJob");
				writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Job State");
				RevisedJobWithPartsNoRetailer(farmerExcelInputData);
			}}catch(Exception e){
			
				driver.findElement(By.partialLinkText("Upcoming")).click();
				Thread.sleep(5000);
				
				searchOrderID(farmerExcelInputData);
				if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
					Thread.sleep(3000);
					String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
					System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Job");
					writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Job State");
					RevisedJobWithPartsNoRetailer(farmerExcelInputData);
				}
			}
				
		}
	
	
	
	
	
	public void RevisedJobWithPartsNoRetailer(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //sub method
		System.out.println("Inside RevisedJobWithPartsNoRetailer");
						
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}		
				//Clicking Update Job Status Link:
				driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
				Thread.sleep(30000);
			
				//Click Add New Job:
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[1]")).click();
				Thread.sleep(25000);
				
				DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select");
				
				String expected = farmerExcelInputData.getRevFarmerJobs();	
				if(expected.contains(",")) {
				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("OuterArrayLength:::" + outerArrayLength);
				int checkcount = 0;
				int count =0;
				for (String OuterArrayValues1 : outerArray){
					System.out.println(OuterArrayValues1);
					System.out.println("count:::"+count);
						
				List<WebElement> RevisedJobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select/*"));
				System.out.println("RevisedJobList.size():::"+RevisedJobList.size());
				
				for (int i = 1; i <= RevisedJobList.size(); i++) {
					String RevisedJobListText = RevisedJobList.get(i).getText();
					System.out.println("RevisedJobListText:::" + i + ":::::" + RevisedJobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[1]/select"));
					System.out.println(SelJob);
					SelJob.click();  							  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement RevLabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[2]/input"));
					System.out.println(RevLabourAmt);
					RevLabourAmt.click();								
					RevLabourAmt.sendKeys(farmerExcelInputData.getRevLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getRevLabAmt() + "  entered::::Passed");
					
					WebElement RevPartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[3]/input"));
					System.out.println(RevPartsAmt);
					RevPartsAmt.click();
					RevPartsAmt.sendKeys(farmerExcelInputData.getRevPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getRevPartsAmt() + "  entered::::Passed");
					
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);		
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
			Thread.sleep(3000);
			//Add Retailer button:		
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[4]/button[1]")).click();
			Thread.sleep(22000);
			DriverWait("//*[@id='myModal']");
			
			//I couldn find any Retailer:
			driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div[1]/label")).click();
			Thread.sleep(8000);
			
			//Confirm Button:
			driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
			Thread.sleep(10000);
			
		}
			else {
			System.out.println("Else block");
			}
	}
	
	
	public void AddRevisedJobWithoutParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main method
		
		System.out.println("Inside AddRevisedJobWithoutParts");
			
			searchOrderID(farmerExcelInputData);
		try {
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
				System.out.println("Clicked::"+PendingActionText+"link in UpdateJob");
				writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Job State");
				RevisedJobWithoutParts(farmerExcelInputData);
			}}catch(Exception e){
			
				driver.findElement(By.partialLinkText("Upcoming")).click();
				Thread.sleep(5000);
				
				searchOrderID(farmerExcelInputData);
				if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
					Thread.sleep(3000);
					String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
					System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Job");
					writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Job State");
					RevisedJobWithoutParts(farmerExcelInputData);
				}
			}
			
			
		}
		
	public void RevisedJobWithoutParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //sub method
		System.out.println("Inside RevisedJobWithoutParts");
						
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}		
				//Clicking Update Job Status Link:
				driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
				Thread.sleep(15000);
			
				//Click Add New Job:
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[1]")).click();
				Thread.sleep(25000);
				
				DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select");
				
				String expected = farmerExcelInputData.getRevFarmerJobs();	
				if(expected.contains(",")) {
				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("OuterArrayLength:::" + outerArrayLength);
				int checkcount = 0;
				int count =0;
				for (String OuterArrayValues1 : outerArray){
					System.out.println(OuterArrayValues1);
					System.out.println("count:::"+count);
						
				List<WebElement> RevisedJobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr/td[1]/select/*"));
				System.out.println("RevisedJobList.size():::"+RevisedJobList.size());
				
				for (int i = 1; i <= RevisedJobList.size(); i++) {
					String RevisedJobListText = RevisedJobList.get(i).getText();
					System.out.println("RevisedJobListText:::" + i + ":::::" + RevisedJobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[1]/select"));
					System.out.println(SelJob);
					SelJob.click();  							  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					WebElement Job=oSelect.getFirstSelectedOption();
					writeReport(LogStatus.PASS, "Revised Job::::" + RevisedJobListText + "  entered::::Passed");
					System.out.println("Revised Job::::" + RevisedJobListText + "  entered::::Passed");
					
								
					Thread.sleep(1000);
					WebElement RevLabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[2]/input"));
					System.out.println(RevLabourAmt);
					RevLabourAmt.click();								
					RevLabourAmt.sendKeys(farmerExcelInputData.getRevLabAmt());
					writeReport(LogStatus.PASS, "Revised Labour Amount::::" + farmerExcelInputData.getRevLabAmt() + "  entered::::Passed");
					
					/*WebElement RevPartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/table[1]/tbody/tr["+count+"]/td[3]/input"));
					System.out.println(RevPartsAmt);
					RevPartsAmt.click();
					RevPartsAmt.sendKeys(farmerExcelInputData.getRevPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getRevPartsAmt() + "  entered::::Passed");*/
					
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);		
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[3]/form/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
			Thread.sleep(3000);
			
			//Submit Estimate Button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-addmore-jobs/div/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Submit Estimate Button:::::Passed");
			Thread.sleep(10000);
			
		}
			else {
			System.out.println("Else block");
			}
	}
	public void ApproveAdditionalJobs(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main			
		
		System.out.println("Inside ApproveAdditionalJobs");
		
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in ApproveAdditionalJobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Approve Additional Jobs State");
			ApproveRevisedEstimate(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in ApproveAdditionalJobs");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Approve Additional Jobs State");
				ApproveRevisedEstimate(farmerExcelInputData);
			}
		}
	}
		
	public void ApproveRevisedEstimate(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //sub			
		
		System.out.println("Inside ApproveRevisedEstimate");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Approve Revised Estimate button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
		}
		
		//Clicking Approve Revised Estimate Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[6]/a")).click();
		Thread.sleep(12000);			
		
		//Confirm Approval:
		Thread.sleep(6000);			
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-revised/div/div[2]/form/div/button")).click();
		writeReport(LogStatus.PASS, "Confirm Approval button clicked::::Passed");
		Thread.sleep(10000);
		WebConfiguration.TakePositiveSnapshot();
	}
	
	public void RevisedJobSparePayments(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //main			
		
		System.out.println("Inside RevisedSparePayments");
		
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(4000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in RevisedJobsSparePayments");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link ->RevisedJobsSparePayments State");
			RevisedSparePayments(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in RevisedJobsSparePayments");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> RevisedJobsSparePayments State");
				RevisedSparePayments(farmerExcelInputData);
			}
		}
	}
	
	
	public void RevisedSparePayments(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{ //sub			
		
		System.out.println("Inside RevisedSparePayments");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Approve Revised Estimate button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking Revised Spare Payments Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[7]/a")).click();
		Thread.sleep(12000);
		
		//Cash radio button:								
		WebElement Cashradio=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[3]/div[4]/input"));
		if(Cashradio.isSelected())
		{writeReport(LogStatus.PASS, "Payment Mode:::CASH is selected by default");}
		
		
		//Click Driver Paid Cash:								
		WebElement DriverPaidCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[3]/input"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", DriverPaidCash);
		writeReport(LogStatus.PASS, "Driver Paid Cash clicked");
				
		//Click Update Spare Payment Status:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[4]/button")).click();
		Thread.sleep(12000);
		writeReport(LogStatus.PASS, "Clicked Update Spare Payment Status button");
		
	}
		
	
	
	
	
	
	public void AddBillStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside AddBillStatus");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Add Bill");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Add Bill State");
			AddBill(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Add Bill");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Add Bill State");
				AddBill(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void AddBill(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside AddBill");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Add Bill button clicked::::Passed");
		Thread.sleep(20000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("ADD BILL::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "ADD BILL::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String LabourAmt = JobList.get(i).getText();
				System.out.println("Labour:::" +LabourAmt);
				writeReport(LogStatus.PASS, "Labour:::" +LabourAmt);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking AddBill Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[9]/a")).click();
		Thread.sleep(15000);
		
		List<WebElement> AddJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/table/tbody/*"));
		System.out.println("AddJobsList.size():::"+AddJobsList.size());
		double LabourCount=0.00;
		for (int i = 0; i <= AddJobsList.size(); i++) {
			String AddJobsListText = AddJobsList.get(i).getText().replaceAll("[^\\.0123456789]",""); //.replaceAll("[^0-9]","")
			System.out.println("AddJobsListText:::" + i + ":::::" +AddJobsListText);
			
			
			LabourCount=LabourCount+Double.parseDouble(AddJobsListText);
			System.out.println(+i+"LabourCount::"+LabourCount);
			if(i==AddJobsList.size()-1) {
				break;
			}
		}
			String PartsAmount=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/p[1]")).getText().replaceAll("[^0-9]","");
			System.out.println("PartsAmount::"+PartsAmount);
			
			String TotalCost=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/div[1]/p")).getText().replaceAll("[^\\.0123456789]","");
			System.out.println("TotalCost::"+TotalCost);
			
			Double AddedTotalCost=LabourCount+Double.parseDouble(PartsAmount);
			System.out.println("AddedTotalCost::::::::::::::::::::::::::::::::::"+AddedTotalCost);
			
			if(AddedTotalCost==Double.parseDouble(TotalCost))
			{
			System.out.println("Total Cost:::"+AddedTotalCost+"matches");
			writeReport(LogStatus.PASS, "Total Cost:::"+AddedTotalCost+"matches");
			}
			else
			{
			 System.out.println("Total Cost:::"+AddedTotalCost+"doesnt matches");
			 writeReport(LogStatus.FAIL, "Total Cost:::"+AddedTotalCost+"doesnt matches");
			}
				
		
		//Entering KM reading:
		WebElement KM =driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/p[5]/input"));
		KM.click();
		KM.sendKeys(farmerExcelInputData.getKM());
				
		List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/table/*"));
		System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
		
		for (int i = 1; i <= VerifyJobsList.size(); i++) {
			String VerifyJobsListText = VerifyJobsList.get(i).getText();
			System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
			break;
		}
		//Clicking Submit Bill Button:
		WebConfiguration.TakePositiveSnapshot();
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[4]/button")).click();
		Thread.sleep(10000);
		System.out.println("Add Bill completed::::Passed");
		
}

	public void UpdatePaymentStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside AddBillStatus");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Update Payment Status");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link ->  UpdatePayment State");
			UpdatePayment(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Payment Status");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> UpdatePayment State");
				UpdatePayment(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void UpdatePayment(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside UpdatePaymentStatus");
		Thread.sleep(10000);
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Payment Status button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE PAYMENT STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE PAYMENT STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking UpdatePaymentStatus Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[10]/a")).click();
		Thread.sleep(15000);
		
		
		//Click Cash radio button:
		WebElement Cashradio=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[10]/input[1]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Cashradio);
		Cashradio.getText();
		System.out.println("Cashradio::"+Cashradio.getText());
		writeReport(LogStatus.PASS, "Payment Mode:::CASH");
		
		//Click Driver Paid Cash:
		WebElement DriverPaidCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[1]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", DriverPaidCash);
		writeReport(LogStatus.PASS, "Driver Paid Cash clicked");
		
		//Clicking TechnicianAcceptedCash:
		WebElement TechAcceptedCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[2]"));
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].click();", TechAcceptedCash);
		writeReport(LogStatus.PASS, "Technician Accepted Cash clicked");
		
		//Clicking UpdatePaymentStatus button:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[4]/button")).click();
		Thread.sleep(20000);
		System.out.println("Update Payment Status ::::Passed");
		writeReport(LogStatus.PASS, "Update Payment Status clicked");
		WebConfiguration.TakePositiveSnapshot();
		
	}
	
	public void UpdateRatingStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside UpdateRatingStatus");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Update Rating Status");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link ->  Update Rating State");
			UpdateRating(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Rating Status");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Rating State");
				UpdateRating(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void UpdateRating(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside UpdateRating");
		//System.out.println("farmerExcelInputData.getGiveRating():::"+farmerExcelInputData.getGiveRating());
		//System.out.println("farmerExcelInputData.getReason():::"+farmerExcelInputData.getReason());
		Thread.sleep(8000);
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Rating button clicked::::Passed");
		Thread.sleep(15000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATING RATING::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATING RATING::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking UpdateRating Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[11]/a")).click();
		Thread.sleep(15000);
		
		List<WebElement> RatingList=driver.findElements(By.xpath("//*[@id=\"stars\"]/*/*"));
		System.out.println("RatingList.size():::"+RatingList.size());
		
		for (int i = 1; i <= RatingList.size(); i++) {
			String RatingListText = RatingList.get(i).getText();
			System.out.println("RatingListText:::" + i + ":::::" + RatingListText);
			int getGiverating=Integer.parseInt(farmerExcelInputData.getGiveRating());
			System.out.println("getGiverating::"+getGiverating);
			if(i==getGiverating) {
				driver.findElement(By.xpath("//*[@id=\"stars\"]/span["+ i +"]/img")).click();
				System.out.println("Rating:::"+farmerExcelInputData.getGiveRating());
				writeReport(LogStatus.PASS, "Give Rating:::"+farmerExcelInputData.getGiveRating()+"::::Passed");
				break;
			}
		}
		//Clicking Submit Bill Button:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-rating/div/div[4]/button")).click();
		Thread.sleep(10000);
	
		//What went wrong:
		List<WebElement> WhatWentWrongList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/*"));
		System.out.println("WhatWentWrongList.size():::"+WhatWentWrongList.size());
		
		for (int j=1; j<=WhatWentWrongList.size(); j++) {
			String WhatWentWrongListText = WhatWentWrongList.get(j).getText();
			System.out.println("WhatWentWrongListText:::" + j + ":::::" + WhatWentWrongListText);
			int getReason=Integer.parseInt(farmerExcelInputData.getReason());
			System.out.println("getReason:::"+getReason);
			if(j==getReason) {
				System.out.println("*******************************INSIDE IF*******************************");
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).click();
				String ReasonText=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).getText();
				System.out.println("Reason:::"+ReasonText);
				writeReport(LogStatus.PASS, "Reason:::"+ReasonText+"::::Passed");
				break;
			}
		}
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[4]/button")).click();
		Thread.sleep(10000);
		System.out.println("Update Rating ::::Passed");
	}

	
//*****************************************************************************************************************************
	public void OldAddJobsWithParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside AddJobsWithParts");
		
		searchOrderID(farmerExcelInputData);
	
		try {	
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
			
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Add Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Add Jobs State");	
			
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();//Update Estimate
		writeReport(LogStatus.PASS, "Update Estimate button clicked::::Passed");
		Thread.sleep(5000);
		
		
		//Click AddJobs:
		DriverWait("//*[@id='first-row']/div[1]");
		Thread.sleep(8000);
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[1]/a")).click(); 
		DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div");
		Thread.sleep(10000);
		
			String expected = farmerExcelInputData.getFarmerJobs();	
			if(expected.contains(",")) {
			String[] outerArray = expected.split(",");
			int outerArrayLength = outerArray.length;
			System.out.println("OuterArrayLength:::" + outerArrayLength);
			int checkcount = 0;
			int count =0;
			for (String OuterArrayValues1 : outerArray){
				System.out.println(OuterArrayValues1);
				System.out.println("count:::"+count);
				
				List<WebElement> JobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr/td[1]/select/*"));
				System.out.println("JobList.size():::"+JobList.size());
				
				for (int i = 1; i <= JobList.size(); i++) {
					String JobListText = JobList.get(i).getText();
					System.out.println("JobListText:::" + i + ":::::" + JobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[1]/select"));
					SelJob.click();  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement LabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[2]/input"));
					LabourAmt.click();								
					LabourAmt.sendKeys(farmerExcelInputData.getFarmerLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
					
					WebElement PartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[3]/input"));
					PartsAmt.click();
					PartsAmt.sendKeys(farmerExcelInputData.getFarmerPartsAmt());
					writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getFarmerPartsAmt() + "  entered::::Passed");
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
	
			//Add Retailer button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[4]/button[1]")).click();
			Thread.sleep(22000);
			DriverWait("//*[@id='myModal']");
			
			//Retailer:
			List<WebElement> RetailerList=driver.findElements(By.xpath("//*[@id='myModal']/*/*/*"));
			System.out.println("RetailerList.size():::"+RetailerList.size());
			for(int j=2;j<RetailerList.size();j++)
			{String RetailerListText = RetailerList.get(j).getText();
			System.out.println("RetailerListText:::" + j + ":::::" + RetailerListText);
			if(RetailerListText.contains(farmerExcelInputData.getRetailer()))
			{
			driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div["+(j-2)+"]/div[2]/label")).click();
			writeReport(LogStatus.PASS, "Retailer selected:::::Passed");
			break;  
			}
			
			}
			//Confirm Button:
			driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
			writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
			Thread.sleep(10000);
			System.out.println("Pending Action::::Passed");
		}
			else {
			System.out.println("Else block");}
		}}
		catch(Exception e){
			driver.findElement(By.partialLinkText("Upcoming")).click();
	
			searchOrderID(farmerExcelInputData);
			
			String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
			System.out.println("Clicked::"+UpcomingOrdersText+"link in Add Jobs");
			writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Add Jobs");
			
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Update Estimate button clicked::::Passed");
			Thread.sleep(5000);
			
			//Click AddJobs:
			DriverWait("//*[@id='first-row']/div[1]");
			Thread.sleep(8000);
			driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[1]/a")).click();
			DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div");
			Thread.sleep(10000);
			
				String expected = farmerExcelInputData.getFarmerJobs();	
				if(expected.contains(",")) {
				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("outerArrayLength:::" + outerArrayLength);
				int checkcount = 0;
				int count =0;
				for (String OuterArrayValues1 : outerArray){
					System.out.println(OuterArrayValues1);
					System.out.println("count:::"+count);
					
					List<WebElement> JobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr/td[1]/select/*"));
					System.out.println("JobList.size():::"+JobList.size());
					
					for (int i = 1; i <= JobList.size(); i++) {
						String JobListText = JobList.get(i).getText();
						System.out.println("JobListText:::" + i + ":::::" + JobListText);
					    int result=Integer.parseInt(OuterArrayValues1);
					    System.out.println("result:::" + result);
					 
					   					    
						if(result==i){
						count = count+1;
						System.out.println("count value:::"+count);
						System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
						System.out.println("i value::::" + i + "Passed");
						WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[1]/select"));
						SelJob.click();  
						Select oSelect = new Select(SelJob);
						oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
						
						Thread.sleep(1000);
						WebElement LabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[2]/input"));
						LabourAmt.click();								
						LabourAmt.sendKeys(farmerExcelInputData.getFarmerLabAmt());
						String LabourAmount=LabourAmt.getText();
						System.out.println("LabourAmount:::"+LabourAmount);
						writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
						
						WebElement PartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[3]/input"));
						PartsAmt.click();
						PartsAmt.sendKeys(farmerExcelInputData.getFarmerPartsAmt());
						String PartsAmount=PartsAmt.getText();
						System.out.println("PartsAmount:::"+PartsAmount);
						writeReport(LogStatus.PASS, "Parts Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
						checkcount = checkcount + 1;
						System.out.println("checkcount" + checkcount);
						if (checkcount != outerArrayLength) {
							Thread.sleep(1000);
							String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/a/span";
							driver.findElement(By.xpath(addJobButton)).click();
							break;
							
						}else{
							break;
							
						
						}
						
						}
					}
				}
		
				//Add Retailer button:
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[4]/button[1]")).click();
				Thread.sleep(22000);
				DriverWait("//*[@id='myModal']");
				
				//Retailer:
				List<WebElement> RetailerList=driver.findElements(By.xpath("//*[@id='myModal']/*/*/*"));
				System.out.println("RetailerList.size():::"+RetailerList.size());
				for(int j=2;j<RetailerList.size();j++)
				{String RetailerListText = RetailerList.get(j).getText();
				System.out.println("RetailerListText:::" + j + ":::::" + RetailerListText);
				if(RetailerListText.contains(farmerExcelInputData.getRetailer()))
				{
				driver.findElement(By.xpath("//*[@id='myModal']/div[3]/div["+(j-2)+"]/div[2]/label")).click();
				writeReport(LogStatus.PASS, "Retailer selected:::::Passed");
				break;  
				}
				
				}
				//Confirm Button:
				driver.findElement(By.xpath("//*[@id='myModal']/div[4]/button[2]")).click();
				writeReport(LogStatus.PASS, "Clicked Confirm button in Retailer:::::Passed");
				Thread.sleep(10000);
				System.out.println("Upcoming Action::::Passed");
			}
			else {
				System.out.println("Else block");}
			}
			
		
}
	
	public void OldgetJobApproval(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		
		System.out.println("Inside getJobApproval");
		searchOrderID(farmerExcelInputData);
				
		/*DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		Thread.sleep(9000);*/
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in ApproveJobs");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link->ApproveJobs State");
			
		//Click GetJobApproval:
		DriverWait("//*[@id=\"section-two\"]/div/div/div[1]");
		Thread.sleep(4000);
		
		
		List<WebElement> ListItems=driver.findElements(By.xpath("//*[@id=\"section-two\"]/div/div/div[1]/*"));
		System.out.println("ListItems.size():::"+ListItems.size());
		
		for (int i = 1; i <= ListItems.size(); i++) {
			String ListItemsText = ListItems.get(i).getText();
			System.out.println("ListItemsText:::" + i + ":::::" + ListItemsText);
			if(ListItemsText.contains("GET JOB APPROVAL"))
			{
			 System.out.println("Inside If");
			 driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			 Thread.sleep(18000);
			 break;
			}
		}
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("APPROVE JOBS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "APPROVE JOBS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Approve Jobs Link click:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[2]/a")).click();
		Thread.sleep(18000);
		
			
		//Verifying the Jobs and LabourParts:
		List<WebElement> EstimateListItems=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[2]/*/*/*"));
		System.out.println("EstimateListItems.size():::"+EstimateListItems.size());
		for (int i = 1; i <= EstimateListItems.size(); i++) {
			String EstimateListItemsText = EstimateListItems.get(i).getText();
			System.out.println("ListItemsText:::" + i + ":::::" + EstimateListItemsText);
			if(i==(EstimateListItems.size()-1))
			{
				break;	
			}
			
		}
		//Confirm button click:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[3]/button")).click();
		Thread.sleep(10000);
		System.out.println("Get Job Approval::::Passed");
		}}
		catch(Exception e) {
			System.out.println("Inside Upcoming Orders- getJobApproval");
			Thread.sleep(20000);
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(13000);
			
			String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
			System.out.println("Clicked::"+UpcomingOrdersText+"link in ApproveJobs");
			writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> ApproveJobs");
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(Keys.TAB);
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();
			Thread.sleep(15000);
			/*DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			Thread.sleep(9000);*/
			
					
			//Click GetJobApproval:
			DriverWait("//*[@id=\"section-two\"]/div/div/div[1]");
			Thread.sleep(4000);
			
			
			List<WebElement> ListItems=driver.findElements(By.xpath("//*[@id=\"section-two\"]/div/div/div[1]/*"));
			System.out.println("ListItems.size():::"+ListItems.size());
			
			for (int i = 1; i <= ListItems.size(); i++) {
				String ListItemsText = ListItems.get(i).getText();
				System.out.println("ListItemsText:::" + i + ":::::" + ListItemsText);
				if(ListItemsText.contains("GET JOB APPROVAL"))
				{
				 System.out.println("Inside If");
				 driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
				 Thread.sleep(18000);
				 break;
				}
			}
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("APPROVE JOBS::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "APPROVE JOBS::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
				
			}
			
			//Approve Jobs Link click:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[2]/a")).click();
			Thread.sleep(18000);
			
			
			
			//Verifying the Jobs and LabourParts:
			List<WebElement> EstimateListItems=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[2]/*/*/*"));
			System.out.println("EstimateListItems.size():::"+EstimateListItems.size());
			for (int i = 1; i <= EstimateListItems.size(); i++) {
				String EstimateListItemsText = EstimateListItems.get(i).getText();
				System.out.println("ListItemsText:::" + i + ":::::" + EstimateListItemsText);
				if(i==(EstimateListItems.size()-1))
				{
					break;	
				}
				
			}
			//Confirm button click:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-approve-jobs/div/div[2]/div[3]/button")).click();
			Thread.sleep(10000);
			System.out.println("Get Job Approval::::Passed");
		}
		
	}

	public void OldgetSparesPayment(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		
		System.out.println("Inside getSparesPayment");
		Thread.sleep(10000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(12000);
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			
		Thread.sleep(1000);
		String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
		System.out.println("Clicked::"+UpcomingOrdersText+"link in SparePayments");
		writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> SparePayments");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Get Spares for Payment button clicked::::Passed");
		Thread.sleep(15000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("SPARE PAYMENTS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "SPARE PAYMENTS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
//		if(i==(EstimateListItems.size()-1))
//		{
//		break;	
//		}	
			
		
		
		//Spare Payments click:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[3]/a")).click();
		Thread.sleep(22000);
				
		//DriverPaidCash checkbox:
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 
		WebElement element = driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[3]/input"));
		
		js.executeScript("arguments[0].click();", element);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[4]/button")).click();
		System.out.println("Get Spares Payment::::Passed");
		}
		}
		catch(Exception e) {
			System.out.println("Inside PENDING-getSparesPayment");
			Thread.sleep(2000);
			driver.findElement(By.partialLinkText("Pending")).click();
			Thread.sleep(13000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in SparePayments");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link -> SparePayments");
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
			Thread.sleep(18000);
			
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
			System.out.println("GetButtonText::::"+GetButtonText);
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Get Spares for Payment button clicked::::Passed");
			Thread.sleep(20000);
			
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("SPARE PAYMENTS::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "SPARE PAYMENTS::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
				
			}
			
			//Spare Payments click:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[3]/a")).click();
			Thread.sleep(20000);
			
			//DriverPaidCash checkbox:
			JavascriptExecutor js = (JavascriptExecutor) driver;
			 
			WebElement element = driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[3]/input"));
			
			js.executeScript("arguments[0].click();", element);
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//html/body/modal-overlay/bs-modal-container/div/div/app-spare-payment/div/div[4]/button")).click();
			System.out.println("Get Spares Payment::::Passed");
		}
 }
	
	public void OldconfirmJobStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside OldconfirmJobStatus");
		Thread.sleep(5000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(13000);
		searchOrderID(farmerExcelInputData);
		
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
		Thread.sleep(1000);
		
		String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
		System.out.println("Clicked::"+UpcomingOrdersText+"link in Start Jobs");
		writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Start Jobs");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Confirm Job Status button clicked::::Passed");
		Thread.sleep(20000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("START JOBS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "START JOBS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
		}				
		
		//Click Start Jobs Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(10000);
		
			
		
		//Confirm Job Start Status:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-start-jobs/div/div[4]/button")).click();
		Thread.sleep(15000);
		System.out.println("Confirm Job Status::::Passed");
		}}
		catch(Exception e) {
			System.out.println("Inside Pending-ConfirmJobStatus");
			Thread.sleep(10000);
			driver.findElement(By.partialLinkText("Pending")).click();
			Thread.sleep(13000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in Start Jobs");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link -> Start Jobs");
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
			Thread.sleep(15000);
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
			System.out.println("GetButtonText::::"+GetButtonText);
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Confirm Job Status button clicked::::Passed");
			Thread.sleep(20000);
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("START JOBS::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "START JOBS::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
			}	
			
			//Click Start Jobs Link:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[4]/a")).click();
			Thread.sleep(10000);
			//Confirm Job Start Status:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-start-jobs/div/div[4]/button")).click();
			Thread.sleep(15000);
			System.out.println("Confirm Job Status::::Passed");
		}
	}
	
	public void OldUpdateJobStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside UpdateJobStatus");
		Thread.sleep(20000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(13000);
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
		Thread.sleep(1000);
		
		String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
		System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Job Status");
		writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Job Status");
				
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
		Thread.sleep(20000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking Update Job Status Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
		Thread.sleep(12000);
				
		//Click JobsCompleted button: 
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[3]")).click();
		writeReport(LogStatus.PASS, "Jobs Completed button clicked::::Passed");
		Thread.sleep(10000);
		List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[3]/table/*")); //tbody/*
		System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
		
		for (int i = 1; i <= VerifyJobsList.size(); i++) {
			String VerifyJobsListText = VerifyJobsList.get(i).getText();
			System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
			break;
		}
		//Confirm Job Completion:
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[4]/button")).click();
		writeReport(LogStatus.PASS, "Confirm Job Completion button clicked::::Passed");
		Thread.sleep(10000);
		System.out.println("Update Job Status::::Passed");
		WebConfiguration.TakePositiveSnapshot();
		
		}}
		catch(Exception e){
			System.out.println("Inside Pending- UpdateJobStatus");
			Thread.sleep(12000);
			driver.findElement(By.partialLinkText("Pending")).click();
			Thread.sleep(13000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in Update Job Status");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link -> Update Job Status");
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(2000);
			//driver.findElement(By.xpath("//*[@id='style-1']/li")).click();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(Keys.TAB);
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(Keys.TAB);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
			Thread.sleep(18000);
			
							
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
			System.out.println("GetButtonText::::"+GetButtonText);
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Update Job Status button clicked::::Passed");
			Thread.sleep(20000);
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("UPDATE JOB STATUS::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "UPDATE JOB STATUS::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
				
			}
			
			
			//Clicking Update Job Status Link:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[5]/a")).click();
			
			Thread.sleep(15000);
			//Click JobsCompleted button: 
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-start-status/div/div[4]/button[3]")).click();
			writeReport(LogStatus.PASS, "Jobs Completed button clicked::::Passed");
			Thread.sleep(10000);
			List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[3]/table/*")); //tbody/*
			System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
			
			for (int i = 1; i <= VerifyJobsList.size(); i++) {
				String VerifyJobsListText = VerifyJobsList.get(i).getText();
				System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
				break;
			}
			//Confirm Job Completion:
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-close-job/div/div[4]/button")).click();
			writeReport(LogStatus.PASS, "Confirm Job Completion button clicked::::Passed");
			Thread.sleep(10000);
			WebConfiguration.TakePositiveSnapshot();
			System.out.println("Update Job Status::::Passed");
		}
	}
	
	public void OldAddBill(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside AddBill");
		Thread.sleep(12000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(17000);
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
		
		String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
		System.out.println("Clicked::"+UpcomingOrdersText+"link in Add Bill");
		writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Add Bill");	
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Add Bill button clicked::::Passed");
		Thread.sleep(20000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("ADD BILL::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "ADD BILL::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String LabourAmt = JobList.get(i).getText();
				System.out.println("Labour:::" +LabourAmt);
				writeReport(LogStatus.PASS, "Labour:::" +LabourAmt);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking AddBill Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[9]/a")).click();
		Thread.sleep(15000);
		
		List<WebElement> AddJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/table/tbody/*"));
		System.out.println("AddJobsList.size():::"+AddJobsList.size());
		double LabourCount=0.00;
		for (int i = 0; i <= AddJobsList.size(); i++) {
			String AddJobsListText = AddJobsList.get(i).getText().replaceAll("[^\\.0123456789]",""); //.replaceAll("[^0-9]","")
			System.out.println("AddJobsListText:::" + i + ":::::" +AddJobsListText);
			
			
			LabourCount=LabourCount+Double.parseDouble(AddJobsListText);
			System.out.println(+i+"LabourCount::"+LabourCount);
			if(i==AddJobsList.size()-1) {
				break;
			}
		}
			String PartsAmount=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/p[1]")).getText().replaceAll("[^0-9]","");
			System.out.println("PartsAmount::"+PartsAmount);
			
			String TotalCost=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/div[1]/p")).getText().replaceAll("[^\\.0123456789]","");
			System.out.println("TotalCost::"+TotalCost);
			
			Double AddedTotalCost=LabourCount+Double.parseDouble(PartsAmount);
			System.out.println("AddedTotalCost::::::::::::::::::::::::::::::::::"+AddedTotalCost);
			
			if(AddedTotalCost==Double.parseDouble(TotalCost))
			{
			System.out.println("Total Cost:::"+AddedTotalCost+"matches");
			writeReport(LogStatus.PASS, "Total Cost:::"+AddedTotalCost+"matches");
			}
			else
			{
			 System.out.println("Total Cost:::"+AddedTotalCost+"doesnt matches");
			 writeReport(LogStatus.FAIL, "Total Cost:::"+AddedTotalCost+"doesnt matches");
			}
				
		
		//Entering KM reading:
		WebElement KM =driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/p[5]/input"));
		KM.click();
		KM.sendKeys(farmerExcelInputData.getKM());
				
		List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/table/*"));
		System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
		
		for (int i = 1; i <= VerifyJobsList.size(); i++) {
			String VerifyJobsListText = VerifyJobsList.get(i).getText();
			System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
			break;
		}
		//Clicking Submit Bill Button:
		WebConfiguration.TakePositiveSnapshot();
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[4]/button")).click();
		Thread.sleep(10000);
		System.out.println("Add Bill completed::::Passed");
		
		}}
		catch(Exception e) {
			System.out.println("Inside AddBill");
			Thread.sleep(12000);
			driver.findElement(By.partialLinkText("Pending")).click();
			Thread.sleep(17000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in Add Bill");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link -> Add Bill");	
			
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
			Thread.sleep(18000);
					
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
			System.out.println("GetButtonText::::"+GetButtonText);
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Add Bill button clicked::::Passed");
			Thread.sleep(20000);
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("ADD BILL::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "ADD BILL::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
				
			}
			//Clicking AddBill Link:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[9]/a")).click();
			Thread.sleep(15000);
			//Entering KM reading:
			WebElement KM =driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/div/p[5]/input"));
			KM.click();
			KM.sendKeys(farmerExcelInputData.getKM());
					
			List<WebElement> VerifyJobsList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[3]/table/*"));
			System.out.println("VerifyJobsList.size():::"+VerifyJobsList.size());
			
			for (int i = 1; i <= VerifyJobsList.size(); i++) {
				String VerifyJobsListText = VerifyJobsList.get(i).getText();
				System.out.println("VerifyJobsListText:::" + i + ":::::" + VerifyJobsListText);
				break;
			}
			//Clicking Submit Bill Button:
			WebConfiguration.TakePositiveSnapshot();
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-bill/div/div[4]/button")).click();
			Thread.sleep(10000);
			System.out.println("Add Bill completed::::Passed");
	}
	}
	
	public void OldUpdatePaymentStatus(FarmerExcelInputData farmerExcelInputData) throws InterruptedException, IOException{
		System.out.println("Inside UpdatePaymentStatus");
		Thread.sleep(10000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(13000);
		searchOrderID(farmerExcelInputData);
		try{
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
			
			String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
			System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Payment Status");
			writeReport(LogStatus.PASS, "Clicked UpcomingOrders link -> Update Payment Status");	
				
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Payment Status button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATE PAYMENT STATUS::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATE PAYMENT STATUS::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking UpdatePaymentStatus Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[10]/a")).click();
		Thread.sleep(10000);
		
		
		//Click Cash radio button:
		WebElement Cashradio=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[10]/input[1]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Cashradio);
		Cashradio.getText();
		System.out.println("Cashradio::"+Cashradio.getText());
		writeReport(LogStatus.PASS, "Payment Mode:::CASH");
		
		//Click Driver Paid Cash:
		WebElement DriverPaidCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[1]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", DriverPaidCash);
		writeReport(LogStatus.PASS, "Driver Paid Cash clicked");
		
		//Clicking TechnicianAcceptedCash:
		WebElement TechAcceptedCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[2]"));
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].click();", TechAcceptedCash);
		writeReport(LogStatus.PASS, "Technician Accepted Cash clicked");
		
		//Clicking UpdatePaymentStatus button:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[4]/button")).click();
		Thread.sleep(20000);
		System.out.println("Update Payment Status ::::Passed");
		writeReport(LogStatus.PASS, "Update Payment Status clicked");
		WebConfiguration.TakePositiveSnapshot();
		}
		}
		catch(Exception e){
			System.out.println("Inside UpdatePaymentStatus");
			Thread.sleep(8000);
			driver.findElement(By.partialLinkText("Pending")).click();
			Thread.sleep(13000);
			
			String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionsText+"link in Update Payment Status");
			writeReport(LogStatus.PASS, "Clicked PendingActions Link -> Update Payment Status");
			
			//Search using OrderID:
			DriverWait("//*[@id='order']");
			driver.findElement(By.xpath("//*[@id='order']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='order']")).clear();
			driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
			Thread.sleep(18000);
			
			DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
			String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
			System.out.println("GetButtonText::::"+GetButtonText);
			driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
			writeReport(LogStatus.PASS, "Update Payment Status button clicked::::Passed");
			Thread.sleep(18000);
			
			//Job State & Job
			DriverWait("//*[@id='style-1']/*/*");
			List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
			
			System.out.println("JobList.size():::"+JobList.size());
			for (int i = 0; i <= JobList.size()-1; i++) {
				if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
						
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("UPDATE PAYMENT STATUS::Job State & Job:::"+ JobListItemsText);
				writeReport(LogStatus.PASS, "UPDATE PAYMENT STATUS::Job State & Job:::" +JobListItemsText);
				}
				if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
						
					//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Labour:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
					}
				if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
					String JobListItemsText = JobList.get(i).getText();
					System.out.println("Parts:::" +JobListItemsText);
					writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
				}
				
			}
			
			//Clicking UpdatePaymentStatus Link:
			driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[10]/a")).click();
			Thread.sleep(18000);
			//Click Cash radio button:
			WebElement Cashradio=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[10]/input[1]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", Cashradio);
			Cashradio.getText();
			System.out.println("Cashradio::"+Cashradio.getText());
			writeReport(LogStatus.PASS, "Payment Mode:::CASH");
			
			//Click Driver Paid Cash:
			WebElement DriverPaidCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[1]"));
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", DriverPaidCash);
			writeReport(LogStatus.PASS, "Driver Paid Cash clicked");
			
			//Clicking TechnicianAcceptedCash:
			WebElement TechAcceptedCash=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[3]/div[13]/input[2]"));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].click();", TechAcceptedCash);
			writeReport(LogStatus.PASS, "Technician Accepted Cash clicked");
			
			//Clicking UpdatePaymentStatus button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-payment-status/div[1]/div[4]/button")).click();
			Thread.sleep(20000);
			System.out.println("Update Payment Status ::::Passed");
			writeReport(LogStatus.PASS, "Update Payment Status clicked");
			WebConfiguration.TakePositiveSnapshot();
		}
	}
	
	public void OldUpdateRating(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside UpdateRating");
		System.out.println("farmerExcelInputData.getGiveRating():::"+farmerExcelInputData.getGiveRating());
		System.out.println("farmerExcelInputData.getReason():::"+farmerExcelInputData.getReason());
		Thread.sleep(8000);
		driver.findElement(By.partialLinkText("Upcoming")).click();
		Thread.sleep(13000);
		searchOrderID(farmerExcelInputData);
		try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(1000);
		
		String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
		System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Rating");
		writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Rating");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Rating button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATING RATING::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATING RATING::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
		
		//Clicking UpdateRating Link:
		driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[11]/a")).click();
		Thread.sleep(15000);
		
		List<WebElement> RatingList=driver.findElements(By.xpath("//*[@id=\"stars\"]/*"));
		System.out.println("RatingList.size():::"+RatingList.size());
		
		for (int i = 1; i <= RatingList.size(); i++) {
			String RatingListText = RatingList.get(i).getText();
			System.out.println("RatingListText:::" + i + ":::::" + RatingListText);
			int getGiverating=Integer.parseInt(farmerExcelInputData.getGiveRating());
			System.out.println("getGiverating::"+getGiverating);
			if(i==getGiverating) {
				driver.findElement(By.xpath("//*[@id=\"stars\"]/span["+ i +"]/img")).click();
				String GiveRatingText=driver.findElement(By.xpath("//*[@id=\"stars\"]/span["+ i +"]/img")).getText();
				System.out.println("Rating:::"+farmerExcelInputData.getGiveRating());
				writeReport(LogStatus.PASS, "Give Rating:::"+GiveRatingText+"::::Passed");
				
				break;
			}
		}
		//Clicking Submit Bill Button:
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-rating/div/div[4]/button")).click();
		Thread.sleep(10000);
	
		//What went wrong:
		List<WebElement> WhatWentWrongList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/*"));
		System.out.println("WhatWentWrongList.size():::"+WhatWentWrongList.size());
		
		for (int j=1; j<=WhatWentWrongList.size(); j++) {
			String WhatWentWrongListText = WhatWentWrongList.get(j).getText();
			System.out.println("WhatWentWrongListText:::" + j + ":::::" + WhatWentWrongListText);
			int getReason=Integer.parseInt(farmerExcelInputData.getReason());
			System.out.println("getReason:::"+getReason);
			if(j==getReason) {
				System.out.println("*******************************INSIDE IF*******************************");
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).click();
				String ReasonText=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).getText();
				System.out.println("Reason:::"+ReasonText);
				writeReport(LogStatus.PASS, "Reason:::"+ReasonText+"::::Passed");
				break;
			}
		}
		driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[4]/button")).click();
		Thread.sleep(10000);
		System.out.println("Update Rating ::::Passed");
		}}
		catch(Exception e) {
		System.out.println("Inside Pending- UpdateRating");
		driver.findElement(By.partialLinkText("Pending")).click();
		Thread.sleep(13000);
		
		String PendingActionsText=driver.findElement(By.partialLinkText("Pending")).getText();
		System.out.println("Clicked::"+PendingActionsText+"link in Update Rating");
		writeReport(LogStatus.PASS, "Clicked PendingActions Link -> Update Rating");
		
		//Search using OrderID:
		DriverWait("//*[@id='order']");
		driver.findElement(By.xpath("//*[@id='order']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='order']")).clear();
		driver.findElement(By.xpath("//*[@id='order']")).sendKeys(farmerExcelInputData.getOrderID());
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id='section-one']/div/form/div[2]/div[2]/button")).click();//Search
		Thread.sleep(18000);
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		String GetButtonText=driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).getText();
		System.out.println("GetButtonText::::"+GetButtonText);
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();
		writeReport(LogStatus.PASS, "Update Rating button clicked::::Passed");
		Thread.sleep(18000);
		
		//Job State & Job
		DriverWait("//*[@id='style-1']/*/*");
		List<WebElement> JobList=driver.findElements(By.xpath("//*[@id='style-1']/*/*"));
		
		System.out.println("JobList.size():::"+JobList.size());
		for (int i = 0; i <= JobList.size()-1; i++) {
			if((i==0)||(i==4)||(i==8)||(i==12)||(i==16)||(i==20)||(i==24)||(i==28)||(i==32)||(i==36)||(i==40)) {
					
			String JobListItemsText = JobList.get(i).getText();
			System.out.println("UPDATING RATING::Job State & Job:::"+ JobListItemsText);
			writeReport(LogStatus.PASS, "UPDATING RATING::Job State & Job:::" +JobListItemsText);
			}
			if((i==1)||(i==5)||(i==9)||(i==13)||(i==17)||(i==21)||(i==25)||(i==29)||(i==33)||(i==37)||(i==41)) {
					
				//||(i==2)||(i==3)||(i==5)||(i==2)||(i==3)||(i==5)
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Labour:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Labour:::" +JobListItemsText);
				}
			if((i==2)||(i==6)||(i==10)||(i==14)||(i==18)||(i==22)||(i==26)||(i==30)||(i==34)||(i==38)||(i==42)) {
				String JobListItemsText = JobList.get(i).getText();
				System.out.println("Parts:::" +JobListItemsText);
				writeReport(LogStatus.PASS, "Parts:::" +JobListItemsText);
			}
			
		}
				
		
		//Clicking UpdateRating Link:
				driver.findElement(By.xpath("//*[@id=\"second-row\"]/div[2]/div/ul/li[11]/a")).click();
				Thread.sleep(15000);
				
				List<WebElement> RatingList=driver.findElements(By.xpath("//*[@id=\"stars\"]/*"));
				System.out.println("RatingList.size():::"+RatingList.size());
				
				for (int i = 1; i <= RatingList.size(); i++) {
					String RatingListText = RatingList.get(i).getText();
					System.out.println("RatingListText:::" + i + ":::::" + RatingListText);
					int getGiverating=Integer.parseInt(farmerExcelInputData.getGiveRating());
					System.out.println("getGiverating::"+getGiverating);
					if(i==getGiverating) {
						driver.findElement(By.xpath("//*[@id=\"stars\"]/span["+ i +"]/img")).click();
						String GiveRatingText=driver.findElement(By.xpath("//*[@id=\"stars\"]/span["+ i +"]/img")).getText();
						System.out.println("Rating:::"+farmerExcelInputData.getGiveRating());
						writeReport(LogStatus.PASS, "Give Rating:::"+GiveRatingText+"::::Passed");
						
						break;
					}
				}
				//Clicking Submit Bill Button:
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-update-rating/div/div[4]/button")).click();
				Thread.sleep(10000);
			
				//What went wrong:
				List<WebElement> WhatWentWrongList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/*"));
				System.out.println("WhatWentWrongList.size():::"+WhatWentWrongList.size());
				
				for (int j=1; j<=WhatWentWrongList.size(); j++) {
					String WhatWentWrongListText = WhatWentWrongList.get(j).getText();
					System.out.println("WhatWentWrongListText:::" + j + ":::::" + WhatWentWrongListText);
					int getReason=Integer.parseInt(farmerExcelInputData.getReason());
					System.out.println("getReason:::"+getReason);
					if(j==getReason) {
						System.out.println("*******************************INSIDE IF*******************************");
						driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).click();
						String ReasonText=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[3]/div/label["+ j +"]")).getText();
						System.out.println("Reason:::"+ReasonText);
						writeReport(LogStatus.PASS, "Reason:::"+ReasonText+"::::Passed");
						break;
					}
				}
				driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-submit-rating/div/div[4]/button")).click();
				Thread.sleep(10000);
				System.out.println("Update Rating ::::Passed");
	}
	}

	//*****************************************************************************************************************************
	//Without Parts:
	//-------------
	public void BDAddJobsWithoutParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException{
		System.out.println("Inside BDAddJobsWithoutParts");
		
		searchOrderID(farmerExcelInputData);
	try {
		if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
			Thread.sleep(3000);
			String PendingActionText=driver.findElement(By.partialLinkText("Pending")).getText();
			System.out.println("Clicked::"+PendingActionText+"link in Update Estimate");
			writeReport(LogStatus.PASS, "Clicked PendingAction Link -> Update Estimate State");
			AddJobsWithoutParts(farmerExcelInputData);
		}}catch(Exception e){
		
			driver.findElement(By.partialLinkText("Upcoming")).click();
			Thread.sleep(5000);
			
			searchOrderID(farmerExcelInputData);
			if(driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[1]/div/div/div/h4")).isDisplayed()){
				Thread.sleep(3000);
				String UpcomingOrdersText=driver.findElement(By.partialLinkText("Upcoming")).getText();
				System.out.println("Clicked::"+UpcomingOrdersText+"link in Update Estimate");
				writeReport(LogStatus.PASS, "Clicked UpcomingOrders Link -> Update Estimate State");
				AddJobsWithoutParts(farmerExcelInputData);
			}
		}
		
		
	}
	
	public void AddJobsWithoutParts(FarmerExcelInputData farmerExcelInputData) throws InterruptedException {
		System.out.println("Inside BDAddJobsWithoutParts");
		
		DriverWait("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button");
		driver.findElement(By.xpath("//*[@id='section-two']/div/div/div[1]/div[3]/div/div/div/div[1]/div[1]/button")).click();//Update Estimate
		writeReport(LogStatus.PASS, "Update Estimate button clicked::::Passed");
		Thread.sleep(5000);
		
		//Click AddJobs:
		Thread.sleep(8000);
		driver.findElement(By.xpath("//*[@id='second-row']/div[2]/div/ul/li[1]/a")).click(); 
		DriverWait("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div");
		Thread.sleep(10000);
		
			String expected = farmerExcelInputData.getFarmerJobs();	
			if(expected.contains(",")) {
			String[] outerArray = expected.split(",");
			int outerArrayLength = outerArray.length;
			System.out.println("OuterArrayLength:::" + outerArrayLength);
			int checkcount = 0;
			int count =0;
			for (String OuterArrayValues1 : outerArray){
				System.out.println(OuterArrayValues1);
				System.out.println("count:::"+count);
				
				List<WebElement> JobList=driver.findElements(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr/td[1]/select/*"));
				System.out.println("JobList.size():::"+JobList.size());
				
				for (int i = 1; i <= JobList.size(); i++) {
					String JobListText = JobList.get(i).getText();
					System.out.println("JobListText:::" + i + ":::::" + JobListText);
				    int result=Integer.parseInt(OuterArrayValues1);
				    System.out.println("result:::" + result);
				 
				    
					if(result==i){
					count = count+1;
					System.out.println("count value:::"+count);
					System.out.println("OuterArrayValues1::::" + OuterArrayValues1 + "Passed");
					System.out.println("i value::::" + i + "Passed");
					WebElement SelJob=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[1]/select"));
					SelJob.click();  
					Select oSelect = new Select(SelJob);
					oSelect.selectByIndex(Integer.parseInt(OuterArrayValues1));
					
								
					Thread.sleep(1000);
					WebElement LabourAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[2]/input"));
					LabourAmt.click();								
					LabourAmt.sendKeys(farmerExcelInputData.getFarmerLabAmt());
					writeReport(LogStatus.PASS, "Labour Amount::::" + farmerExcelInputData.getFarmerLabAmt() + "  entered::::Passed");
					
					/*WebElement PartsAmt=driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/table/tbody/tr["+count+"]/td[3]/input"));
					PartsAmt.click();
					PartsAmt.sendKeys(farmerExcelInputData.getBDPartsAmount());
					writeReport(LogStatus.PASS, "Parts Amount::::" + PartsAmt + "  entered::::Passed");*/
					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[3]/a/span";
						driver.findElement(By.xpath(addJobButton)).click();
						break;
						
					}else{
						break;
						}
					
					}
				}
			}
			
		}//Save button:
			driver.findElement(By.xpath("/html/body/modal-overlay/bs-modal-container/div/div/app-add-jobs/div/div[4]/button[2]")).click();
			Thread.sleep(10000);
			writeReport(LogStatus.PASS, "Jobs added without Parts:::::Passed");
			System.out.println("Add Jobs Without Parts::::Passed");
	}
			
			
		



}//Last brace

	


package com.servicemandi.webapps.workflows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

import org.springframework.util.FileSystemUtils;

import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.webapps.WebAppWorkFlows;
import com.servicemandi.workflow.SendMailAttachment;

public class WebAppUi extends WebAppWorkFlows{

	private static final int EXIT_ON_CLOSE = 0;
	ImageIcon ii;
	private static JFrame frame;
	JPanel centerPanel = new JPanel();
	JPanel footerPanel = new JPanel();
	JPanel belowfooterPanel = new JPanel();

	final JLabel copyRightLabel = new JLabel("Copyrights@ServiceMandi_2018, All rights reserved");

	final JTextArea textArea = new JTextArea("BreakDown Flow Status");
	JComboBox<String> FlowStatus = new JComboBox<String>();
	JCheckBox selectDeselect = new JCheckBox("Select/DeSelect All CheckBox");
	JComboBox<String> bDBaseFlow = new JComboBox<String>();
	JCheckBox wf1;
	//BreakDownFlow1 b1;
	JList places;
	//JLabel SMworkFlowLabel = new JLabel("AllCheckBox::");
	JLabel JL1 = new JLabel();
	JLabel JL2 = new JLabel();
	JLabel JL3 = new JLabel();
	JLabel JL4 = new JLabel();
	JLabel JL11 = new JLabel("<html><u><font color='black'>HUNTER FLOWS</font></u></html>");
	JLabel JL14 = new JLabel();
	JLabel JL15 = new JLabel();
	JLabel JL5 = new JLabel();
	JLabel JL6 = new JLabel();
	JLabel JL7 = new JLabel();
	JLabel JL8 = new JLabel();
	JLabel JL81 = new JLabel();
	JLabel JL82 = new JLabel();
	JLabel JL83 = new JLabel();
	JLabel JL9 = new JLabel("<html><u><font color='black'>FARMER FLOWS</font></u></html>");

	JLabel JL10 = new JLabel();
	JLabel JL12 = new JLabel("<html><font color='black'> FlowStatus :</font><size=20></html>");
	JLabel JL13 = new JLabel();
	
	
/*	JLabel JL51 = new JLabel();
	JLabel JL52 = new JLabel();
	JLabel JL15 = new JLabel();*/
	
	// HUNTER-BD Flows Checkbox
	
	JCheckBox HBD1 = new JCheckBox("CreateOnlineBDOrder");
	JCheckBox HBD2 = new JCheckBox("CreateNewFMBDFlow");//State Code
	JCheckBox HBD3 = new JCheckBox("CreateNewVehicleBDFlow");
	JCheckBox HBD4 = new JCheckBox("CreateNewDriverBDFlow");
	JCheckBox HBD5 = new JCheckBox("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow");
	JCheckBox HBD6 = new JCheckBox("DeleteBDOrderAtConfirmOrder");
	JCheckBox HBD7 = new JCheckBox("DeleteBDOrderAtAssignOrder");
	JCheckBox HBD8 = new JCheckBox("DeleteBDOrderAtConfirmWorkshop");
	JCheckBox HBD9 = new JCheckBox("DeleteBDOrderAtConfirmVehicle");
	
	JCheckBox HBD10 = new JCheckBox("VerifyMandatoryAlertsBDFlowCreateNewFM");
	JCheckBox HBD11 = new JCheckBox("CreateOnlineBDOrderAddNotes");
	JCheckBox HBD12 = new JCheckBox("FieldLevelValidations");
	JCheckBox HBD13 = new JCheckBox("OrderIdProcessing");
	JCheckBox HBD14 = new JCheckBox("CreateOnlineBDOrderVerifyDetailsinResources");
	JCheckBox HBD15 = new JCheckBox("CreateOnlineBDOrderVerifyWorkshopLocatorinResources");
	JCheckBox HBD16 = new JCheckBox("CreateOnlineBDOrderVerifyPriceEstimatorinResources");
	
	
	//HUNTER-SR FLows Checkbox
	JCheckBox HSR1 = new JCheckBox("CreateOnlineSROrder");
	JCheckBox HSR2 = new JCheckBox("CreateNewFMSRFlow");
	JCheckBox HSR3 = new JCheckBox("CreateNewVehicleSRFlow");
	JCheckBox HSR4 = new JCheckBox("CreateNewDriverSRFlow");
	JCheckBox HSR5 = new JCheckBox("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow");
	JCheckBox HSR6 = new JCheckBox("DeleteSROrderAtConfirmOrder");
	JCheckBox HSR7 = new JCheckBox("DeleteSROrderAtConfirmVehicle");
	JCheckBox HSR8 =new JCheckBox("SRSelectWorkshopDelAtConfirmVehicleinWorkshp");
	JCheckBox HSR9 =new JCheckBox("DeleteSROrderAtAssignOrder");
	JCheckBox HSR10 =new JCheckBox("CreateSROrderIDontKnowWhatsWrong");
	
	//FARMER-BD FLows Checkbox
	JCheckBox FBD1 =new JCheckBox("FarmerBDWithPartsProcessing");
	JCheckBox FBD2 =new JCheckBox("FarmerBDWithoutPartsProcessing");
	JCheckBox FBD3 =new JCheckBox("FarmerBDWithPartsNoRetailer");
	JCheckBox FBD4 =new JCheckBox("FRBDWithPartsRevEstWithParts");
	JCheckBox FBD5 =new JCheckBox("FRBDWithOutPartsRevEstWithParts");
	JCheckBox FBD6 =new JCheckBox("FRBDWithPartsRevEstWithOutParts");
	JCheckBox FBD7 =new JCheckBox("FRBDWithOutPartsRevEstWithOutParts");
	JCheckBox FBD8 =new JCheckBox("FRBDWithPartsNoRetailerRevEstWithParts"); 
	JCheckBox FBD9 =new JCheckBox("FRBDWithPartsRevEstWithPartsNoRetailer");
	JCheckBox FBD10 =new JCheckBox("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer");
	JCheckBox FBD11 =new JCheckBox("FRBDWithOutPartsRevEstWithPartsNoRetailer");
	JCheckBox FBD12 =new JCheckBox("FRBDWithPartsNoRetailerRevEstWithOutParts");
	JCheckBox FBD13 =new JCheckBox("BDAutoInvoice");
	JCheckBox FBD14 =new JCheckBox("FRReallocateRetailer");
	
	
	
	//FARMER-SR FLows Checkbox
	JCheckBox FSR1 =new JCheckBox("FarmerSROrderProcessing");
	
	
	protected String newWorkFlow;
	

	public static void main(String[] args) throws Exception {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				new WebAppUi();
			}
		});
	}

	public WebAppUi() {
		initComponents();
	}

	void initComponents() {
		try {
			frame = new JFrame("main");
			frame.setTitle("SMANDI-WEB APPS-Automation");
			JPanel contentPane = new JPanel();
			SendMailAttachment sm= new SendMailAttachment();
			contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			contentPane.setLayout(new BorderLayout(5, 5));
			centerPanel.setLayout(new GridLayout(0, 3, 4, 4));
			footerPanel.setLayout(new GridLayout(0, 14, 4, 4));
			
			JButton runWorkFlow = new JButton("RunFlows");

			//JButton runWorkFlow = new JButton("Execute");
			//MultipleWorkflows
			//SendEmailMultipleWorkflows
			/*JButton sendMultipleEmail = new JButton("Zip&Email");
			sendMultipleEmail.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					try {
						sm.SendMailAttachment(newWorkFlow);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});*/
			/*JButton saveReport = new JButton("SaveReport");
			saveReport.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					Configuration.saveReport();
				}
			});*/

			/*JButton uploadExcelSheet = new JButton("UploadExcel");
			uploadExcelSheet.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					File source = new File(
							"D:\\SM_EXE\\Service_Mandi_Input_new.xls");
					File dest = new File("D:\\Myfolder\\Service_Mandi_Input_new.xls"); //D:\\SM_EXE\\Service_Mandi_Input_new.xls
					try {
						// FileUtils.copyDirectory(source, dest);
						FileSystemUtils.copyRecursively(source, dest);
						} catch (IOException e1) {
						e1.printStackTrace();
					}

				}
			});*/
			
			Vector v = new Vector();
			v.add("Hunter BD Flows");
			//v.add(BDAll);
			v.add(HBD1);
			v.add(HBD2);
			v.add(HBD3);
			v.add(HBD4);
			v.add(HBD5);
			v.add(HBD6);
			v.add(HBD7);
			v.add(HBD8);
			v.add(HBD9);
			v.add(HBD10);
			v.add(HBD11);
			v.add(HBD12);
			v.add(HBD13);
			v.add(HBD14);
			v.add(HBD15);
			v.add(HBD16);
			
			Vector v1 = new Vector();
			v1.add("Hunter SR Flows");
			v1.add(HSR1);
			v1.add(HSR2);
			v1.add(HSR3);
			v1.add(HSR4);
			v1.add(HSR5);
			v1.add(HSR6);
			v1.add(HSR7);
			v1.add(HSR8);
			v1.add(HSR9);
			v1.add(HSR10);
		
			Vector v2 = new Vector();
			v2.add("Farmer SR Flows");
			//v2.add(FSR1);
			
			Vector v3 = new Vector();
			v3.add("Farmer BD Flows");
			v3.add(FBD1);
			v3.add(FBD2);
			v3.add(FBD3);
			v3.add(FBD4);
			v3.add(FBD5);
			v3.add(FBD6);
			v3.add(FBD7);
			v3.add(FBD8);
			v3.add(FBD9);
			v3.add(FBD10);
			v3.add(FBD11);
			v3.add(FBD12);
			v3.add(FBD13);
			v3.add(FBD14);


			//--------------------------------------------BDBase Flow-----------------------------------------------------

			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD1.isSelected()) {
						try {
							CreateOnlineBDOrder();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineBDOrder Completed");
						textArea.setText("CreateOnlineBDOrder Completed");
						HBD1.setToolTipText("CreateOnlineBDOrder Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD2.isSelected()) {
						try {
							CreateNewFMBDFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewFMBDFlow Completed");
						textArea.setText("CreateNewFMBDFlow Completed");
						HBD2.setToolTipText("CreateNewFMBDFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD3.isSelected()) {
						try {
							CreateNewVehicleBDFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewVehicleBDFlow Completed");
						textArea.setText("CreateNewVehicleBDFlow Completed");
						HBD3.setToolTipText("CreateNewVehicleBDFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD4.isSelected()) {
						try {
							CreateNewDriverBDFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewDriverBDFlow Completed");
						textArea.setText("CreateNewDriverBDFlow Completed");
						HBD4.setToolTipText("CreateNewDriverBDFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD5.isSelected()) {
						try {
							CreateOrderExistingFMWithNewVehicleNewDriverBDFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow Completed");
						textArea.setText("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow Completed");
						HBD5.setToolTipText("CreateOrderExistingFMWithNewVehicleNewDriverBDFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD6.isSelected()) {
						try {
							DeleteBDOrderAtConfirmOrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteBDOrderAtAssignOrder Completed");
						textArea.setText("DeleteBDOrderAtAssignOrder Completed");
						HBD6.setToolTipText("DeleteBDOrderAtAssignOrder Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD7.isSelected()) {
						try {
							DeleteBDOrderAtAssignOrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteBDOrderAtAssignOrder Completed");
						textArea.setText("DeleteBDOrderAtAssignOrder Completed");
						HBD7.setToolTipText("DeleteBDOrderAtAssignOrder Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD8.isSelected()) {
						try {
							DeleteBDOrderAtConfirmWorkshop();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteBDOrderAtConfirmWorkshop Completed");
						textArea.setText("DeleteBDOrderAtConfirmWorkshop Completed");
						HBD8.setToolTipText("DeleteBDOrderAtConfirmWorkshop Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD9.isSelected()) {
						try {
							DeleteBDOrderAtConfirmVehicle();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteBDOrderAtConfirmVehicle Completed");
						textArea.setText("DeleteBDOrderAtConfirmVehicle Completed");
						HBD9.setToolTipText("DeleteBDOrderAtConfirmVehicle Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD10.isSelected()) {
						try {
							VerifyMandatoryAlertsBDFlowCreateNewFM();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("VerifyMandatoryAlertsBDFlowCreateNewFM Completed");
						textArea.setText("VerifyMandatoryAlertsBDFlowCreateNewFM Completed");
						HBD10.setToolTipText("VerifyMandatoryAlertsBDFlowCreateNewFM Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD11.isSelected()) {
						try {
							CreateOnlineBDOrderAddNotes();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineBDOrderAddNotes Completed");
						textArea.setText("CreateOnlineBDOrderAddNotes Completed");
						HBD11.setToolTipText("CreateOnlineBDOrderAddNotes Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD12.isSelected()) {
						try {
							FieldLevelValidations();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FieldLevelValidations Completed");
						textArea.setText("FieldLevelValidations Completed");
						HBD12.setToolTipText("FieldLevelValidations Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD13.isSelected()) {
						try {
							OrderIdProcessing();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("OrderIdProcessing Completed");
						textArea.setText("OrderIdProcessing Completed");
						HBD13.setToolTipText("OrderIdProcessing Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD14.isSelected()) {
						try {
							CreateOnlineBDOrderVerifyDetailsinResources();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineBDOrderVerifyDetailsinResources Completed");
						textArea.setText("CreateOnlineBDOrderVerifyDetailsinResources Completed");
						HBD14.setToolTipText("CreateOnlineBDOrderVerifyDetailsinResources Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD15.isSelected()) {
						try {
							CreateOnlineBDOrderVerifyWorkshopLocatorinResources();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineBDOrderVerifyWorkshopLocatorinResources Completed");
						textArea.setText("CreateOnlineBDOrderVerifyWorkshopLocatorinResources Completed");
						HBD15.setToolTipText("CreateOnlineBDOrderVerifyWorkshopLocatorinResources Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HBD16.isSelected()) {
						try {
							CreateOnlineBDOrderVerifyPriceEstimatorinResources();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineBDOrderVerifyPriceEstimatorinResources Completed");
						textArea.setText("CreateOnlineBDOrderVerifyPriceEstimatorinResources Completed");
						HBD16.setToolTipText("CreateOnlineBDOrderVerifyPriceEstimatorinResources Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
//===================================SR Flows==================================================
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR1.isSelected()) {
						try {
							CreateOnlineSROrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOnlineSROrder Completed");
						textArea.setText("CreateOnlineSROrder Completed");
						HSR1.setToolTipText("CreateOnlineSROrder Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR2.isSelected()) {
						try {
							CreateNewFMSRFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewFMSRFlow Completed");
						textArea.setText("CreateNewFMSRFlow Completed");
						HSR2.setToolTipText("CreateNewFMSRFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR3.isSelected()) {
						try {
							CreateNewVehicleSRFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewVehicleSRFlow Completed");
						textArea.setText("CreateNewVehicleSRFlow Completed");
						HSR3.setToolTipText("CreateNewVehicleSRFlow Completed");
						
					}else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) { 
					if (HSR4.isSelected()) {
						try {
							CreateNewDriverSRFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateNewDriverSRFlow Completed");
						textArea.setText("CreateNewDriverSRFlow Completed");
						HSR4.setToolTipText("CreateNewDriverSRFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR5.isSelected()) {
						try {
							CreateOrderExistingFMWithNewVehicleNewDriverSRFlow();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow Completed");
						textArea.setText("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow Completed");
						HSR5.setToolTipText("CreateOrderExistingFMWithNewVehicleNewDriverSRFlow Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR6.isSelected()) {
						try {
							DeleteSROrderAtConfirmOrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteSROrderAtConfirmOrder Completed");
						textArea.setText("DeleteSROrderAtConfirmOrder Completed");
						HSR6.setToolTipText("DeleteSROrderAtConfirmOrder Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR7.isSelected()) {
						try {
							DeleteSROrderAtConfirmVehicle();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteSROrderAtConfirmVehicle Completed");
						textArea.setText("DeleteSROrderAtConfirmVehicle Completed");
						HSR7.setToolTipText("DeleteSROrderAtConfirmVehicle Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR8.isSelected()) {
						try {
							SRSelectWorkshopDelAtConfirmVehicleinWorkshop();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("SRSelectWorkshopDelAtConfirmVehicleinWorkshop Completed");
						textArea.setText("SRSelectWorkshopDelAtConfirmVehicleinWorkshop Completed");
						HSR8.setToolTipText("SRSelectWorkshopDelAtConfirmVehicleinWorkshop Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
				
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR9.isSelected()) {
						try {
							DeleteSROrderAtAssignOrder();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("DeleteSROrderAtAssignOrder Completed");
						textArea.setText("DeleteSROrderAtAssignOrder Completed");
						HSR9.setToolTipText("DeleteSROrderAtAssignOrder Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (HSR10.isSelected()) {
						try {
							CreateSROrderIDontKnowWhatsWrong();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("CreateSROrderIDontKnowWhatsWrong Completed");
						textArea.setText("CreateSROrderIDontKnowWhatsWrong Completed");
						HSR10.setToolTipText("CreateSROrderIDontKnowWhatsWrong Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
//================FARMER BD-FLOWS===============================================================================================================
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD1.isSelected()) {
						try {
							FarmerBDWithPartsProcessing();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FarmerBDOrderProcessing Completed");
						textArea.setText("FarmerBDOrderProcessing Completed");
						FBD1.setToolTipText("FarmerBDOrderProcessing Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD2.isSelected()) {
						try {
							FarmerBDWithoutPartsProcessing();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FarmerBDWithoutPartsProcessing Completed");
						textArea.setText("FarmerBDWithoutPartsProcessing Completed");
						FBD1.setToolTipText("FarmerBDWithoutPartsProcessing Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD3.isSelected()) {
						try {
							FarmerBDWithPartsNoRetailer();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FarmerBDWithPartsNoRetailer Completed");
						textArea.setText("FarmerBDWithPartsNoRetailer Completed");
						FBD3.setToolTipText("FarmerBDWithPartsNoRetailer Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD4.isSelected()) {
						try {
							FRBDWithPartsRevEstWithParts();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsRevEstWithParts Completed");
						textArea.setText("FRBDWithPartsRevEstWithParts Completed");
						FBD4.setToolTipText("FRBDWithPartsRevEstWithParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD5.isSelected()) {
						try {
							FRBDWithOutPartsRevEstWithParts();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithOutPartsRevEstWithParts Completed");
						textArea.setText("FRBDWithOutPartsRevEstWithParts Completed");
						FBD5.setToolTipText("FRBDWithOutPartsRevEstWithParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD6.isSelected()) {
						try {
							FRBDWithPartsRevEstWithOutParts();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsRevEstWithOutParts Completed");
						textArea.setText("FRBDWithPartsRevEstWithOutParts Completed");
						FBD6.setToolTipText("FRBDWithPartsRevEstWithOutParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD7.isSelected()) {
						try {
							FRBDWithOutPartsRevEstWithOutParts();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithOutPartsRevEstWithOutParts Completed");
						textArea.setText("FRBDWithOutPartsRevEstWithOutParts Completed");
						FBD7.setToolTipText("FRBDWithOutPartsRevEstWithOutParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});	
			
			
			
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD8.isSelected()) {
						try {
							FRBDWithPartsNoRetailerRevEstWithParts();
						
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsNoRetailerRevEstWithParts Completed");
						textArea.setText("FRBDWithPartsNoRetailerRevEstWithParts Completed");
						FBD8.setToolTipText("FRBDWithPartsNoRetailerRevEstWithParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD9.isSelected()) {
						try {
							FRBDWithPartsRevEstWithPartsNoRetailer();
						
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsRevEstWithPartsNoRetailer Completed");
						textArea.setText("FRBDWithPartsRevEstWithPartsNoRetailer Completed");
						FBD9.setToolTipText("FRBDWithPartsRevEstWithPartsNoRetailer Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD10.isSelected()) {
						try {
							FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer();
						
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer Completed");
						textArea.setText("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer Completed");
						FBD10.setToolTipText("FRBDWithPartsNoRetailerRevEstWithPartsNoRetailer Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD11.isSelected()) {
						try {
							FRBDWithOutPartsRevEstWithPartsNoRetailer();
						
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithOutPartsRevEstWithPartsNoRetailer Completed");
						textArea.setText("FRBDWithOutPartsRevEstWithPartsNoRetailer Completed");
						FBD11.setToolTipText("FRBDWithOutPartsRevEstWithPartsNoRetailer Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
						
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD12.isSelected()) {
						try {
							FRBDWithPartsNoRetailerRevEstWithOutParts();
						
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDWithPartsNoRetailerRevEstWithOutParts Completed");
						textArea.setText("FRBDWithPartsNoRetailerRevEstWithOutParts Completed");
						FBD12.setToolTipText("FRBDWithPartsNoRetailerRevEstWithOutParts Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD13.isSelected()) {
						try {
							FRBDAutoInvoice();
						
						} catch (Exception e) {
						
							e.printStackTrace();
						}
						FlowStatus.addItem("FRBDAutoInvoice Completed");
						textArea.setText("FRBDAutoInvoice Completed");
						FBD13.setToolTipText("FRBDAutoInvoice Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
			runWorkFlow.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (FBD14.isSelected()) {
						try {
							FRReallocateRetailer();
						
						} catch (Exception e) {
						
							e.printStackTrace();
						}
						FlowStatus.addItem("FRReallocateRetailer Completed");
						textArea.setText("FRReallocateRetailer Completed");
						FBD14.setToolTipText("FRReallocateRetailer Completed");
						
					} else {
						//System.out.println("BreakDownWithParts method unselected");
					}
				}
			});
// -----------------------------------------------------------------------------------------------------------------------------------
			/*BDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == BDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (BDAll.isSelected() == true) {
							BD1.setSelected(true);
							BD2.setSelected(true);
							BD3.setSelected(true);
							BD4.setSelected(true);
							BD5.setSelected(true);
							BD6.setSelected(true);
							BD7.setSelected(true);
							BD8.setSelected(true);
							BD9.setSelected(true);
							BD10.setSelected(true);
							BD11.setSelected(true);
							BD12.setSelected(true);
							BD13.setSelected(true);
							BD14.setSelected(true);
						} else {
							BD1.setSelected(false);
							BD2.setSelected(false);
							BD3.setSelected(false);
							BD4.setSelected(false);
							BD5.setSelected(false);
							BD6.setSelected(false);
							BD7.setSelected(false);
							BD8.setSelected(false);
							BD9.setSelected(false);
							BD10.setSelected(false);
							BD11.setSelected(false);
							BD12.setSelected(false);
							BD13.setSelected(false);
							BD14.setSelected(false);
						}
					}
				}
			});
			
			BDCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == BDCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (BDCRAll.isSelected() == true) {
							BDCR1.setSelected(true);
							BDCR2.setSelected(true);
							BDCR3.setSelected(true);
							BDCR4.setSelected(true);
							BDCR5.setSelected(true);
							BDCR6.setSelected(true);
							BDCR7.setSelected(true);
							BDCR8.setSelected(true);
							BDCR9.setSelected(true);

						} else {
							BDCR1.setSelected(false);
							BDCR2.setSelected(false);
							BDCR3.setSelected(false);
							BDCR4.setSelected(false);
							BDCR5.setSelected(false);
							BDCR6.setSelected(false);
							BDCR7.setSelected(false);
							BDCR8.setSelected(false);
							BDCR9.setSelected(false);

						}
					}
				}
			});
			NBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NBDAll.isSelected() == true) {
							NBD1.setSelected(true);
							NBD2.setSelected(true);
							NBD3.setSelected(true);
							NBD4.setSelected(true);
						} else {
							NBD1.setSelected(false);
							NBD2.setSelected(false);
							NBD3.setSelected(false);
							NBD4.setSelected(false);
						}
					}
				}
			});

			SRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == SRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (SRAll.isSelected() == true) {
							SR1.setSelected(true);
							SR2.setSelected(true);
							SR3.setSelected(true);
							//SR4.setSelected(true);
							SR5.setSelected(true);
							SR6.setSelected(true);
							SR7.setSelected(true);
							SR8.setSelected(true);
							SR9.setSelected(true);
							SR10.setSelected(true);
							SR11.setSelected(true);
							SR12.setSelected(true);

						} else {
							SR1.setSelected(false);
							SR2.setSelected(false);
							SR3.setSelected(false);
							//SR4.setSelected(false);
							SR5.setSelected(false);
							SR6.setSelected(false);
							SR7.setSelected(false);
							SR8.setSelected(false);
							SR9.setSelected(false);
							SR10.setSelected(false);
							SR11.setSelected(false);
							SR12.setSelected(false);
						}
					}
				}
			});
			SRCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == SRCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (SRCRAll.isSelected() == true) {
							SRCR1.setSelected(true);
							SRCR2.setSelected(true);
							SRCR3.setSelected(true);
							SRCR4.setSelected(true);
							SRCR5.setSelected(true);
							SRCR6.setSelected(true);
							SRCR7.setSelected(true);
							SRCR8.setSelected(true);
							SRCR9.setSelected(true);
							SRCR10.setSelected(true);
							SRCR11.setSelected(true);
							SRCR12.setSelected(true);
						} else {
							SRCR1.setSelected(false);
							SRCR2.setSelected(false);
							SRCR3.setSelected(false);
							SRCR4.setSelected(false);
							SRCR5.setSelected(false);
							SRCR6.setSelected(false);
							SRCR7.setSelected(false);
							SRCR8.setSelected(false);
							SRCR9.setSelected(false);
							SRCR10.setSelected(false);
							SRCR11.setSelected(false);
							SRCR12.setSelected(false);
						}
					}
				}
			});
			NSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NSRAll.isSelected() == true) {
							NSR1.setSelected(true);
							NSR2.setSelected(true);
							NSR3.setSelected(true);
							NSR4.setSelected(true);
							NSR5.setSelected(true);
							NSR6.setSelected(true);
						} else {
							NSR1.setSelected(false);
							NSR2.setSelected(false);
							NSR3.setSelected(false);
							NSR4.setSelected(false);
							NSR5.setSelected(false);
							NSR6.setSelected(false);
						}
					}
				}
			});

			DBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DBDAll.isSelected() == true) {
							DBD1.setSelected(true);
							DBD2.setSelected(true);
							DBD3.setSelected(true);
							DBD4.setSelected(true);
							DBD5.setSelected(true);
							DBD6.setSelected(true);
							DBD7.setSelected(true);
							DBD8.setSelected(true);
							DBD9.setSelected(true);
							DBD10.setSelected(true);
							DBD11.setSelected(true);
							DBD12.setSelected(true);
							DBD13.setSelected(true);

						} else {
							DBD1.setSelected(false);
							DBD2.setSelected(false);
							DBD3.setSelected(false);
							DBD4.setSelected(false);
							DBD5.setSelected(false);
							DBD6.setSelected(false);
							DBD7.setSelected(false);
							DBD8.setSelected(false);
							DBD9.setSelected(false);
							DBD10.setSelected(false);
							DBD11.setSelected(false);
							DBD12.setSelected(false);
							DBD13.setSelected(false);
						}
					}
				}
			});
			DBDCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DBDCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DBDCRAll.isSelected() == true) {
							DBDCR1.setSelected(true);
							DBDCR2.setSelected(true);
							DBDCR3.setSelected(true);
							DBDCR4.setSelected(true);
							DBDCR5.setSelected(true);
							DBDCR6.setSelected(true);
							DBDCR7.setSelected(true);
							DBDCR8.setSelected(true);

						} else {
							DBDCR1.setSelected(false);
							DBDCR2.setSelected(false);
							DBDCR3.setSelected(false);
							DBDCR4.setSelected(false);
							DBDCR5.setSelected(false);
							DBDCR6.setSelected(false);
							DBDCR7.setSelected(false);
							DBDCR8.setSelected(false);
						}
					}
				}
			});
			NDBDAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NDBDAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NDBDAll.isSelected() == true) {
							NDBD1.setSelected(true);
							NDBD2.setSelected(true);
							NDBD3.setSelected(true);
							NDBD4.setSelected(true);

						} else {
							NDBD1.setSelected(false);
							NDBD2.setSelected(false);
							NDBD3.setSelected(false);
							NDBD4.setSelected(false);
						}
					}
				}
			});

			DSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DSRAll.isSelected() == true) {
							DSR1.setSelected(true);
							DSR2.setSelected(true);
							DSR3.setSelected(true);
							DSR4.setSelected(true);
							DSR5.setSelected(true);
							DSR6.setSelected(true);
							DSR7.setSelected(true);
							DSR8.setSelected(true);
							//DSR9.setSelected(true);
							DSR10.setSelected(true);
							DSR11.setSelected(true);
							DSR12.setSelected(true);

						} else {
							DSR1.setSelected(false);
							DSR2.setSelected(false);
							DSR3.setSelected(false);
							DSR4.setSelected(false);
							DSR5.setSelected(false);
							DSR6.setSelected(false);
							DSR7.setSelected(false);
							DSR8.setSelected(false);
							//DSR9.setSelected(false);
							DSR10.setSelected(false);
							DSR11.setSelected(false);
							DSR12.setSelected(false);
						}
					}
				}
			});
			DSRCRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == DSRCRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (DSRCRAll.isSelected() == true) {
							DSRCR1.setSelected(true);
							DSRCR2.setSelected(true);
							DSRCR3.setSelected(true);
							DSRCR4.setSelected(true);
							DSRCR5.setSelected(true);
							DSRCR6.setSelected(true);
							DSRCR7.setSelected(true);
							DSRCR8.setSelected(true);
							DSRCR9.setSelected(true);
							DSRCR10.setSelected(true);
						} else {
							DSRCR1.setSelected(false);
							DSRCR2.setSelected(false);
							DSRCR3.setSelected(false);
							DSRCR4.setSelected(false);
							DSRCR5.setSelected(false);
							DSRCR6.setSelected(false);
							DSRCR7.setSelected(false);
							DSRCR8.setSelected(false);
							DSRCR9.setSelected(false);
							DSRCR10.setSelected(false);
						}
					}
				}
			});
			NDSRAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NDSRAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NDSRAll.isSelected() == true) {
							NDSR1.setSelected(true);
							NDSR2.setSelected(true);
							NDSR3.setSelected(true);
							NDSR4.setSelected(true);
							NDSR5.setSelected(true);
						} else {
							NDSR1.setSelected(false);
							NDSR2.setSelected(false);
							NDSR3.setSelected(false);
							NDSR4.setSelected(false);
							NDSR5.setSelected(false);
						}
					}
				}
			});

			NEGAll.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == NEGAll) {
						// Set all JCheckBox to select when JCheckBox a is
						// select
						if (NEGAll.isSelected() == true) {
							Neg1.setSelected(true);
							Neg2.setSelected(true);
							Neg3.setSelected(true);
							//Neg4.setSelected(true);
							//Neg5.setSelected(true);
						} else {
							Neg1.setSelected(false);
							Neg2.setSelected(false);
							Neg3.setSelected(false);
							//Neg4.setSelected(false);
							//Neg5.setSelected(false);
						}
					}
				}
			});*/
			
			//centerPanel.add(JL1);
			//centerPanel.add(JL2);
			//centerPanel.add(JL3);
			centerPanel.add(JL11);
			centerPanel.add(JL14);
			centerPanel.add(JL15);
			//centerPanel.add(JL5);
			centerPanel.add(new JComboCheckBox(v1));
			
			
			centerPanel.add(new JComboCheckBox(v));
			centerPanel.add(JL82);
			//centerPanel.add(JL81);
			/*centerPanel.add(new JComboCheckBox(v2));
			centerPanel.add(new JComboCheckBox(v10));
			centerPanel.add(new JComboCheckBox(v8));
			centerPanel.add(new JComboCheckBox(v5));
			centerPanel.add(new JComboCheckBox(v13));
			centerPanel.add(new JComboCheckBox(v14));
			centerPanel.add(new JComboCheckBox(v16));*/
			/*centerPanel.add(JL51);
			centerPanel.add(JL52);*/
			centerPanel.add(JL9);
			centerPanel.add(JL4);
			//centerPanel.add(JL6);
			centerPanel.add(JL7);
			centerPanel.add(new JComboCheckBox(v2));
			//centerPanel.add(JL10);
			centerPanel.add(new JComboCheckBox(v3));
		
			
			
			/*centerPanel.add(new JComboCheckBox(v9));
			centerPanel.add(new JComboCheckBox(v7));
			centerPanel.add(new JComboCheckBox(v4));
			centerPanel.add(new JComboCheckBox(v11));
			centerPanel.add(new JComboCheckBox(v12));
			centerPanel.add(new JComboCheckBox(v6));
			centerPanel.add(new JComboCheckBox(v15));*/
			//centerPanel.add(new JComboCheckBox(v16));
			//centerPanel.add(new JComboCheckBox(v17));
			
			contentPane.add(centerPanel, BorderLayout.PAGE_START);
			
			//footerPanel.add(SMworkFlowLabel);
			/*footerPanel.add(SRAll);
			footerPanel.add(SRCRAll);
			footerPanel.add(NSRAll);
			footerPanel.add(DSRAll);
			footerPanel.add(DSRCRAll);
			footerPanel.add(NDSRAll);
			footerPanel.add(BDAll);
			footerPanel.add(BDCRAll);
			footerPanel.add(NBDAll);
			footerPanel.add(DBDAll);
			footerPanel.add(DBDCRAll);
			footerPanel.add(NDBDAll);
			footerPanel.add(NEGAll);*/
			/*footerPanel.add(JL1);
			footerPanel.add(JL2);
			footerPanel.add(JL3);
			footerPanel.add(JL4);
			footerPanel.add(JL5);*/
			//footerPanel.add(JL11);
			
			
			//footerPanel.add(uploadExcelSheet);
			//footerPanel.add(startServerButton);
			footerPanel.add(runWorkFlow);
		//	footerPanel.add(sendMultipleEmail);
			footerPanel.add(JL12);
			footerPanel.add(FlowStatus);
			contentPane.add(footerPanel, BorderLayout.CENTER);
			// contentPane.add(afterCenterPanel, BorderLayout.CENTER);
			// contentPane.add(afterCenterPanel, BorderLayout.set);

			// belowfooterPanel, copyRightLabel
			belowfooterPanel.add(copyRightLabel);
			contentPane.add(belowfooterPanel, BorderLayout.PAGE_END);
			//frame.getContentPane().add(new CheckCombo().getContent());
			frame.setContentPane(contentPane);
			frame.pack();
			frame.setVisible(true);
			// CYAN
			frame.getContentPane().setBackground(new java.awt.Color(255,140,0));
			centerPanel.setBackground(new java.awt.Color(255,140,0));//new Color(0,0,182,155)
			footerPanel.setBackground(new java.awt.Color(255,140,0));
			belowfooterPanel.setBackground(new java.awt.Color(255,140,0));
			//ImageIcon ii = new ImageIcon("D:\\R\\sm\\Image\\ServiceaMand_Logo2New_Latest.png");  //Priya
			//ImageIcon ii = new ImageIcon("C:\\Users\\14402\\git\\demoservicemandiautomation\\Image\\ServiceaMand_Logo2New_Latest.png"); //Rakesh
			//ImageIcon ii = new ImageIcon("D:\\Deepa\\GitFolder\\GitPull\\Image\\Img3.png");//Deepa
			ImageIcon i1 = new ImageIcon("D:\\Deepa\\GitFolder\\GitPull\\Image\\Untitled.png");
			//JLabel lable = new JLabel(ii);
			JLabel label1 = new JLabel(i1);
			//JScrollPane jsp = new JScrollPane(lable);
			JScrollPane jsp1 = new JScrollPane(label1);
			frame.getContentPane().add(jsp1);
			//frame.getContentPane().add(jsp);
			frame.setSize(1000, 700);
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			frame.setMaximizedBounds(env.getMaximumWindowBounds());
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		} catch (Exception e) {
		}
	}

	private void add(JLabel jL122) {
		// TODO Auto-generated method stub
	}
}

	class JComboCheckBox extends JComboBox {
	public JComboCheckBox() {
		init();
	}

	public JComboCheckBox(JCheckBox[] items) {
		super(items);
		init();
	}

	public JComboCheckBox(Vector items) {
		super(items);
		init();
	}

	public JComboCheckBox(ComboBoxModel aModel) {
		super(aModel);
		init();
	}

	private void init() {
		setRenderer(new ComboBoxRenderer());
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				itemSelected();
			}
		});
	}

	private void itemSelected() {
		if (getSelectedItem() instanceof JCheckBox) {
			JCheckBox jcb = (JCheckBox) getSelectedItem();
			jcb.setSelected(!jcb.isSelected());
		}
	}

	class ComboBoxRenderer implements ListCellRenderer {
		private JLabel label;

		public ComboBoxRenderer() {
			setOpaque(true);
		}

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			if (value instanceof Component) {
				Component c = (Component) value;
				if (isSelected) {
					c.setBackground(list.getSelectionBackground());
					c.setForeground(list.getSelectionForeground());
				} else {
					c.setBackground(list.getBackground());
					c.setForeground(list.getForeground());
				}

				return c;
			} else {
				if (label == null) {
					label = new JLabel(value.toString());
				} else {
					label.setText(value.toString());
				}

				return label;
			}
		}
	}
}
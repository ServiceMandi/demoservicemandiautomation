package com.servicemandi.webapps;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.LocatorPath2;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.common.WebConfiguration.LocatorType;
import com.servicemandi.webapps.utils.ExcelInputData;


public class createNewFM extends WebConfiguration {
	private String today;
	public createNewFM() {

	}
	public void webLogin(ExcelInputData excelInputData) throws InterruptedException {
	
	webLaunch();
	
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]")).click();
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]")).sendKeys(excelInputData.getEmailID());
	
	
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]")).click();
	WebElement password=driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]"));
	password.sendKeys(excelInputData.getPassword());
	password.sendKeys(Keys.TAB);
	password.sendKeys(Keys.ENTER);
	Thread.sleep(5000);
	writeReport(LogStatus.PASS, "Hunter App Launched and Logged in::::Passed");
	}
	
	public void createOrder(ExcelInputData excelInputData) throws InterruptedException {
		
		System.out.println("Inside createOrder");
		//Get Today's date
	    today = getCurrentDay();
	    System.out.println("Today's date: " + today + "\n");
	    
		Thread.sleep(10000);
		//System.out.println("excelInputData.getFMNameNumber():::"+excelInputData.getFMNameNumber());
		driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();//"//*[@id="fleetSearchId_chosen"]/a/span"
		Thread.sleep(1000);
		
		
		clickElement(LocatorType.XPATH, LocatorPath2.FMentrypath);
		enterValue(LocatorType.XPATH, LocatorPath2.FMentrypath,"Create New");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.FMentrypath)).sendKeys(Keys.RETURN);
		
				
		clickElement(LocatorType.XPATH, LocatorPath2.AddFMNameNumber);
		enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getAddNewFM());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.AddFMNameNumber)).sendKeys(Keys.RETURN);	
		
		/*Thread.sleep(2000);
		clickElement(LocatorType.XPATH, LocatorPath2.AddState);
		clickElement(LocatorType.XPATH, LocatorPath2.AddState);
		Select selectState=new Select(driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect']")));
		Thread.sleep(1000);
		selectState.selectByVisibleText(excelInputData.getAddState());
		driver.findElement(By.xpath(LocatorPath2.AddState)).sendKeys(Keys.TAB);*/
		
		Thread.sleep(5000);
		clickElement(LocatorType.XPATH, LocatorPath2.AddState);
		Select selectState=new Select(driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect']")));
		Thread.sleep(1000);
		selectState.selectByVisibleText(excelInputData.getAddState());
//		WebElement state=driver.findElement(By.xpath("//*[@id='newFMStateCodeSelect_chosen']/div/div/input"));
//		state.sendKeys(excelInputData.getAddState());
//		state.sendKeys(Keys.TAB);
		
		Thread.sleep(8000);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleRegNo);
		enterValue(LocatorType.XPATH, LocatorPath2.VehicleRegNo, excelInputData.getVehicleRegNo());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		
		Thread.sleep(8000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNo);
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNo, excelInputData.getDriverNameNumber());
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNo)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.LicenseExpDate);
						
		 WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns = LEDate.findElements(By.tagName("td"));
		 for (WebElement cell: columns) {
	            /*
	            //If you want to click 18th Date
	            if (cell.getText().equals("18")) {
	            */
	            //Select Today's Date
	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }
		
		 Thread.sleep(2000);
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 Select selectRating=new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
		 selectRating.selectByVisibleText(excelInputData.getStarRating());
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);
		
		
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		Thread.sleep(7000);
		writeReport(LogStatus.PASS, "Create Order:=>New FM::"+excelInputData.getAddNewFM()+" Created:::::::Passed");
	
		
	}

	public void vehicleRegister(ExcelInputData excelInputData) throws InterruptedException {
		
		
		Thread.sleep(2000);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleMake);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleMake);
		Select selectState=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleMake)));
		selectState.selectByVisibleText(excelInputData.getVehicleMake());
		driver.findElement(By.xpath(LocatorPath2.VehicleMake)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleCategory);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleCategory);
		Select selectCategory=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleCategory)));
		selectCategory.selectByVisibleText(excelInputData.getVehicleCategory());
		driver.findElement(By.xpath(LocatorPath2.VehicleCategory)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleLoadRange);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleLoadRange);
		Select loadRange=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleLoadRange)));
		loadRange.selectByVisibleText(excelInputData.getVehicleLoadRange());
		driver.findElement(By.xpath(LocatorPath2.VehicleLoadRange)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleModelYr);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleModelYr);
		Select VehicleModelYr=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleModelYr)));
		VehicleModelYr.selectByVisibleText(excelInputData.getVehicleModelYr());
		driver.findElement(By.xpath(LocatorPath2.VehicleModelYr)).sendKeys(Keys.TAB);
		
			
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleKMReading);
		enterValue(LocatorType.XPATH, LocatorPath2.VehicleKMReading, excelInputData.getVehicleKMReading());
		driver.findElement(By.xpath(LocatorPath2.VehicleKMReading)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleType);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleType);
		Select VehicleType=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleType)));
		VehicleType.selectByVisibleText(excelInputData.getVehicleType());
		driver.findElement(By.xpath(LocatorPath2.VehicleType)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleTyres);
		clickElement(LocatorType.XPATH, LocatorPath2.VehicleTyres);
		Select VehicleTyres=new Select(driver.findElement(By.xpath(LocatorPath2.VehicleTyres)));
		VehicleTyres.selectByVisibleText(excelInputData.getVehicleTyres());
		driver.findElement(By.xpath(LocatorPath2.VehicleTyres)).sendKeys(Keys.TAB);
		
		clickElement(LocatorType.XPATH, LocatorPath2.VehInsExpDate);
		WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns = LEDate.findElements(By.tagName("td"));
		 for (WebElement cell: columns) {
	            
	                        
	            //Select Today's Date
	            if (cell.getText().equals(today)) {
	                cell.click();
	                break;
	            }
	        }
		 
		clickElement(LocatorType.XPATH,LocatorPath2.FCValidityDate);
		WebElement FCDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> column = FCDate.findElements(By.tagName("td"));
		 for (WebElement cell1: column) {
	          //Select Today's Date
	            if (cell1.getText().equals(excelInputData.getFCValidityDate())) {
	                cell1.click();
	                break;
	            }
	        }
		clickElement(LocatorType.XPATH,LocatorPath2.InputLocBtn);
		Thread.sleep(7000);
		
		//Select location:
		Actions action = new Actions(driver);
		WebElement location=driver.findElement(By.xpath("//*[@id='pac-input']"));
		//Double click
		Thread.sleep(2000);
		action.doubleClick(location).perform();
		location.clear();
		Thread.sleep(1000);
		System.out.println("excelInputData.getLocation()"+excelInputData.getLocation());
		location.sendKeys(excelInputData.getLocation());
		
		
		driver.findElement(By.xpath("//*[@id='pac-input']")).sendKeys(Keys.RETURN);
				
		Thread.sleep(1000);
		//Scrolling and click BD button:
		WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
		Thread.sleep(1000); 
		bdElement.click();
		
		writeReport(LogStatus.PASS, "Created BD order::::Passed");
		
		/*Thread.sleep(4000);
		List<WebElement> VerifydataList=driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div"));
		System.out.println("Verifydata size :::" + VerifydataList.size());
		
		for (int i = 1; i <= VerifydataList.size(); i++) {
			String Verifydatatext = VerifydataList.get(i).getText();
			System.out.println("ConfirmWorkshopList:::"+i+":::::"+Verifydatatext);
		}*/
		/*if(Verifydatatext.contains("HTL AT Garage"))
		{	System.out.println("Workshop matches");
			VerifydataList.get(i).click();
			writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
			break;
		}
		else {
			System.out.println("Workshop doesnt match");}
		}*/
		//Clicking Confirm Garage button:
		Thread.sleep(7000);
		driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
		Thread.sleep(7000);
		
		
	}
	
	public void UpcomingOrders(ExcelInputData excelInputData) throws InterruptedException {
		
		//Upcoming Orders:
				//---------------
				//Clicking Upcoming Orders:
				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
				writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
				Thread.sleep(6000);
				//Clicking FM: 
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(1000);	
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000); 
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
				Thread.sleep(1000);  
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
				Thread.sleep(4000);
				//Assign Order to Workshop button:
				
				/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
				Thread.sleep(2000);
				if(GetVehicleNumber.contains("TN12341"))
				{*/
					//System.out.println("Vehicle Number matches");
					driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
					writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
					Thread.sleep(8000);
				//}
				
				//ConfirmWorkshop:
				//-----------------
				DriverWait("//*[@id='g_form_submit']/div[1]/*");
				List<WebElement> ConfirmWorkshopList =driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
				System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());
				
				for(int i = 1; i <= ConfirmWorkshopList.size(); i++) {
					String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
					System.out.println("ConfirmWorkshopList:::"+i+":::::"+ConfirmWorkshopListtext);
				
				if(ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName()))
				{
					System.out.println("Workshop matches");
					ConfirmWorkshopList.get(i).click();
					writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
					break;
				}
				else {
					System.out.println("Workshop doesnt match");}
					}
				//Clicking Confirm Garage button:
				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
				Thread.sleep(8000);
				

				//Confirm Workshop with FM:
				//Clicking FM: 
				DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(1000);	
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000); 
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
				Thread.sleep(1000);  
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
				Thread.sleep(6000);
				
				/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
				Thread.sleep(2000);
				if(GetVehicleNumber.contains("TN12341"))
				{*/
					//System.out.println("Vehicle Number matches");
					DriverWait("//*[contains(@name,'Confirm Workshop with Fleet Manager')]");
					driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
					writeReport(LogStatus.PASS, "Confirm Workshop with Fleet Manager::::Passed");
					Thread.sleep(10000);
				//}
					//Confirm Vehicle with Technician:
					//Clicking FM:
					DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
					Thread.sleep(1000);	
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
					Thread.sleep(2000); 
					driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
					Thread.sleep(1000);			
					driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
					Thread.sleep(1000);  
					driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
					driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
					Thread.sleep(4000);
					
					/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
					Thread.sleep(2000);
					if(GetVehicleNumber.contains("TN12341"))
					{*/
						//System.out.println("Vehicle Number matches");
						driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
						writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop::::Passed");
						Thread.sleep(6000);
					//}
						
				//Verifying the Order is displayed:
					//Clicking FM: 
					System.out.println("getNoResults::");
					DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
					driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
					Thread.sleep(2000);
					WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
					String getNoResults=getresultsMatchText.getText();				
					System.out.println("getNoResults::"+getNoResults);
					Thread.sleep(1000);
					if(getNoResults.startsWith("No results match"))
					{	System.out.println("Order is moved from HUNTER to FARMER::::Passed");
						writeReport(LogStatus.PASS, "Order is moved from HUNTER to FARMER::::Passed");
					}

	
	}
	
}
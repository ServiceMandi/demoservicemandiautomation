package com.servicemandi.webapps;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.LocatorPath2;
import com.servicemandi.common.WebConfiguration;
import com.servicemandi.common.WebConfiguration.LocatorType;
import com.servicemandi.webapps.utils.ExcelInputData;

public class createNewDriver extends WebConfiguration {
	private String today;
	public createNewDriver() {

	}
	public void webLogin(ExcelInputData excelInputData) throws InterruptedException {
	
	webLaunch();
	
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]")).click();
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_login\"]")).sendKeys(excelInputData.getEmailID());
	
	
	driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]")).click();
	WebElement password=driver.findElement(By.xpath("//*[@id=\"_custom_login_module_INSTANCE_axb93woHlrvx_password\"]"));
	password.sendKeys(excelInputData.getPassword());
	password.sendKeys(Keys.TAB);
	password.sendKeys(Keys.ENTER);
	Thread.sleep(5000);
	writeReport(LogStatus.PASS, "Hunter App Launched and Logged in::::Passed");
	
	}
	
	public void createOrder(ExcelInputData excelInputData) throws InterruptedException {
		
 		System.out.println("Inside createOrder");
		//Get Today's date
	    today = getCurrentDay();
	    System.out.println("Today's date: " + today + "\n");
	    
		Thread.sleep(6000);
		System.out.println("excelInputData.getFMNameNumber():::"+excelInputData.getFMNameNumber());
		System.out.println("excelInputData.AddVehicleRegNo:::"+excelInputData.getVehicleRegNo());
		
		driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();
		
		//driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();//"//*[@id="fleetSearchId_chosen"]/a/span"
		Thread.sleep(1000);
		
		clickElement(LocatorType.XPATH, "//*[@id='fleetManagerNameNumberSelect_chosen']/a/span");
		Thread.sleep(500);
		driver.findElement(By.xpath(LocatorPath2.EnterFMNameNo)).sendKeys(excelInputData.getFMNameNumber());	
		//enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getFMNameNumber());
		Thread.sleep(3000);
		driver.findElement(By.xpath(LocatorPath2.EnterFMNameNo)).sendKeys(Keys.TAB);	
		
		Thread.sleep(8000);
		clickElement(LocatorType.XPATH, "//*[@id='vehicleRegistrationNumberSelect_chosen']/a/span");	
		clickElement(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo);
		enterValue(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo, excelInputData.getVehicleRegNo());
		driver.findElement(By.xpath(LocatorPath2.EnterVehicleRegNo)).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		//driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		
		Thread.sleep(8000);			
		clickElement(LocatorType.XPATH,"//*[@id='driverNameNumberSelect_chosen']/a/span");
		clickElement(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo);
		enterValue(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo, "Create New");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.TAB);
		
		Thread.sleep(8000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNumber);
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNumber, excelInputData.AddDriverNameNo);
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.TAB);
		
		Thread.sleep(5000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverLicExpiryDate);
		WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns = LEDate.findElements(By.tagName("td"));
		 for (WebElement cell: columns) {
	            
	                        
	            //Select Today's Date
	            if (cell.getText().equals(today)) {
	                cell.click();
	                Thread.sleep(3000);
	                break;
	            }
	        }
		 
			
		 Thread.sleep(2000);
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 Select selectRating=new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
		 selectRating.selectByVisibleText(excelInputData.getStarRating());
		 //driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		Thread.sleep(8000);
		writeReport(LogStatus.PASS, "Create Order:=>New Driver Created::::Passed");
		
		System.out.println("InputLocationVerification");
		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*");
		List<WebElement> createorderList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());
					
		/*for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::"+i+":::::"+createorderListtext);
			if(createorderListtext.equalsIgnoreCase("Input Location"))
			{
				break;
			}
		}*/
		Thread.sleep(5000);
		DriverWait("//*[@id='makemodelsubmit']");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
	     //This will scroll the page till the element is found		
	     js.executeScript("arguments[0].scrollIntoView();", Element);
	     Element.click();
	     writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
		
	   //Select location:
			Actions action = new Actions(driver);
			WebElement location=driver.findElement(By.xpath("//*[@id='pac-input']"));
			//Double click
			Thread.sleep(2000);
			action.doubleClick(location).perform();
			location.clear();
			Thread.sleep(1000);
			System.out.println("excelInputData.getLocation()"+excelInputData.getLocation());
			location.sendKeys(excelInputData.getLocation());
			
			
			driver.findElement(By.xpath("//*[@id='pac-input']")).sendKeys(Keys.RETURN);
					
			Thread.sleep(1000);
			//Scrolling and click BD button:
			WebElement bdElement = driver.findElement(By.xpath("//*[@id='breakdown_button']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bdElement);
			Thread.sleep(1000); 
			bdElement.click();
			
			writeReport(LogStatus.PASS, "Created BD order::::Passed");
			
			//Clicking Confirm Order button:
			
			/*Thread.sleep(4000);
			List<WebElement> VerifydataList=driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div"));
			System.out.println("Verifydata size :::" + VerifydataList.size());
			
			for (int i = 1; i <= VerifydataList.size(); i++) {
				String Verifydatatext = VerifydataList.get(i).getText();
				System.out.println("ConfirmWorkshopList:::"+i+":::::"+Verifydatatext);
			}*/
			/*if(Verifydatatext.contains("HTL AT Garage"))
			{	System.out.println("Workshop matches");
				VerifydataList.get(i).click();
				writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
				break;
			}
			else {
				System.out.println("Workshop doesnt match");}
			}*/
			Thread.sleep(7000);
			driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();
			Thread.sleep(7000);
		
		
	}

	public void createSROrder(ExcelInputData excelInputData) throws InterruptedException {
		
 		System.out.println("Inside createOrder");
		//Get Today's date
	    today = getCurrentDay();
	    System.out.println("Today's date: " + today + "\n");
	    
		Thread.sleep(6000);
		System.out.println("excelInputData.getFMNameNumber():::"+excelInputData.getFMNameNumber());
		System.out.println("excelInputData.AddVehicleRegNo:::"+excelInputData.AddVehicleRegNo);
		
		driver.findElement(By.xpath("//*[@id='layout_1']/a/span[1]")).click();
		
		//driver.findElement(By.xpath("//*[@id='fleetManagerNameNumberSelect_chosen']/a/span")).click();//"//*[@id="fleetSearchId_chosen"]/a/span"
		Thread.sleep(1000);
		
		clickElement(LocatorType.XPATH, "//*[@id='fleetManagerNameNumberSelect_chosen']/a/span");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.EnterFMNameNo)).sendKeys(excelInputData.getFMNameNumber());	
		//enterValue(LocatorType.XPATH, LocatorPath2.AddFMNameNumber, excelInputData.getFMNameNumber());
		Thread.sleep(3000);
		driver.findElement(By.xpath(LocatorPath2.EnterFMNameNo)).sendKeys(Keys.TAB);	
		
		Thread.sleep(4000);
		clickElement(LocatorType.XPATH, "//*[@id='vehicleRegistrationNumberSelect_chosen']/a/span");
		Thread.sleep(1000);
		clickElement(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo);
		Thread.sleep(1000);
		enterValue(LocatorType.XPATH, LocatorPath2.EnterVehicleRegNo, excelInputData.AddVehicleRegNo);
		Thread.sleep(2000);
		driver.findElement(By.xpath(LocatorPath2.EnterVehicleRegNo)).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		//driver.findElement(By.xpath(LocatorPath2.VehicleRegNo)).sendKeys(Keys.TAB);
		
		//Thread.sleep(2000);			
		clickElement(LocatorType.XPATH,"//*[@id='driverNameNumberSelect_chosen']/a/span");
		Thread.sleep(1000);
		clickElement(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo);
		Thread.sleep(1000);
		enterValue(LocatorType.XPATH, LocatorPath2.EnterDriverNameNo, "Create New");
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.xpath(LocatorPath2.EnterDriverNameNo)).sendKeys(Keys.TAB);
		
		Thread.sleep(3000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverNameNumber);
		Thread.sleep(1000);
		enterValue(LocatorType.XPATH, LocatorPath2.DriverNameNumber, excelInputData.AddDriverNameNo);
		Thread.sleep(1000);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(LocatorPath2.DriverNameNumber)).sendKeys(Keys.TAB);
		
		Thread.sleep(5000);
		clickElement(LocatorType.XPATH, LocatorPath2.DriverLicExpiryDate);
		WebElement LEDate = driver.findElement(By.xpath("/html/body/div[2]/div[1]/table/tbody"));
		 List<WebElement> columns = LEDate.findElements(By.tagName("td"));
		 for (WebElement cell: columns) {
	            
	                        
	            //Select Today's Date
	            if (cell.getText().equals(today)) {
	                cell.click();
	                Thread.sleep(3000);
	                break;
	            }
	        }
		 
			
		 Thread.sleep(2000);
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 driver.findElement(By.xpath(LocatorPath2.StarRating)).click();
		 Select selectRating=new Select(driver.findElement(By.xpath("//*[@id='driverStarRatingSelect']")));
		 selectRating.selectByVisibleText(excelInputData.getStarRating());
		 //driver.findElement(By.xpath(LocatorPath2.StarRating)).sendKeys(Keys.TAB);
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='createOrderOffline']")).click();
		Thread.sleep(8000);
		writeReport(LogStatus.PASS, "Create Order:=>New Driver:"+excelInputData.AddDriverNameNo+" Created::::with Vehicle Reg No:"+excelInputData.AddVehicleRegNo+"Passed");
		
		System.out.println("InputLocationVerification");
		DriverWait("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*");
		List<WebElement> createorderList = driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div/*/*"));
		System.out.println("CreateOrderList size :::" + createorderList.size());
					
		/*for (int i = 1; i <= createorderList.size(); i++) {
			String createorderListtext = createorderList.get(i).getText();
			System.out.println("createorderListtext:::"+i+":::::"+createorderListtext);
			if(createorderListtext.equalsIgnoreCase("Input Location"))
			{
				break;
			}
		}*/
		Thread.sleep(5000);
		DriverWait("//*[@id='makemodelsubmit']");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 WebElement Element = driver.findElement(By.xpath("//*[@id='makemodelsubmit']"));
	     //This will scroll the page till the element is found		
	     js.executeScript("arguments[0].scrollIntoView();", Element);
	     Element.click();
	     writeReport(LogStatus.PASS, "Clicked Input Location::::Passed");
		
	   //Select location:
			Actions action = new Actions(driver);
			WebElement location=driver.findElement(By.xpath("//*[@id='pac-input']"));
			//Double click
			Thread.sleep(2000);
			action.doubleClick(location).perform();
			location.clear();
			Thread.sleep(1000);
			System.out.println("excelInputData.getLocation()"+excelInputData.getLocation());
			location.sendKeys(excelInputData.getLocation());
			
			
			driver.findElement(By.xpath("//*[@id='pac-input']")).sendKeys(Keys.RETURN);
					
			Thread.sleep(1000);
			//Scrolling and click BD button:
			WebElement srElement = driver.findElement(By.xpath("//*[@id='servicerepair_button']"));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", srElement);
			Thread.sleep(1000); 
			srElement.click();
			
			writeReport(LogStatus.PASS, "Created SR order::::Passed");
			
			//Clicking Confirm Order button:
			
			/*Thread.sleep(4000);
			List<WebElement> VerifydataList=driver.findElements(By.xpath("//*[@id='p_p_id_Hunter_Create_order_']/div/div/div/div/div/div"));
			System.out.println("Verifydata size :::" + VerifydataList.size());
			
			for (int i = 1; i <= VerifydataList.size(); i++) {
				String Verifydatatext = VerifydataList.get(i).getText();
				System.out.println("ConfirmWorkshopList:::"+i+":::::"+Verifydatatext);
			}*/
			/*if(Verifydatatext.contains("HTL AT Garage"))
			{	System.out.println("Workshop matches");
				VerifydataList.get(i).click();
				writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
				break;
			}
			else {
				System.out.println("Workshop doesnt match");}
			}*/
			/*Thread.sleep(7000);
			driver.findElement(By.xpath("//button[@class='btn btn-default']")).click();*/
			Thread.sleep(7000);
			
			//SR::::Job Selection 
			Thread.sleep(4000);
			String selectJob = "//*[@id='accordionParent']/*";
			DriverWait(selectJob);
			List<WebElement> selectJobOuterList = driver.findElements(By.xpath(selectJob));
			Thread.sleep(1000);
			System.out.println("Job List Size :" + selectJobOuterList.size());

			for (int i = 0; i <= selectJobOuterList.size(); i++) {
				Thread.sleep(1000);
				String selectJobText = selectJobOuterList.get(i).getText();
				System.out.println("selectJobText:::::::" + selectJobText);

				String expected = excelInputData.getSROuterJob();
				String[] outerArray = expected.split(",");

				for (String OuterArrayValues1 : outerArray) {
					System.out.println(OuterArrayValues1);
					int result = Integer.parseInt(OuterArrayValues1);

					Thread.sleep(1000);

					selectJobOuterList.get(result).click();

					// String OuterText=selectJobOuterList.get(result).getText();
					// System.out.println("OuterText::"+OuterText);
					Thread.sleep(1000);

					List<WebElement> selectInnerJobList = driver.findElements(By.xpath("//div[@class='panel-collapse collapse in']/*/*"));

					/*
					 * String
					 * OuterText=driver.findElement(By.xpath("//*[@id='accordionParent']/div["+
					 * OuterArrayValues1+"]/div[1]/h4/a")).getAttribute("href");
					 * System.out.println("OuterText::"+OuterText);
					 */

					// List<WebElement> selectInnerJobList =
					// driver.findElements(By.xpath("//*[@id='Steering']/div/*"));//*[@id="Steering"]/div

					// List<WebElement> selectInnerJobList =
					// driver.findElements(By.xpath("//div[@class='panel-body']"));
					// //selectJobOuterList.get(result)
					System.out.println("selectInnerJobList.size()::::" + selectInnerJobList.size());

	 				String OuterText = driver.findElement(By.xpath("//*[@class='panel-collapse collapse in']")).getAttribute("id");
					System.out.println("OuterText:" + OuterText);
					String expectedInnerjob = excelInputData.getSRInnerJob();
					for (int j = 0; j<Integer.parseInt(expectedInnerjob); j++) {
						Thread.sleep(1000);

						System.out.println("selectInnerJobList.get(j).getText();:::" + selectInnerJobList.get(j).getText());

						if (j < Integer.parseInt(expectedInnerjob)) {
							System.out.println("Inside InnerJob If");
							Thread.sleep(1000);
							String getInnerJobText=driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" +(j+1)+ "]/label/following-sibling::input[@type='checkbox']")).getText();
							System.out.println("GetInnerJobText:::::"+getInnerJobText);
							driver.findElement(By.xpath("//*[@id='" + OuterText + "']/div/div[" +(j+1)+ "]/label/following-sibling::input[@type='checkbox']")).click();
							writeReport(LogStatus.PASS, "SR - Jobs::::"+ selectInnerJobList.get(j).getText()+"::::Selected");
						}
						if (j == Integer.parseInt(expectedInnerjob)) {
							System.out.println("Inside InnerJob If");
							Thread.sleep(1000);
							// driver.findElement(By.xpath("//*[@id='accordionParent']/div["+result+"]/div[1]/h4/a")).click();
							break;
						}
						
					} // InnerFor

					// }else { break;}
				}
				driver.findElement(By.xpath("//button[@id='submitJobsButton']")).click();
				break;
				
			} // FirstFor
			writeReport(LogStatus.PASS, "SR - Jobs Selected::::Passed");
				
	}

	public void UpcomingOrders(ExcelInputData excelInputData) throws InterruptedException {
		
		//Upcoming Orders:
				//---------------
				//Clicking Upcoming Orders:
				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='layout_3']/a/span[1]")).click();
				writeReport(LogStatus.PASS, "Clicked Upcoming Orders::::Passed");
				Thread.sleep(8000);
				//Clicking FM: 
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(1000);	
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000); 
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
				Thread.sleep(1000);  
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
				Thread.sleep(9000);
				//Assign Order to Workshop button:
				
				/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
				Thread.sleep(2000);
				if(GetVehicleNumber.contains("TN12341"))
				{*/
					//System.out.println("Vehicle Number matches");
					driver.findElement(By.xpath("//*[contains(@name,'Assign Order to Workshop')]")).click();
					writeReport(LogStatus.PASS, "Assigned Order to Workshop::::Passed");
					Thread.sleep(10000);
				//}
				
				//ConfirmWorkshop:
				//-----------------
				DriverWait("//*[@id='g_form_submit']/div[1]/*");
				List<WebElement> ConfirmWorkshopList =driver.findElements(By.xpath("//*[@id='g_form_submit']/div[1]/*"));
				System.out.println("ConfirmWorkshopList size :::" + ConfirmWorkshopList.size());
				
				for(int i = 1; i <= ConfirmWorkshopList.size(); i++) {
					String ConfirmWorkshopListtext = ConfirmWorkshopList.get(i).getText();
					System.out.println("ConfirmWorkshopList:::"+i+":::::"+ConfirmWorkshopListtext);
				
				if(ConfirmWorkshopListtext.contains(excelInputData.getWorkshopName()))
				{
					System.out.println("Workshop matches");
					ConfirmWorkshopList.get(i).click();
					writeReport(LogStatus.PASS, "Confirm Workshop::::Passed");
					break;
				}
				else {
					System.out.println("Workshop doesnt match");}
					}
				//Clicking Confirm Garage button:
				Thread.sleep(5000);
				driver.findElement(By.xpath("//*[@id='confirm_garage_button']")).click();
				Thread.sleep(5000);
				

				//Confirm Workshop with FM:
				//Clicking FM: 
				DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
				Thread.sleep(8000);
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(1000);	
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000); 
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
				Thread.sleep(1000);  
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
				Thread.sleep(9000);
				
				/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
				Thread.sleep(2000);
				if(GetVehicleNumber.contains("TN12341"))
				{*/
					//System.out.println("Vehicle Number matches");
					driver.findElement(By.xpath("//*[contains(@name,'Confirm Workshop with Fleet Manager')]")).click();
					writeReport(LogStatus.PASS, "Confirm Workshop with Fleet Manager::::Passed");
					Thread.sleep(3000);
				//}
					
				//Confirm Vehicle with Technician:
				//Clicking FM:
				DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
				Thread.sleep(10000);
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(1000);	
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000); 
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);			
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getVehicleRegNo());
				Thread.sleep(1000);  
				driver.findElement(By.xpath("//*[@id='vehicleSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/a/span")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getDriverNameNumber());
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='driverSearchId_chosen']/div/div/input")).sendKeys(Keys.RETURN);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='searchSubmit']")).click();
				Thread.sleep(9000);
				
				/*String GetVehicleNumber=driver.findElement(By.xpath("//*[@id='requestCard_1']"));
				Thread.sleep(2000);
				if(GetVehicleNumber.contains("TN12341"))
				{*/
					//System.out.println("Vehicle Number matches");
					driver.findElement(By.xpath("//*[contains(@name,'Confirm Vehicle in Workshop')]")).click();
					writeReport(LogStatus.PASS, "Confirm Vehicle in Workshop::::Passed");
					Thread.sleep(9000);
				//}
					
			//Verifying the Order is displayed:
				//Clicking FM: 
				Thread.sleep(10000);
				System.out.println("getNoResults::");
				DriverWait("//*[@id='fleetSearchId_chosen']/a/span");
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/a/span")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).click();
				driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/div/input")).sendKeys(excelInputData.getFMNameNumber());
				Thread.sleep(2000);
				WebElement getresultsMatchText=driver.findElement(By.xpath("//*[@id='fleetSearchId_chosen']/div/ul/li"));
				String getNoResults=getresultsMatchText.getText();				
				System.out.println("getNoResults::"+getNoResults);
				Thread.sleep(1000);
				if(getNoResults.startsWith("No results match"))
				{	System.out.println("Order is moved from HUNTER to FARMER::::Passed");
					writeReport(LogStatus.PASS, "Order is moved from HUNTER to FARMER::::Passed");
				}
	
	}	
}
package com.servicemandi.webapps.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FarmerUtils {

	public static List<FarmerExcelInputData> FarmerExcelInputDataList;
	private static Date previousTime;
	private static long startTime = 0;
	private static long endTime = 0;
	private static long totalTime = 0;
	public static double finalBillAmt=0.0;
	public static Object excelInputDataList;
	public static Object farmerDetailsList;
	
	public static void sleepTimeLong() throws InterruptedException {

		Thread.sleep(20000);
	}

	public static void sleepTimeMedium() throws InterruptedException {
		Thread.sleep(10000);
	}

	public static void sleepTimeLow() throws InterruptedException {
		Thread.sleep(5000);
	}

	public static String timeCalculation() {

		if (startTime == 0) {
			startTime = System.nanoTime();
			//System.out.println("startTime:::"+TimeUnit.SECONDS.convert(startTime, TimeUnit.NANOSECONDS));
		}
		endTime = System.nanoTime();
		//System.out.println("endTime:::"+TimeUnit.SECONDS.convert(endTime, TimeUnit.NANOSECONDS));
		totalTime = endTime - startTime;
	//	System.out.println("totalTime:::"+TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS));
		startTime = endTime; 
		return "The time duration is " + TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS) + " Seconds";

	}

}

package com.servicemandi.webapps.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class ReadingExcelData {

	private static Workbook workbook = null;

	public static Workbook getWorkBook() {

		try {
			if (workbook == null) {
				String newFilePath = System.getProperty("user.dir") + "\\SM_WebApps.xls";
				System.out.println("New file path :::" + newFilePath);
				FileInputStream fs = new FileInputStream(newFilePath);
				workbook = Workbook.getWorkbook(fs);
			}

		} catch (Exception e) {

		}
		return workbook;
	}

	public static void readingWorkFlowData() throws IOException, BiffException {

		try {
			if (Utils.ExcelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("HunterWorkflow");

				List<String> ExpectedColumns = new ArrayList<String>();
				System.out.println("::::::::::::::::::::::::::::");
				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 1);
					String d = celll.getContents();
				//	System.out.println("D values: "+d);
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}
				List<String> EmailIDList = columnDataValues.get("EmailID");
				List<String> PasswordList = columnDataValues.get("Password");
				
				List<String> AddNewFMList = columnDataValues.get("AddNewFM");
				List<String> AddStateList = columnDataValues.get("AddState");
				List<String> AddVehicleRegNoList = columnDataValues.get("AddVehicleRegNo");
				List<String> AddDriverNameNoList = columnDataValues.get("AddDriverNameNo");
				List<String> StarRatingList = columnDataValues.get("StarRating");
				
				List<String> VehicleMakeList = columnDataValues.get("VehicleMake");
				List<String> VehicleCategoryList = columnDataValues.get("VehicleCategory");
				List<String> VehicleLoadRangeList = columnDataValues.get("VehicleLoadRange");
				List<String> VehicleModelYrList = columnDataValues.get("VehicleModelYr");
				List<String> VehicleKMReadingList = columnDataValues.get("VehicleKMReading");
				List<String> VehicleTypeList = columnDataValues.get("VehicleType");  
				List<String> VehicleTyresList = columnDataValues.get("VehicleTyres");
				List<String> LicenseExpDateList = columnDataValues.get("LicenseExpDate");
				List<String> FCValidityDateList = columnDataValues.get("FCValidityDate");
				List<String> LocationList = columnDataValues.get("Location");
				List<String> PinCodeList = columnDataValues.get("PinCodeNo");
				
				
				List<String> FMNameNumberList = columnDataValues.get("FMNameNumber");
				List<String> VehicleRegNoList=columnDataValues.get("VehicleRegNo");
				List<String> DriverNameNumberList=columnDataValues.get("DriverNameNumber");
				List<String> SROuterJobList=columnDataValues.get("SROuterJob");
				List<String> SRInnerJobList=columnDataValues.get("SRInnerJobs");
				List<String> WorkshopNameList=columnDataValues.get("WorkshopName");
				List<String> CancelReasonList=columnDataValues.get("CancelReason");
				List<String> OrderIDList=columnDataValues.get("OrderID");
				List<String> AddNotesList=columnDataValues.get("AddNotes");
				
				
				
				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < FMNameNumberList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setEmailID(EmailIDList.get(i));
					userDetailsList.get(i).setPassword(PasswordList.get(i));
					
					userDetailsList.get(i).setAddNewFM(AddNewFMList.get(i));
					userDetailsList.get(i).setAddState(AddStateList.get(i));
					userDetailsList.get(i).setAddVehicleRegNo(AddVehicleRegNoList.get(i));
					userDetailsList.get(i).setAddDriverNameNo(AddDriverNameNoList.get(i));
					userDetailsList.get(i).setStarRating(StarRatingList.get(i));
										
					userDetailsList.get(i).setVehicleMake(VehicleMakeList.get(i));
					userDetailsList.get(i).setVehicleCategory(VehicleCategoryList.get(i));
					userDetailsList.get(i).setVehicleLoadRange(VehicleLoadRangeList.get(i));
					userDetailsList.get(i).setVehicleModelYr(VehicleModelYrList.get(i));
					userDetailsList.get(i).setVehicleKMReading(VehicleKMReadingList.get(i));
					userDetailsList.get(i).setVehicleType(VehicleTypeList.get(i));
					userDetailsList.get(i).setVehicleTyres(VehicleTyresList.get(i));
					userDetailsList.get(i).setLicenseExpDate(LicenseExpDateList.get(i));					
					userDetailsList.get(i).setFCValidityDate(FCValidityDateList.get(i));
					userDetailsList.get(i).setLocation(LocationList.get(i));
					userDetailsList.get(i).setPincode(PinCodeList.get(i));
					
											
					userDetailsList.get(i).setFMNameNumber(FMNameNumberList.get(i));
					userDetailsList.get(i).setVehicleRegNo(VehicleRegNoList.get(i));
					userDetailsList.get(i).setDriverNameNumber(DriverNameNumberList.get(i));
					userDetailsList.get(i).setSROuterJob(SROuterJobList.get(i));
					userDetailsList.get(i).setSRInnerJob(SRInnerJobList.get(i));
					userDetailsList.get(i).setWorkshopName(WorkshopNameList.get(i));
					userDetailsList.get(i).setCancelReason(CancelReasonList.get(i));
					userDetailsList.get(i).setOrderID(OrderIDList.get(i));
					userDetailsList.get(i).setAddNotes(AddNotesList.get(i));
										
				}

				Utils.ExcelInputDataList = userDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readingWorkFlowData2() throws IOException,BiffException {

		try {
			if (Utils.ExcelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("HunterNegativeValidation");

				List<String> ExpectedColumns = new ArrayList<String>();
				System.out.println("::::::::::::::::::::::::::::");
				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 1);
					String d = celll.getContents();
				//	System.out.println("D values: "+d);
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}
				List<String> EmailIDList = columnDataValues.get("EmailID");
				List<String> PasswordList = columnDataValues.get("Password");
				
				List<String> AddNewFMList = columnDataValues.get("AddNewFM");
				List<String> AddStateList = columnDataValues.get("AddState");
				List<String> AddVehicleRegNoList = columnDataValues.get("AddVehicleRegNo");
				List<String> AddDriverNameNoList = columnDataValues.get("AddDriverNameNo");
				List<String> StarRatingList = columnDataValues.get("StarRating");
				
				List<String> VehicleMakeList = columnDataValues.get("VehicleMake");
				List<String> VehicleCategoryList = columnDataValues.get("VehicleCategory");
				List<String> VehicleLoadRangeList = columnDataValues.get("VehicleLoadRange");
				List<String> VehicleModelYrList = columnDataValues.get("VehicleModelYr");
				List<String> VehicleKMReadingList = columnDataValues.get("VehicleKMReading");
				List<String> VehicleTypeList = columnDataValues.get("VehicleType");  
				List<String> VehicleTyresList = columnDataValues.get("VehicleTyres");
				List<String> LicenseExpDateList = columnDataValues.get("LicenseExpDate");
				List<String> FCValidityDateList = columnDataValues.get("FCValidityDate");
				List<String> LocationList = columnDataValues.get("Location");
				
				
				List<String> FMNameNumberList = columnDataValues.get("FMNameNumber");
				List<String> VehicleRegNoList=columnDataValues.get("VehicleRegNo");
				List<String> DriverNameNumberList=columnDataValues.get("DriverNameNumber");
				List<String> SROuterJobList=columnDataValues.get("SROuterJob");
				List<String> SRInnerJobList=columnDataValues.get("SRInnerJobs");
				List<String> CancelReasonList=columnDataValues.get("CancelReason");
				
				List<String> InvalidFMNameNoList=columnDataValues.get("InvalidFMNameNo");
				List<String> InvalidVehicleRegNoList=columnDataValues.get("InvalidVehicleRegNo");
				List<String> InvalidDriverNameNoList=columnDataValues.get("InvalidDriverNameNo");
				
				
				
				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < FMNameNumberList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setEmailID(EmailIDList.get(i));
					userDetailsList.get(i).setPassword(PasswordList.get(i));
					
					userDetailsList.get(i).setAddNewFM(AddNewFMList.get(i));
					userDetailsList.get(i).setAddState(AddStateList.get(i));
					userDetailsList.get(i).setAddVehicleRegNo(AddVehicleRegNoList.get(i));
					userDetailsList.get(i).setAddDriverNameNo(AddDriverNameNoList.get(i));
					userDetailsList.get(i).setStarRating(StarRatingList.get(i));
										
					userDetailsList.get(i).setVehicleMake(VehicleMakeList.get(i));
					userDetailsList.get(i).setVehicleCategory(VehicleCategoryList.get(i));
					userDetailsList.get(i).setVehicleLoadRange(VehicleLoadRangeList.get(i));
					userDetailsList.get(i).setVehicleModelYr(VehicleModelYrList.get(i));
					userDetailsList.get(i).setVehicleKMReading(VehicleKMReadingList.get(i));
					userDetailsList.get(i).setVehicleType(VehicleTypeList.get(i));
					userDetailsList.get(i).setVehicleTyres(VehicleTyresList.get(i));
					userDetailsList.get(i).setLicenseExpDate(LicenseExpDateList.get(i));
					userDetailsList.get(i).setFCValidityDate(FCValidityDateList.get(i));
					userDetailsList.get(i).setLocation(LocationList.get(i));
										
					userDetailsList.get(i).setFMNameNumber(FMNameNumberList.get(i));
					userDetailsList.get(i).setVehicleRegNo(VehicleRegNoList.get(i));
					userDetailsList.get(i).setDriverNameNumber(DriverNameNumberList.get(i));
					userDetailsList.get(i).setSROuterJob(SROuterJobList.get(i));
					userDetailsList.get(i).setSRInnerJob(SRInnerJobList.get(i));
					userDetailsList.get(i).setCancelReason(CancelReasonList.get(i));
					
					userDetailsList.get(i).setInvalidFMNameNo(InvalidFMNameNoList.get(i));
					userDetailsList.get(i).setInvalidVehicleRegNo(InvalidVehicleRegNoList.get(i));
					userDetailsList.get(i).setInvalidDriverNameNo(InvalidDriverNameNoList.get(i));
					
				}

				Utils.ExcelInputDataList = userDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public static void readingWorkFlowData3() throws IOException,BiffException {

		try {
			if (Utils.ExcelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("HunterResources");

				List<String> ExpectedColumns = new ArrayList<String>();
				System.out.println("::::::::::::::::::::::::::::");
				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 1);
					String d = celll.getContents();
				//	System.out.println("D values: "+d);
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}
				List<String> ResEmailIDList = columnDataValues.get("ResEmailID");
				List<String> ResPasswordList = columnDataValues.get("ResPassword");
							
				List<String> ResLocationList = columnDataValues.get("ResLocation");
				List<String> ResFMNameNoList = columnDataValues.get("ResFMNameNo");
				List<String> ResVehicleRegNoList=columnDataValues.get("ResVehicleRegNo");
				List<String> ResDriverNameNoList=columnDataValues.get("ResDriverNameNo");
				List<String> ResCancelReasonList=columnDataValues.get("ResCancelReason");
				List<String> ResPinCodeNoList=columnDataValues.get("ResPinCodeNo");
				List<String> ResWorkshopNameList=columnDataValues.get("ResWorkshopName");
				List<String> ResAddNotesList=columnDataValues.get("ResAddNotes");
				List<String> ResOrderIDList=columnDataValues.get("ResOrderID");
				List<String> ResVehicleMakeList=columnDataValues.get("ResVehicleMake");
				List<String> ResVehicleLoadRangeList=columnDataValues.get("ResVehicleLoadRange");
				List<String> SROuterJobList=columnDataValues.get("SROuterJob");
				List<String> SRInnerJobList=columnDataValues.get("SRInnerJobs");
				
//				List<String> InvalidFMNameNoList=columnDataValues.get("InvalidFMNameNo");
//				List<String> InvalidVehicleRegNoList=columnDataValues.get("InvalidVehicleRegNo");
//				List<String> InvalidDriverNameNoList=columnDataValues.get("InvalidDriverNameNo");
				
				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < ResFMNameNoList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setResEmailID(ResEmailIDList.get(i));
					userDetailsList.get(i).setResPassword(ResPasswordList.get(i));
					
					userDetailsList.get(i).setResLocation(ResLocationList.get(i));
					userDetailsList.get(i).setResFMNameNo(ResFMNameNoList.get(i));
					userDetailsList.get(i).setResVehicleRegNo(ResVehicleRegNoList.get(i));
					userDetailsList.get(i).setResDriverNameNo(ResDriverNameNoList.get(i));
					userDetailsList.get(i).setResCancelReason(ResCancelReasonList.get(i));
					userDetailsList.get(i).setResPinCodeNo(ResPinCodeNoList.get(i));
					userDetailsList.get(i).setResWorkshopName(ResWorkshopNameList.get(i));
					userDetailsList.get(i).setResAddNotes(ResAddNotesList.get(i));
					userDetailsList.get(i).setResOrderID(ResOrderIDList.get(i));
					userDetailsList.get(i).setResVehicleMake(ResVehicleMakeList.get(i));
					userDetailsList.get(i).setResVehicleLoadRange(ResVehicleLoadRangeList.get(i));
					userDetailsList.get(i).setSROuterJob(SROuterJobList.get(i));
					userDetailsList.get(i).setSRInnerJob(SRInnerJobList.get(i));
					
					
//					userDetailsList.get(i).setInvalidFMNameNo(InvalidFMNameNoList.get(i));
//					userDetailsList.get(i).setInvalidVehicleRegNo(InvalidVehicleRegNoList.get(i));
//					userDetailsList.get(i).setInvalidDriverNameNo(InvalidDriverNameNoList.get(i));
					
				}

				Utils.ExcelInputDataList = userDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void readingWorkFlowData4() throws IOException, BiffException {

		try {
			if (FarmerUtils.FarmerExcelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("FarmerWorkflows");

				List<String> ExpectedColumns = new ArrayList<String>();
				System.out.println("::::::::::::::::::::::::::::");
				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 1);
					String d = celll.getContents();
				//	System.out.println("D values: "+d);
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}
				List<String> FarmerMailIDList = columnDataValues.get("FarmerMailID");
				List<String> PasswordList = columnDataValues.get("Password");
						
				List<String> WorkshopNameList=columnDataValues.get("WorkshopName");
				List<String> CancelReasonList=columnDataValues.get("CancelReason");
				List<String> OrderIDList=columnDataValues.get("OrderID");
				List<String> AddNotesList=columnDataValues.get("AddNotes");
				
				List<String> RetailerList=columnDataValues.get("Retailer");
				List<String> KMList=columnDataValues.get("KM");
				List<String> FarmerJobsList=columnDataValues.get("FarmerJobs");
				List<String> FarmerLabAmtList=columnDataValues.get("FarmerLabAmt");
				List<String> FarmerPartsList=columnDataValues.get("FarmerPartsAmt");
								
				List<String> RevFarmerJobsList=columnDataValues.get("RevFarmerJobs");
				List<String> RevLabAmtList=columnDataValues.get("RevLabAmt");
				List<String> RevPartsAmtList=columnDataValues.get("RevPartsAmt");
				
				List<String> GiveRatingList=columnDataValues.get("GiveRating");
				List<String> ReasonList=columnDataValues.get("Reason");
				List<String> FRCancelReasonList=columnDataValues.get("FRCancelReason");
				
				
				ArrayList<FarmerExcelInputData> farmerDetailsList = new ArrayList<FarmerExcelInputData>();

				for (int i = 0; i < FarmerMailIDList.size(); i++) {
					FarmerExcelInputData farmappInputDetail = new FarmerExcelInputData();
					farmerDetailsList.add(farmappInputDetail);
					farmerDetailsList.get(i).setFarmerMailID(FarmerMailIDList.get(i));
					farmerDetailsList.get(i).setPassword(PasswordList.get(i));
					
					farmerDetailsList.get(i).setOrderID(OrderIDList.get(i));
					farmerDetailsList.get(i).setFRCancelReason(FRCancelReasonList.get(i));
					farmerDetailsList.get(i).setAddNotes(AddNotesList.get(i));
								
					farmerDetailsList.get(i).setRetailer(RetailerList.get(i));
					farmerDetailsList.get(i).setKM(KMList.get(i));
					farmerDetailsList.get(i).setFarmerJobs(FarmerJobsList.get(i));
					farmerDetailsList.get(i).setFarmerLabAmt(FarmerLabAmtList.get(i));
					farmerDetailsList.get(i).setFarmerPartsAmt(FarmerPartsList.get(i));
					
					
					farmerDetailsList.get(i).setRevFarmerJobs(RevFarmerJobsList.get(i));
					farmerDetailsList.get(i).setRevLabAmt(RevLabAmtList.get(i));
					farmerDetailsList.get(i).setRevPartsAmt(RevPartsAmtList.get(i));
					farmerDetailsList.get(i).setGiveRating(GiveRatingList.get(i));
					farmerDetailsList.get(i).setReason(ReasonList.get(i));
					
					
					
				}

				FarmerUtils.FarmerExcelInputDataList = farmerDetailsList;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	/*public static LinkedHashMap<String, List<String>> readingFleetManagerData(String sheetName) {

		try {

			Sheet sheet = getWorkBook().getSheet(sheetName);
			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 1);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();
			/// read values from user edit sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}
			return columnDataValues;
		} catch (Exception e) {

		}
		return null;
	}*/


	/*public static void readingNegativeData() throws IOException, BiffException {
		
			if (Utils.excelInputDataList == null) {
			Sheet sheet = getWorkBook().getSheet("NegativeWorkFlow");

			List<String> ExpectedColumns = new ArrayList<String>();
			System.out.println("::::::::::::::::::::::::::::");
			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 1);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();

			/// read values from driver sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}

			List<String> mobileNumberList = columnDataValues.get("MobileNumber");
			List<String> inValidMobileNo = columnDataValues.get("InvalidMobileNo");
			List<String> vehicleNumberList = columnDataValues.get("VehicleNumber");
			List<String> driverNameList = columnDataValues.get("DriverNumber");
			List<String> locationList = columnDataValues.get("Location");
			List<String> mechanicNameList = columnDataValues.get("MechanicName");
			List<String> mechanicNumberList = columnDataValues.get("MechanicNumber");
			List<String> fmselectJobList = columnDataValues.get("FMselectJob");
			List<String> fmselectJob1List = columnDataValues.get("FMselectJob1");
			List<String> selectGarageList = columnDataValues.get("SelectGarage");
			List<String> enterParts = columnDataValues.get("Enter Parts");
			List<String> srlabourAmountList = columnDataValues.get("SRLabourAmount");
			List<String> srpartsAmountList = columnDataValues.get("SRPartsAmount");
			List<String> selectRetailerList = columnDataValues.get("SRSelectRetailer");
			List<String> bdJobSelectionList = columnDataValues.get("BDJobSelection");
			List<String> bdRevisedJobList = columnDataValues.get("BDRevisedJob");
			List<String> bdLabourAmountList = columnDataValues.get("BDLabourAmount");
			List<String> bdPartsAmountList = columnDataValues.get("BDPartsAmount");
			List<String> bdSelectRetailerList = columnDataValues.get("BDSelectRetailer");
			List<String> multiJobCountList = columnDataValues.get("MultiJobCount");
			List<String> revisedBillValue1List = columnDataValues.get("RevisedBillValue1");
			List<String> revisedBillValue2List = columnDataValues.get("RevisedBillValue2");
			
			

			ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

			for (int i = 0; i < mobileNumberList.size(); i++) {
				ExcelInputData appInputDetail = new ExcelInputData();
				userDetailsList.add(appInputDetail);
				userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
				userDetailsList.get(i).setVehicleNumber(vehicleNumberList.get(i));
				userDetailsList.get(i).setDriverName(driverNameList.get(i));
				userDetailsList.get(i).setLocation(locationList.get(i));
				userDetailsList.get(i).setMechanicNumber(mechanicNumberList.get(i));
				userDetailsList.get(i).setMechanicName(mechanicNameList.get(i));
				userDetailsList.get(i).setFmselectJob(fmselectJobList.get(i));
				userDetailsList.get(i).setFmselectJob1(fmselectJob1List.get(i));
				userDetailsList.get(i).setSelectGarage(selectGarageList.get(i));
				userDetailsList.get(i).setEnterParts(enterParts.get(i));
				userDetailsList.get(i).setSelectRetailer(selectRetailerList.get(i));
				userDetailsList.get(i).setSrLabourAmount(srlabourAmountList.get(i));
				userDetailsList.get(i).setSrPartsAmount(srpartsAmountList.get(i));
				userDetailsList.get(i).setBdJobSelection(bdJobSelectionList.get(i));
				userDetailsList.get(i).setRevisedBillValue1(revisedBillValue1List.get(i));
				userDetailsList.get(i).setRevisedBillValue2(revisedBillValue2List.get(i));
				userDetailsList.get(i).setMultiJobCount(multiJobCountList.get(i));
				userDetailsList.get(i).setInvalidMobileNo(inValidMobileNo.get(i));

			}

			Utils.excelInputDataList = userDetailsList;

		} 
	}


public static void readingFMUserData() throws IOException, BiffException, InterruptedException {

	if (Utils.excelInputDataList == null) {
		Sheet sheet = getWorkBook().getSheet("FMUserData");

		List<String> ExpectedColumns = new ArrayList<String>();

		int masterSheetColumnIndex = sheet.getColumns();

		for (int x = 0; x < masterSheetColumnIndex; x++) {
			Cell celll = sheet.getCell(x, 0);
			String d = celll.getContents();
			ExpectedColumns.add(d);
		}

		LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

		List<String> column1 = new ArrayList<String>();

		/// read values from driver sheet for each column
		for (int j = 0; j < masterSheetColumnIndex; j++) {
			column1 = new ArrayList<String>();
			for (int i = 1; i < sheet.getRows(); i++) {
				Cell cell = sheet.getCell(j, i);
				column1.add(cell.getContents());
			}
			columnDataValues.put(ExpectedColumns.get(j), column1);
		}
		

		List<String> ProfileNameList = columnDataValues.get("Name");
		List<String> ProfileMobileNoList = columnDataValues.get("Mobile No");
		List<String> ProfileEmailList = columnDataValues.get("Email");
		List<String> ProfileGSTINList = columnDataValues.get("GSTIN");
		List<String> ProfileAddressList = columnDataValues.get("Address");
		List<String> ProfileStateList = columnDataValues.get("State");
		

		ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();
		
		for (int i = 0; i < columnDataValues.size(); i++) {
			ExcelInputData appInputDetail = new ExcelInputData();
			userDetailsList.add(appInputDetail);
			userDetailsList.get(i).setProfileName(ProfileNameList.get(i));
			userDetailsList.get(i).setProfileMobileNo(ProfileMobileNoList.get(i));
			userDetailsList.get(i).setProfileEmail(ProfileEmailList.get(i));
			userDetailsList.get(i).setProfileGSTIN(ProfileGSTINList.get(i));
			userDetailsList.get(i).setProfileAddress(ProfileAddressList.get(i));
			userDetailsList.get(i).setProfileState(ProfileStateList.get(i));
			Thread.sleep(1000);
			
		}

		Utils.excelInputDataList = userDetailsList;

	}*/

}

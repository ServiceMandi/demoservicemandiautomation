package com.servicemandi.webapps.utils;


public class FarmerExcelInputData {

	public String FarmerMailID; //done
	public String Password;//done
	public String OrderID;//done
	
	public String AddNewFM;
	public String AddState;
	public String AddVehicleRegNo;
	public String AddDriverNameNo;
	public String StarRating;
	
	public String VehicleMake;
	public String VehicleCategory;
	public String VehicleLoadRange;
	public String VehicleModelYr;
	public String VehicleKMReading;
	public String VehicleType;
	public String VehicleTyres;
	public String FCValidityDate;
	public String LicenseExpDate;
	public String Location;
	public String Pincode;

	public String FMNameNumber;
	public String VehicleRegNo;
	public String DriverNameNumber;
	public String SROuterJob;
	public String SRInnerJob;
	public String WorkshopName;
	public String CancelReason;
	public String AddNotes;
	public String Retailer;
	public String KM;
	public String FarmerJobs;
	public String FarmerLabAmt; 
	public String FarmerPartsAmt;
	
	

	public String RevFarmerJobs;
	public String RevLabAmt;
	public String RevPartsAmt;
	public String GiveRating;
	public String Reason;
	
	public String FRCancelReason;

	
	//Invalid Values:
	public String InvalidFMNameNo;
	public String InvalidVehicleRegNo;
	public String InvalidDriverNameNo;
		
	
	
	//Generate Getters and setters:
	//=========================================
	public String getFarmerMailID() {
		return FarmerMailID;
	}

	public void setFarmerMailID(String farmerMailID) {
		FarmerMailID = farmerMailID;
	}
	//=========================================
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	//=========================================
	public String getAddNewFM() {
		return AddNewFM;
	}

	public void setAddNewFM(String addNewFM) {
		AddNewFM = addNewFM;
	}
	//=========================================
	public String getAddState() {
		return AddState;
	}

	public void setAddState(String addState) {
		AddState = addState;
	}
	//=========================================
	public String getAddVehicleRegNo() {
		return AddVehicleRegNo;
	}

	public void setAddVehicleRegNo(String addVehicleRegNo) {
		AddVehicleRegNo = addVehicleRegNo;
	}
	//=========================================
	public String getAddDriverNameNo() {
		return AddDriverNameNo;
	}

	public void setAddDriverNameNo(String addDriverNameNo) {
		AddDriverNameNo = addDriverNameNo;
	}
	//=========================================
	public String getStarRating() {
		return StarRating;
	}

	public void setStarRating(String starRating) {
		StarRating = starRating;
	}
	//=========================================	
	public String getVehicleMake() {
		return VehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		VehicleMake = vehicleMake;
	}

	public String getVehicleCategory() {
		return VehicleCategory;
	}

	public void setVehicleCategory(String vehicleCategory) {
		VehicleCategory = vehicleCategory;
	}

	public String getVehicleLoadRange() {
		return VehicleLoadRange;
	}

	public void setVehicleLoadRange(String vehicleLoadRange) {
		VehicleLoadRange = vehicleLoadRange;
	}

	public String getVehicleModelYr() {
		return VehicleModelYr;
	}

	public void setVehicleModelYr(String vehicleModelYr) {
		VehicleModelYr = vehicleModelYr;
	}

	public String getVehicleKMReading() {
		return VehicleKMReading;
	}

	public void setVehicleKMReading(String vehicleKMReading) {
		VehicleKMReading = vehicleKMReading;
	}

	public String getVehicleType() {
		return VehicleType;
	}

	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}

	public String getVehicleTyres() {
		return VehicleTyres;
	}

	public void setVehicleTyres(String vehicleTyres) {
		VehicleTyres = vehicleTyres;
	}
	
	public String getLicenseExpDate() {
		return LicenseExpDate;
	}

	public void setLicenseExpDate(String licenseExpDate) {
		LicenseExpDate = licenseExpDate;
	}
	
	public String getFCValidityDate() {
		return FCValidityDate;
	}

	public void setFCValidityDate(String fCValidityDate) {
		FCValidityDate = fCValidityDate;
	}
	
	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}
	//========================================
	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String pincode) {
		Pincode = pincode;
	}
	//=========================================
	public String getFMNameNumber() {
		return FMNameNumber;
	}

	public void setFMNameNumber(String fMNameNumber) {
		FMNameNumber = fMNameNumber;
	}
	//=========================================
	public String getVehicleRegNo() {
		return VehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		VehicleRegNo = vehicleRegNo;
	}
	//=========================================
	public String getDriverNameNumber() {
		return DriverNameNumber;
	}

	public void setDriverNameNumber(String driverNameNumber) {
		DriverNameNumber = driverNameNumber;
	}
	//=========================================

	public String getSROuterJob() {
		return SROuterJob;
	}

	public void setSROuterJob(String sROuterJob) {
		SROuterJob = sROuterJob;
	}
	//=========================================
	public String getSRInnerJob() {
		return SRInnerJob;
	}

	public void setSRInnerJob(String sRInnerJob) {
		SRInnerJob = sRInnerJob;
	}
	//=========================================
	
	public String getWorkshopName() {
		return WorkshopName;
	}

	public void setWorkshopName(String workshopName) {
		WorkshopName = workshopName;
	}
	//=========================================
	
	public String getCancelReason() {
		return CancelReason;
	}

	public void setCancelReason(String cancelReason) {
		CancelReason = cancelReason;
	}
	//=========================================
	public String getOrderID() {
		return OrderID;
	}

	public void setOrderID(String orderID) {
		OrderID = orderID;
	}
	//=============================================
	public String getAddNotes() {
		return AddNotes;
	}

	public void setAddNotes(String addNotes) {
		AddNotes = addNotes;
	}
	
	
	//Invalid Values:
	//=================
		
	public String getInvalidFMNameNo() {
		return InvalidFMNameNo;
	}

	public void setInvalidFMNameNo(String invalidFMNameNo) {
		InvalidFMNameNo = invalidFMNameNo;
	}
	//=========================================
	public String getInvalidVehicleRegNo() {
		return InvalidVehicleRegNo;
	}

	public void setInvalidVehicleRegNo(String invalidVehicleRegNo) {
		InvalidVehicleRegNo = invalidVehicleRegNo;
	}
	
	public String getInvalidDriverNameNo() {
		return InvalidDriverNameNo;
	}

	public void setInvalidDriverNameNo(String invalidDriverNameNo) {
		InvalidDriverNameNo = invalidDriverNameNo;
	}
	//===================================================
	public String getRetailer() {
		return Retailer;
	}

	public void setRetailer(String retailer) {
		Retailer = retailer;
	}
	//===================================================
	public String getKM() {
		return KM;
	}

	public void setKM(String kM) {
		KM = kM;
	}
	//===================================================
	public String getFarmerJobs() {
		return FarmerJobs;
	}

	public void setFarmerJobs(String farmerJobs) {
		FarmerJobs = farmerJobs;
	}

	//===================================================
	public String getFarmerLabAmt() {
		return FarmerLabAmt;
	}

	public void setFarmerLabAmt(String farmerLabAmt) {
		FarmerLabAmt = farmerLabAmt;
	}
	//===================================================
	public String getFarmerPartsAmt() {
		return FarmerPartsAmt;
	}

	public void setFarmerPartsAmt(String farmerPartsAmt) {
		FarmerPartsAmt = farmerPartsAmt;
	}
	//===================================================
	public String getGiveRating() {
		return GiveRating;
	}

	public void setGiveRating(String giveRating) {
		GiveRating = giveRating;
	}
	//===================================================

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}
	
	//===================================================
	public String getRevFarmerJobs() {
		return RevFarmerJobs;
	}

	public void setRevFarmerJobs(String revFarmerJobs) {
		RevFarmerJobs = revFarmerJobs;
	}

	public String getRevLabAmt() {
		return RevLabAmt;
	}

	public void setRevLabAmt(String revLabAmt) {
		RevLabAmt = revLabAmt;
	}

	
	public String getRevPartsAmt() {
		return RevPartsAmt;
	}

	public void setRevPartsAmt(String revPartsAmt) {
		RevPartsAmt = revPartsAmt;
	}

	//===================================================
	public String getFRCancelReason() {
		return FRCancelReason;
	}

	public void setFRCancelReason(String fRCancelReason) {
		FRCancelReason = fRCancelReason;
	}

	
	}

	
	
	

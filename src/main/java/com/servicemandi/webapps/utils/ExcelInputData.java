package com.servicemandi.webapps.utils;

import java.util.List;

public class ExcelInputData {

	public String EmailID;
	public String Password;
	
	public String AddNewFM;
	public String AddState;
	public String AddVehicleRegNo;
	public String AddDriverNameNo;
	public String StarRating;
	
	public String VehicleMake;
	public String VehicleCategory;
	public String VehicleLoadRange;
	public String VehicleModelYr;
	public String VehicleKMReading;
	public String VehicleType;
	public String VehicleTyres;
	public String FCValidityDate;
	public String LicenseExpDate;
	public String Location;
	public String Pincode;

	public String FMNameNumber;
	public String VehicleRegNo;
	public String DriverNameNumber;
	public String SROuterJob;
	public String SRInnerJob;
	public String WorkshopName;
	public String CancelReason;
	public String OrderID;
	public String AddNotes;
	public String BDJobs;
	//Invalid Values:
	public String InvalidFMNameNo;
	public String InvalidVehicleRegNo;
	public String InvalidDriverNameNo;
	
	//Resources:
	public String ResEmailID;
	public String ResPassword;
	
	public String ResLocation;
	public String ResFMNameNo;
	public String ResVehicleRegNo;
	public String ResDriverNameNo;
	public String ResCancelReason;
	public String ResPinCodeNo;
	public String ResWorkshopName;
	public String ResAddNotes;
	public String ResOrderID;
	public String ResVehicleMake;
	public String ResVehicleLoadRange;
	
	//Generate Getters and setters:
	//=========================================
	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}
	//=========================================
	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}
	//=========================================
	public String getAddNewFM() {
		return AddNewFM;
	}

	public void setAddNewFM(String addNewFM) {
		AddNewFM = addNewFM;
	}
	//=========================================
	public String getAddState() {
		return AddState;
	}

	public void setAddState(String addState) {
		AddState = addState;
	}
	//=========================================
	public String getAddVehicleRegNo() {
		return AddVehicleRegNo;
	}

	public void setAddVehicleRegNo(String addVehicleRegNo) {
		AddVehicleRegNo = addVehicleRegNo;
	}
	//=========================================
	public String getAddDriverNameNo() {
		return AddDriverNameNo;
	}

	public void setAddDriverNameNo(String addDriverNameNo) {
		AddDriverNameNo = addDriverNameNo;
	}
	//=========================================
	public String getStarRating() {
		return StarRating;
	}

	public void setStarRating(String starRating) {
		StarRating = starRating;
	}
	//=========================================	
	public String getVehicleMake() {
		return VehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		VehicleMake = vehicleMake;
	}

	public String getVehicleCategory() {
		return VehicleCategory;
	}

	public void setVehicleCategory(String vehicleCategory) {
		VehicleCategory = vehicleCategory;
	}

	public String getVehicleLoadRange() {
		return VehicleLoadRange;
	}

	public void setVehicleLoadRange(String vehicleLoadRange) {
		VehicleLoadRange = vehicleLoadRange;
	}

	public String getVehicleModelYr() {
		return VehicleModelYr;
	}

	public void setVehicleModelYr(String vehicleModelYr) {
		VehicleModelYr = vehicleModelYr;
	}

	public String getVehicleKMReading() {
		return VehicleKMReading;
	}

	public void setVehicleKMReading(String vehicleKMReading) {
		VehicleKMReading = vehicleKMReading;
	}

	public String getVehicleType() {
		return VehicleType;
	}

	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}

	public String getVehicleTyres() {
		return VehicleTyres;
	}

	public void setVehicleTyres(String vehicleTyres) {
		VehicleTyres = vehicleTyres;
	}
	
	public String getLicenseExpDate() {
		return LicenseExpDate;
	}

	public void setLicenseExpDate(String licenseExpDate) {
		LicenseExpDate = licenseExpDate;
	}
	
	public String getFCValidityDate() {
		return FCValidityDate;
	}

	public void setFCValidityDate(String fCValidityDate) {
		FCValidityDate = fCValidityDate;
	}
	
	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}
	//========================================
	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String pincode) {
		Pincode = pincode;
	}
	//=========================================
	public String getFMNameNumber() {
		return FMNameNumber;
	}

	public void setFMNameNumber(String fMNameNumber) {
		FMNameNumber = fMNameNumber;
	}
	//=========================================
	public String getVehicleRegNo() {
		return VehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		VehicleRegNo = vehicleRegNo;
	}
	//=========================================
	public String getDriverNameNumber() {
		return DriverNameNumber;
	}

	public void setDriverNameNumber(String driverNameNumber) {
		DriverNameNumber = driverNameNumber;
	}
	//=========================================

	public String getSROuterJob() {
		return SROuterJob;
	}

	public void setSROuterJob(String sROuterJob) {
		SROuterJob = sROuterJob;
	}
	//=========================================
	public String getSRInnerJob() {
		return SRInnerJob;
	}

	public void setSRInnerJob(String sRInnerJob) {
		SRInnerJob = sRInnerJob;
	}
	//=========================================
	
	public String getWorkshopName() {
		return WorkshopName;
	}

	public void setWorkshopName(String workshopName) {
		WorkshopName = workshopName;
	}
	//=========================================
	
	public String getCancelReason() {
		return CancelReason;
	}

	public void setCancelReason(String cancelReason) {
		CancelReason = cancelReason;
	}
	//=========================================
	public String getOrderID() {
		return OrderID;
	}

	public void setOrderID(String orderID) {
		OrderID = orderID;
	}
	//=============================================
	public String getAddNotes() {
		return AddNotes;
	}

	public void setAddNotes(String addNotes) {
		AddNotes = addNotes;
	}
	
	
	//Invalid Values:
	//=================
		
	public String getInvalidFMNameNo() {
		return InvalidFMNameNo;
	}

	public void setInvalidFMNameNo(String invalidFMNameNo) {
		InvalidFMNameNo = invalidFMNameNo;
	}
	//=========================================
	public String getInvalidVehicleRegNo() {
		return InvalidVehicleRegNo;
	}

	public void setInvalidVehicleRegNo(String invalidVehicleRegNo) {
		InvalidVehicleRegNo = invalidVehicleRegNo;
	}
	
	public String getInvalidDriverNameNo() {
		return InvalidDriverNameNo;
	}

	public void setInvalidDriverNameNo(String invalidDriverNameNo) {
		InvalidDriverNameNo = invalidDriverNameNo;
	}
	//===================================================
	public String getBDJobs() {
		return BDJobs;
	}

	public void setBDJobs(String bDJobs) {
		BDJobs = bDJobs;
	}
	//===================================================
	//Resources:
	
	public String getResEmailID() {
		return ResEmailID;
	}

	public void setResEmailID(String resEmailID) {
		ResEmailID = resEmailID;
	}

	public String getResPassword() {
		return ResPassword;
	}

	public void setResPassword(String resPassword) {
		ResPassword = resPassword;
	}

	public String getResLocation() {
		return ResLocation;
	}

	public void setResLocation(String resLocation) {
		ResLocation = resLocation;
	}

	public String getResFMNameNo() {
		return ResFMNameNo;
	}

	public void setResFMNameNo(String resFMNameNo) {
		ResFMNameNo = resFMNameNo;
	}

	public String getResVehicleRegNo() {
		return ResVehicleRegNo;
	}

	public void setResVehicleRegNo(String resVehicleRegNo) {
		ResVehicleRegNo = resVehicleRegNo;
	}

	public String getResDriverNameNo() {
		return ResDriverNameNo;
	}

	public void setResDriverNameNo(String resDriverNameNo) {
		ResDriverNameNo = resDriverNameNo;
	}

	public String getResCancelReason() {
		return ResCancelReason;
	}

	public void setResCancelReason(String resCancelReason) {
		ResCancelReason = resCancelReason;
	}
	

	public String getResPinCodeNo() {
		return ResPinCodeNo;
	}

	public void setResPinCodeNo(String resPinCodeNo) {
		ResPinCodeNo = resPinCodeNo;
	}

	public String getResWorkshopName() {
		return ResWorkshopName;
	}

	public void setResWorkshopName(String resWorkshopName) {
		ResWorkshopName = resWorkshopName;
	}

	public String getResAddNotes() {
		return ResAddNotes;
	}

	public void setResAddNotes(String resAddNotes) {
		ResAddNotes = resAddNotes;
	}

	public String getResOrderID() {
		return ResOrderID;
	}

	public void setResOrderID(String resOrderID) {
		ResOrderID = resOrderID;
	}
	
	public String getResVehicleMake() {
		return ResVehicleMake;
	}
	public void setResVehicleMake(String resVehicleMake) {
		ResVehicleMake = resVehicleMake;
	}
	
	public String getResVehicleLoadRange() {
		return ResVehicleLoadRange;
	}
	public void setResVehicleLoadRange(String resVehicleLoadRange) {
		ResVehicleLoadRange = resVehicleLoadRange;
	}
	
	//===================================================
	
	}

	
	
	

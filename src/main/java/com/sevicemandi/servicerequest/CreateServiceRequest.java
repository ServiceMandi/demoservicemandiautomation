package com.sevicemandi.servicerequest;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;

import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.StringTokenizer;


public class CreateServiceRequest extends Configuration {

	public CreateServiceRequest() {

	}

	public void createServiceRequest(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("In CreateServiceRequest method");
				Thread.sleep(4000);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(4000);
				fleetManagerWait(LocatorPath.createNewRequest);
				clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
				Thread.sleep(4000);
				
				fleetManagerWait(LocatorPath.selectVehicle);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
				System.out.println("FM:Entered Create New Request method ::: ");
				System.out.println("excelInputData.getVehicleNumber():::"+excelInputData.getVehicleNumber());
				enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
				System.out.println("FM:: Vehicle selected  ::: ");
				fleetManagerWait(LocatorPath.selectDriver);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
				enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverNumber());
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
				System.out.println("FM:: Driver selected  ::: ");
				fleetManagerWait(LocatorPath.selectLocation);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
				enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
				System.out.println("FM::Location selected  ::: ");

				fleetManagerWait(LocatorPath.serviceRepairsButton);
				clickFMElement(LocatorType.XPATH, LocatorPath.serviceRepairsButton);
				fleetManagerWait(LocatorPath.serviceYesBtn);
				clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
				Thread.sleep(5000);
				writeReport(LogStatus.PASS, "FM::New Order created-Passed");
			
	}

	public void createServiceRequestNegScenario(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

		
				Thread.sleep(4000);
				fleetManagerWait(LocatorPath.liveOrderTab);
				clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
				Thread.sleep(2000);
				fleetManagerWait(LocatorPath.createNewRequest);
				clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
				Thread.sleep(5000);
				
				LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData.readingFleetManagerData("NegativeWorkFlow");
				List<String> VehicleNumberList = userEditDetails.get("VehicleNumber");
				List<String> DriverNumberList = userEditDetails.get("DriverNumber");
				List<String> LocationList = userEditDetails.get("Location");
				
				String VehicleNumber=userEditDetails.get("VehicleNumber").get(1);
				String DriverNumber = userEditDetails.get("DriverNumber").get(1);
				String Location = userEditDetails.get("Location").get(1);
				
				
				System.out.println("FM:Entered Create New Request method ::: ");
				fleetManagerWait(LocatorPath.selectVehicle);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
				enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, VehicleNumber);
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
				System.out.println("FM:: Vehicle selected  ::: ");
				fleetManagerWait(LocatorPath.selectDriver);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
				enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, DriverNumber);
				Thread.sleep(1000);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
				System.out.println("FM:: Driver selected  ::: ");
				fleetManagerWait(LocatorPath.selectLocation);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
				enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, Location);
				Thread.sleep(1000);
				hideKeyBoard(fleetManagerDriver);
				clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
				System.out.println("FM::Location selected  ::: ");

				fleetManagerWait(LocatorPath.serviceRepairsButton);
				clickFMElement(LocatorType.XPATH, LocatorPath.serviceRepairsButton);
				fleetManagerWait(LocatorPath.serviceYesBtn);
				clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
				Thread.sleep(5000);
				writeReport(LogStatus.PASS, "FM::New Order created-Passed");
				isVerified = true;
			
	}
	
	public void createServiceRequestFromMyVehicle(ExcelInputData excelInputData) throws InterruptedException {

		
			Thread.sleep(5000);
			Thread.sleep(5000);
			System.out.println("FM::Entered Create New Request method ::: ");

			fleetManagerWait(LocatorPath.newRequestFromMyVehicles);
			clickFMElement(LocatorType.XPATH, LocatorPath.newRequestFromMyVehicles);
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.selectDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			System.out.println("FM::Entered Create New Request Driver selected  ::: ");
			fleetManagerWait(LocatorPath.selectLocation);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			System.out.println("FM::Entered Create New Request Location selected  ::: ");

			fleetManagerWait(LocatorPath.serviceRepairsButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceRepairsButton);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "FM::New Order created-Passed");
		
	}

	public void selectUnKnowJobs(boolean isVerified) throws InterruptedException {

		
			System.out.println("selectUnKnowJobs try");
			
				Thread.sleep(3000);
				fleetManagerWait(LocatorPath.selectJob);
				fleetManagerWait(LocatorPath.iDontKnowPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.iDontKnowPath);
				fleetManagerWait(LocatorPath.searchGaragesPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
				writeReport(LogStatus.PASS, "FM::Service request-> I DONT KNOW WHATS WRONG-> Job Selected");
			
			
	}

	public void selectJobs1(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
			throws InterruptedException, IOException {

		Thread.sleep(4000);
		
			System.out.println("isVerified in selectJobs1");
			
				Thread.sleep(4000);
				
				String selectJob = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-content/div[2]/ion-list/*";
				fleetManagerWait(selectJob);
				List<WebElement> selectJobOuterList = fleetManagerDriver.findElements(By.xpath(selectJob));
				Thread.sleep(1000);
				System.out.println("Job List Size :" + selectJobOuterList.size());
				for (int i = 0; i <= selectJobOuterList.size(); i++) {
					Thread.sleep(1000);
					String selectJobText = selectJobOuterList.get(i).getText();
					String expected = excelInputData.getFmselectJob();
					
					System.out.println("expected"+expected);
					String[] outerArray = expected.split(",");
									
					for(String OuterArrayValues1 : outerArray){
						System.out.println(OuterArrayValues1);
						int result=Integer.parseInt(OuterArrayValues1);
						selectJobOuterList.get(result).click();
						//InnerLoopClick:        
						String selectInnerJob1 = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-content/div[2]/ion-list/div["+ (result + 1) + "]/div/ion-list/*";
						List<WebElement> selectJobInnerList = fleetManagerDriver.findElements(By.xpath(selectInnerJob1));
						System.out.println("Inner Job List Size :::" + selectJobInnerList.size());
							for (int j = 0; j < selectJobInnerList.size(); j++) {
								Thread.sleep(1000);
								String selectInnerJobText = selectJobInnerList.get(j).getText();
								String expectedInner = excelInputData.getFmselectJob1();
								if (j<=Integer.parseInt(expectedInner)) {
									selectJobInnerList.get(j).click();
								}else {break;}
							} 
					}
					break;
					}
					Thread.sleep(1000);
					//String GetJobCount="//ion-badge[@class='badge badge-md']";
					String GetJobCount="body > ion-app > ng-component > ion-nav > page-select-service > ion-header > ion-navbar > div.toolbar-content.toolbar-content-md > ion-icon.icon_cart.icon.icon-md.ion-md-ion-cart-outline > ion-badge";
					String SelectJobsCount=fleetManagerDriver.findElement(By.cssSelector(GetJobCount)).getText();
					System.out.println("Total Jobs Selected :"+SelectJobsCount);
					Thread.sleep(1000);
					fleetManagerWait(LocatorPath.searchGaragesPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
					Thread.sleep(8000);
					writeReport(LogStatus.PASS, "FM: Selected Jobs Count::"+SelectJobsCount);
					writeReport(LogStatus.PASS, "FM: Select Jobs method::: Passed");
					
				
				}

	public void NegselectJobs(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
				throws InterruptedException, IOException {

			Thread.sleep(4000);
			
				System.out.println("in selectJobs1");
			
					Thread.sleep(4000);
					
					String selectJob = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-content/div[2]/ion-list/*";
					List<WebElement> selectJobOuterList = fleetManagerDriver.findElements(By.xpath(selectJob));
					Thread.sleep(1000);
					System.out.println("Job List Size :" + selectJobOuterList.size());
					
					LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData.readingFleetManagerData("NegativeWorkFlow");
					List<String> FMselectJobList = userEditDetails.get("FMselectJob");
					List<String> FMselectJob1List = userEditDetails.get("FMselectJob1");
										
					String FMselectJob=userEditDetails.get("FMselectJob").get(1);
					String FMselectJob1 = userEditDetails.get("FMselectJob1").get(1);
					
					
					for (int i = 0; i <= selectJobOuterList.size(); i++) {
						Thread.sleep(1000);
						String selectJobText = selectJobOuterList.get(i).getText();
						String expected = FMselectJob;
						
						System.out.println("expected"+expected);
						String[] outerArray = expected.split(",");
										
						for(String OuterArrayValues1 : outerArray){
							System.out.println(OuterArrayValues1);
							int result=Integer.parseInt(OuterArrayValues1);
							selectJobOuterList.get(result).click();
							//InnerLoopClick:        
							String selectInnerJob1 = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-content/div[2]/ion-list/div["+ (result + 1) + "]/div/ion-list/*";
							List<WebElement> selectJobInnerList = fleetManagerDriver.findElements(By.xpath(selectInnerJob1));
							System.out.println("Inner Job List Size :::" + selectJobInnerList.size());
								for (int j = 0; j < selectJobInnerList.size(); j++) {
									Thread.sleep(1000);
									String selectInnerJobText = selectJobInnerList.get(j).getText();
									String expectedInner = FMselectJob1;
									if (j<=Integer.parseInt(expectedInner)) {
										selectJobInnerList.get(j).click();
									}else {break;}
								} 
						}
						break;
						}
						Thread.sleep(1000);
						//String GetJobCount="//ion-badge[@class='badge badge-md']";
						String GetJobCount="body > ion-app > ng-component > ion-nav > page-select-service > ion-header > ion-navbar > div.toolbar-content.toolbar-content-md > ion-icon.icon_cart.icon.icon-md.ion-md-ion-cart-outline > ion-badge";
						String SelectJobsCount=fleetManagerDriver.findElement(By.cssSelector(GetJobCount)).getText();
						System.out.println("Total Jobs Selected :"+SelectJobsCount);
						Thread.sleep(1000);
						fleetManagerWait(LocatorPath.searchGaragesPath);
						clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
						Thread.sleep(8000);
						writeReport(LogStatus.PASS, "FM: Selected Jobs Count::"+SelectJobsCount);
						writeReport(LogStatus.PASS, "FM: Select Jobs method::: Passed");
						
					}

	

	public String findGarage(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("in findGarage");
			
				Thread.sleep(7000);
				String orderId = "";
				Thread.sleep(3000);
				fleetManagerWait(LocatorPath.garageListPath1);
				List<WebElement> garageTotalList = fleetManagerDriver.findElements(By.xpath(LocatorPath.garageListPath1));
				System.out.println("Total Garage list size :::" + garageTotalList.size());
				boolean ifFindGarage = false;
				for (int i = 1; i <= garageTotalList.size(); i++) {

					if (!ifFindGarage) {
						// *[@id=\"garagebox\"]/div[" + i + "]/*/*/*
						String garagePathList = "//*[@class='garagebox']/div[" + i + "]/*/*/*";
						List<WebElement> garageList = fleetManagerDriver.findElements(By.xpath(garagePathList));
						for (int g = 0; g < garageList.size(); g++) {
							String garageName = garageList.get(g).getText();
							System.out.println("Garage name ::: " + garageName);
							if (garageName.equalsIgnoreCase(excelInputData.getSelectGarage())) {

								System.out.println("Excel garage::::" + excelInputData.getSelectGarage());
								writeReport(LogStatus.PASS,
										excelInputData.getSelectGarage() + " :::Selected Workshop:::::");
								garageTotalList.get((i - 1)).click();
								Thread.sleep(1000);
								ifFindGarage = true;
								break;
							}
						}
					} else
						break;
				}
				
				fleetManagerWait(LocatorPath.confirmGaragePath);
				while(fleetManagerDriver.findElement(By.xpath(LocatorPath.confirmGaragePath)).isDisplayed()){
					clickFMElement(LocatorType.XPATH, LocatorPath.confirmGaragePath);
					Thread.sleep(6000);
					break;
				}
				
				
				

				fleetManagerWait(LocatorPath.requestOrderIdPath);
				orderId = getFMWebElement(LocatorType.XPATH, LocatorPath.requestOrderIdPath).getText()
						.replaceAll("[^0-9]", "");
				writeReport(LogStatus.PASS, "FM::SR-Order Id::" + orderId);
				System.out.println("Request number:::" + orderId);
				fleetManagerWait(LocatorPath.manageOrder);
				clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
				System.out.println("FM::Manage Orders clicked");
				Thread.sleep(1000);
				writeReport(LogStatus.PASS, "FM:: Confirm Workshop Passed");
				isVerified = true;
				return orderId;
			
	}

	public void findNoGarage(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {

		try {

			System.out.println(" User garage name :::" + excelInputData.getSelectGarage());
			String totalGarageListPath = "//*[@class='garagebox']/*";
			// "//*[@id=\"garagebox\"]/*";
			fleetManagerWait(totalGarageListPath);
			List<WebElement> webElements = fleetManagerDriver.findElements(By.xpath(totalGarageListPath));
			boolean ifFindGarage = false;
			for (int i = 1; i <= webElements.size(); i++) {

				if (!ifFindGarage) {
					String garagePathList = "//*[@id=\"garagebox\"]/div[" + i + "]/*/*/*";
					List<WebElement> garageList = fleetManagerDriver.findElements(By.xpath(garagePathList));

					
					LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData.readingFleetManagerData("NegativeWorkFlow");
					List<String> SelectGarageList = userEditDetails.get("SelectGarage");
															
					String SelectGarage=userEditDetails.get("SelectGarage").get(1);
				
					
					
					for (int g = 0; g < garageList.size(); g++) {
						String garageName = garageList.get(g).getText();
						if (garageName.equalsIgnoreCase(SelectGarage)) {
							webElements.get((i - 1)).click();
							writeReport(LogStatus.PASS, "FM: SR -:: " + garageName + " -> selected");
							Thread.sleep(1000);
							ifFindGarage = true;
							break;
						}
					}
				} else
					break;
				; // else break;
			}

			fleetManagerWait(LocatorPath.confirmGaragePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.confirmGaragePath);

			// Garage does not exists validation:

			String PlsSelectGarageMsg = "/html/body/ion-app/ion-alert/div/div[1]";
			fleetManagerWait(PlsSelectGarageMsg);
			String PlsSelectGarageMsg1 = fleetManagerDriver.findElement(By.xpath(PlsSelectGarageMsg)).getText();
			WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(PlsSelectGarageMsg));
			Thread.sleep(2000);
			if (AlertMsg1.isDisplayed()) {// Msg can be taken to excel
				System.out.println("FM::Inside Workshop Alert" + PlsSelectGarageMsg1);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				writeReport(LogStatus.FAIL, "FM: Workshop Not Selected - Verified");
			}

		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "FM: Issue in Workshop Name");
		}
	}

}

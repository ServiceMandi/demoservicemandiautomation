package com.sevicemandi.servicerequest;

import com.gargoylesoftware.htmlunit.html.Keyboard;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;


public class ServiceRequestStartJob extends Configuration {

	public ServiceRequestStartJob() {

	}

	public void startJobs(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified) throws InterruptedException, IOException {

		
		System.out.println("StartJobs");
		
		Thread.sleep(5000);
		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(4000);
		System.out.println("Entered start jobs method :::");
		mechanicWait(LocatorPath.jobsCompletePath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
		writeReport(LogStatus.PASS, "Technician: Jobs completed");
		Thread.sleep(5000);
		
		
		/*String KMReading="//input[@class='txtReg_Number ng-untouched ng-pristine ng-valid']";
		mechanicWait(KMReading);
		clickMechanicElement(LocatorType.XPATH, KMReading);
		enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
		writeReport(LogStatus.PASS, "Technician::KM Reading Value entered::Passed");
		
		String SubmitBilBtn="//*[@id=\"nav\"]/page-final-bill/ion-footer/button[2]/span";
		mechanicWait(SubmitBilBtn);
		clickMechanicElement(LocatorType.XPATH,SubmitBilBtn);
		Thread.sleep(4000);*/
		
		
		mechanicWait(LocatorPath.jobSubmitBillPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
		Thread.sleep(1000);
		
		//Mandatory alert for KM Reading:
		String Mandateforkm=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
		if(Mandateforkm.equalsIgnoreCase("Please enter km Reading"))
		{   
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			Thread.sleep(1000);
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+Mandateforkm);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(2000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
		}else {
			Thread.sleep(1000);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}
		writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
		
		}
		
	
	
	public void startJobsWithAddImage(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified) throws InterruptedException {

		if(isVerified) {
			System.out.println(excelInputData.getAddImageCount());
		System.out.println("isVerified startJobsWithAddImage try"+isVerified);
		try {
		Thread.sleep(3000);
		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(2000);
		System.out.println("Entered start jobs method :::");
		mechanicWait(LocatorPath.jobsCompletePath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
		writeReport(LogStatus.PASS, "Technician:: Jobs completed");
		Thread.sleep(4000);
		
		mechanicWait(LocatorPath.ViewAddImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.ViewAddImage);
		Thread.sleep(2000);
		mechanicWait(LocatorPath.AddImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.AddImage);
		Thread.sleep(1000);
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		Thread.sleep(1000);
		List<MobileElement> li = mechanicDriver.findElements(By.className("android.widget.TextView"));
		li.get(1).click(); //Open Gallery Click
				
		
		List<MobileElement> imagesList = mechanicDriver.findElements(By.xpath("//android.widget.GridView[@index='0']/*"));
		System.out.println(imagesList.size());  
		
		System.out.println(excelInputData.getAddImageCount());
		Thread.sleep(1000);
		for(int i=0;i<imagesList.size();i++) { 
			imagesList.get(i).click();
			Thread.sleep(1000);
			if(i==5)
			{break;}
		}
		
		MECH_TakeClick();
		
		Thread.sleep(1000);
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		Thread.sleep(1000);
		mechanicDriver.findElement(By.xpath("//android.widget.FrameLayout[@index=0]//android.widget.Button[@text='OK']")).click();
		Thread.sleep(1000);
		
		mechanicDriver.findElement(By.xpath("//android.widget.TextView[@text='OK']")).click();
		Thread.sleep(2000);
		
		List<MobileElement> li2 = mechanicDriver.findElements(By.className("android.widget.TextView"));
		System.out.println("li2 size:::"+li2.size());
		
		if(li2.size()==5)
		{
			writeReport(LogStatus.PASS, "Uploaded Images Count=>" +li2.size());
		}
		switchToMechanicWebView();
		
		
		Thread.sleep(1000);
		mechanicWait(LocatorPath.UploadImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.UploadImage);
		Thread.sleep(5000);
		
		mechanicWait(LocatorPath.UploadImage);
		Thread.sleep(4000);
		String UploadImageSuccesMsg=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
		//"//*[@id=\"alert-subhdr-1\"]"); 
		System.out.println("UploadImageSuccesMsg::::::::"+UploadImageSuccesMsg);
		writeReport(LogStatus.PASS, "UploadedImageSuccesMsg::::::::" +UploadImageSuccesMsg);
		Thread.sleep(1000);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
		Thread.sleep(3000);
				
		mechanicWait(LocatorPath.jobSubmitBillPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
		Thread.sleep(1000);
		
		String MandateforKM=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
		if(MandateforKM.equalsIgnoreCase("Please enter km Reading"))
		{   
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+MandateforKM);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
		}else {
			Thread.sleep(1000);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			mechanicDriver.findElement(By.xpath(KMReading)).sendKeys(excelInputData.getKMReading());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}
		writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
		isVerified=true;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Technician::Submit Bill::Failed:::");
			writeReport(LogStatus.FAIL, "Technician::Submit Bill::Failed::");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL,  "Technician::Submit Bill::Failed::");
			isVerified=false;
			}
	}
	
	
	public void startJobsWithRecursiveImages(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified) throws InterruptedException {

		if(isVerified) {
			System.out.println(excelInputData.getAddImageCount());
		System.out.println("isVerified startJobsWithAddImage try"+isVerified);
		try {
		Thread.sleep(3000);
		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(2000);
		System.out.println("Entered start jobs method :::");
		mechanicWait(LocatorPath.jobsCompletePath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
		writeReport(LogStatus.PASS, "Technician:: Jobs completed");
		Thread.sleep(4000);
		
		mechanicWait(LocatorPath.ViewAddImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.ViewAddImage);
		Thread.sleep(2000);
		mechanicWait(LocatorPath.AddImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.AddImage);
		Thread.sleep(1000);
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		Thread.sleep(1000);
		List<MobileElement> li = mechanicDriver.findElements(By.className("android.widget.TextView"));
		li.get(1).click(); //Open Gallery Click
				
		
		List<MobileElement> imagesList = mechanicDriver.findElements(By.xpath("//android.widget.GridView[@index='0']/*"));
		System.out.println(imagesList.size());  
		
		Thread.sleep(1000);
		for(int i=0;i<imagesList.size();i++) { 
			imagesList.get(i).click();
			Thread.sleep(1000);
			if(i==2)
			{break;}
		}
		
	
		Thread.sleep(1000);
		MECH_TakeClick();
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		mechanicDriver.findElement(By.xpath("//android.widget.TextView[@text='OK']")).click();
		Thread.sleep(2000);
		
		switchToMechanicWebView();
		
		Thread.sleep(1000);
		mechanicWait(LocatorPath.AddImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.AddImage);
		Thread.sleep(1000);
		
		Thread.sleep(1000);
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		Thread.sleep(1000);
		List<MobileElement> list = mechanicDriver.findElements(By.className("android.widget.TextView"));
		list.get(1).click(); //Open Gallery Click
		
		List<MobileElement> imagesList2 = mechanicDriver.findElements(By.xpath("//android.widget.GridView[@index='0']/*"));
		System.out.println(imagesList2.size());  
		
		Thread.sleep(1000);
		for(int i=0;i<imagesList2.size();i++) { 
			imagesList2.get(i).click();
			Thread.sleep(1000);
			if(i==5)
			{break;}
		}
		
		Thread.sleep(2000);
		MECH_TakeClick();
		Thread.sleep(1000);
		((AppiumDriver<WebElement>)mechanicDriver).context("NATIVE_APP");
		mechanicDriver.findElement(By.xpath("//android.widget.FrameLayout[@index=0]//android.widget.Button[@text='OK']")).click();
		Thread.sleep(1000);
		mechanicDriver.findElement(By.xpath("//android.widget.TextView[@text='OK']")).click();
		Thread.sleep(3000);
		switchToMechanicWebView();
		
		mechanicWait(LocatorPath.UploadImage);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.UploadImage);
		Thread.sleep(5000);
		
		String UploadImageSuccesMsg=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
		//"//*[@id=\"alert-subhdr-1\"]"); 
		System.out.println("UploadImageSuccesMsg::::::::"+UploadImageSuccesMsg);
		writeReport(LogStatus.PASS, "UploadedImageSuccesMsg::::::::" +UploadImageSuccesMsg);
		Thread.sleep(1000);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
		Thread.sleep(3000);
				
		mechanicWait(LocatorPath.jobSubmitBillPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
		Thread.sleep(1000);
		
		String MandateforKM=mechanicDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
		if(MandateforKM.equalsIgnoreCase("Please enter km Reading"))
		{   
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
			writeReport(LogStatus.PASS, "Technician::KM Reading Value not entered,Alert message displays::::"+MandateforKM);
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
		}else {
			Thread.sleep(1000);
			String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
			mechanicWait(KMReading);
			clickMechanicElement(LocatorType.XPATH, KMReading);
			enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
			writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.jobSubmitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitBillPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}
		writeReport(LogStatus.PASS, "Technician::Submit Bill::Passed");
		isVerified=true;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Technician::Submit Bill::Failed:::");
			writeReport(LogStatus.FAIL, "Technician::Submit Bill::Failed::");
			isVerified=false;
		}}
		else {
			writeReport(LogStatus.FAIL,  "Technician::Submit Bill::Failed::");
			isVerified=false;
			}
	}
	

	public void revisedEstimationWithParts(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified)
		throws InterruptedException, IOException {
		
				//New method:
				//==============
				
					System.out.println("in revisedEstimationWithParts " + isVerified);
					Thread.sleep(2000);
					mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
					Thread.sleep(3000);
					System.out.println("Entered revisedEstimationWithParts-Start jobs method :::");
					mechanicWait(LocatorPath.addJobsLinkPath);
					Thread.sleep(2000);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
					Thread.sleep(4000);
					
						System.out.println("Entered the trackLocationWithParts method :::");
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
						Thread.sleep(1000);

						String expected = excelInputData.getSrRevisedJob();
						System.out.println("expected" + expected);

						String[] outerArray = expected.split(",");
						int outerArrayLength = outerArray.length;
						System.out.println("outerArrayLength" + outerArrayLength);
						int checkcount = 0;
						for (String OuterArrayValues1 : outerArray) {
							mechanicWait(LocatorPath.totalJobListPath);
							List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
							System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
							System.out.println("checkcount:::" + checkcount);
							System.out.println("OuterArrayValues1" + OuterArrayValues1);
							int result = Integer.parseInt(OuterArrayValues1);
							selectJobOuterList.get(result).click();
							Thread.sleep(1000);

							mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
							Thread.sleep(1000);
							mechanicWait(LocatorPath.trackLocationLabourPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
							enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
							Thread.sleep(1000);
							System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
							writeReport(LogStatus.PASS,"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());

							mechanicWait(LocatorPath.PartsPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
							enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
							hideKeyBoard(mechanicDriver);
							writeReport(LogStatus.PASS,"Technician::BD-Parts amount entered:- "+excelInputData.getBdPartsAmount());

							checkcount = checkcount + 1;
							System.out.println("checkcount" + checkcount);
							if (checkcount != outerArrayLength) {
								Thread.sleep(1000);
								String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
								clickMechanicElement(LocatorType.XPATH, addJobButton);
								mechanicWait(LocatorPath.selectJobPath);
								clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
								Thread.sleep(1000);

							} else {
								mechanicWait(LocatorPath.submitRevisedEstimatePath);
								clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
								Thread.sleep(500);
								System.out.println("Technician: Submit Revised Estimate With Parts Bill");
								writeReport(LogStatus.PASS, "Technician::Revised Estimate With Parts:::Passed");
								break;
							}
							
						}	
		
		
			/*System.out.println("in revisedEstimationWithParts");

			Thread.sleep(4000);
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			System.out.println("Technician::Entered Start Jobs method :::");
			Thread.sleep(2000);
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
			System.out.println("jobList.size()"+jobList.size());
			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getSrRevisedJob())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Technician::Select job:-" + excelInputData.getSrRevisedJob());
					Thread.sleep(1000);
					break;
				}
				String expectedInner = excelInputData.getBdRevisedJobSelection();
				if (i==Integer.parseInt(expectedInner)) {
					jobList.get(i).click();
				}
				}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			// String labourPath =
			// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";
			mechanicWait(LocatorPath.trackLocationLabourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getSrLabourAmount());
			Thread.sleep(1000);
 			// String partsPath =
			// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(LocatorPath.PartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getSrPartsAmount());

			hideKeyBoard(mechanicDriver);
			Thread.sleep(2000);
			
			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Clicked Submit Revised Estimate Path:::");
			writeReport(LogStatus.PASS,
					" Revised Estimate With Parts entered:::" + excelInputData.getSrPartsAmount());*/
			
	}

	public void revisedEstimationWithPartsYesNo(ExcelInputData excelInputData, Mechanic mechanic,
			ServicesRequestTrackLocation trackLocation,boolean isVerified) throws InterruptedException {

		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(2000);
		System.out.println("Entered start jobs method :::");

		mechanicWait(LocatorPath.addJobsLinkPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
		Thread.sleep(2000);

		//trackLocation.completeOrYes(isVerified);

		mechanicWait(LocatorPath.selectJobPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
		Thread.sleep(1000);
		mechanicWait(LocatorPath.totalJobListPath);
		List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

		for (int i = 0; i < jobList.size(); i++) {
			/*String selectJob = jobList.get(i).getText();
			System.out.println("Inside for loop::::" + selectJob);
			if (selectJob.equalsIgnoreCase(excelInputData.getSrRevisedJob())) {
				jobList.get(i).click();
				writeReport(LogStatus.PASS, "Technician:: Job selected:-" + excelInputData.getSrRevisedJob());
				Thread.sleep(1000);
				break;
			}*/
			
			String expectedInner = excelInputData.getBdRevisedJobSelection();
			if (i==Integer.parseInt(expectedInner)) {
				jobList.get(i).click();
			}
			}
		

		Thread.sleep(1000);
		mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
		Thread.sleep(1000);

		// EnterLabour:
		// String labourPath =
		// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";
		mechanicWait(LocatorPath.trackLocationLabourPath);
		Thread.sleep(1000);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
		enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getSrLabourAmount());
		Thread.sleep(1000);

		// String partsPath =
		// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
		mechanicWait(LocatorPath.PartsPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
		enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getSrPartsAmount());

		hideKeyBoard(mechanicDriver);
		Thread.sleep(2000);

		mechanicWait(LocatorPath.submitRevisedEstimatePath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
		System.out.println("Clicked Submit Revised Estimate Path:::");
		writeReport(LogStatus.PASS, " Revised Estimate with parts entered" + excelInputData.getSrPartsAmount());

	}

	public void revisedEstimationWithoutParts(ExcelInputData excelInputData, Mechanic mechanic,boolean isVerified)
			throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("Technician::revisedEstimationWithoutParts try"+isVerified);
			try {
			Thread.sleep(5000);	
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(1000);
			System.out.println("Technician::Entered start jobs method :::");
			Thread.sleep(3000);
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				System.out.println("jobList.size()"+jobList.size()+i);
				/*String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdRevisedJobSelection())) {
					jobList.get(i).click();
					//writeReport(LogStatus.PASS, "Technician:: select --> Brake Work jobs ");
					Thread.sleep(1000);
					break;
				}*/
				String expectedInner = excelInputData.getBdRevisedJobSelection();
				if (i==Integer.parseInt(expectedInner)) {
					jobList.get(i).click();
				}
				}
			

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			// String labourPath =
			// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";
			mechanicWait(LocatorPath.trackLocationLabourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getSrLabourAmount());
			Thread.sleep(1000);

			// String partsPath =
			// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(LocatorPath.PartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, "00");

			hideKeyBoard(mechanicDriver);
			Thread.sleep(2000);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Clicked Submit Revised Estimate Path:::");
			writeReport(LogStatus.PASS, "Technician:: Revised Estimate without parts entered ");
			isVerified=true;
		} catch (Exception e) {
			e.printStackTrace();
			MECH_TakeClick();
			writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts method failed:::");
			isVerified=false;
		}}else {
			writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts method failed");
			isVerified=false;
			}

	}

	public void revisedEstimationWithoutPartsYesNo(ExcelInputData excelInputData, Mechanic mechanic,
			ServicesRequestTrackLocation trackLocation,boolean isVerified) throws InterruptedException, IOException {
		if(isVerified) {
			System.out.println("Technician::revisedEstimationWithoutPartsYesNo try"+isVerified);
		try {
		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(3000);
		System.out.println("Technician::Entered start jobs method :::");

		mechanicWait(LocatorPath.addJobsLinkPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
		Thread.sleep(2000);

		trackLocation.completeOrYes(isVerified);

		mechanicWait(LocatorPath.selectJobPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
		Thread.sleep(1000);
		mechanicWait(LocatorPath.totalJobListPath);
		List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

		for (int i = 0; i < jobList.size(); i++) {
			/*String selectJob = jobList.get(i).getText();
			System.out.println("Inside for loop::::" + selectJob);
			if (selectJob.equalsIgnoreCase(excelInputData.getBdRevisedJobSelection())) {
				jobList.get(i).click();
				writeReport(LogStatus.PASS, "Technician:: Revised Estimate Amount entered -->"+excelInputData.getBdRevisedJobSelection());
				Thread.sleep(1000);
				break;
			}*/
			
			String expectedInner = excelInputData.getBdRevisedJobSelection();
			if (i==Integer.parseInt(expectedInner)) {
				jobList.get(i).click();
			}
			}
		

		Thread.sleep(1000);
		mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
		Thread.sleep(1000);

		// EnterLabour:
		// String labourPath =
		// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";
		mechanicWait(LocatorPath.trackLocationLabourPath);
		Thread.sleep(1000);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
		enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getSrLabourAmount());
		Thread.sleep(1000);

		// String partsPath =
		// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
		mechanicWait(LocatorPath.PartsPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
		enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, "00");

		hideKeyBoard(mechanicDriver);
		Thread.sleep(2000);

		mechanicWait(LocatorPath.submitRevisedEstimatePath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
		System.out.println("Technician::Clicked Submit Revised Estimate Path:::");
		writeReport(LogStatus.PASS, "Technician:: Revised Estimate without parts entered ");
		/*
		 * Utils.sleepTimeLow(); mechanicWait(LocatorPath.revisedPaymentRequestOk);
		 * clickMechanicElement(LocatorType.XPATH, LocatorPath.revisedPaymentRequestOk);
		 * Thread.sleep(3000);
		 */
		isVerified=true;
	} catch (Exception e) {
	e.printStackTrace();
	MECH_TakeClick();
	writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts method failed:::");
	isVerified=false;
	}
	}else {
	writeReport(LogStatus.FAIL, "Technician::Revised Estimate without parts method failed");
	isVerified=false;
	}
	}
}

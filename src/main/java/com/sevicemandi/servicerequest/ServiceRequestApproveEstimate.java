package com.sevicemandi.servicerequest;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.Driver;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;


public class ServiceRequestApproveEstimate extends Configuration {

	public ServiceRequestApproveEstimate() {

	}

	public void approveEstimate(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in approveEstimate try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM:: Approved Estimate::Passed");
			Thread.sleep(2000);
			Thread.sleep(2000);
			
	}

	public void approveEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified)
			throws InterruptedException {
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(5000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Entered Approve Estimate method");
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "FM:::ApproveEstimateWithPayCash method:Passed");
			fleetManagerWait(LocatorPath.liveOrderTab);
	}

	public void driverApproveEstimateWithoutPayCash(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in driverApproveEstimateWithoutPayCash try"+isVerified);
			
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM:::Approved the estimate");
			Thread.sleep(1000);
			// driver.pushNotification();
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "FM:::DriverApproveEstimateWithoutPayCash Passed");
				
	}

	public void driverApproveEstimate(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("isVerified in driverApproveEstimatewithPayCash try"+isVerified);
			
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM:::DriverApproveEstimate method::Passed");
					
	}

	public void driverAppEstwithPayCash(ExcelInputData excelInputData, FleetManager fleetManager, Driver driver,boolean isVerified)
			throws InterruptedException, IOException {
		
		
			System.out.println("isVerified in driverAppEstwithPayCash try"+isVerified);

		
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);

			Thread.sleep(2000);
			writeReport(LogStatus.PASS,  "FM::::Pay Cash method:: Passed");
			fleetManagerWait(LocatorPath.liveOrderTab);
			
	}

	public void driverRevisedApproveEstimatewithPayCash(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified ) throws InterruptedException {
		
			System.out.println("isVerified in driverRevisedApproveEstimatewithPayCash try"+isVerified);
			
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			writeReport(LogStatus.PASS, "FM::DriverRevisedApproveEstimate method::Passed");
			Thread.sleep(1000);
			
		
	}

	public void driverRevisedApproveEstwithPayCash(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in driverRevisedApproveEstwithPayCash try"+isVerified);
			
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);

			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM:: Select PayCash method::Passed");
			fleetManagerWait(LocatorPath.liveOrderTab);
			
		
	}

	public void ApproveRevisedEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("ApproveRevisedEstimateWithPayCash try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate With PayCash method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			writeReport(LogStatus.PASS, "FM::ApproveRevisedEstimateWithPayCash method::Passed");
		}

	public void ApproveRevisedEstimateWithoutPay(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified)
			throws InterruptedException {
		
		System.out.println("ApproveRevisedEstimateWithoutPay"+isVerified);

		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "FM::ApproveRevisedEstimateWithoutPay method::Passed");
			//isVerified=true;
		
	}

	public void driverRevisedApproveEstimatewithoutPayCash(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException {
		
		System.out.println("isVerified in driverRevisedApproveEstimatewithoutPayCash try"+isVerified);
		
		Thread.sleep(2000);			
		fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
		System.out.println("Entered Approve Revised Estimate Without Pay method ::");
		Thread.sleep(3000);	
		fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
		clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
		fleetManagerWait(LocatorPath.liveOrderTab);
		
	}

	public void approveEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("isVerified in ApproveEstimateWithAlert try"+isVerified);

		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "FM::Approved estimate::Passed");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			String NoRetailerText=fleetManagerDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
			System.out.println("NoRetailerText:::"+NoRetailerText);
			FM_TakeClick();
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM::ApproveEstimateWithNoRetailerAlert method Passed");
		
	}

	public void ApproveRevisedEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager, boolean isVerified) throws InterruptedException, IOException {
	
			System.out.println("isVerified in ApproveRevisedEstimateWithAlert try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("FM::Entered Approve Revised Estimate With Alert method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			String NoRetailerText=fleetManagerDriver.findElement(By.xpath("//div[@class='alert-head']")).getText();
			System.out.println("NoRetailerText:::"+NoRetailerText);
			FM_TakeClick();
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "FM::ApproveRevisedEstimateWithNoRetailerAlert method:: Passed");
		}

	public void approveEstimateCancelOrder(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in approveEstimateCancelOrder try"+isVerified);
	
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimateCancelJob);
			//check delete button and click
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimateCancelJob);
			fleetManagerWait(LocatorPath.approveEstimateCancelButton);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimateCancelButton);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			
	}

	public void approveEstimateCancelAllJobs(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in approveEstimateCancelAllJobs try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Entered the cancel job method :::");
						
			List<WebElement> loopSize = fleetManagerDriver.findElements(By.xpath("/html/body/ion-app/ng-component/ion-nav/page-estimate/ion-content/div[2]/ion-grid/div/ion-list/*"));
			loopSize.size();
			System.out.println("loopSize::"+loopSize.size());
			for (int i = 1; i <= loopSize.size(); i++) {

				String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[" + i + "]/ion-col[4]/ion-checkbox/div";
				System.out.println("Cancel jobs path ::" + cancelCheckBox);
				WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
				Actions action = new Actions(fleetManagerDriver);
				action.moveToElement(cancelElement).click().perform();
				Thread.sleep(500);
				if(i==loopSize.size())
				{
					break;
				}
			}

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "FM:: Cancelled all jobs:::Passed");
		
	}

	/*public void approveEstimateCancelOneJobs(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in approveEstimateCancelOneJobs try"+isVerified);
	
			Thread.sleep(2000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Entered cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			System.out.println("AL cancel jobs path ::" + cancelCheckBox);
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "ApproveEstimateCancelOneJobs Passed");
			fleetManagerWait(LocatorPath.liveOrderTab);
			
		}*/
	public void approveEstimateCancelOneJob(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
		System.out.println("isVerified in approveEstimateCancelAllJobs try"+isVerified);
	
		Thread.sleep(3000);
		fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
		System.out.println("Entered Approve Estimate method ::");
		Thread.sleep(2000);
		fleetManagerWait(LocatorPath.approveEstimatePath);
		System.out.println("Entered the cancel one job method :::");
					
		List<WebElement> loopSize = fleetManagerDriver.findElements(By.xpath("/html/body/ion-app/ng-component/ion-nav/page-estimate/ion-content/div[2]/ion-grid/div/ion-list/*"));
		loopSize.size();
		System.out.println("loopSize::"+loopSize.size());
		for (int i = 1; i <= loopSize.size(); i++) {

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[" + i + "]/ion-col[4]/ion-checkbox/div";
			System.out.println("Cancel jobs path ::" + cancelCheckBox);
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);
			if(i==1)
			{
				break;
			}
		}

		clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
		writeReport(LogStatus.PASS, "FM:: Cancelled one job:::Passed");
	
}


	public void approveEstimateCancelOneJobWithAlert(ExcelInputData excelInputData,FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		  
			System.out.println("isVerified in ApproveEstimateCancelOneJobWithAlert try"+isVerified);
			
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			
			fleetManagerWait(LocatorPath.approveEstimatePath);
			
			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);
			System.out.println("Unchecked cancel job:::");
			
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			System.out.println("Clicked noRetailerAlertPath:::");

			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "ApproveEstimateCancelOneJobsWithPayCashWithAlert Passed");
						
	}

	public void driverApproveEstimateCancelOneJobsWithPayCashWithAlert(ExcelInputData excelInputData,
			FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in driverApproveEstimateCancelOneJobsWithPayCashWithAlert try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Entered the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			// fleetManagerWait(LocatorPath.payCashButtonPath);
			// clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);

			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);

			 fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "Technician:::Cancelled One Job:::Passed");
					
	}

	public void approveEstimateCancelOneJobsWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in approveEstimateCancelOneJobsWithPayCash try"+isVerified);
		
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "ApproveEstimateCancelOneJobsWithPayCash");
					
	}

	public void driverApproveEstimateWithoutPayCash1(ExcelInputData excelInputData, FleetManager fleetManager,
			Driver driver,boolean isVerified) throws InterruptedException {
		
			System.out.println("isVerified in driverApproveEstimateWithoutPayCash1 try"+isVerified);
	
			Thread.sleep(3000);
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber(),isVerified);
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approved the estimate");
			Thread.sleep(1000);
			// driver.pushNotification();
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "Fleet manager:DriverApproveEstimateWithoutPayCash Passed");
		
	}

}

package com.sevicemandi.servicerequest;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import org.apache.commons.collections.set.SynchronizedSortedSet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidKeyCode;



public class ServicesRequestTrackLocation extends Configuration {

	public ServicesRequestTrackLocation() {

	}

	public void giveEstimate(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println(" in giveEstimate");
			
				Thread.sleep(5000);
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				mechanicWait(LocatorPath.giveEstimatePath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
				Thread.sleep(1000);
				System.out.println("Technician: Clicked Give Estimate method :::");
				writeReport(LogStatus.PASS, "Technician: Clicked Give Estimate method");
				
			
	}

	public void completeOrYes(boolean isVerified) throws InterruptedException, IOException {

		
			System.out.println(" in completeOrYes " );

		
				Thread.sleep(9000);
				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				mechanicWait(YesBtn);
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				System.out.println("Technician:: Clicked Yes Button :::");
				writeReport(LogStatus.PASS, "Technician::Entered Mechanic:: YES as per Dealer cut off configuiration");
				
	}

	public void trackLocationForUnknownIssueWithOutParts(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in trackLocationForUnknownIssueWithOutParts try" + isVerified);
			try {

				Thread.sleep(3000);
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);

				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
				for (int i = 0; i < jobList.size(); i++) {
					
					System.out.println("Technician: Inside for loop::::" + jobList.get(i));
					if (excelInputData.getFmselectJob().equals(jobList.get(i))) {
						jobList.get(i).click();
						writeReport(LogStatus.PASS, "Technician select --> " + excelInputData.getFmselectJob());
						Thread.sleep(1000);
						break;
					}
				}

				Thread.sleep(1000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);

				mechanicWait(LocatorPath.trackLocationLabourPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getSrLabourAmount());
				Thread.sleep(1000);
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS, "Technician::Enter the labour amount "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				System.out.println("Technician::Enter the Break down parts :::");
				writeReport(LogStatus.PASS, "Technician::TrackLocationForUnknownIssueWithOutParts Passed");
				isVerified = true;

			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::TrackLocationForUnknownIssueWithOutParts Failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::TrackLocationForUnknownIssueWithOutParts Failed");
			isVerified = false;
		}
	}

	public void trackLocationForUnknownIssueWithParts(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in trackLocationForUnknownIssueWithParts try" + isVerified);
			try {
				Utils.sleepTimeLow();
				System.out.println("Technician::Entered the trackLocationForUnknownIssueWithParts method :: ");
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.totalJobListPath);

				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop::::" + selectJob);
					if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
						jobList.get(i).click();
						writeReport(LogStatus.PASS, "Technician select --> " + excelInputData.getFmselectJob());
						Thread.sleep(1000);
						break;
					}
				}

				Thread.sleep(1000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);

				mechanicWait(LocatorPath.trackLocationLabourPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getSrLabourAmount());
				Thread.sleep(1000);
				writeReport(LogStatus.PASS, "Technician:: Labour Amount entered "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

				// String partsPath =
				// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
				mechanicWait(LocatorPath.PartsPath);
				Thread.sleep(1000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
				Thread.sleep(1000);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getSrPartsAmount());
				hideKeyBoard(mechanicDriver);

				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);
				/*
				 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				 */
				System.out.println("Technician:: Parts Amount entered:::");
				writeReport(LogStatus.PASS,
						"Technician::Parts Amount:::: " + excelInputData.getSrPartsAmount() + " entered ");
				writeReport(LogStatus.PASS, "Technician::Submit Bill method Passed");
				isVerified = true;

			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.FAIL, "Technician::TrackLocationForUnknownIssueWithParts failed");
				isVerified = false;
			}
		} else {
			writeReport(LogStatus.FAIL, "Technician::TrackLocationForUnknownIssueWithParts failed");
			isVerified = false;
		}
	}

	public void trackLocationWithCompleteYesNo(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {
		if (isVerified) {
			System.out.println("isVerified in trackLocationWithCompleteYesNo try" + isVerified);

			try {
				System.out.println("Entered the trackLocationWithParts method :::");
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(3000);

				System.out.println("Entered completeOrYes method :::");

				Thread.sleep(4000);
				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				mechanicWait(YesBtn);
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				Thread.sleep(2000);

				System.out.println("excelInputData.getSrPartsAmount()" + excelInputData.getSrPartsAmount());
				String PartsPathOnly = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div/ion-row[2]/ion-col[3]/input";
				mechanicWait(PartsPathOnly);
				clickMechanicElement(LocatorType.XPATH, PartsPathOnly);
				enterValueMechanic(LocatorType.XPATH, PartsPathOnly, excelInputData.getSrPartsAmount());
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS,
						"Mechanic entered the Parts amount :- " + excelInputData.getSrPartsAmount());

				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);
				/*
				 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				 */
				System.out.println("Mechanic: Submitted Bill With Parts");
				writeReport(LogStatus.PASS, "Submitted Bill With Parts");
				isVerified = true;
			} catch (Exception e) {
				e.printStackTrace();
				writeReport(LogStatus.ERROR, "Submitted Bill With Parts Failed");
				isVerified = false;
			}
		}

		else {
			writeReport(LogStatus.FAIL, "Submitted Bill With Parts Failed");
			isVerified = false;
		}
	}

	public void trackLocationWithParts(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("in trackLocationWithParts " );
			
				System.out.println("Entered the Track location with parts method :::");
				Thread.sleep(4000);
				mechanicWait(LocatorPath.PartsPathOnly);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPathOnly);
				Thread.sleep(1000);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPathOnly, excelInputData.getSrPartsAmount());
				Thread.sleep(1000);
				hideKeyBoard(mechanicDriver);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);

				System.out.println("Technician::Entered Parts :::");
				writeReport(LogStatus.PASS,
						"Technician::Parts Amount " + excelInputData.getSrPartsAmount() + "entered ");
				
	}

	public void trackLocationWithOutParts(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException {
	
			System.out.println("isVerified in trackLocationWithOutParts " );
			
				Utils.sleepTimeLow();
				mechanicWait(LocatorPath.PartsPathOnly);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPathOnly);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPathOnly, "00");
				Thread.sleep(1000);
				hideKeyBoard(mechanicDriver);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);

				System.out.println("Entered TrackLocation Without Parts :::");
				writeReport(LogStatus.PASS, "Technician::Entered TrackLocation Without Parts ");
			
	}

	
	public void trackLocationMultiJobWithPartsWithoutSubmit(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException {
		
			System.out.println("isVerified in trackLocationMultiJobWithParts" );
			
				System.out.println("Entered the track location with parts method :::");
				Thread.sleep(5000);
				// mechanicWait(partsPath);
				// System.out.println("excelInputData.getFmSelectJobs1()::::"+excelInputData.getFmSelectJobs1());

				String JobCardPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/*";
				System.out.println("JobCardPath::::" + JobCardPath);
				List<WebElement> JobCardPathSize = mechanicDriver.findElements(By.xpath(JobCardPath));
				System.out.println("JobCardPathSize:::" + JobCardPathSize.size());
				// int JobCardValue=Integer.parseInt(excelInputData.getFmSelectJobs1()+1);

				for (int i = 1; i <= JobCardPathSize.size(); i++) {

					// String partsPath = "//div[2]/ion-card[1]/div[" + i +
					// "]/ion-row[2]/ion-col[3]/input";
					String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div[" + i
							+ "]/ion-row[2]/ion-col[3]/input";
					WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
					partsElement.clear();
					partsElement.clear();
					partsElement.sendKeys(excelInputData.getSrPartsAmount());
					Thread.sleep(500);

				}
				hideKeyBoard(mechanicDriver);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.partsSubmitPath);
				Thread.sleep(500);
				/*
				 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				 */
				writeReport(LogStatus.PASS, "Technician:::::TrackLocation MultiJob WithParts Without Submit::Passed");
				
	}

	public void withOutRetailer(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {

		
			System.out.println("isVerified in withOutRetailer");

			
				Thread.sleep(3000);
				Utils.sleepTimeLow();
				System.out.println("Enter the service request retailer method :::");
				mechanicClassPathWait(LocatorPath.noRetailerPath);
				clickMechanicElement(LocatorType.CLASS, LocatorPath.noRetailerPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.retailerConfirmPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
				writeReport(LogStatus.PASS, "Technician:: WithOutRetailer selection:::Passed ");
		}

	public void selectRetailer(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
		
			System.out.println("isVerified in selectRetailer ");
			
				Thread.sleep(10000);
				System.out.println("Entered the service request retailer method :::");
				mechanicWait(LocatorPath.retailersListPath);
				List<WebElement> retailersList = mechanicDriver.findElements(By.xpath(LocatorPath.retailersListPath));
				System.out.println("retailersList.size()" + retailersList.size());
				int loopSize = retailersList.size();

				boolean isClicked = false;
				for (int i = 1; i <= loopSize; i++) {
											
					String insideRetailerPath = "//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/div[3]/ion-card[" + i + "]" + "/*";
					
					List<WebElement> retailersInsideList = mechanicDriver.findElements(By.xpath(insideRetailerPath));
					System.out.println("retailersInsideList.size();" + retailersInsideList.size());
					// if (!isClicked) {
					for (int j = 0; j < retailersInsideList.size(); j++) {
						System.out.println("Retailer name :::" + retailersInsideList.get(j).getText());
						if (retailersInsideList.get(j).getText().contains(excelInputData.getSrSelectRetailer())){
							Thread.sleep(2000);
							
							 String inneraddress="//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/div[3]/ion-card/div["+(j+1)+"]/ion-card-content/ion-row[2]/ion-col";
							 mechanicDriver.findElement(By.xpath(inneraddress)).click();
							
							Thread.sleep(2000);
							mechanicWait(LocatorPath.retailerConfirmPath);
							clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
							Thread.sleep(4000);
							System.out.println(
									"excelInputData.getSrSelectRetailer()" + excelInputData.getSrSelectRetailer());
							writeReport(LogStatus.PASS, "Technician: SR-Retailer selected :- "	+ excelInputData.getSrSelectRetailer());
							isVerified = true;
							
							break;
						}}
					}
				
	}

	public void UpdateJobStatusRevisedBill(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified) throws InterruptedException {

		
			System.out.println("in UpdateJobStatusRevisedBill ");
		
				Utils.sleepTimeLow();
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(4000);
				System.out.println("Entered UpdateJobStatus method :::");
				mechanicWait(LocatorPath.completeJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
				Thread.sleep(5000);
				

				/*Thread.sleep(1000);
				WebElement revisedBillElement = mechanicDriver
						.findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath));
				revisedBillElement.clear();
				revisedBillElement.clear();
				revisedBillElement.sendKeys(excelInputData.getRevisedBillValue2());
				writeReport(LogStatus.PASS, "Mechanic - Revised bill -Passed");*/

				
				Thread.sleep(2000);
				
				WebElement revisedBillElement = mechanicDriver.findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath));
			     //This will scroll the page till the element is found	
				 
			    revisedBillElement.clear();
				revisedBillElement.clear();
				revisedBillElement.sendKeys(excelInputData.getRevisedBillValue1());
				writeReport(LogStatus.PASS, "Mechanic - Revised bill -Passed");
				Thread.sleep(4000);
			
				//KM reading:
				String KMReading="//input[@class='txtReg_Number ng-untouched ng-pristine ng-valid']";
				mechanicWait(KMReading);
				clickMechanicElement(LocatorType.XPATH, KMReading);
				enterValueMechanic(LocatorType.XPATH, KMReading, excelInputData.getKMReading());
				Thread.sleep(1000);
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(2000);
				
				mechanicWait(LocatorPath.submitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);

				/*
				 * Thread.sleep(500); mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				 * Thread.sleep(3000); mechanicWait(LocatorPath.submitBillPath);
				 * clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
				 */
				Thread.sleep(2000);
				mechanicWait(LocatorPath.paymentRequestOk);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
				writeReport(LogStatus.PASS, "Technician::::UpdateJobStatusRevisedBill method- Passed::");
				
	}

	public void UpdateJobStatus(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("UpdateJobStatus try");
			
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Utils.sleepTimeLow();
				System.out.println("Entered Update Job Status method :::");
				Thread.sleep(2000);
				mechanicWait(LocatorPath.completeJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
				Utils.sleepTimeLow();
				System.out.println("Enter the mechanic submit bill method :::");
				//KM reading:
				Thread.sleep(2000);
				String KMReading="//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input";
				mechanicWait(KMReading);
				mechanicDriver.findElement(By.xpath("//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input")).click();
				System.out.println("excelInputData.getKMReading()"+excelInputData.getKMReading());
				mechanicDriver.findElement(By.xpath("//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-card/ion-item[4]/div[1]/div/ion-label/span[2]/input")).sendKeys(excelInputData.getKMReading());
				Thread.sleep(1000);
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS, "Technician::KM Reading Value:::::"+excelInputData.getKMReading()+ "entered::Passed");
				Thread.sleep(2000);
				mechanicWait(LocatorPath.submitBillPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
							
				Thread.sleep(2000);
				mechanicWait(LocatorPath.paymentRequestOk);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
				writeReport(LogStatus.PASS, "Technician::UpdateJobStatus method- Passed::");
			
	}

	public void deleteJobwithSubmitEstimate(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {
		
			System.out.println("in deleteJobwithSubmitEstimate");
		
				System.out.println("Entered the Delete jobs method :::");
				String deleteicon = "//div[1]/ion-row[2]/ion-col[3]/ion-icon";
				mechanicWait(deleteicon);
				System.out.println("deleteicon:");
				clickMechanicElement(LocatorType.XPATH, deleteicon);
				String YES = "//html/body/ion-app/ion-alert/div/div[3]/button[1]";
				mechanicWait(YES);
				clickMechanicElement(LocatorType.XPATH, YES);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				/*
				 * Thread.sleep(500); mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				 * Thread.sleep(1000); clickMechanicElement(LocatorType.XPATH,
				 * LocatorPath.partsSubmitPath);
				 */
				writeReport(LogStatus.PASS, "Technician: One Job is Deleted");
				
	}

	public void DeletejobwithoutSubmitEstimate(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {

		
			System.out.println("in DeletejobwithoutSubmitEstimate");
			
				Thread.sleep(3000);
				System.out.println("Entered the Delete jobs method :::");
				/*
				 * System.out.println("Entered completeOrYes method :::"); Thread.sleep(1000);
				 * String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				 * mechanicWait(YesBtn); clickMechanicElement(LocatorType.XPATH, YesBtn);
				 * Thread.sleep(2000);
				 */

				String deleteicon = "//div[1]/ion-row[2]/ion-col[3]/ion-icon";
				mechanicWait(deleteicon);
				System.out.println("deleteicon:");
				clickMechanicElement(LocatorType.XPATH, deleteicon);
				Thread.sleep(1000);
				String YES = "//html/body/ion-app/ion-alert/div/div[3]/button[1]";
				mechanicWait(YES);
				clickMechanicElement(LocatorType.XPATH, YES);
				writeReport(LogStatus.PASS, "Technician: One Job is Deleted");
			}

	public void trackLocationWithAddOneNewJob(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException {
		
			System.out.println(" in trackLocationWithAddOneNewJob ");
		
				Thread.sleep(3000);
				// String addJobPath =
				// "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
				String addJobPath = "//*[contains(text(), 'ADD JOBS')]";
				mechanicWait(addJobPath);
				System.out.println("Clicked the add job link  ");
				clickMechanicElement(LocatorType.XPATH, addJobPath);
				Thread.sleep(3000);
				// Select Job:
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);
				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop::::" + selectJob);
					if (selectJob.equalsIgnoreCase(excelInputData.getFmselectJob())) {
						jobList.get(i).click();
						writeReport(LogStatus.PASS, "Mechanic select job:-" + excelInputData.getFmselectJob());
						Thread.sleep(1000);
						break;
					}
				}

				Thread.sleep(1000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(1000);

				// EnterLabour:
				// String labourPath =
				// "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

				mechanicWait(LocatorPath.trackLocationLabourPath);
				Thread.sleep(1000);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getSrLabourAmount());
				Thread.sleep(1000);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				/*
				 * Thread.sleep(500); mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				 * Thread.sleep(1000); clickMechanicElement(LocatorType.XPATH,
				 * LocatorPath.partsSubmitPath);
				 */
				System.out.println("Technician::::Added One job :::");
				writeReport(LogStatus.PASS, "Technician:::Added One job:::Passed ");
				
	}

	public void trackLocationWithMultiJobs(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException {
		
			System.out.println("in trackLocationWithMultiJobs");

	
				Thread.sleep(3000);
				/*mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);*/

				for (int j = 0; j < 4; j++) {

					mechanicWait(LocatorPath.totalJobListPath);
					List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

					for (int i = 0; i < jobList.size(); i++) {
						String selectJob = jobList.get(i).getText();
						System.out.println("Inside for loop::::" + selectJob);

						if (i == j) {
							jobList.get(i).click();
							Thread.sleep(500);
							writeReport(LogStatus.PASS, "Technician::Select job :- " + jobList.get(i).getText());
							break;
						}
					}

					Thread.sleep(500);
					mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(500);

					mechanicWait(LocatorPath.trackLocationLabourPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath, excelInputData.getSrLabourAmount());
					Thread.sleep(500);
					writeReport(LogStatus.PASS,"Technician:: entered the labour amount " + excelInputData.getSrLabourAmount());

					mechanicWait(LocatorPath.PartsPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getSrPartsAmount());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS,
							"Technician:: entered the parts value " + excelInputData.getSrPartsAmount());

					if (j < 3) {
						Thread.sleep(1000);
						String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
						clickMechanicElement(LocatorType.XPATH, addJobButton);
						Thread.sleep(2000);
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
					}
				}

				Thread.sleep(1000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				/*
				 * Thread.sleep(500); mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				 * Thread.sleep(1000); clickMechanicElement(LocatorType.XPATH,
				 * LocatorPath.partsSubmitPath);
				 */
				Thread.sleep(3000);
				
	}

	public void giveEstimateReturnBack(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException {
		
			System.out.println("in giveEstimateReturnBack");

			
				System.out.println("Entered the Give Estimate ReturnBack method :::");
				mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
				Thread.sleep(2000);
				mechanicWait(LocatorPath.giveEstimatePath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
				Thread.sleep(5000);
				String YesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[2]/span";
				mechanicWait(YesBtn);
				clickMechanicElement(LocatorType.XPATH, YesBtn);
				Thread.sleep(2000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(5000);
				mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(5000);
				
	}

	public void trackLocationMultiJobWithParts(ExcelInputData excelInputData, boolean isVerified)
			throws InterruptedException {

	
			System.out.println(" in trackLocationMultiJobWithParts");
		
				System.out.println("Entered the track location with parts method :::");
				Thread.sleep(5000);
													
				String JobCardPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/*";
				System.out.println("JobCardPath::::" + JobCardPath);
				Thread.sleep(2000);
				List<WebElement> JobCardPathSize = mechanicDriver.findElements(By.xpath(JobCardPath));
				System.out.println("JobCardPathSize:::" + JobCardPathSize.size());
				

				for (int i = 1; i <= JobCardPathSize.size(); i++) {

					String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
					WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
					partsElement.clear();
					partsElement.clear();
					partsElement.sendKeys(excelInputData.getSrPartsAmount());
					Thread.sleep(500);

				}
				hideKeyBoard(mechanicDriver);
				Thread.sleep(2000);
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(500);

				System.out.println("Technician::Clicked Submit estimate :::");
				writeReport(LogStatus.PASS,"Technician::Parts Amount::" + excelInputData.getSrPartsAmount() + " entered ");
		
	}
	
	public void trackLocationMultiJobWithoutParts(ExcelInputData excelInputData, boolean isVerified) throws InterruptedException, IOException {
			System.out.println("in trackLocationMultiJobWithoutParts");
			
				System.out.println("Entered the track location without parts method :::");
				Thread.sleep(5000);
													
				/*String JobCardPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/*";
				System.out.println("JobCardPath::::" + JobCardPath);
				List<WebElement> JobCardPathSize = mechanicDriver.findElements(By.xpath(JobCardPath));
				System.out.println("JobCardPathSize:::" + JobCardPathSize.size());
				

				for (int i = 1; i <= JobCardPathSize.size(); i++) {
					
					String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
					WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
					partsElement.clear();
					partsElement.clear();
					partsElement.sendKeys("00");
					Thread.sleep(500);

				}
				hideKeyBoard(mechanicDriver);*/
				
				mechanicWait(LocatorPath.partsSubmitPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
				Thread.sleep(1000);

				System.out.println("Technician::Clicked Submit estimate :::");
				writeReport(LogStatus.PASS,	"Technician::Parts Amount:::" +00 + " entered ");
		}

	public void SRTrackLocationWithParts(ExcelInputData excelInputData, Mechanic mechanic, boolean isVerified)
			throws InterruptedException, IOException {
		
			System.out.println("in SRTrackLocationWithParts");
			
				mechanicWait(LocatorPath.selectJobPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				Thread.sleep(1000);

				String expected = excelInputData.getBdJobSelection();
				System.out.println("expected" + expected);

				String[] outerArray = expected.split(",");
				int outerArrayLength = outerArray.length;
				System.out.println("outerArrayLength" + outerArrayLength);
				int checkcount = 0;
				for (String OuterArrayValues1 : outerArray) {
					mechanicWait(LocatorPath.totalJobListPath);
					List<WebElement> selectJobOuterList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
					System.out.println("selectJobOuterList.size()" + selectJobOuterList.size());
					System.out.println("checkcount:::" + checkcount);
					System.out.println("OuterArrayValues1" + OuterArrayValues1);
					int result = Integer.parseInt(OuterArrayValues1);
					selectJobOuterList.get(result).click();
					Thread.sleep(1000);

					mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
					Thread.sleep(1000);
					mechanicWait(LocatorPath.trackLocationLabourPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
					enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,excelInputData.getBdLabourAmount());
					Thread.sleep(1000);
					System.out.println("getBdLabourAmount" + excelInputData.getBdLabourAmount());
					writeReport(LogStatus.PASS,
							"Technician::BD-Labour amount entered:- " + excelInputData.getBdLabourAmount());

					mechanicWait(LocatorPath.PartsPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.PartsPath);
					System.out.println("getBdPartsAmount" + excelInputData.getBdPartsAmount());
					enterValueMechanic(LocatorType.XPATH, LocatorPath.PartsPath, excelInputData.getBdPartsAmount());
					hideKeyBoard(mechanicDriver);
					writeReport(LogStatus.PASS,
							"Technician::BD-Parts amount entered:- " + excelInputData.getBdPartsAmount());

					checkcount = checkcount + 1;
					System.out.println("checkcount" + checkcount);
					if (checkcount != outerArrayLength) {
						Thread.sleep(1000);
						String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
						clickMechanicElement(LocatorType.XPATH, addJobButton);
						mechanicWait(LocatorPath.selectJobPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
						Thread.sleep(1000);

					} else {
						mechanicWait(LocatorPath.partsSubmitPath);
						clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						Thread.sleep(500);
						/*
						 * mechanicDriver.pressKeyCode(AndroidKeyCode.BACK); Thread.sleep(1000);
						 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
						 */
						System.out.println("Technician: Submitted With Parts Bill");
						writeReport(LogStatus.PASS, "Technician: TrackLocation With Parts method:::Passed");
						isVerified = true;
						break;
					}
				}
			}







}
